# The Helpers module defines convenience methods for writing specs
module Helpers

  def setup_specs(version, endpoint)
    endpoint = "/#{version}/#{endpoint}"
    organization = self.setup_organization()
    another_organization = self.setup_organization()

    # setup users
    User.destroy_all
    root                = self.setup_user_root(organization)
    admin               = setup_user_admin(organization)
    user                = setup_user(organization)
    facility_owner      = setup_user_facility_owner(organization)
    care_facility_admin = setup_user_care_facility_admin(organization)

    # Login users to get tokens
    root_token = self.login(root.email, root.password)
    admin_token = self.login(admin.email, admin.password)
    care_facility_admin_token = self.login(care_facility_admin.email, care_facility_admin.password)
    user_token = self.login(user.email, user.password)
    facility_owner_token = self.login(facility_owner.email, facility_owner.password)

    # another user for testing out of scope requests
    another_admin  = setup_user_admin(another_organization)
    another_admin_token = self.login(another_admin.email, another_admin.password)

    return {
      endpoint: endpoint,
      organization: organization,
      root: root,
      admin: admin,
      care_facility_admin: care_facility_admin,
      user: user,
      facility_owner: facility_owner,
      root_token: root_token,
      admin_token: admin_token,
      care_facility_admin_token: care_facility_admin_token,
      user_token: user_token,
      facility_owner_token: facility_owner_token,
      another_organization: another_organization,
      another_admin: another_admin,
      another_admin_token: another_admin_token,
    }
  end

  def setup_specs_public(version, endpoint)
    endpoint = "/#{version}/public/#{endpoint}"
    organization = self.setup_organization()
    another_organization = self.setup_organization()

    return {
      endpoint: endpoint,
      organization: organization,
      another_organization: another_organization,
    }
  end

  # Setup Helpers
  def setup_organization()
    FactoryBot.create(:organization)
  end

  def setup_user_root(organization)
    FactoryBot.create(:user_root, organization: organization)
  end

  def setup_user(organization)
    FactoryBot.create(:user, organization: organization)
  end

  def setup_user_facility_owner(organization)
    FactoryBot.create(:user_facility_owner, organization: organization)
  end

  def setup_user_care_facility_admin(organization)
    FactoryBot.create(:user_care_facility_admin, organization: organization)
  end

  def setup_user_admin(organization)
    FactoryBot.create(:user_admin, organization: organization)
  end

  # -------- Request helpers ---------
  def request_headers(api_token = nil)
    headers = {
      'Content-Type' => 'application/json',
      'Request-Source' => 'web',
      'Request-Platform' => 'something'
    }
    unless api_token.nil?
      headers.merge!(authorization_header(api_token))
    end
    headers
  end

  def authorization_header_public_restricted(username, password)
    encoded_credentials = Base64.encode64("#{username}:#{password}")

    {
      'Authorization' => "Basic #{encoded_credentials}"
    }
  end

  def authorization_header(api_token)
    {
      'Authorization' => "Bearer #{api_token}"
    }
  end

  def parsed_response
    JSON.parse(response.body)
  end

  def login(email, password)
    post_json('/v1/auth', {'email': email, 'password': password})
    parsed_response['jwt_token']
  end

  def post_json(endpoint, params_hash, api_token = nil, headers={})
    post endpoint, params: params_hash.to_json, headers: request_headers(api_token).merge(headers)
  end

  def put_json(endpoint, params_hash, api_token = nil)
    put endpoint, params: params_hash.to_json, headers: request_headers(api_token)
  end

  def patch_json(endpoint, params_hash, api_token = nil)
    patch endpoint, params: params_hash.to_json, headers: request_headers(api_token)
  end

  def get_json(endpoint, params = nil, api_token = nil)
    get endpoint, params: params, headers: request_headers(api_token)
  end

  def delete_json(endpoint, params_hash, api_token = nil)
    delete endpoint, params: params_hash.to_json, headers: request_headers(api_token)
  end

  # public restricted
  def get_json_public(endpoint, params = nil, organization)
    username = organization.public_username
    password = organization.public_password
    get endpoint, params: params, headers: authorization_header_public_restricted(username, password)
  end

  def post_json_public(endpoint, params = nil, organization)
    username = organization.public_username
    password = organization.public_password
    post endpoint, params: params, headers: authorization_header_public_restricted(username, password)
  end
end
