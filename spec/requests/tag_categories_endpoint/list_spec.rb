require "rails_helper"

RSpec.describe "TagCategory Endpoints - v1/tag_categories", type: :request do
  describe '#list' do
    before(:all) do
      TagCategory.destroy_all
      @setup = setup_specs("v1", "tag_categories")
      @list = FactoryBot.create_list(:tag_category, 10, organization: @setup[:organization])
      @endpoint = @setup[:endpoint]
    end

    context 'fetching all categories' do
      context 'for role root' do
        before(:all) do
          @list2 = FactoryBot.create_list(:tag_category, 6, organization: @setup[:organization], parent_id: @list.last.id)
          @list = @list + @list2
          @endpoint = "#{@setup[:endpoint]}?show_all=true"
          @token = @setup[:root_token]
          @model = @setup[:organization].tag_categories
        end
        include_examples "list", "allowed", 16
        it_behaves_like 'list'
      end
    end

    context 'fetching root categories' do
      context 'when ressources are found' do
        context 'for role root' do
          before(:all) do
            @token = @setup[:root_token]
            @model = @setup[:organization].tag_categories
          end
          include_examples "list", "allowed", 10
          it_behaves_like 'list'
        end

        context 'for role admin' do
          before(:all) do
            @token = @setup[:admin_token]
            @model = @setup[:organization].tag_categories
          end
          include_examples "list", "allowed", 10
          it_behaves_like 'list'
        end

        context 'for role user' do
          before(:all) do
            @token = @setup[:user_token]
            @model = @setup[:organization].tag_categories
          end
          include_examples "list", "allowed", 10
          it_behaves_like 'list'
        end
      end
    end

    context 'fetching sub categories' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}?parent_id=#{@list.first.id}"

        @list = FactoryBot.create_list(:tag_category, 5, organization: @setup[:organization], parent_id: @list.first.id)
        
        @token = @setup[:admin_token]
        @model = @setup[:organization].tag_categories
      end
      context 'for role admin' do
        include_examples "list", "allowed", 5
        it_behaves_like 'list'
      end
    end
  end
end