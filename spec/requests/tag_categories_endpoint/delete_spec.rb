require "rails_helper"

RSpec.describe "TagCategory Endpoints - v1/tag_categories", type: :request do
  describe '#delete' do
    before(:all) do
      TagCategory.destroy_all
      @setup = setup_specs("v1", "tag_categories")
    end
    context 'when resource is found' do
      context 'for role root' do
        before(:all) do
          @resource = FactoryBot.create(:tag_category, organization: @setup[:organization])
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:root_token]
        end
        include_examples "delete", TagCategory, "allowed"
        it_behaves_like 'delete'
      end

      context 'for role admin' do
        before(:all) do
          @resource = FactoryBot.create(:tag_category, organization: @setup[:organization])
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:admin_token]
        end
        include_examples "delete", TagCategory, "allowed"
        it_behaves_like 'delete'
      end

      context 'for role user' do
        before(:all) do
          @resource = FactoryBot.create(:tag_category, organization: @setup[:organization])
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:user_token]
        end
        include_examples "delete", TagCategory, "forbidden"
        it_behaves_like 'delete'
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "delete", TagCategory, "not_found"
        it_behaves_like 'delete'
      end
    end
  end
end