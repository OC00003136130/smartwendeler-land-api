require "rails_helper"

RSpec.describe "TagCategory Endpoints - v1/tag_categories", type: :request do
  describe '#create' do
    before(:all) do
      TagCategory.destroy_all
      @setup = setup_specs("v1", "tag_categories")
      @resource = FactoryBot.build(:tag_category)
    end
    describe 'without parent id' do
      before(:all) do
        @endpoint = @setup[:endpoint]

        @post_json = {
          name: @resource.name
        }
      end
      context 'when data is valid' do
        context 'for role root' do
          before(:all) do
            @token = @setup[:root_token]
          end
          include_examples "create", "allowed"
          it_behaves_like 'create'
        end

        context 'for role admin' do
          before(:all) do
            @token = @setup[:admin_token]
          end
          include_examples "create", "allowed"
          it_behaves_like 'create'

          it 'should have put the menu_order after the next entry' do
            expect(@setup[:organization].tag_categories.all.order(:menu_order).last.menu_order).to eq 1
          end
        end

        context 'for role user' do
          before(:all) do
            @token = @setup[:user_token]
          end
          include_examples "create", "forbidden"
          it_behaves_like 'create'
        end
      end

      context 'when data is not valid' do
        context 'name missing' do
          before(:all) do
            @post_json = {
            }
            @token = @setup[:admin_token]
            @expected_error_code = 'tagcategory.name.invalid'
          end
          include_examples "create", "not_valid"
          it_behaves_like 'create'
        end
      end
    end

    describe 'with parent id' do
      before(:all) do
        @endpoint = @setup[:endpoint]
        @tag_category = FactoryBot.create(:tag_category, organization: @setup[:organization])
        @resource = FactoryBot.build(:tag_category)
        
        @post_json = {
          name: @resource.name,
          parent_id: @tag_category.id
        }
        @token = @setup[:admin_token]
      end
      include_examples "create", "allowed"

      it 'should create a sub category for the category' do
        @tag_category.reload
        expect(@tag_category.tag_categories.count).to eq 1
      end
    end
  end
end