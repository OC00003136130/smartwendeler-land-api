require "rails_helper"

RSpec.describe "PollAnswer Endpoints - v1/poll_answers", type: :request do
  describe '#create_update' do
    before(:all) do
      PollAnswer.destroy_all
      @setup = setup_specs("v1", "poll_answers")
      @resource = FactoryBot.build(:poll_answer)

      @poll_question = FactoryBot.create(:poll_question, organization: @setup[:organization])
      @poll_question_in_different_organization = FactoryBot.create(:poll_question, organization: @setup[:another_organization])

      @endpoint = "#{@setup[:endpoint]}/poll_question/#{@poll_question.id}"
      @user = @setup[:user]
      @post_json = {
        rating_value: @resource.rating_value
      }
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      describe 'poll game' do
        before(:all) do
          @count = @user.gamification_trophies.count
          @game = FactoryBot.create(:gamification_game, kind: 'poll', organization: @setup[:organization])
          @reward = FactoryBot.create(:gamification_reward, gamification_game: @game, threshold: 1)
          post_json @endpoint, @post_json, @setup[:user_token]
        end
  
        it 'should create a trophy for the user' do
          @user.gamification_trophies.reload
          expect(@user.gamification_trophies.count).to eq @count + 1
        end
      end
    end

    context 'when poll answer exists' do
      context 'for role admin' do
        before(:all) do
          PollAnswer.destroy_all
          @poll_answer = FactoryBot.create(:poll_answer, organization: @setup[:organization], user: @setup[:admin], rating_value: 5, poll_question: @poll_question)
          @token = @setup[:admin_token]
          @post_json = {
            rating_value: 3
          }
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'

        it 'should have updated the existing poll answer' do
          @poll_answer.reload
          expect(@poll_answer.rating_value).to eq @post_json[:rating_value]
        end
      end
    end

    context 'when data is not valid' do
      context 'poll_question not found' do
        before(:all) do
          @post_json = {
            rating_value: @resource.rating_value
          }
          @token = @setup[:admin_token]
          @endpoint = "#{@setup[:endpoint]}/poll_question/394792387489237498324"
        end
        include_examples "create", "not_found"
        it_behaves_like 'create'
      end

      context 'poll_question in different organization' do
        before(:all) do
          @post_json = {
            rating_value: @resource.rating_value
          }
          @token = @setup[:admin_token]
          @endpoint = "#{@setup[:endpoint]}/poll_question/#{@poll_question_in_different_organization.id}"
        end
        include_examples "create", "not_found"
        it_behaves_like 'create'
      end
    end
  end
end