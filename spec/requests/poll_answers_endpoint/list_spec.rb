require "rails_helper"

RSpec.describe "PollQuestion Endpoints - v1/poll_answers", type: :request do
  describe '#list' do
    before(:all) do
      PollQuestion.destroy_all
      @setup = setup_specs("v1", "poll_answers")
      @poll_question = FactoryBot.create(:poll_question, organization: @setup[:organization])
      @list = FactoryBot.create_list(:poll_answer, 10, organization: @setup[:organization], poll_question: @poll_question)

      @endpoint = "#{@setup[:endpoint]}/poll_question/#{@poll_question.id}"
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @poll_question.poll_answers
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @poll_question.poll_answers
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @poll_question.poll_answers
        end
        include_examples "list", "forbidden"
        it_behaves_like 'list'
      end
    end

    context 'when ressources are not found' do
      context 'poll_question not found' do
        before(:all) do
          @token = @setup[:admin_token]
          @endpoint = "#{@setup[:endpoint]}/poll_question/394792387489237498324"
        end
        include_examples "list", "not_found"
        it_behaves_like 'list'
      end
    end
  end
end