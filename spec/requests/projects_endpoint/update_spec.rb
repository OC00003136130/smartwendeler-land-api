require "rails_helper"

RSpec.describe "Project Endpoints - v1/projects", type: :request do
  describe '#update' do
    before(:all) do
      Log.destroy_all
      Project.destroy_all
      @setup = setup_specs("v1", "projects")
      @resource = FactoryBot.create(:project, organization: @setup[:organization], is_active: false, rating_results_public: false)
      @resource_updated = FactoryBot.build(:project, organization: @setup[:organization])
      @base64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
    end
    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
      end
      context 'for role root' do
        before(:all) do
          @put_json = {
            name: @resource.name,
            start_time: @resource.start_time,
            end_time: @resource.end_time,
            description: @resource.description,
            file: @base64,
            is_active: true,
            slug: 'lalal-11-A'
          }
          @token = @setup[:root_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'

        it 'should attach an image' do
          @resource.reload
          expect(@resource.image_url).not_to eq nil
          expect(@resource.image.attached?).to be_truthy
        end

        it 'should have created a log' do
          resource = parsed_response["resource"]
          id = resource['id']
          expect(Log.where(model_id: id).last.log_code).to eq 'project.is_active'
        end
      end

      context 'for role admin' do
        before(:all) do
          @put_json = {
            name: @resource.name,
            start_time: @resource.start_time,
            end_time: @resource.end_time,
            description: @resource.description,
            file: @base64,
            is_active: true,
            slug: 'lalal-11-B'
          }
          @token = @setup[:admin_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'

        describe 'purge image and set rating_results_public' do
          before(:all) do
            @put_json = {
              name: @resource.name,
              file: 'purge',
              rating_results_public: true
            }
          end
          include_examples "update", "allowed"
          
          it 'should remove the image' do
            @resource.reload
            expect(@resource.image_url).to eq nil
            expect(@resource.image.attached?).to be_falsey
          end

          it 'should have created a log' do
            resource = parsed_response["resource"]
            id = resource['id']
            expect(Log.where(model_id: id, log_code: "project.rating_results_public").count).to eq 1
          end
        end
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "update", "forbidden"
        it_behaves_like 'update'
      end

      describe 'assign/remove categories' do
        before(:all) do
          @category1 = FactoryBot.create(:category, organization: @setup[:organization])
          @category2 = FactoryBot.create(:category, organization: @setup[:organization])
          @category3 = FactoryBot.create(:category, organization: @setup[:organization])
          @category4 = FactoryBot.create(:category, organization: @setup[:organization])

          @resource = FactoryBot.create(:project, organization: @setup[:organization])
          @resource.categories << @category1
          @resource.categories << @category2
          @resource.categories << @category3
          @resource.categories << @category4

          @put_json = {
            category_ids: [@category2.id, @category3.id]     
          }

          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:admin_token]
        end

        include_examples "update", "allowed"
        
        it 'should assign/remove categories' do
          @resource.reload
          expect(@resource.categories).not_to include @category1
          expect(@resource.categories).to include @category2
          expect(@resource.categories).to include @category3
          expect(@resource.categories).not_to include @category4
        end
      end

      describe 'assign/remove categories' do
        before(:all) do
          @community1 = FactoryBot.create(:community, organization: @setup[:organization])
          @community2 = FactoryBot.create(:community, organization: @setup[:organization])
          @community3 = FactoryBot.create(:community, organization: @setup[:organization])
          @community4 = FactoryBot.create(:community, organization: @setup[:organization])

          @resource = FactoryBot.create(:project, organization: @setup[:organization])
          @resource.communities << @community1
          @resource.communities << @community2
          @resource.communities << @community3
          @resource.communities << @community4

          @put_json = {
            community_ids: [@community2.id, @community3.id]     
          }

          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:admin_token]
        end

        include_examples "update", "allowed"
        
        it 'should assign/remove communities' do
          @resource.reload
          expect(@resource.communities).not_to include @community1
          expect(@resource.communities).to include @community2
          expect(@resource.communities).to include @community3
          expect(@resource.communities).not_to include @community4
        end
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "not_found"
        it_behaves_like 'update'
      end
    end

    context 'when data is not valid' do
      describe 'end date before start date' do
        before(:all) do
          @put_json = {
            name: @resource.name,
            start_time: Time.now,
            end_time: Time.now - 2.seconds,
          }
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:admin_token]
          @expected_error_code = 'project.start_and_end_time_must_be_in_correct_order'
        end
        include_examples "update", "not_valid"
        it_behaves_like 'update'
      end
      describe 'end date before start date' do
        before(:all) do
          @put_json = {
            name: @resource.name,
            rating_start: Time.now,
            rating_end: Time.now - 2.seconds,
          }
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:admin_token]
          @expected_error_code = 'project.rating_start_and_end_must_be_in_correct_order'
        end
        include_examples "update", "not_valid"
        it_behaves_like 'update'
      end
      describe 'slug invalid' do
        before(:all) do
          @put_json = {
            name: @resource.name,
            rating_start: Time.now,
            rating_end: Time.now + 2.seconds,
            slug: "AAA BBB"
          }
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:admin_token]
          @expected_error_code = 'project.slug.invalid'
        end
        include_examples "update", "not_valid"
        it_behaves_like 'update'
      end
    end
  end
end