require "rails_helper"

RSpec.describe "Project Endpoints - v1/projects", type: :request do
  describe '#remove_vote' do
    before(:all) do
      Project.destroy_all
      @setup = setup_specs("v1", "projects")
      @resource = FactoryBot.create(:project, organization: @setup[:organization])

      @project_rating = FactoryBot.create(:project_rating, project: @resource, user: @setup[:user])
    end
    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}/remove_vote"
        @token = @setup[:user_token]
        delete_json @endpoint, {}, @token
      end

      it 'should return status 204' do
        expect(response.status).to eq 204
      end

      it 'should have removed the project rating' do
        expect(ProjectRating.exists? @project_rating.id).to be_falsey
      end
    end
  end
end