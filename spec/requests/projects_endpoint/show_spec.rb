require "rails_helper"

RSpec.describe "Project Endpoints - v1/projects", type: :request do
  describe '#show' do
    before(:all) do
      Project.destroy_all
      @setup = setup_specs("v1", "projects")
      @resource = FactoryBot.create(:project, organization: @setup[:organization], is_active: true)
    end
    context 'when resource is found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "show", "allowed"
        it_behaves_like 'show'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "show", "allowed"
        it_behaves_like 'show'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "show", "allowed"
        it_behaves_like 'show'
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "show", "not_found"
        it_behaves_like 'show'
      end

      describe 'project not public' do
        before(:all) do
          @resource = FactoryBot.create(:project, organization: @setup[:organization], is_active: false)
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
        end

        context 'for role user' do
          before(:all) do
            @token = @setup[:user_token]
          end
          include_examples "show", "not_found"
          it_behaves_like 'show'
        end
      end
    end

    context 'rating results' do
      context 'when results are not public' do
        before(:all) do
          @resource = FactoryBot.create(:project, organization: @setup[:organization], rating_results_public: false)

          @project_rating1 = FactoryBot.create(:project_rating, user: @setup[:user], project: @resource, score: 1)
          @project_rating2 = FactoryBot.create(:project_rating, user: @setup[:user], project: @resource, score: -1)
          @project_rating3 = FactoryBot.create(:project_rating, user: @setup[:user], project: @resource)
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
        end

        context 'for user role' do
          before(:all) do
            @token = @setup[:user_token]
          end

          include_examples "show", "allowed"

          it 'should not show results' do
            expect(parsed_response["resource"]["upvote_count"]).to eq nil
            expect(parsed_response["resource"]["downvote_count"]).to eq nil
            expect(parsed_response["resource"]["total_vote_count"]).to eq nil
          end
        end

        context 'for admin role' do
          before(:all) do
            @token = @setup[:admin_token]
          end

          include_examples "show", "allowed"

          it 'should still show results' do
            expect(parsed_response["resource"]["upvote_count"]).not_to eq nil
            expect(parsed_response["resource"]["downvote_count"]).not_to eq nil
            expect(parsed_response["resource"]["total_vote_count"]).not_to eq nil
          end
        end
      end
    end
  end
end