require "rails_helper"

RSpec.describe "Project Endpoints - v1/projects", type: :request do
  describe '#downvote' do
    before(:all) do
      Project.destroy_all
      @setup = setup_specs("v1", "projects")
      @user = @setup[:user]
      @resource = FactoryBot.create(:project, organization: @setup[:organization], rating_kind: 'upvote_downvote')
    end
    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}/downvote"
        @token = @setup[:user_token]
        put_json @endpoint, {}, @token
      end

      it 'should return status 200' do
        expect(response.status).to eq 200
      end

      it 'should have downvoted the project' do
        @resource.reload
        project_rating = @resource.project_ratings.find_by(
          user_id: @setup[:user],
          project_id: @resource.id
        )
        expect(project_rating.score).to eq -1
      end

      describe 'rating game' do
        before(:all) do
          @count = @user.gamification_trophies.count
          @game = FactoryBot.create(:gamification_game, kind: 'rating', organization: @setup[:organization])
          @reward = FactoryBot.create(:gamification_reward, gamification_game: @game, threshold: 1)
          put_json "#{@setup[:endpoint]}/#{@resource.id}/downvote", {}, @setup[:user_token]
        end
  
        it 'should create a trophy for the user' do
          @user.gamification_trophies.reload
          expect(@user.gamification_trophies.count).to eq @count + 1
        end
      end
    end

    context 'for projects with rating kind upvote' do
      before(:all) do
        @resource = FactoryBot.create(:project, organization: @setup[:organization], rating_kind: 'upvote')
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}/downvote"
        @token = @setup[:user_token]
        @expected_error_code = 'project.downvote.blocked'
        put_json @endpoint, {}, @token
      end

      it 'should return status 422' do
        expect(response.status).to eq 422
      end

      it 'should thrown an error' do
        expect(parsed_response['errors'][0]['code']).to eq @expected_error_code
      end
    end
  end
end