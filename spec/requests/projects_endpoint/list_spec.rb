require "rails_helper"

RSpec.describe "Project Endpoints - v1/projects", type: :request do
  describe '#list' do
    before(:all) do
      Project.destroy_all
      @setup = setup_specs("v1", "projects")
      @endpoint = @setup[:endpoint]
      @list1 = FactoryBot.create_list(:project, 5, organization: @setup[:organization], is_active: false)
      @list2 = FactoryBot.create_list(:project, 5, organization: @setup[:organization], is_active: true)
      @list = @list1 + @list2
      @locations = FactoryBot.create_list(:location, 2, project: @list2[0], organization: @setup[:organization])
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @setup[:organization].projects
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @setup[:organization].projects
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @setup[:organization].projects
          @list = @list2
        end
        include_examples "list", "allowed", 5
        it_behaves_like 'list'

        it 'should contain locations' do
          project_with_locations = parsed_response["resources"].find {|element| element["id"] == @list2[0].id}
          expect(project_with_locations["locations"].length()).to eq 2
        end
      end
    end
  end
end