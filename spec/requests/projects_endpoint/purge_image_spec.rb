require "rails_helper"

RSpec.describe "Project Endpoints - v1/projects", type: :request do
  describe '#images' do
    before(:all) do
      Project.destroy_all
      @setup = setup_specs("v1", "projects")
      @resource = FactoryBot.create(:project, organization: @setup[:organization])
      @base64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
    
      file = Rails.root.join('spec', 'fixtures', 'files', 'test.jpg')
      @resource.images.attach(io: File.open(file, 'rb'), filename: 'test.jpg')
      @signed_id = @resource.images[0].signed_id
    end

    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}/images/#{@signed_id}"
        @token = @setup[:admin_token]
        delete_json @endpoint, {}, @token
      end

      it 'should return status 204' do
        expect(response.status).to eq 204
      end

      it 'should have removed the image' do
        @resource.reload
        expect(@resource.sanitized_images.count).to eq 0
      end
    end

    context 'signed_id not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}/images/ey11111111"
        @token = @setup[:admin_token]
        delete_json @endpoint, {}, @token
      end

      it 'should return status 404' do
        expect(response.status).to eq 404
      end
    end
  end
end