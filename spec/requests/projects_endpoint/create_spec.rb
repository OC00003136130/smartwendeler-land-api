require "rails_helper"

RSpec.describe "Project Endpoints - v1/projects", type: :request do
  describe '#create' do
    before(:all) do
      Project.destroy_all
      @setup = setup_specs("v1", "projects")
      @endpoint = @setup[:endpoint]
      @resource = FactoryBot.build(:project)

      @base64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @post_json = {
            name: @resource.name,
            start_time: @resource.start_time,
            end_time: @resource.end_time,
            description: @resource.description,
            costs: 3500000000,
            file: @base64,
            slug: 'hallo-huhuh'
          }
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'

        it 'should attach an image' do
          @new_project = @setup[:organization].projects.find parsed_response['resource']['id']
          expect(@new_project.image_url).not_to eq nil
          expect(@new_project.image.attached?).to be_truthy
        end
      end

      context 'for role admin' do
        before(:all) do
          @post_json = {
            name: @resource.name,
            start_time: @resource.start_time,
            end_time: @resource.end_time,
            description: @resource.description,
            costs: 3500000000,
            file: @base64,
            slug: 'lalal-88_csdfdsf'
          }
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'

        it 'should have put the menu_order after the next entry' do
          expect(@setup[:organization].projects.all.order(:menu_order).last.menu_order).to eq 1
        end
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "forbidden"
        it_behaves_like 'create'
      end

      describe 'assign categories' do
        before(:all) do
          @category1 = FactoryBot.create(:category, organization: @setup[:organization])
          @category2 = FactoryBot.create(:category, organization: @setup[:organization])
          @category3 = FactoryBot.create(:category, organization: @setup[:organization])
          @category4 = FactoryBot.create(:category, organization: @setup[:organization])

          @post_json = {
            name: @resource.name,
            start_time: @resource.start_time,
            end_time: @resource.end_time,
            description: @resource.description,
            category_ids: [@category1.id, @category2.id, @category3.id, @category4.id]     
          }
          @token = @setup[:admin_token]
        end

        include_examples "create", "allowed"
        it 'should assign categories' do
          resource = Project.find parsed_response["resource"]["id"]
          
          expect(resource.categories).to include @category1
          expect(resource.categories).to include @category2
          expect(resource.categories).to include @category3
          expect(resource.categories).to include @category4
        end
      end

      describe 'assign communities' do
        before(:all) do
          @community1 = FactoryBot.create(:community, organization: @setup[:organization])
          @community2 = FactoryBot.create(:community, organization: @setup[:organization])
          @community3 = FactoryBot.create(:community, organization: @setup[:organization])
          @community4 = FactoryBot.create(:community, organization: @setup[:organization])

          @post_json = {
            name: @resource.name,
            start_time: @resource.start_time,
            end_time: @resource.end_time,
            description: @resource.description,
            community_ids: [@community1.id, @community2.id, @community3.id, @community4.id]     
          }
          @token = @setup[:admin_token]
        end

        include_examples "create", "allowed"
        it 'should assign communities' do
          resource = Project.find parsed_response["resource"]["id"]
          
          expect(resource.communities).to include @community1
          expect(resource.communities).to include @community2
          expect(resource.communities).to include @community3
          expect(resource.communities).to include @community4
        end
      end
    end

    context 'when data is not valid' do
      describe 'end date before start date' do
        before(:all) do
          @post_json = {
            name: @resource.name,
            start_time: Time.now,
            end_time: Time.now - 2.seconds,
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'project.start_and_end_time_must_be_in_correct_order'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end

      describe 'end date before start date' do
        before(:all) do
          @post_json = {
            name: @resource.name,
            rating_start: Time.now,
            rating_end: Time.now - 2.seconds,
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'project.rating_start_and_end_must_be_in_correct_order'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end

      describe 'slug in wrong format' do
        before(:all) do
          @post_json = {
            name: @resource.name,
            rating_start: Time.now,
            rating_end: Time.now + 10.days,
            slug: 'udhsfsdf AA %%'
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'project.slug.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
    end
  end
end