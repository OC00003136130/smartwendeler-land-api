require "rails_helper"

RSpec.describe "Project Endpoints - v1/projects", type: :request do
  describe '#list' do

    before(:all) do
      Project.destroy_all
      Location.destroy_all
      @setup = setup_specs("v1", "projects")
      @base_endpoint = @setup[:endpoint]

      @project1 = FactoryBot.create(:project, organization: @setup[:organization])
      @project2 = FactoryBot.create(:project, organization: @setup[:organization])
      @project3 = FactoryBot.create(:project, organization: @setup[:organization])

      FactoryBot.create(:location, project: @project1, longitude: 7.165657, latitude: 49.467739, organization: @setup[:organization]) # St. Wendel center
      FactoryBot.create(:location, project: @project2, longitude: 6.990262, latitude: 49.234284, organization: @setup[:organization]) # Saarbrücken center
      FactoryBot.create(:location, project: @project1, longitude: 6.646474, latitude: 49.760242, organization: @setup[:organization]) # Trier center
    end

    context 'when location filter matches location 1' do
      before(:all) do
        @token = @setup[:user_token]
        @model = @setup[:organization].projects
        @endpoint = @base_endpoint + '?filter_georectangle=7.145656,49.455117,7.189487,49.478640'
        get_json @endpoint, {}, @token
      end

      it 'it should return the correct result' do
        expect(parsed_response["resources"].length()).to eq 1
        expect(parsed_response["resources"][0]["id"]).to eq @project1.id
      end
    end

    context 'when location filter matches location 2' do
      before(:all) do
        @token = @setup[:user_token]
        @model = @setup[:organization].projects
        @endpoint = @base_endpoint + '?filter_georectangle=6.911511,49.177192,7.092220,49.285252'
        get_json @endpoint, {}, @token
      end

      it 'it should return the correct result' do
        expect(parsed_response["resources"].length()).to eq 1
        expect(parsed_response["resources"][0]["id"]).to eq @project2.id
      end
    end

    context 'when location filter matches location 1 and 2' do
      before(:all) do
        @token = @setup[:user_token]
        @model = @setup[:organization].projects
        @endpoint = @base_endpoint + '?filter_georectangle=6.713210,49.128220,7.361836,49.570162&sort_by=created_at'
        get_json @endpoint, {}, @token
      end

      it 'it should return the correct result' do
        expect(parsed_response["resources"].length()).to eq 2
        expect(parsed_response["resources"][0]["id"]).to eq @project1.id
        expect(parsed_response["resources"][1]["id"]).to eq @project2.id
      end
    end

    context 'when location filter matches 2 locations of the same project' do
      before(:all) do
        @token = @setup[:user_token]
        @model = @setup[:organization].projects
        @endpoint = @base_endpoint + '?filter_georectangle=6.436592,49.396632,7.302343,49.827310&sort_by=created_at'
        get_json @endpoint, {}, @token
      end

      it 'it should return the project only once' do
        expect(parsed_response["resources"].length()).to eq 1
        expect(parsed_response["resources"][0]["id"]).to eq @project1.id
      end
    end
  end
end