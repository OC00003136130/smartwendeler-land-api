require "rails_helper"

RSpec.describe "Tag Endpoints - v1/tags", type: :request do
  describe '#list' do
    before(:all) do
      Tag.destroy_all
      @setup = setup_specs("v1", "tags")
      @list = FactoryBot.create_list(:tag, 10, organization: @setup[:organization])

      @endpoint = @setup[:endpoint]
    end
    context 'all tags' do
      before(:all) do
        @endpoint = @setup[:endpoint]
      end
      context 'when ressources are found' do
        context 'for role root' do
          before(:all) do
            @token = @setup[:root_token]
            @model = @setup[:organization].tags
          end
          include_examples "list", "allowed", 10
          it_behaves_like 'list'
        end

        context 'for role admin' do
          before(:all) do
            @token = @setup[:admin_token]
            @model = @setup[:organization].tags
          end
          include_examples "list", "allowed", 10
          it_behaves_like 'list'
        end

        context 'for role user' do
          before(:all) do
            @token = @setup[:user_token]
            @model = @setup[:organization].tags
          end
          include_examples "list", "allowed", 10
          it_behaves_like 'list'
        end
      end
    end
    # currently deactivated as it fails on circle ci:
    
    # context 'filtered by tag category' do
    #   before(:all) do
    #     Tag.destroy_all
    #     @setup = setup_specs("v1", "tags")
    #     @tag_category = FactoryBot.create(:tag_category, organization: @setup[:organization])
    #     @tags = FactoryBot.create_list(:tag, 10, organization: @setup[:organization])

    #     @list = FactoryBot.create_list(:tag, 7, organization: @setup[:organization], tag_category: @tag_category)

    #     @endpoint = "#{@setup[:endpoint]}?tag_category_id=#{@tag_category.id}"
    #   end

    #   context 'for role admin' do
    #     before(:all) do
    #       @token = @setup[:admin_token]
    #       @model = @setup[:organization].tags
    #     end
    #     include_examples "list", "allowed", 7
    #     it_behaves_like 'list'
    #   end
    # end
  end
end