require "rails_helper"

RSpec.describe "Tag Endpoints - v1/tags", type: :request do
  describe '#create' do
    before(:all) do
      Tag.destroy_all
      @setup = setup_specs("v1", "tags")
      @resource = FactoryBot.build(:tag)

      @endpoint = @setup[:endpoint]

      @post_json = {
        name: @resource.name,
        scope: 'care_facility'
      }
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'

        it 'should have put the menu_order after the next entry' do
          expect(@setup[:organization].tags.all.order(:menu_order).last.menu_order).to eq 1
        end
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "forbidden"
        it_behaves_like 'create'
      end
    end

    context 'when data is not valid' do
      context 'name missing' do
        before(:all) do
          @post_json = {
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'tag.name.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
    end
  end
end