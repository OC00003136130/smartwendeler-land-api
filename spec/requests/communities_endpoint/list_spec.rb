require "rails_helper"

RSpec.describe "Community Endpoints - v1/communities", type: :request do
  describe '#list' do
    before(:all) do
      Project.destroy_all
      Community.destroy_all
      @setup = setup_specs("v1", "communities")
      @endpoint = @setup[:endpoint]
      @list = FactoryBot.create_list(:community, 10, organization: @setup[:organization])
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @setup[:organization].communities
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @setup[:organization].communities
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @setup[:organization].communities
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end
    end
  end
end