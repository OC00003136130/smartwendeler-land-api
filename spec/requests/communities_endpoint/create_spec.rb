require "rails_helper"

RSpec.describe "Community Endpoints - v1/communities", type: :request do
  describe '#create' do
    before(:all) do
      Project.destroy_all
      Community.destroy_all
      @setup = setup_specs("v1", "communities")
      @endpoint = @setup[:endpoint]
      @resource = FactoryBot.build(:community)
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @post_json = {
            name: Faker::Company.name,
          }
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role admin' do
        before(:all) do
          @post_json = {
            name: Faker::Company.name,
          }
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'

        it 'should have put the menu_order after the next entry' do
          expect(@setup[:organization].communities.order(:menu_order).last.menu_order).to eq 1
        end
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "forbidden"
        it_behaves_like 'create'
      end
    end

    context 'when data is not valid' do
      describe 'community name already exists' do
        before(:all) do
          @post_json = {
            name: Community.last.name
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'community.name.taken'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
    end
  end
end