require "rails_helper"

RSpec.describe "Community Endpoints - v1/communities", type: :request do
  describe '#update' do
    before(:all) do
      Project.destroy_all
      Community.destroy_all
      @setup = setup_specs("v1", "communities")
      @resource = FactoryBot.create(:community, organization: @setup[:organization])
      @resource_updated = FactoryBot.build(:community, organization: @setup[:organization])

      @put_json = {
        name: @resource.name,
      }
    end
    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "update", "forbidden"
        it_behaves_like 'update'
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "not_found"
        it_behaves_like 'update'
      end
    end

    context 'when data is not valid' do
      describe 'community name already exists' do
        before(:all) do
          @resource2 = FactoryBot.create(:community, organization: @setup[:organization])
          @put_json = {
            name: @resource2.name
          }
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:admin_token]
          @expected_error_code = 'community.name.taken'
        end
        include_examples "update", "not_valid"
        it_behaves_like 'update'
      end
    end
  end
end