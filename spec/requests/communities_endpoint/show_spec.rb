require "rails_helper"

RSpec.describe "Community Endpoints - v1/communities", type: :request do
  describe '#show' do
    before(:all) do
      Project.destroy_all
      Community.destroy_all
      @setup = setup_specs("v1", "communities")
      @resource = FactoryBot.create(:community, organization: @setup[:organization])
    end
    context 'when resource is found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "show", "allowed"
        it_behaves_like 'show'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "show", "allowed"
        it_behaves_like 'show'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "show", "allowed"
        it_behaves_like 'show'
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "show", "not_found"
        it_behaves_like 'show'
      end
    end
  end
end