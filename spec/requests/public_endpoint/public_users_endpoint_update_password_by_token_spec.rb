require "rails_helper"

RSpec.describe "User Endpoints - v1/users", type: :request do
  describe '#update_password_by_token' do
    before(:all) do
      @organization = FactoryBot.create(:organization)
      @password_token = SecureRandom.base64(80).tr('+/=', '0aZ')
      @user = FactoryBot.create(:user, password_token: @password_token)
      @password_before = @user.password
    end

    describe 'success cases' do
      before(:all) do
        @post_json = {
          password: "abc1234",
          password_confirmation: "abc1234"
        }
        @endpoint = "/v1/public/users/update-password/#{@password_token}"
        put_json @endpoint, @post_json, nil
      end

      it 'should return status 200' do
        expect(response.status).to eq 200
      end

      it 'should let the user login with the new password' do
        @user.reload
        @new_user_token = login(@user.email, @post_json[:password])
        expect(@new_user_token).not_to eq nil
      end
    end

    describe 'fail cases' do
      describe 'not found' do
        before(:all) do
          @post_json = {
            password: "abc1234",
            password_confirmation: "abc1234"
          }
          @endpoint = "/v1/public/users/update-password/abc"
          put_json @endpoint, @post_json, nil
        end

        it 'should return status 404' do
          expect(response.status).to eq 404
        end
      end

      describe 'password not matching' do
        before(:all) do
          @post_json = {
            password: "abc1234",
            password_confirmation: "abc12345"
          }
          @password_token = SecureRandom.base64(80).tr('+/=', '0aZ')
          @user = FactoryBot.create(:user, password_token: @password_token)
          @endpoint = "/v1/public/users/update-password/#{@password_token}"
          put_json @endpoint, @post_json, nil
        end

        it 'should return status 422' do
          expect(response.status).to eq 422
        end
      end
    end
  end
end