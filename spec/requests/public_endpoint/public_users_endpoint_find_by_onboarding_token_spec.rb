require "rails_helper"

RSpec.describe "User Endpoints - v1/users", type: :request do
  describe '#find_by_token' do
    before(:all) do
      @organization = FactoryBot.create(:organization)
      @onboarding_token = SecureRandom.base64(80).tr('+/=', '0aZ')
      @user = FactoryBot.create(:user, onboarding_token: @onboarding_token)
    end

    describe 'success cases' do
      before(:all) do
        @endpoint = "/v1/public/users/find-by-onboarding-token/#{@onboarding_token}"
        get_json @endpoint, {}, nil
      end

      it 'should return status 200' do
        expect(response.status).to eq 200
      end

      it 'should receive a user' do
        expect(parsed_response["code"]).to eq "user.found"
      end
    end

    describe 'fail cases' do
      before(:all) do
        @endpoint = "/v1/public/users/find-by-onboarding-token/abc"
        get_json @endpoint, {}, nil
      end

      it 'should return status 404' do
        expect(response.status).to eq 404
      end
    end
  end
end