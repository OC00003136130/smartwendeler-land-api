require "rails_helper"

RSpec.describe "Public Users Endpoints - v1/public/users", type: :request do
  before(:all) do
    @endpoint = '/v1/public/users/reset-password'
    @organization = setup_organization

    # setup users
    @user = FactoryBot.create(:user, organization: @organization)

    @password_before = @user.password
  end

  context 'Reset password' do
    before(:all) do
      @post_json = {
        email: @user.email
      }

      post_json @endpoint, @post_json, nil
    end

    it 'should return status 200' do
      expect(response.status).to eq 200
    end

    it 'should confirm be successful' do
      expect(response.parsed_body['code']).to eq 'successfully.confirmed'
    end
  end
end
