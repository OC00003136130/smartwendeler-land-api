require "rails_helper"

RSpec.describe "Poll Endpoints - v1/polls", type: :request do
  describe '#list' do
    before(:all) do
      Poll.destroy_all
      @setup = setup_specs("v1", "polls")
      @list = FactoryBot.create_list(:poll, 10, organization: @setup[:organization])

      @endpoint = @setup[:endpoint]
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @setup[:organization].polls
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @setup[:organization].polls
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @setup[:organization].polls
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end
    end
  end
end