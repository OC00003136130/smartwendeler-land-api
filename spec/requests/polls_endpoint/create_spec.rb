require "rails_helper"

RSpec.describe "Poll Endpoints - v1/polls", type: :request do
  describe '#create' do
    before(:all) do
      Poll.destroy_all
      @setup = setup_specs("v1", "polls")
      @resource = FactoryBot.build(:poll)

      @endpoint = @setup[:endpoint]

      @post_json = {
        name: @resource.name,
        ends_at: @resource.ends_at,
        is_active: @resource.is_active
      }
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "forbidden"
        it_behaves_like 'create'
      end
    end

    context 'when data is not valid' do
      context 'name missing' do
        before(:all) do
          @post_json = {
            ends_at: @resource.ends_at
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'poll.name.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
    end
  end
end