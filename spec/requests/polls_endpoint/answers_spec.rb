require "rails_helper"

RSpec.describe "Poll Endpoints - v1/polls", type: :request do
  describe '#show' do
    before(:all) do
      Poll.destroy_all
      @setup = setup_specs("v1", "polls")
      @resource = FactoryBot.create(:poll, organization: @setup[:organization])

      @q1 = FactoryBot.create(:poll_question, organization: @setup[:organization], poll: @resource, kind: 'rating', max_score_count: 5)
      @q2 = FactoryBot.create(:poll_question, organization: @setup[:organization], poll: @resource, kind: 'rating', max_score_count: 5)
      @q3 = FactoryBot.create(:poll_question, organization: @setup[:organization], poll: @resource, kind: 'text')
      @q4 = FactoryBot.create(:poll_question, organization: @setup[:organization], poll: @resource, kind: 'single_choice')

      @a1 = FactoryBot.create(:poll_answer, organization: @setup[:organization], poll_question: @q1, rating_value: 2)
      @a2 = FactoryBot.create(:poll_answer, organization: @setup[:organization], poll_question: @q1, rating_value: 3)
      @a3 = FactoryBot.create(:poll_answer, organization: @setup[:organization], poll_question: @q1, rating_value: 5)
      @a4 = FactoryBot.create(:poll_answer, organization: @setup[:organization], poll_question: @q1, rating_value: 1)

      @a5 = FactoryBot.create(:poll_answer, organization: @setup[:organization], poll_question: @q2, rating_value: 4)
      @a6 = FactoryBot.create(:poll_answer, organization: @setup[:organization], poll_question: @q2, rating_value: 1)
      @a7 = FactoryBot.create(:poll_answer, organization: @setup[:organization], poll_question: @q2, rating_value: 2)
      @a8 = FactoryBot.create(:poll_answer, organization: @setup[:organization], poll_question: @q2, rating_value: 1)

      @a5 = FactoryBot.create(:poll_answer, organization: @setup[:organization], poll_question: @q3, text_value: 'ABC')
      @a6 = FactoryBot.create(:poll_answer, organization: @setup[:organization], poll_question: @q3, text_value: 'DEF')
      @a7 = FactoryBot.create(:poll_answer, organization: @setup[:organization], poll_question: @q3, text_value: 'GHI')
      @a8 = FactoryBot.create(:poll_answer, organization: @setup[:organization], poll_question: @q3, text_value: 'JKL')

      @a9 = FactoryBot.create(:poll_answer, organization: @setup[:organization], poll_question: @q4, choices_answers: 'GGGGG')
      @a10 = FactoryBot.create(:poll_answer, organization: @setup[:organization], poll_question: @q4, choices_answers: 'QQQQQ')
      @a11 = FactoryBot.create(:poll_answer, organization: @setup[:organization], poll_question: @q4, choices_answers: 'VVVVV')
      @a12 = FactoryBot.create(:poll_answer, organization: @setup[:organization], poll_question: @q4, choices_answers: 'RRRRR')
    end
    context 'when resource is found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}/answers"
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          get_json @endpoint, {}, @token
        end

        it 'should plot the answers as json' do
          resource = parsed_response["resource"]
          
          expect(parsed_response[@q1.id]["question"]).to eq @q1.name
          expect(parsed_response[@q1.id]["kind"]).to eq @q1.kind
          expect(parsed_response[@q1.id]["average_rating"]).to eq @q1.poll_answers.average(:rating_value)

          expect(parsed_response[@q2.id]["question"]).to eq @q2.name
          expect(parsed_response[@q2.id]["kind"]).to eq @q2.kind
          expect(parsed_response[@q2.id]["average_rating"]).to eq @q2.poll_answers.average(:rating_value)

          expect(parsed_response[@q3.id]["question"]).to eq @q3.name
          expect(parsed_response[@q3.id]["kind"]).to eq @q3.kind
          expect(parsed_response[@q3.id]["values"]).to eq @q3.poll_answers.pluck(:text_value)

          expect(parsed_response[@q4.id]["question"]).to eq @q4.name
          expect(parsed_response[@q4.id]["kind"]).to eq @q4.kind
          expect(parsed_response[@q4.id]["values"]).to eq @q4.poll_answers.pluck(:choices_answers)
        end
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "show", "forbidden"
        it_behaves_like 'show'
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875/answers"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "show", "not_found"
        it_behaves_like 'show'
      end
    end
  end
end