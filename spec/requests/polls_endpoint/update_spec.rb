require "rails_helper"

RSpec.describe "Poll Endpoints - v1/polls", type: :request do
  describe '#update' do
    before(:all) do
      Poll.destroy_all
      @setup = setup_specs("v1", "polls")
      @resource = FactoryBot.create(:poll, organization: @setup[:organization], is_active: false)
      @resource_updated = FactoryBot.build(:poll, organization: @setup[:organization], is_active: false)
      @put_json = {
        name: @resource.name,
        ends_at: @resource.ends_at,
        is_active: @resource.is_active
      }
    end
    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "update", "forbidden"
        it_behaves_like 'update'
      end
    end

    context 'try to make multiple public polls active' do
      before(:all) do
        Poll.destroy_all
        @existing_active = FactoryBot.create(:poll, organization: @setup[:organization], is_active: true, project: nil)
        @resource = FactoryBot.create(:poll, organization: @setup[:organization], is_active: false, project: nil)
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
        @put_json = {
          is_active: true
        }
        @token = @setup[:admin_token]
        put_json @endpoint, @put_json, @token
      end

      it 'should not set the poll active' do
        expect(response.status).to eq 422
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "not_found"
        it_behaves_like 'update'
      end
    end
  end
end