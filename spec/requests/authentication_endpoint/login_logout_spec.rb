require "rails_helper"

RSpec.describe "Authentication Endpoints - Login and Logout - v1", type: :request do
  before(:all) do
    User.destroy_all
    @organization = setup_organization()
    @login_endpoint = '/v1/auth'
    @logout_endpoint = '/v1/users/logout'

    @user = FactoryBot.create(:user, organization: @organization)
    @user_never_expire = FactoryBot.create(:user, organization: @organization, never_expire: true)
  end

  describe 'standard auth login' do
    before(:all) do
      post_json @login_endpoint, { email: @user.email, password: @user.password }, nil
      @parsed_response = JSON.parse(response.body)
      @decoded_token = JWT.decode(@parsed_response["jwt_token"],
        Rails.application.credentials[:secret_key_base], true, { algorithm: 'HS512'})
    end

    it 'should be successful' do
      expect(response.status).to eq 200
    end

    it 'should set a correct jwt_token for the user' do
      expect(@parsed_response["jwt_token"]).not_to eq nil

      expect(@decoded_token[0]["email"]).to eq @user.email
      expect(@decoded_token[0]["user_id"]).to eq @user.id
      expect(@decoded_token[0]["organization_id"]).to eq @user.organization.id
      expect(@decoded_token[0]["role"]).to eq "user"
      expect(@decoded_token[0]["never_expire"]).to be_falsey
    end

    it 'should set expiration time ~24h' do
      expiration_in_seconds = ((Time.now - Time.at(@decoded_token[0]["exp"]).to_datetime).to_i) * -1
      expect(expiration_in_seconds < 86401).to be_truthy
      expect(expiration_in_seconds > 86395).to be_truthy
    end

    describe 'login game' do
      before(:all) do
        @count = @user.gamification_trophies.count
        @game = FactoryBot.create(:gamification_game, kind: 'login', organization: @organization)
        @reward = FactoryBot.create(:gamification_reward, gamification_game: @game, threshold: 1)
        post_json @login_endpoint, { email: @user.email, password: @user.password }, nil
      end

      it 'should create a trophy for the user' do
        @user.gamification_trophies.reload
        expect(@user.gamification_trophies.count).to eq @count + 1
      end
    end
  end

  describe 'jwt token expired' do
    before(:all) do
      expiration_time = (Time.now - 2.days).to_i
      payload = {
        'email': @user.email,
        'user_id': @user.id,
        'organization_id': @organization.id,
        'role': @user.role,
        'exp': expiration_time
      }

      @jwt = JWT.encode(payload,
        Rails.application.credentials[:secret_key_base], 'HS512',
        { exp: expiration_time, :user_id => @user.id }
      )

      # try something with the expired token
      get_json '/v1/users/me', {}, @jwt
    end

    it 'should return 401' do
      # TODO: check why no response body is available
      expect(response.status).to eq 401
    end
  end

  describe 'multiple users with same email in the database' do
    before(:all) do
      @organization2 = setup_organization()
      @organization3 = setup_organization()
      @user1 = FactoryBot.create(:user, organization: @organization, email: "one_unique@email.de")
      @user2 = FactoryBot.create(:user, organization: @organization2, email: "one_unique@email.de")
      @user3 = FactoryBot.create(:user, organization: @organization3, email: "one_unique@email.de")
      post_json @login_endpoint, { email: @user2.email, password: @user2.password, register_token: @organization2.register_token }, nil
      @parsed_response = JSON.parse(response.body)
    end

    it 'should be successful' do
      expect(response.status).to eq 200
    end

    it 'should return the correct user' do
      expect(@parsed_response["user"]["id"]).to eq @user2.id
    end
  end

  describe 'never expiring login' do
    before(:all) do
      post_json @login_endpoint, { email: @user_never_expire.email, password: @user_never_expire.password }, nil
      @parsed_response = JSON.parse(response.body)
      @decoded_token = JWT.decode(@parsed_response["jwt_token"],
        Rails.application.credentials[:secret_key_base], true, { algorithm: 'HS512'})
    end

    it 'should be successful' do
      expect(response.status).to eq 200
    end

    # currently disabled
    # it 'should set expiration time ~6 months' do
    #   expiration_in_seconds = ((Time.now - Time.at(@decoded_token[0]["exp"]).to_datetime).to_i) * -1
    #   expect(expiration_in_seconds < 15898599).to be_truthy
    #   expect(expiration_in_seconds > 15892599).to be_truthy
    # end
  end

  describe 'wrong credentials' do
    before(:all) do
      post_json @login_endpoint, { email: @user.email, password: "definetelywrong" }, nil
      @parsed_response = JSON.parse(response.body)
    end

    it 'should not be successful' do
      expect(response.status).to eq 401
    end
  end

  describe 'logout' do
    before(:all) do
      # login a user to logout later
      @user_token = login(@user.email, @user.password)
      delete_json @logout_endpoint, {}, @user_token
    end

    it 'should remove the jwt_token from the user' do
      @user.reload
      expect(@user.jwt_token).to eq nil
    end
  end
end
