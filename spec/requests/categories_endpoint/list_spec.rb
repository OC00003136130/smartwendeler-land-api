require "rails_helper"

RSpec.describe "Category Endpoints - v1/categories", type: :request do
  describe '#list' do
    before(:all) do
      Project.destroy_all
      Category.destroy_all
      @setup = setup_specs("v1", "categories")
      @endpoint = @setup[:endpoint]
      @list = FactoryBot.create_list(:category, 10, organization: @setup[:organization])
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @setup[:organization].categories
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @setup[:organization].categories
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @setup[:organization].categories
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end
    end
  end
end