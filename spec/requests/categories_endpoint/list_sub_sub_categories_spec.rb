require "rails_helper"

RSpec.describe "Category Endpoints - v1/categories", type: :request do
  describe '#list_sub_sub_categories' do
    before(:all) do
      Category.destroy_all
      @setup = setup_specs("v1", "categories")
      @category = FactoryBot.create(:category, organization: @setup[:organization])
      @sub_category = FactoryBot.create(:sub_category, organization: @setup[:organization], category: @category)
      @endpoint = "#{@setup[:endpoint]}/#{@category.id}/sub_categories/#{@sub_category.id}"
      @list = FactoryBot.create_list(:sub_sub_category, 10, organization: @setup[:organization], sub_category: @sub_category)
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @sub_category.sub_sub_categories
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @sub_category.sub_sub_categories
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @sub_category.sub_sub_categories
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end
    end
  end
end