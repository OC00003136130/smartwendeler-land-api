require "rails_helper"

RSpec.describe "Category Endpoints - v1/categories", type: :request do
  describe '#create' do
    before(:all) do
      Project.destroy_all
      Category.destroy_all
      @setup = setup_specs("v1", "categories")
      @endpoint = @setup[:endpoint]
      @resource = FactoryBot.build(:category)
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          base64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
          @post_json = {
            name: Faker::Company.name,
            scope: 'project',
            file: base64
          }
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'

        it 'should have added an image' do
          res = response.parsed_body['resource']
          expect(res['image_url']).not_to eq nil
        end
      end

      context 'for role admin' do
        before(:all) do
          @post_json = {
            name: Faker::Company.name,
            scope: 'project'
          }
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'

        it 'should have put the menu_order after the next entry' do
          expect(@setup[:organization].categories.order(:menu_order).last.menu_order).to eq 1
        end
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "forbidden"
        it_behaves_like 'create'
      end
    end

    context 'when data is not valid' do
      describe 'scope missing' do
        before(:all) do
          @post_json = {
            name: Category.last.name
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'category.scope.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
    end
  end
end