require "rails_helper"

RSpec.describe "Category Endpoints - v1/categories", type: :request do
  describe '#update' do
    before(:all) do
      Project.destroy_all
      Category.destroy_all
      @setup = setup_specs("v1", "categories")
      @resource = FactoryBot.create(:category, organization: @setup[:organization])
      @resource_updated = FactoryBot.build(:category, organization: @setup[:organization])
      base64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
      @put_json = {
        name: @resource.name,
        file: base64
      }
    end
    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'

        it 'should have added an image' do
          res = response.parsed_body['resource']
          expect(res['image_url']).not_to eq nil
        end
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "update", "forbidden"
        it_behaves_like 'update'
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "not_found"
        it_behaves_like 'update'
      end
    end
  end
end