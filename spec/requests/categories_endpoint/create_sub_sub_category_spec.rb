require "rails_helper"

RSpec.describe "Category Endpoints - v1/categories", type: :request do
  describe '#create_sub_sub_category' do
    before(:all) do
      Project.destroy_all
      Category.destroy_all
      @setup = setup_specs("v1", "categories")
      @category = FactoryBot.create(:category, organization: @setup[:organization])
      @sub_category = FactoryBot.create(:sub_category, organization: @setup[:organization], category: @category)
      @endpoint = "#{@setup[:endpoint]}/#{@category.id}/sub_categories/#{@sub_category.id}"
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @post_json = {
            name: Faker::Company.name,
            tags: ['aaa', 'bbb'],
            scope: 'project'
          }
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role admin' do
        before(:all) do
          @post_json = {
            name: Faker::Company.name,
            scope: 'project'
          }
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'

        it 'should have put the menu_order after the next entry' do
          expect(@sub_category.sub_sub_categories.order(:menu_order).last.menu_order).to eq 1
        end
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "forbidden"
        it_behaves_like 'create'
      end
    end

    context 'when data is not valid' do
      describe 'scope missing' do
        before(:all) do
          @post_json = {
            name: Category.last.name
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'subsubcategory.scope.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
    end
  end
end