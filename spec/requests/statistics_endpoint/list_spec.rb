require "rails_helper"

RSpec.describe "Statistic Endpoints - v1/statistics", type: :request do
  describe '#list' do
    before(:all) do
      CareFacility.destroy_all
      Project.destroy_all
      Comment.destroy_all
      Message.destroy_all
      PollAnswer.destroy_all
      CommentReport.destroy_all

      @setup = setup_specs("v1", "statistics")
      @endpoint = @setup[:endpoint]
      # sample data
      FactoryBot.create_list(:care_facility, 14, organization: @setup[:organization], is_active: true)
      FactoryBot.create_list(:project, 16, organization: @setup[:organization], is_active: true)
      FactoryBot.create_list(:comment, 7, organization: @setup[:organization])
      FactoryBot.create_list(:message, 44, organization: @setup[:organization])
      FactoryBot.create_list(:poll_answer, 23, organization: @setup[:organization])
      FactoryBot.create_list(:comment_report, 10, organization: @setup[:organization])
    end

    context 'for role user' do
      before(:all) do
        @token = @setup[:user_token]
        @model = @setup[:organization].care_facilities
        get_json @endpoint, {}, @token
      end
      
      it 'should return status 403' do
        expect(response.status).to eq 403
      end
    end

    context 'for role admin' do
      before(:all) do
        @token = @setup[:admin_token]
        @model = @setup[:organization].care_facilities
        get_json @endpoint, {}, @token
      end

      it 'should return status 200' do
        expect(response.status).to eq 200
      end
      
      it 'should output statistics' do
        expect(parsed_response['users_count']).to eq 5
        expect(parsed_response['active_care_facilities_count']).to eq 14
        expect(parsed_response['active_projects_count']).to eq 16
        expect(parsed_response['comments_count']).to eq 7
        expect(parsed_response['messages_count']).to eq 44
        expect(parsed_response['poll_answers_count']).to eq 23
        expect(parsed_response['comment_reports_count']).to eq 10
      end
    end
  end
end