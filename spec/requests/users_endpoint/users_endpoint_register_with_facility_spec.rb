require "rails_helper"

RSpec.describe "Users Endpoints - v1/users", type: :request do
  before(:all) do
    @organization = FactoryBot.create(:organization, scope: 'health')
    @endpoint = "/v1/public/users/register_with_facility/#{@organization.register_token}"
  end

  context 'register' do
    before(:all) do
      @community = FactoryBot.create(:community)
      @post_json = {
        "firstname": "Karl",
        "lastname": "Lauterbach",
        "commercial_register_number": "ABCDEFG",
        "care_facility_name": "My Facility",
        "care_facility_zip": "66606",
        "care_facility_town": "Winterbach",
        "care_facility_community_id": @community.id,
        "email": Faker::Internet.email,
      }
      post_json @endpoint, @post_json, nil
    end

    it 'should return status 201' do
      expect(response.status).to eq 201
    end

    it 'should have registered the user to the organization' do
      @new_user = @organization.users.find parsed_response['id']

      expect(@new_user.firstname).to eq @post_json[:firstname]
      expect(@new_user.lastname).to eq @post_json[:lastname]
      expect(@new_user.commercial_register_number).to eq @post_json[:commercial_register_number]
      expect(@new_user.email).to eq @post_json[:email]
      expect(@new_user.status).to eq 'confirmed'
      expect(@new_user.role).to eq 'facility_owner'
      expect(@new_user.is_active_on_health_scope).to be_falsey
    end

    it 'should have created a new care facility scoped to the user' do
      @new_user = @organization.users.find parsed_response['id']
      expect(@new_user.care_facilities.last.name).to eq @post_json[:care_facility_name]
      expect(@new_user.care_facilities.last.zip).to eq @post_json[:care_facility_zip]
      expect(@new_user.care_facilities.last.town).to eq @post_json[:care_facility_town]
      expect(@new_user.care_facilities.last.community).to eq @community
    end

    it 'should have set the registration data' do
      @new_user = @organization.users.find parsed_response['id']
      expect(@new_user.registration_source).to eq 'web'
      expect(@new_user.registration_platform).to eq 'something'
    end
  end

  context 'fail cases' do
    describe 'with invalid email' do
      before(:all) do
        @post_json_invalid = {
          "firstname": "Karl",
          "lastname": "Lauterbach",
          "commercial_register_number": "ABCDEFG",
          "care_facility_name": "My Facility",
          "email": "invalidmail"
        }
      end

      before(:all) do
        post_json @endpoint, @post_json_invalid, nil
      end

      it 'should return status 422' do
        # expect(JSON.parse(response.body)['errors'][0]['message']) == 'email must_be_valid'
        expect(response.status).to eq 422
      end
    end

    describe 'try to set root as admin' do
      before(:all) do
        @post_json = {
          "firstname": "Olaf",
          "lastname": "Scholz",
          "email": Faker::Internet.email,
          "commercial_register_number": "ABCDEFG",
          "care_facility_name": "My Facility",
          "role": 'admin'
        }
        post_json @endpoint, @post_json, @admin_token
      end

      it 'should not set the status to root' do
        expect(response.status).to eq 422
      end
    end

    describe 'try to set root as role' do
      before(:all) do
        @post_json = {
          "firstname": "Olaf",
          "lastname": "Scholz",
          "email": Faker::Internet.email,
          "commercial_register_number": "ABCDEFG",
          "care_facility_name": "My Facility",
          "role": 'root'
        }
        post_json @endpoint, @post_json, @admin_token
      end

      it 'should not set the status to root' do
        expect(response.status).to eq 422
      end
    end

    describe 'with email already in the database' do
      before(:all) do
        @post_json_invalid = {
          "firstname": "Karl",
          "lastname": "Lauterbach",
          "commercial_register_number": "ABCDEFG",
          "care_facility_name": "My Facility",
          "email": User.last.email,
        }
      end

      before(:all) do
        post_json @endpoint, @post_json_invalid, nil
      end

      it 'should return status 422' do
        # expect(JSON.parse(response.body)['errors'][0]['message']) == 'email must_be_unique'
        expect(response.status).to eq 422
      end
    end

    describe 'with organization in wrong scope' do
      before(:all) do
        @organization = FactoryBot.create(:organization, scope: 'project')
        @endpoint = "/v1/public/users/register_with_facility/#{@organization.register_token}"
        @post_json = {
          "firstname": "Karl",
          "lastname": "Lauterbach",
          "commercial_register_number": "ABCDEFG",
          "care_facility_name": "My Facility",
          "email": Faker::Internet.email,
        }
      end

      before(:all) do
        post_json @endpoint, @post_json, nil
      end

      it 'should return status 422' do
        # expect(JSON.parse(response.body)['errors'][0]['message']) == 'email must_be_unique'
        expect(response.status).to eq 422
      end
    end
  end
end
