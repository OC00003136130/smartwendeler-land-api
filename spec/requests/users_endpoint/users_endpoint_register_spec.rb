require "rails_helper"

RSpec.describe "Users Endpoints - v1/users", type: :request do
  before(:all) do
    @organization = FactoryBot.create(:organization)
    @endpoint = "/v1/public/users/register/#{@organization.register_token}"
  end

  context 'register' do
    before(:all) do
      @post_json = {
        "firstname": "Karl",
        "lastname": "Lauterbach",
        "email": Faker::Internet.email,
      }
      post_json @endpoint, @post_json, nil
    end

    it 'should return status 201' do
      expect(response.status).to eq 201
    end

    it 'should have registered the user to the organization' do
      @new_user = @organization.users.find parsed_response['id']

      expect(@new_user.firstname).to eq @post_json[:firstname]
      expect(@new_user.lastname).to eq @post_json[:lastname]
      expect(@new_user.email).to eq @post_json[:email]
      expect(@new_user.status).to eq 'confirmed'
      expect(@new_user.role).to eq 'user'
    end

    it 'should have set the registration data' do
      @new_user = @organization.users.find parsed_response['id']
      expect(@new_user.registration_source).to eq 'web'
      expect(@new_user.registration_platform).to eq 'something'
    end
  end

  context 'fail cases' do
    describe 'with invalid email' do
      before(:all) do
        @post_json_invalid = {
          "firstname": "Karl",
          "lastname": "Lauterbach",
          "email": "invalidmail"
        }
      end

      before(:all) do
        post_json @endpoint, @post_json_invalid, nil
      end

      it 'should return status 422' do
        # expect(JSON.parse(response.body)['errors'][0]['message']) == 'email must_be_valid'
        expect(response.status).to eq 422
      end
    end

    describe 'try to set root as admin' do
      before(:all) do
        @post_json = {
          "firstname": "Olaf",
          "lastname": "Scholz",
          "email": Faker::Internet.email,
          "role": 'admin'
        }
        post_json @endpoint, @post_json, @admin_token
      end

      it 'should not set the status to root' do
        expect(response.status).to eq 422
      end
    end

    describe 'try to set root as role' do
      before(:all) do
        @post_json = {
          "firstname": "Olaf",
          "lastname": "Scholz",
          "email": Faker::Internet.email,
          "role": 'root'
        }
        post_json @endpoint, @post_json, @admin_token
      end

      it 'should not set the status to root' do
        expect(response.status).to eq 422
      end
    end

    describe 'with email already in the database' do
      before(:all) do
        @post_json_invalid = {
          "firstname": "Karl",
          "lastname": "Lauterbach",
          "email": User.last.email,
        }

        @endpoint = "/v1/public/users/register/#{User.last.organization.register_token}"
      end

      before(:all) do
        post_json @endpoint, @post_json_invalid, nil
      end

      it 'should return status 422' do
        # expect(JSON.parse(response.body)['errors'][0]['message']) == 'email must_be_unique'
        expect(response.status).to eq 422
      end
    end
  end
end
