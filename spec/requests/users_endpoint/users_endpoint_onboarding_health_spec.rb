require "rails_helper"

RSpec.describe "Users Endpoints - v1/users", type: :request do
  before(:all) do
    @organization = FactoryBot.create(:organization)
    @user = FactoryBot.create(:user, organization: @organization, onboarding_token: SecureRandom.uuid)
    @care_facility = FactoryBot.create(:care_facility, organization: @organization, user: @user)
    @endpoint = "/v1/public/users/onboarding-health/#{@user.onboarding_token}"
  end

  context 'register' do
    before(:all) do
      @post_json = {
        "email": Faker::Internet.email,
        "firstname": Faker::Name.first_name,
        "lastname": Faker::Name.last_name
      }
      post_json @endpoint, @post_json, nil
    end

    it 'should return status 200' do
      expect(response.status).to eq 200
    end

    it 'should have build a result' do
      expect(parsed_response["code"]).to eq "successfully.onboarded"
    end

    it 'should have reset the onboarding token' do
      @user.reload
      expect(@user.onboarding_token).to eq nil
    end
  end

  context 'fail cases' do
    describe 'with invalid email' do
      before(:all) do
        @post_json_invalid = {
          "email": "invalidmail",
          "firstname": Faker::Name.first_name,
          "lastname": Faker::Name.last_name
        }
      end

      before(:all) do
        post_json @endpoint, @post_json_invalid, nil
      end

      it 'should return status 422' do
        expect(response.status).to eq 422
      end
    end

    describe 'email exists' do
      before(:all) do
        FactoryBot.create(:user, organization: @organization, email: "john@doe.de", onboarding_token: nil)
        @post_json = {
          "email": "john@doe.de",
          "firstname": Faker::Name.first_name,
          "lastname": Faker::Name.last_name
        }
        post_json @endpoint, @post_json, nil
      end

      it 'should return status 422' do
        expect(response.status).to eq 422
      end
    end
  end
end
