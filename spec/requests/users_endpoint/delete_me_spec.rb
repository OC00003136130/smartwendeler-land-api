require "rails_helper"

RSpec.describe "User Endpoints - v1/users", type: :request do
  describe '#delete' do
    before(:all) do
      User.destroy_all
      @setup = setup_specs("v1", "users")
      @endpoint = "#{@setup[:endpoint]}/delete-me"
    end
    context 'when resource is found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          delete_json @endpoint, @put_json, @token
        end

        it 'should return status 204' do
          expect(response.status).to eq 204
        end

        it 'should have removed root user' do
          expect(User.where(id: @setup[:root].id).count).to eq 0
        end
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          delete_json @endpoint, @put_json, @token
        end

        it 'should return status 204' do
          expect(response.status).to eq 204
        end

        it 'should have removed admin user' do
          expect(User.where(id: @setup[:admin].id).count).to eq 0
        end
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          delete_json @endpoint, @put_json, @token
        end

        it 'should return status 204' do
          expect(response.status).to eq 204
        end

        it 'should have removed user' do
          expect(User.where(id: @setup[:user].id).count).to eq 0
        end
      end
    end
  end
end