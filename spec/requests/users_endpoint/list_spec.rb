require "rails_helper"

RSpec.describe "User Endpoints - v1/users", type: :request do
  describe '#list' do
    before(:all) do
      User.destroy_all
      @setup = setup_specs("v1", "users")
      @organization = @setup[:organization]
      @endpoint = @setup[:endpoint]
      @list = FactoryBot.create_list(:user, 10, organization: @organization)
      @list << @setup[:root]
      @list << @setup[:admin]
      @list << @setup[:user]
      @list << @setup[:facility_owner]
      @list << @setup[:care_facility_admin]
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @organization.users
        end
        include_examples "list", "allowed", 15
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @organization.users
        end
        include_examples "list", "allowed", 15
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @organization.users
        end
        include_examples "list", "forbidden"
        it_behaves_like 'list'
      end
    end
  end
end