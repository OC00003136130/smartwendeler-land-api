require "rails_helper"

RSpec.describe "GamificationGame Endpoints - v1/gamification_games", type: :request do
  describe '#list' do
    before(:all) do
      GamificationGame.destroy_all
      @setup = setup_specs("v1", "gamification_games")
      @endpoint = @setup[:endpoint]
      game1 = FactoryBot.create(:gamification_game, kind: 'poll', organization: @setup[:organization])
      game2 = FactoryBot.create(:gamification_game, kind: 'rating', organization: @setup[:organization])
      game3 = FactoryBot.create(:gamification_game, kind: 'comment', organization: @setup[:organization])
      game4 = FactoryBot.create(:gamification_game, kind: 'login', organization: @setup[:organization])

      @list = []
      @list << game1
      @list << game2
      @list << game3
      @list << game4
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @setup[:organization].gamification_games
        end
        include_examples "list", "allowed", 4
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @setup[:organization].gamification_games
        end
        include_examples "list", "allowed", 4
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @setup[:organization].gamification_games
        end
        include_examples "list", "allowed", 4
        it_behaves_like 'list'
      end
    end
  end
end