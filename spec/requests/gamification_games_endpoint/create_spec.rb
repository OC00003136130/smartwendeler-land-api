require "rails_helper"

RSpec.describe "GamificationGame Endpoints - v1/gamification_games", type: :request do
  describe '#create' do
    before(:all) do
      GamificationGame.destroy_all
      @setup = setup_specs("v1", "gamification_games")
      @endpoint = @setup[:endpoint]
      @resource = FactoryBot.build(:gamification_game)
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @post_json = {
            kind: 'poll',
            name: Faker::Company.name,
          }
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role admin' do
        before(:all) do
          @post_json = {
            kind: 'rating',
            name: Faker::Company.name,
          }
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "forbidden"
        it_behaves_like 'create'
      end
    end

    context 'when data is not valid' do
      describe 'name missing' do
        before(:all) do
          @post_json = {
            kind: 'comment'
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'gamificationgame.name.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end

      describe 'kind taken' do
        before(:all) do
          @post_json = {
            kind: 'poll',
            name: Faker::Company.name
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'gamificationgame.kind.taken'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
    end
  end
end