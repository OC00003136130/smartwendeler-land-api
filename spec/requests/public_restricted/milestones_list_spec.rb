require "rails_helper"

RSpec.describe "Public Restricted Milestones Endpoints - v1/public", type: :request do
  before(:all) do
    Project.destroy_all
    @setup = setup_specs_public("v1", "projects")
    @project = FactoryBot.create(:project, organization: @setup[:organization])
    @list = FactoryBot.create_list(:milestone, 10, project: @project)
 
    @endpoint = "#{@setup[:endpoint]}/#{@project.id}/milestones"
  end

  context 'when ressources are found' do
    before(:all) do
      @model = @project.milestones
    end

    include_examples "list", "allowed", 10, true
    it_behaves_like 'list'
  end

  context 'when ressources are not found' do
    context 'project not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/5453453532222/milestones"
        @model = @project.milestones
      end

      include_examples "list", "not_found", 0, true
      it_behaves_like 'list'
    end
  end
end
