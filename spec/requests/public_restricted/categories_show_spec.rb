require "rails_helper"

RSpec.describe "Public Restricted Category Endpoints - v1/public", type: :request do
  before(:all) do
    Category.destroy_all
    @setup = setup_specs_public("v1", "categories")
    @resource = FactoryBot.create(:category, organization: @setup[:organization])
  end

  context 'when ressources are found' do
    before(:all) do
      @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
    end

    include_examples "show", "allowed", true
    it_behaves_like "show"
  end

  context 'when resource is not found' do
    before(:all) do
      @endpoint = "#{@setup[:endpoint]}/39754493857349875"
    end

    include_examples "show", "not_found", true
    it_behaves_like 'show'
  end
end
