require "rails_helper"

RSpec.describe "Public Restricted Care Facilities Endpoints - v1/public", type: :request do
  before(:all) do
    CareFacility.destroy_all
    @setup = setup_specs_public("v1", "care_facilities")
    @endpoint = @setup[:endpoint]
    @list = FactoryBot.create_list(:care_facility, 10, organization: @setup[:organization])
  end

  context 'when ressources are found' do
    before(:all) do
      @model = @setup[:organization].care_facilities
    end

    include_examples "list", "allowed", 10, true
    it_behaves_like 'list'

    context 'with filter' do
      before(:all) do
        CareFacility.destroy_all

        @tag_category1 = FactoryBot.create(:tag_category, organization: @setup[:organization])
        @tag_category2 = FactoryBot.create(:tag_category, organization: @setup[:organization])

        @care_facility1 = FactoryBot.create(:care_facility, organization: @setup[:organization])
        @care_facility1.tag_categories << @tag_category1
        @care_facility1.tag_categories << @tag_category2

        @care_facility2 = FactoryBot.create(:care_facility, organization: @setup[:organization])
        @care_facility2.tag_categories << @tag_category1

        @care_facility3 = FactoryBot.create(:care_facility, organization: @setup[:organization])
        @care_facility3.tag_categories << @tag_category1
        @care_facility3.tag_categories << @tag_category2

        @list = []
        @list << @care_facility1
        @list << @care_facility3

        @endpoint = "#{@setup[:endpoint]}?filter_care_facility_tag_categories=#{@tag_category1.id},#{@tag_category2.id}"
        @model = @setup[:organization].care_facilities
      end

      include_examples "list", "allowed", 2, true
      it_behaves_like 'list'
    end
  end

  context 'with resources of users with is_active_on_health_scope falsey' do
    before(:all) do
      CareFacility.destroy_all
      @setup = setup_specs_public("v1", "care_facilities")
      @endpoint = @setup[:endpoint]

      @user1 = FactoryBot.create(:user, organization: @setup[:organization], is_active_on_health_scope: true)
      @user2 = FactoryBot.create(:user, organization: @setup[:organization], is_active_on_health_scope: false)
      @user3 = FactoryBot.create(:user, organization: @setup[:organization], is_active_on_health_scope: true)
      @user4 = FactoryBot.create(:user, organization: @setup[:organization], is_active_on_health_scope: false)
      @user5 = FactoryBot.create(:user, organization: @setup[:organization], is_active_on_health_scope: true)

      @resource1 = FactoryBot.create(:care_facility, organization: @setup[:organization], user: @user1)
      @resource2 = FactoryBot.create(:care_facility, organization: @setup[:organization], user: @user2)
      @resource3 = FactoryBot.create(:care_facility, organization: @setup[:organization], user: @user3)
      @resource4 = FactoryBot.create(:care_facility, organization: @setup[:organization], user: @user4)
      @resource5 = FactoryBot.create(:care_facility, organization: @setup[:organization], user: @user5)

      @list = []
      @list << @resource1
      @list << @resource2
      @list << @resource3
      @list << @resource4
      @list << @resource5

      @model = @setup[:organization].care_facilities
    end

    include_examples "list", "allowed", 3, true
    it_behaves_like 'list'
  end

  context 'when ressources are not found' do
    context 'list is in scope of another organization' do
      before(:all) do
        CareFacility.destroy_all
        @list = FactoryBot.create_list(:care_facility, 10, organization: @setup[:another_organization])
        @model = @setup[:another_organization].care_facilities
      end

      include_examples "list", "allowed", 0, true
      it_behaves_like 'list'
    end
  end
end
