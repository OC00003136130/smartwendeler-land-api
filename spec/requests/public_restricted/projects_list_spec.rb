require "rails_helper"

RSpec.describe "Public Restricted Projects Endpoints - v1/public", type: :request do
  before(:all) do
    Project.destroy_all
    @setup = setup_specs_public("v1", "projects")
    @endpoint = @setup[:endpoint]
    @list = FactoryBot.create_list(:project, 10, organization: @setup[:organization])
  end

  context 'when ressources are found' do
    before(:all) do
      @model = @setup[:organization].projects
    end

    include_examples "list", "allowed", 10, true
    it_behaves_like 'list'
  end

  context 'when ressources are not found' do
    context 'list is in scope of another organization' do
      before(:all) do
        Project.destroy_all
        @list = FactoryBot.create_list(:project, 10, organization: @setup[:another_organization])
        @model = @setup[:another_organization].projects
      end

      include_examples "list", "allowed", 0, true
      it_behaves_like 'list'
    end
  end
end
