require "rails_helper"

RSpec.describe "Public Restricted Pinboards Endpoints - v1/public", type: :request do
  before(:all) do
    Project.destroy_all
    Pinboard.destroy_all
    @setup = setup_specs_public("v1", "pinboards")
    @resource = FactoryBot.create(:pinboard, organization: @setup[:organization], is_active: true)
  end

  context 'when ressources are found' do
    before(:all) do
      @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
    end
    
    include_examples "show", "allowed", true
    it_behaves_like 'show'
  end

  context 'when ressources are not found' do
    context 'pinboard not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/424234234234"
      end

      include_examples "show", "not_found", true
      it_behaves_like 'show'
    end
  end
end
