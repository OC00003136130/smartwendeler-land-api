require "rails_helper"

RSpec.describe "Public Restricted Tooltips Endpoints - v1/public", type: :request do
  before(:all) do
    Tooltip.destroy_all
    @setup = setup_specs_public("v1", "tooltips")
    @list = FactoryBot.create_list(:tooltip, 10, organization: @setup[:organization])
 
    @endpoint = @setup[:endpoint]
  end

  before(:all) do
    @model = @setup[:organization].tooltips
  end

  include_examples "list", "allowed", 10, true
  it_behaves_like 'list'
end
