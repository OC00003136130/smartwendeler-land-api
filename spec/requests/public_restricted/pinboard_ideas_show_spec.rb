require "rails_helper"

RSpec.describe "Public Restricted Pinboards Endpoints - v1/public", type: :request do
  before(:all) do
    Project.destroy_all
    PinboardIdea.destroy_all
    @setup = setup_specs_public("v1", "pinboard_ideas")
    @pinboard = FactoryBot.create(:pinboard, organization: @setup[:organization], is_active: true)
    @resource = FactoryBot.create(:pinboard_idea, pinboard: @pinboard, status: 'confirmed')
    @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
  end

  context 'when ressource is found' do
    include_examples "show", "allowed", true
    it_behaves_like 'show'
  end

  context 'when status is not confirmed' do
    before(:all) do
      @resource = FactoryBot.create(:pinboard_idea, pinboard: @pinboard, status: 'is_checked')
      @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
    end

    include_examples "show", "not_found", true
    it_behaves_like 'show'
  end
end
