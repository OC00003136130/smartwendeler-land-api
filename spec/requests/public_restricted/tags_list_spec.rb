require "rails_helper"

RSpec.describe "Public Restricted Tags Endpoints - v1/public", type: :request do
  context 'all tags' do
    before(:all) do
      Tag.destroy_all
      @setup = setup_specs_public("v1", "tags")
      @list = FactoryBot.create_list(:tag, 10, organization: @setup[:organization])
  
      @endpoint = @setup[:endpoint]
      @model = @setup[:organization].tags
    end

    include_examples "list", "allowed", 10, true
    it_behaves_like 'list'
  end
  # currently fails on Circle Ci but passes locally:
  
  # context 'filtered by tag category' do
  #   before(:all) do
  #     Tag.destroy_all
  #     @setup = setup_specs_public("v1", "tags")
  #     @tag_category = FactoryBot.create(:tag_category, organization: @setup[:organization])
  #     @tags = FactoryBot.create_list(:tag, 10, organization: @setup[:organization])

  #     @list = FactoryBot.create_list(:tag, 7, organization: @setup[:organization], tag_category: @tag_category)

  #     @endpoint = "#{@setup[:endpoint]}?tag_category_id=#{@tag_category.id}"
  #     @model = @setup[:organization].tags
  #   end

  #   include_examples "list", "allowed", 7, true
  #   it_behaves_like 'list'
  # end
end
