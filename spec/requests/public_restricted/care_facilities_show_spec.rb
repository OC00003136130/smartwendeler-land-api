require "rails_helper"

RSpec.describe "Public Restricted CareFacilities Endpoints - v1/public", type: :request do
  before(:all) do
    CareFacility.destroy_all
    @setup = setup_specs_public("v1", "care_facilities")
    @resource = FactoryBot.create(:care_facility, organization: @setup[:organization], slug: 'abc-123')
  end

  context 'when ressources are found' do
    before(:all) do
      @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
    end

    include_examples "show", "allowed", true
    it_behaves_like "show"
  end

  context 'find by slug' do
    before(:all) do
      @endpoint = "#{@setup[:endpoint]}/#{@resource.slug}"
    end

    include_examples "show", "allowed", true
    it_behaves_like "show"
  end

  context 'with resources of users with is_active_on_health_scope falsey' do
    before(:all) do
      @user = FactoryBot.create(:user, is_active_on_health_scope: false, organization: @setup[:organization])
      @resource = FactoryBot.create(:care_facility, organization: @setup[:organization], user: @user)
      @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
    end

    include_examples "show", "not_found", true
    it_behaves_like "show"
  end

  context 'when resource is not found' do
    before(:all) do
      @endpoint = "#{@setup[:endpoint]}/39754493857349875"
    end

    include_examples "show", "not_found", true
    it_behaves_like 'show'
  end
end
