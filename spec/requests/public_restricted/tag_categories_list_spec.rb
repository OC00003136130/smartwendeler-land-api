require "rails_helper"

RSpec.describe "Public Restricted Tag Categories Endpoints - v1/public", type: :request do
  before(:all) do
    TagCategory.destroy_all
    @setup = setup_specs_public("v1", "tag_categories")
    @list = FactoryBot.create_list(:tag_category, 10, organization: @setup[:organization])
  end
  
  context 'root tag categories' do
    before(:all) do
      @endpoint = @setup[:endpoint]
    end

    before(:all) do
      @model = @setup[:organization].tag_categories
    end

    include_examples "list", "allowed", 10, true
    it_behaves_like 'list'
  end

  context 'sub tag categories' do
    before(:all) do
      TagCategory.destroy_all
      @setup = setup_specs_public("v1", "tag_categories")
      @endpoint = "#{@setup[:endpoint]}?parent_id=#{@list.first.id}"
      @list = FactoryBot.create_list(:tag_category, 5, organization: @setup[:organization], parent_id: @list.first.id)
    end

    before(:all) do
      @model = @setup[:organization].tag_categories
    end

    include_examples "list", "allowed", 5, true
    it_behaves_like 'list'
  end
end
