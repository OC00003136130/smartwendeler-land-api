require "rails_helper"

RSpec.describe "Public Restricted Pinboards Endpoints - v1/public", type: :request do
  before(:all) do
    Project.destroy_all
    PinboardIdea.destroy_all
    @setup = setup_specs_public("v1", "pinboard_ideas")
    @pinboard = FactoryBot.create(:pinboard, organization: @setup[:organization], is_active: true)
    @endpoint = "#{@setup[:endpoint]}/pinboard/#{@pinboard.id}"
    @list = FactoryBot.create_list(:pinboard_idea, 10, pinboard: @pinboard, status: 'confirmed')
  end

  context 'when ressource is found' do
    before(:all) do
      @model = @pinboard.pinboard_ideas
    end

    include_examples "list", "allowed", 10, true
    it_behaves_like 'list'
  end

  context 'when ressource is not found' do
    context 'list is in scope of another pinboard' do
      before(:all) do
        Project.destroy_all
        PinboardIdea.destroy_all
        @another_pinboard = FactoryBot.create(:pinboard, organization: @setup[:organization], is_active: true)
        @list = FactoryBot.create_list(:pinboard_idea, 10, pinboard: @another_pinboard)
        @model = @pinboard.pinboard_ideas
      end

      include_examples "list", "allowed", 0, true
      it_behaves_like 'list'
    end
  end
end
