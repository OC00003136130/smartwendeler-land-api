require "rails_helper"

RSpec.describe "Public Restricted Pinboards Endpoints - v1/public", type: :request do
  before(:all) do
    Project.destroy_all
    Pinboard.destroy_all
    @setup = setup_specs_public("v1", "pinboards")
    @endpoint = @setup[:endpoint]
    @list = FactoryBot.create_list(:pinboard, 10, organization: @setup[:organization], is_active: true, start_time: Time.now - 1.day)
  end

  context 'when ressources are found' do
    before(:all) do
      @model = @setup[:organization].pinboards
    end

    include_examples "list", "allowed", 10, true
    it_behaves_like 'list'
  end

  context 'when ressources are not found' do
    context 'list is in scope of another organization' do
      before(:all) do
        Project.destroy_all
        Pinboard.destroy_all
        @list = FactoryBot.create_list(:project, 10, organization: @setup[:another_organization])
        @model = @setup[:another_organization].pinboards
      end

      include_examples "list", "allowed", 0, true
      it_behaves_like 'list'
    end
  end
end
