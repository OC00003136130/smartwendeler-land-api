require "rails_helper"

RSpec.describe "Message Endpoints - v1/messages", type: :request do
  describe '#create' do
    before(:all) do
      Message.destroy_all
      @setup = setup_specs_public("v1", "messages")
      @resource = FactoryBot.build(:message)

      @endpoint = @setup[:endpoint]

      @project = FactoryBot.create(:project, organization: @setup[:organization])
      @user = FactoryBot.create(:user, organization: @setup[:organization])

      @post_json = {
        firstname: @resource.firstname,
        lastname: @resource.lastname,
        email: @resource.email,
        subject: @resource.subject,
        message: @resource.message,
        project_id: @project.id,
        user_id: @user.id
      }
    end
    context 'when data is valid' do
      include_examples "create", "allowed", true
      it_behaves_like 'create'
    end

    context 'when data is not valid' do
      context 'subject missing' do
        before(:all) do
          @post_json = {
            firstname: @resource.firstname,
            lastname: @resource.lastname,
            email: @resource.email,
            message: @resource.message,
            project_id: @project.id,
            user_id: @user.id
          }
          @expected_error_code = 'message.subject.invalid'
        end
        include_examples "create", "not_valid", true
        it_behaves_like 'create'
      end

      context 'message missing' do
        before(:all) do
          @post_json = {
            firstname: @resource.firstname,
            lastname: @resource.lastname,
            email: @resource.email,
            project_id: @project.id,
            user_id: @user.id,
            subject: @resource.subject,
          }
          @expected_error_code = 'message.message.invalid'
        end
        include_examples "create", "not_valid", true
        it_behaves_like 'create'
      end

      context 'email missing' do
        before(:all) do
          @post_json = {
            firstname: @resource.firstname,
            lastname: @resource.lastname,
            project_id: @project.id,
            user_id: @user.id,
            subject: @resource.subject,
            message: @resource.message
          }
          @expected_error_code = 'message.email.invalid'
        end
        include_examples "create", "not_valid", true
        it_behaves_like 'create'
      end
    end
  end
end