require "rails_helper"

RSpec.describe "Public Restricted Categories Endpoints - v1/public", type: :request do
  before(:all) do
    Project.destroy_all
    Category.destroy_all
    @setup = setup_specs_public("v1", "categories")
    @category = FactoryBot.create(:category, organization: @setup[:organization])
    @organization = @setup[:organization]
    @endpoint = "#{@setup[:endpoint]}/#{@category.id}/sub_categories"
    @list = FactoryBot.create_list(:sub_category, 10, organization: @setup[:organization], category: @category)
  end

  context 'when ressources are found' do
    before(:all) do
      @model = @category.sub_categories
    end

    include_examples "list", "allowed", 10, true
    it_behaves_like 'list'
  end

  context 'when ressources are not found' do
    context 'list is in scope of another organization' do
      before(:all) do
        Project.destroy_all
        Category.destroy_all
        @setup = setup_specs_public("v1", "categories")
        @category = FactoryBot.create(:category)
        @endpoint = "#{@setup[:endpoint]}/#{@category.id}/sub_categories"
        @list = FactoryBot.create_list(:sub_category, 10)
        @model = @category.sub_categories
      end

      include_examples "list", "not_found", 0, true
      it_behaves_like 'list'
    end
  end
end
