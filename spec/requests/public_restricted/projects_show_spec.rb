require "rails_helper"

RSpec.describe "Public Restricted Projects Endpoints - v1/public", type: :request do
  before(:all) do
    Project.destroy_all
    @setup = setup_specs_public("v1", "projects")
    @resource = FactoryBot.create(:project, organization: @setup[:organization], slug: "huhu-haha-12")
  end

  context 'when ressources are found' do
    before(:all) do
      @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
    end

    include_examples "show", "allowed", true
    it_behaves_like "show"
  end

  context 'show by slug' do
    before(:all) do
      @endpoint = "#{@setup[:endpoint]}/#{@resource.slug}"
    end

    include_examples "show", "allowed", true
    it_behaves_like "show"
  end

  context 'when resource is not found' do
    before(:all) do
      @endpoint = "#{@setup[:endpoint]}/39754493857349875"
    end

    include_examples "show", "not_found", true
    it_behaves_like 'show'
  end
end
