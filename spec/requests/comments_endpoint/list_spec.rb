require "rails_helper"

RSpec.describe "Comment Endpoints - v1/comments", type: :request do
  describe '#list' do
    before(:all) do
      Comment.destroy_all
      @setup = setup_specs("v1", "comments")
      @project = FactoryBot.create(:project, organization: @setup[:organization])
      @list = FactoryBot.create_list(:comment, 10, organization: @setup[:organization], project: @project)

      # create some replies
      @comment_reply1 = FactoryBot.create(:comment, organization: @setup[:organization], parent_id: @list.last.id, project: @project)
      @comment_reply2 = FactoryBot.create(:comment, organization: @setup[:organization], parent_id: @list.first.id, project: @project)

      @endpoint = "#{@setup[:endpoint]}/project/#{@project.id}"
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @project.comments
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @project.comments
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @project.comments
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end
    end

    context 'when ressources are not found' do
      context 'project not found' do
        before(:all) do
          @token = @setup[:user_token]
          @endpoint = "#{@setup[:endpoint]}/project/394792387489237498324"
        end
        include_examples "list", "not_found"
        it_behaves_like 'list'
      end
    end
  end
end