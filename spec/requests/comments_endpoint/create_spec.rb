require "rails_helper"

RSpec.describe "Comment Endpoints - v1/comments", type: :request do
  describe '#create' do
    before(:all) do
      Log.destroy_all
      Comment.destroy_all
      
      @setup = setup_specs("v1", "comments")
      @resource = FactoryBot.build(:comment)

      @project = FactoryBot.create(:project, organization: @setup[:organization])
      @project_in_different_organization = FactoryBot.create(:project, organization: @setup[:another_organization])

      @endpoint = "#{@setup[:endpoint]}/project/#{@project.id}"
      @user = @setup[:user]
      @post_json = {
        comment: @resource.comment,
        project_id: @project.id
      }
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      describe 'comment game' do
        before(:all) do
          @count = @user.gamification_trophies.count
          @game = FactoryBot.create(:gamification_game, kind: 'comment', organization: @setup[:organization])
          @reward = FactoryBot.create(:gamification_reward, gamification_game: @game, threshold: 1)
          post_json @endpoint, @post_json, @setup[:user_token]
        end
  
        it 'should create a trophy for the user' do
          @user.gamification_trophies.reload
          expect(@user.gamification_trophies.count).to eq @count + 1
        end
      end

      describe 'create a comment reply' do
        before(:all) do
          @resource = FactoryBot.build(:comment, user: @setup[:user])
          @comment_to_reply_to = FactoryBot.create(:comment, user: @setup[:user], parent_id: nil)
          @post_json = {
            comment: @resource.comment,
            project_id: @project.id,
            parent_id: @comment_to_reply_to.id
          }
        end
        context 'for role admin' do
          before(:all) do
            @token = @setup[:admin_token]
          end

          include_examples "create", "allowed"
          it_behaves_like 'create'

          it 'should have created a comment reply' do
            expect(@comment_to_reply_to.replies_count).to eq 1
          end

          it 'should have created a log' do
            resource = parsed_response["resource"]
            id = resource['id']
            expect(Log.where(log_code: 'comment.replied').count).to eq 1
          end
        end

        context 'for role user' do
          before(:all) do
            @token = @setup[:user_token]
          end

          include_examples "create", "allowed"
          it_behaves_like 'create'
        end
      end
    end

    context 'when data is not valid' do
      context 'project not found' do
        before(:all) do
          @post_json = {
            comment: @resource.comment
          }
          @token = @setup[:user_token]
          @endpoint = "#{@setup[:endpoint]}/project/394792387489237498324"
        end
        include_examples "create", "not_found"
        it_behaves_like 'create'
      end

      context 'comment missing' do
        before(:all) do
          @post_json = {
            project_id: @project.id
          }
          @token = @setup[:user_token]
          @expected_error_code = 'comment.comment.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end

      context 'project in different organization' do
        before(:all) do
          @post_json = {
            comment: @resource.comment
          }
          @token = @setup[:user_token]
          @endpoint = "#{@setup[:endpoint]}/project/#{@project_in_different_organization.id}"
        end
        include_examples "create", "not_found"
        it_behaves_like 'create'
      end

      context 'user is blocked for commenting' do
        before(:all) do
          Comment.destroy_all
          @setup[:user].can_comment = false
          @setup[:user].save!
          @post_json = {
            comment: @resource.comment,
            project_id: @project.id
          }
          @token = @setup[:user_token]
          @endpoint = "#{@setup[:endpoint]}/project/#{@project.id}"
          @expected_error_code = 'comment.blocked'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
    end
  end
end