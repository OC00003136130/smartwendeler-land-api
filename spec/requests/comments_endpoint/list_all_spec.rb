require "rails_helper"

RSpec.describe "Comment Endpoints - v1/comments", type: :request do
  describe '#list' do
    before(:all) do
      Comment.destroy_all
      @setup = setup_specs("v1", "comments")
      @project = FactoryBot.create(:project, organization: @setup[:organization])
      @list = FactoryBot.create_list(:comment, 10, organization: @setup[:organization], project: @project)

      # create some replies
      @comment_reply1 = FactoryBot.create(:comment, organization: @setup[:organization], parent_id: @list.last.id, project: @project)
      @comment_reply2 = FactoryBot.create(:comment, organization: @setup[:organization], parent_id: @list.first.id, project: @project)

      @list << @comment_reply1
      @list << @comment_reply2
      @endpoint = @setup[:endpoint]
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @setup[:organization].comments
        end
        include_examples "list", "allowed", 12
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @setup[:organization].comments
        end
        include_examples "list", "allowed", 12
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @setup[:organization].comments
        end
        include_examples "list", "not_allowed"
        it_behaves_like 'list'
      end
    end
  end
end