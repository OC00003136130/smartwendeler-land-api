require "rails_helper"

RSpec.describe "Comment Endpoints - v1/comments", type: :request do
  describe '#replies' do
    before(:all) do
      Comment.destroy_all
      @setup = setup_specs("v1", "comments")
      @comment = FactoryBot.create(:comment, organization: @setup[:organization])
      @list = FactoryBot.create_list(:comment, 10, organization: @setup[:organization], parent_id: @comment.id)

      @endpoint = "#{@setup[:endpoint]}/#{@comment.id}/replies"
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @comment.replies
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @comment.replies
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @comment.replies
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end
    end

    context 'when ressources are not found' do
      context 'project not found' do
        before(:all) do
          @token = @setup[:user_token]
          @endpoint = "#{@setup[:endpoint]}/394792387489237498324/replies"
        end
        include_examples "list", "not_found"
        it_behaves_like 'list'
      end
    end
  end
end