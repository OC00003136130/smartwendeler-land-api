require "rails_helper"

RSpec.describe "Comment Endpoints - v1/comments", type: :request do
  describe '#remove_vote' do
    before(:all) do
      Comment.destroy_all
      @setup = setup_specs("v1", "comments")
      @resource = FactoryBot.create(:comment, organization: @setup[:organization])

      @comment_rating = FactoryBot.create(:comment_rating, comment: @resource, user: @setup[:user])
    end
    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}/remove_vote"
        @token = @setup[:user_token]
        delete_json @endpoint, {}, @token
      end

      it 'should return status 204' do
        expect(response.status).to eq 204
      end

      it 'should have removed the comment rating' do
        expect(CommentRating.exists? @comment_rating.id).to be_falsey
      end
    end
  end
end