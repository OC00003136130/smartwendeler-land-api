require "rails_helper"

RSpec.describe "Comment Endpoints - v1/comments", type: :request do
  describe '#delete' do
    before(:all) do
      Comment.destroy_all
      @setup = setup_specs("v1", "comments")
    end
    context 'when resource is found' do
      context 'for role root' do
        before(:all) do
          @resource = FactoryBot.create(:comment, organization: @setup[:organization])
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:root_token]
        end
        include_examples "delete", Comment, "allowed"
        it_behaves_like 'delete'
      end

      context 'for role admin' do
        before(:all) do
          @resource = FactoryBot.create(:comment, organization: @setup[:organization])
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:admin_token]
        end
        include_examples "delete", Comment, "allowed"
        it_behaves_like 'delete'
      end

      context 'for role user' do
        before(:all) do
          @resource = FactoryBot.create(:comment, organization: @setup[:organization], user: @setup[:user])
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:user_token]
        end
        include_examples "delete", Comment, "allowed"
        it_behaves_like 'delete'
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "delete", Comment, "not_found"
        it_behaves_like 'delete'
      end
    end

    context 'when comment does not belong to user' do
      before(:all) do
        @resource = FactoryBot.create(:comment, organization: @setup[:organization], user: @setup[:admin])
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
        @token = @setup[:user_token]
      end
      include_examples "delete", Comment, "not_found"
      it_behaves_like 'delete'
    end
  end
end