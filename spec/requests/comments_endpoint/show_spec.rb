require "rails_helper"

RSpec.describe "Comment Endpoints - v1/comments", type: :request do
  describe '#show' do
    before(:all) do
      Comment.destroy_all
      @setup = setup_specs("v1", "comments")
      @resource = FactoryBot.create(:comment, organization: @setup[:organization], user: @setup[:user])
    end
    context 'when resource is found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "show", "allowed"
        it_behaves_like 'show'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "show", "allowed"
        it_behaves_like 'show'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "show", "allowed"
        it_behaves_like 'show'
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "show", "not_found"
        it_behaves_like 'show'
      end
    end
  end
end