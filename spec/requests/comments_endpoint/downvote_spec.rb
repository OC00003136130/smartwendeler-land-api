require "rails_helper"

RSpec.describe "Comment Endpoints - v1/comments", type: :request do
  describe '#downvote' do
    before(:all) do
      Comment.destroy_all
      @setup = setup_specs("v1", "comments")
      @resource = FactoryBot.create(:comment, organization: @setup[:organization])
    end
    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}/downvote"
        @token = @setup[:user_token]
        put_json @endpoint, {}, @token
      end

      it 'should return status 200' do
        expect(response.status).to eq 200
      end

      it 'should have downvoted the comment' do
        @resource.reload
        comment_rating = @resource.comment_ratings.find_by(
          user_id: @setup[:user],
          comment_id: @resource.id
        )
        expect(comment_rating.score).to eq -1
      end
    end
  end
end