require "rails_helper"

RSpec.describe "Comment Endpoints - v1/comments", type: :request do
  describe '#upvote' do
    before(:all) do
      Comment.destroy_all
      @setup = setup_specs("v1", "comments")
      @resource = FactoryBot.create(:comment, organization: @setup[:organization])
      @parallelcomment = FactoryBot.create(:comment, organization: @setup[:organization])
    end

    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}/upvote"
        @token = @setup[:user_token]
        put_json @endpoint, {}, @token
      end

      it 'should return status 200' do
        expect(response.status).to eq 200
      end

      it 'should have upvoted the comment' do
        @resource.reload
        comment_rating = @resource.comment_ratings.find_by(
          user_id: @setup[:user],
          comment_id: @resource.id
        )
        expect(comment_rating.score).to eq 1
      end

      it 'should have created a log' do
        resource = parsed_response["resource"]
        id = resource['id']
        expect(Log.where(log_code: 'comment.liked').count).to eq 1
      end
    end

    context 'multiple parallel upvotes' do

      upvotecount = 10

      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@parallelcomment.id}/upvote"
        threads = []
        upvotecount.times do
          threads << Thread.new do
            paralleluser = setup_user(@setup[:organization])
            paralleltoken = login(paralleluser.email, paralleluser.password)
            put_json @endpoint, {}, paralleltoken
          end
        end

        threads.map(&:join)
      end

      it 'should have correct score' do
        @parallelcomment.reload
        expect(@parallelcomment.score).to eq upvotecount
      end
    end
  end
end