require "rails_helper"

RSpec.describe "GamificationTrophy Endpoints - v1/gamification_trophies", type: :request do
  describe '#list' do
    before(:all) do
      GamificationTrophy.destroy_all
      @setup = setup_specs("v1", "gamification_trophies")
      @game = FactoryBot.create(:gamification_game, organization: @setup[:organization])
      @reward1 = FactoryBot.create(:gamification_reward, gamification_game: @game)
      @reward2 = FactoryBot.create(:gamification_reward, gamification_game: @game)
      @reward3 = FactoryBot.create(:gamification_reward, gamification_game: @game)
      @reward4 = FactoryBot.create(:gamification_reward, gamification_game: @game)
      @reward5 = FactoryBot.create(:gamification_reward, gamification_game: @game)

      @endpoint = @setup[:endpoint]
      @trophy1 = FactoryBot.create(:gamification_trophy, gamification_reward: @reward1, user: @setup[:user])
      @trophy2 = FactoryBot.create(:gamification_trophy, gamification_reward: @reward2, user: @setup[:user])
      @trophy3 = FactoryBot.create(:gamification_trophy, gamification_reward: @reward3, user: @setup[:user])
      @trophy4 = FactoryBot.create(:gamification_trophy, gamification_reward: @reward4, user: @setup[:admin])
      @trophy5 = FactoryBot.create(:gamification_trophy, gamification_reward: @reward5, user: @setup[:admin])
      @list = []
    end
    context 'when ressources are found' do
      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @list << @trophy4
          @list << @trophy5
          @model = @setup[:admin].gamification_trophies
        end
        include_examples "list", "allowed", 2
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @list << @trophy1
          @list << @trophy2
          @list << @trophy3
          @model = @setup[:user].gamification_trophies
        end
        include_examples "list", "allowed", 3
        it_behaves_like 'list'
      end
    end
  end
end