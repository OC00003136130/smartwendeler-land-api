require "rails_helper"

RSpec.describe "Pinboard Endpoints - v1/pinboards", type: :request do
  describe '#delete' do
    before(:all) do
      Project.destroy_all
      Pinboard.destroy_all
      @setup = setup_specs("v1", "pinboards")
    end
    context 'when resource is found' do
      context 'for role root' do
        before(:all) do
          @resource = FactoryBot.create(:pinboard, organization: @setup[:organization])
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:root_token]
        end
        include_examples "delete", Pinboard, "allowed"
        it_behaves_like 'delete'
      end

      context 'for role admin' do
        before(:all) do
          @resource = FactoryBot.create(:pinboard, organization: @setup[:organization])
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:admin_token]
        end
        include_examples "delete", Pinboard, "allowed"
        it_behaves_like 'delete'
      end

      context 'for role user' do
        before(:all) do
          @resource = FactoryBot.create(:pinboard, organization: @setup[:organization])
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:user_token]
        end
        include_examples "delete", Pinboard, "forbidden"
        it_behaves_like 'delete'
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "delete", Pinboard, "not_found"
        it_behaves_like 'delete'
      end
    end
  end
end