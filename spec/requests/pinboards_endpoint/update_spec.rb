require "rails_helper"

RSpec.describe "Pinboard Endpoints - v1/pinboards", type: :request do
  describe '#update' do
    before(:all) do
      Project.destroy_all
      Pinboard.destroy_all
      @setup = setup_specs("v1", "pinboards")
      @resource = FactoryBot.create(:pinboard, organization: @setup[:organization])
      @resource_updated = FactoryBot.build(:pinboard, organization: @setup[:organization])

      @put_json = {
        headline: @resource_updated.headline,
      }
    end
    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "update", "forbidden"
        it_behaves_like 'update'
      end

      describe 'assign/remove categories' do
        before(:all) do
          @category1 = FactoryBot.create(:category, organization: @setup[:organization])
          @category2 = FactoryBot.create(:category, organization: @setup[:organization])
          @category3 = FactoryBot.create(:category, organization: @setup[:organization])
          @category4 = FactoryBot.create(:category, organization: @setup[:organization])

          @resource = FactoryBot.create(:pinboard, organization: @setup[:organization])
          @resource.categories << @category1
          @resource.categories << @category2
          @resource.categories << @category3
          @resource.categories << @category4

          @put_json = {
            category_ids: [@category2.id, @category3.id]     
          }

          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:admin_token]
        end

        include_examples "update", "allowed"
        
        it 'should assign/remove categories' do
          @resource.reload
          expect(@resource.categories).not_to include @category1
          expect(@resource.categories).to include @category2
          expect(@resource.categories).to include @category3
          expect(@resource.categories).not_to include @category4
        end
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "not_found"
        it_behaves_like 'update'
      end
    end
  end
end