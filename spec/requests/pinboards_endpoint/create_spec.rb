require "rails_helper"

RSpec.describe "Pinboard Endpoints - v1/pinboards", type: :request do
  describe '#create' do
    before(:all) do
      Project.destroy_all
      Pinboard.destroy_all
      @setup = setup_specs("v1", "pinboards")
      @endpoint = @setup[:endpoint]
      @resource = FactoryBot.build(:pinboard)
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @post_json = {
            headline: @resource.headline,
            content: @resource.content
          }
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role admin' do
        before(:all) do
          @post_json = {
            headline: @resource.headline,
            content: @resource.content
          }
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'

        it 'should have put the menu_order after the next entry' do
          expect(@setup[:organization].pinboards.order(:menu_order).last.menu_order).to eq 1
        end
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "forbidden"
        it_behaves_like 'create'
      end

      describe 'assign categories' do
        before(:all) do
          @category1 = FactoryBot.create(:category, organization: @setup[:organization])
          @category2 = FactoryBot.create(:category, organization: @setup[:organization])
          @category3 = FactoryBot.create(:category, organization: @setup[:organization])
          @category4 = FactoryBot.create(:category, organization: @setup[:organization])

          @post_json = {
            headline: @resource.headline,
            content: @resource.content,
            category_ids: [@category1.id, @category2.id, @category3.id, @category4.id]     
          }
          @token = @setup[:admin_token]
        end

        include_examples "create", "allowed"
        it 'should assign categories' do
          resource = Pinboard.find parsed_response["resource"]["id"]
          
          expect(resource.categories).to include @category1
          expect(resource.categories).to include @category2
          expect(resource.categories).to include @category3
          expect(resource.categories).to include @category4
        end
      end
    end

    context 'when data is not valid' do
      describe 'headline missing' do
        before(:all) do
          @post_json = {
            content: @resource.content
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'pinboard.headline.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end

      describe 'content missing' do
        before(:all) do
          @post_json = {
            headline: @resource.headline
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'pinboard.content.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
    end
  end
end