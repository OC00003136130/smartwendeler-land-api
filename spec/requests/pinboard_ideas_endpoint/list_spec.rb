require "rails_helper"

RSpec.describe "PinboardIdea Endpoints - v1/pinboard_ideas", type: :request do
  describe '#list' do
    before(:all) do
      Project.destroy_all
      PinboardIdea.destroy_all
      @setup = setup_specs("v1", "pinboard_ideas")
      @pinboard = FactoryBot.create(:pinboard, organization: @setup[:organization])
      @endpoint = "#{@setup[:endpoint]}/pinboard/#{@pinboard.id}"
      @list = FactoryBot.create_list(:pinboard_idea, 10, pinboard: @pinboard, status: 'confirmed')
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @pinboard.pinboard_ideas
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @pinboard.pinboard_ideas
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @pinboard.pinboard_ideas
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end
    end
  end
end