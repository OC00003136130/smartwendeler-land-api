require "rails_helper"

RSpec.describe "PinboardIdea Endpoints - v1/pinboard_ideas", type: :request do
  describe '#create' do
    before(:all) do
      Project.destroy_all
      PinboardIdea.destroy_all
      @setup = setup_specs("v1", "pinboard_ideas")
      @pinboard = FactoryBot.create(:pinboard, organization: @setup[:organization])
      @endpoint = "#{@setup[:endpoint]}/pinboard/#{@pinboard.id}"
      @resource = FactoryBot.build(:pinboard_idea)
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @post_json = {
            headline: @resource.headline,
            content: @resource.content
          }
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role admin' do
        before(:all) do
          @post_json = {
            headline: @resource.headline,
            content: @resource.content
          }
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'

        it 'should have put the menu_order after the next entry' do
          expect(@pinboard.pinboard_ideas.order(:menu_order).last.menu_order).to eq 1
        end
      end

      context 'for role user' do
        before(:all) do
          @post_json = {
            headline: @resource.headline,
            content: @resource.content
          }
          @token = @setup[:user_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end
    end

    context 'when data is not valid' do
      describe 'invalid pinboard id' do
        before(:all) do
          @post_json = {
            content: @resource.content,
            headline: @resource.headline
          }
          @token = @setup[:admin_token]
          @endpoint = "#{@setup[:endpoint]}/pinboard/3435345345345345"
        end
        include_examples "create", "not_found"
        it_behaves_like 'create'
      end

      describe 'headline missing' do
        before(:all) do
          @post_json = {
            content: @resource.content
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'pinboardidea.headline.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end

      describe 'content missing' do
        before(:all) do
          @post_json = {
            headline: @resource.headline
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'pinboardidea.content.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
    end
  end
end