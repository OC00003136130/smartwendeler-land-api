require "rails_helper"

RSpec.describe "PinboardIdea Endpoints - v1/pinboard_ideas", type: :request do
  describe '#upvote' do
    before(:all) do
      PinboardIdea.destroy_all
      @setup = setup_specs("v1", "pinboard_ideas")
      @pinboard = FactoryBot.create(:pinboard, organization: @setup[:organization])
      @resource = FactoryBot.create(:pinboard_idea, pinboard: @pinboard)
      @user = @setup[:user]
    end
    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}/upvote"
        @token = @setup[:user_token]
        put_json @endpoint, {}, @token
      end

      it 'should return status 200' do
        expect(response.status).to eq 200
      end

      it 'should have upvoted the pinboard_idea' do
        @pinboard.reload
        pinboard_idea_rating = @resource.pinboard_idea_ratings.find_by(
          user_id: @setup[:user],
          pinboard_idea_id: @resource.id
        )
        expect(pinboard_idea_rating.score).to eq 1
      end

      # describe 'rating game' do
      #   before(:all) do
      #     @count = @user.gamification_trophies.count
      #     @game = FactoryBot.create(:gamification_game, kind: 'rating', organization: @setup[:organization])
      #     @reward = FactoryBot.create(:gamification_reward, gamification_game: @game, threshold: 1)
      #     put_json "#{@setup[:endpoint]}/#{@resource.id}/upvote", {}, @setup[:user_token]
      #   end
  
      #   it 'should create a trophy for the user' do
      #     @user.gamification_trophies.reload
      #     expect(@user.gamification_trophies.count).to eq @count + 1
      #   end
      # end
    end
  end
end