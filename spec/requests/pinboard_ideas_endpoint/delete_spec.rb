require "rails_helper"

RSpec.describe "PinboardIdea Endpoints - v1/pinboard_ideas", type: :request do
  describe '#delete' do
    before(:all) do
      Project.destroy_all
      PinboardIdea.destroy_all
      @setup = setup_specs("v1", "pinboard_ideas")
      @pinboard = FactoryBot.create(:pinboard, organization: @setup[:organization])
    end
    context 'when resource is found' do
      context 'for role root' do
        before(:all) do
          @resource = FactoryBot.create(:pinboard_idea, pinboard: @pinboard)
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:root_token]
        end
        include_examples "delete", PinboardIdea, "allowed"
        it_behaves_like 'delete'
      end

      context 'for role admin' do
        before(:all) do
          @resource = FactoryBot.create(:pinboard_idea, pinboard: @pinboard)
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:admin_token]
        end
        include_examples "delete", PinboardIdea, "allowed"
        it_behaves_like 'delete'
      end

      context 'for role user' do
        describe 'owned by user' do
          before(:all) do
            @resource = FactoryBot.create(:pinboard_idea, pinboard: @pinboard, user: @setup[:user])
            @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
            @token = @setup[:user_token]
          end
          include_examples "delete", PinboardIdea, "allowed"
          it_behaves_like 'delete'
        end

        describe 'not owned by user' do
          before(:all) do
            @resource = FactoryBot.create(:pinboard_idea, pinboard: @pinboard, user: @setup[:admin])
            @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
            @token = @setup[:user_token]
          end
          include_examples "delete", PinboardIdea, "not_found"
          it_behaves_like 'delete'
        end
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "delete", PinboardIdea, "not_found"
        it_behaves_like 'delete'
      end
    end
  end
end