require "rails_helper"

RSpec.describe "PinboardIdea Endpoints - v1/pinboard_ideas", type: :request do
  describe '#update' do
    before(:all) do
      Project.destroy_all
      PinboardIdea.destroy_all
      @setup = setup_specs("v1", "pinboard_ideas")
      @resource_updated = FactoryBot.build(:pinboard_idea)

      @put_json = {
        headline: @resource_updated.headline,
      }
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @pinboard = FactoryBot.create(:pinboard, organization: @setup[:organization])
          @resource = FactoryBot.create(:pinboard_idea, pinboard: @pinboard)
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:root_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'
      end

      context 'for role admin' do
        before(:all) do
          @pinboard = FactoryBot.create(:pinboard, organization: @setup[:organization])
          @resource = FactoryBot.create(:pinboard_idea, pinboard: @pinboard)
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:admin_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'
      end

      context 'for role user' do
        describe 'owned by user' do
          before(:all) do
            @pinboard = FactoryBot.create(:pinboard, organization: @setup[:organization])
            @resource = FactoryBot.create(:pinboard_idea, pinboard: @pinboard, user: @setup[:user])
            @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
            @token = @setup[:user_token]
          end
          include_examples "update", "allowed"
          it_behaves_like 'update'
        end

        describe 'not owned by user' do
          before(:all) do
            @pinboard = FactoryBot.create(:pinboard, organization: @setup[:organization])
            @resource = FactoryBot.create(:pinboard_idea, pinboard: @pinboard, user: @setup[:admin])
            @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
            @token = @setup[:user_token]
          end
          include_examples "update", "not_found"
          it_behaves_like 'update'
        end
      end

      describe 'approve an idea' do
        before(:all) do 
          @put_json = {
            status: 'confirmed',
          }

          @pinboard = FactoryBot.create(:pinboard, organization: @setup[:organization])
          @resource = FactoryBot.create(:pinboard_idea, pinboard: @pinboard, status: 'is_checked')
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:admin_token]
        end
        include_examples "update", "allowed"

        it 'should set approved at datetime' do
          @resource.reload
          expect(@resource.approved_at).not_to eq nil
        end
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "not_found"
        it_behaves_like 'update'
      end
    end
  end
end