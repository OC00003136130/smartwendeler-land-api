require "rails_helper"

RSpec.describe "PinboardIdea Endpoints - v1/pinboard_ideas", type: :request do
  describe '#remove_vote' do
    before(:all) do
      PinboardIdea.destroy_all
      @setup = setup_specs("v1", "pinboard_ideas")
      @pinboard = FactoryBot.create(:pinboard, organization: @setup[:organization])
      @resource = FactoryBot.create(:pinboard_idea, pinboard: @pinboard)

      @pinboard_idea_rating = FactoryBot.create(:pinboard_idea_rating, pinboard_idea: @resource, user: @setup[:user])
    end
    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}/remove_vote"
        @token = @setup[:user_token]
        delete_json @endpoint, {}, @token
      end

      it 'should return status 204' do
        expect(response.status).to eq 204
      end

      it 'should have removed the pinboard_idea rating' do
        expect(PinboardIdeaRating.exists? @pinboard_idea_rating.id).to be_falsey
      end
    end
  end
end