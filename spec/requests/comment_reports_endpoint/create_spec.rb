require "rails_helper"

RSpec.describe "CommentReport Endpoints - v1/comment_reports", type: :request do
  describe '#create' do
    before(:all) do
      Comment.destroy_all
      CommentReport.destroy_all
      @setup = setup_specs("v1", "comment_reports")
      @resource = FactoryBot.build(:comment_report)

      @comment = FactoryBot.create(:comment, organization: @setup[:organization])
      @comment_in_different_organization = FactoryBot.create(:comment, organization: @setup[:another_organization])

      @endpoint = "#{@setup[:endpoint]}/comments/#{@comment.id}"

      @post_json = {}
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'

        context 'own comment' do
          before(:all) do
            @token = @setup[:admin_token]
            @comment_own = FactoryBot.create(:comment, organization: @setup[:organization], user: @setup[:admin])
            @endpoint = "#{@setup[:endpoint]}/comments/#{@comment_own.id}"
            @expected_error_code = "commentreport.own_comment.not_possible"
          end
          include_examples "create", "not_valid"
          it_behaves_like 'create'
        end

        context 'comment already reported' do
          before(:all) do
            @token = @setup[:admin_token]
            @expected_error_code = "commentreport.user.taken"
          end
          include_examples "create", "not_valid"
          it_behaves_like 'create'
        end
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end
    end

    context 'when data is not valid' do
      context 'comment not found' do
        before(:all) do
          @post_json = {}
          @token = @setup[:admin_token]
          @endpoint = "#{@setup[:endpoint]}/comments/394792387489237498324"
        end
        include_examples "create", "not_found"
        it_behaves_like 'create'
      end

      context 'comment in different organization' do
        before(:all) do
          @post_json = {}
          @token = @setup[:admin_token]
          @endpoint = "#{@setup[:endpoint]}/comments/#{@comment_in_different_organization.id}"
        end
        include_examples "create", "not_found"
        it_behaves_like 'create'
      end
    end
  end
end