require "rails_helper"

RSpec.describe "CommentReport Endpoints - v1/comment_reports", type: :request do
  describe '#delete' do
    before(:all) do
      Comment.destroy_all
      CommentReport.destroy_all
      @setup = setup_specs("v1", "comment_reports")
      @comment = FactoryBot.create(:comment, organization: @setup[:organization])
    end
    context 'when resource is found' do
      context 'for role root' do
        before(:all) do
          @resource = FactoryBot.create(:comment_report, organization: @setup[:organization], user: @setup[:user], comment: @comment)
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:root_token]
        end
        include_examples "delete", CommentReport, "allowed"
        it_behaves_like 'delete'
      end

      context 'for role admin' do
        before(:all) do
          @resource = FactoryBot.create(:comment_report, organization: @setup[:organization], user: @setup[:user], comment: @comment)
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:admin_token]
        end
        include_examples "delete", CommentReport, "allowed"
        it_behaves_like 'delete'
      end

      context 'for role user' do
        before(:all) do
          @resource = FactoryBot.create(:comment_report, organization: @setup[:organization], user: @setup[:user], comment: @comment)
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:user_token]
        end
        include_examples "delete", CommentReport, "forbidden"
        it_behaves_like 'delete'
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "delete", CommentReport, "not_found"
        it_behaves_like 'delete'
      end
    end
  end
end