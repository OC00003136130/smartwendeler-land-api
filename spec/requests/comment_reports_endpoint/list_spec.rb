require "rails_helper"

RSpec.describe "CommentReport Endpoints - v1/comment_reports", type: :request do
  describe '#list' do
    before(:all) do
      Comment.destroy_all
      CommentReport.destroy_all
      @setup = setup_specs("v1", "comment_reports")
      @list = FactoryBot.create_list(:comment_report, 10, organization: @setup[:organization])

      @endpoint = @setup[:endpoint]
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @setup[:organization].comment_reports
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @setup[:organization].comment_reports
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @setup[:organization].comment_reports
        end
        include_examples "list", "forbidden"
        it_behaves_like 'list'
      end
    end
  end
end