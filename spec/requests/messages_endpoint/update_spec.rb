require "rails_helper"

RSpec.describe "Message Endpoints - v1/messages", type: :request do
  describe '#update' do
    before(:all) do
      Message.destroy_all
      @setup = setup_specs("v1", "messages")
      @resource = FactoryBot.create(:message, organization: @setup[:organization])
      @resource_updated = FactoryBot.build(:message, organization: @setup[:organization])
      @put_json = {
        firstname: @resource.firstname,
        lastname: @resource.lastname,
        email: @resource.email
      }
    end
    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "update", "forbidden"
        it_behaves_like 'update'
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "not_found"
        it_behaves_like 'update'
      end
    end
  end
end