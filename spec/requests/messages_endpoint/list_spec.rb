require "rails_helper"

RSpec.describe "Message Endpoints - v1/messages", type: :request do
  describe '#list' do
    before(:all) do
      Message.destroy_all
      @setup = setup_specs("v1", "messages")
      @list = FactoryBot.create_list(:message, 10, organization: @setup[:organization])

      @endpoint = @setup[:endpoint]
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @setup[:organization].messages
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @setup[:organization].messages
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @setup[:organization].messages
        end
        include_examples "list", "forbidden"
        it_behaves_like 'list'
      end
    end

    context 'when ressources are not found' do
      context 'project not found' do
        before(:all) do
          @token = @setup[:admin_token]
          @endpoint = "#{@setup[:endpoint]}/394792387489237498324"
        end
        include_examples "list", "not_found"
        it_behaves_like 'list'
      end
    end
  end
end