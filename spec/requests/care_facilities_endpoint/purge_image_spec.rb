require "rails_helper"

RSpec.describe "CareFacility Endpoints - v1/care_facilities", type: :request do
  describe '#images' do
    before(:all) do
      CareFacility.destroy_all
      @setup = setup_specs("v1", "care_facilities")
      @resource = FactoryBot.create(:care_facility, organization: @setup[:organization])
    
      file = Rails.root.join('spec', 'fixtures', 'files', 'test.jpg')
      @resource.images.attach(io: File.open(file, 'rb'), filename: 'test.jpg')
      @signed_id = @resource.images[0].signed_id
    end

    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}/images/#{@signed_id}"
        @token = @setup[:admin_token]
        delete_json @endpoint, {}, @token
      end

      it 'should return status 204' do
        expect(response.status).to eq 204
      end

      it 'should have removed the image' do
        @resource.reload
        expect(@resource.sanitized_images.count).to eq 0
      end
    end

    context 'signed_id not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}/images/ey11111111"
        @token = @setup[:admin_token]
        delete_json @endpoint, {}, @token
      end

      it 'should return status 404' do
        expect(response.status).to eq 404
      end
    end
  end
end