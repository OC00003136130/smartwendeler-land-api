require "rails_helper"

RSpec.describe "CareFacility Endpoints - v1/care_facilities", type: :request do
  describe '#update' do
    before(:all) do
      CareFacility.destroy_all
      @setup = setup_specs("v1", "care_facilities")
      @resource = FactoryBot.create(:care_facility, organization: @setup[:organization])
      @resource_updated = FactoryBot.build(:care_facility, organization: @setup[:organization])
      base64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="

      @put_json = {
        name: @resource.name,
        phone: @resource.phone,
        email: @resource.email,
        website: @resource.website,
        town: @resource.town,
        zip: @resource.zip,
        street: @resource.street,
        description: @resource.description,
        excerpt: @resource.excerpt,
        file: base64,
        logo: base64
      }
    end
    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
      end
      context 'for role root' do
        before(:all) do
          @put_json.merge!(slug: 'hallo-huhuh')
          @token = @setup[:root_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'

        it 'should attach an image' do
          @resource.reload
          expect(@resource.image_url).not_to eq nil
          expect(@resource.image.attached?).to be_truthy
        end

        it 'should attach a logo' do
          @resource.reload
          expect(@resource.logo_url).not_to eq nil
          expect(@resource.logo.attached?).to be_truthy
        end
      end

      context 'for role care_facility_admin' do
        before(:all) do
          @put_json.merge!(slug: 'hallo-huhuh-2')
          @token = @setup[:care_facility_admin_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'

        describe 'purge image' do
          before(:all) do
            @put_json = {
              name: @resource.name,
              file: 'purge'
            }
          end
          include_examples "update", "allowed"
          
          it 'should remove the image' do
            @resource.reload
            expect(@resource.image_url).to eq nil
            expect(@resource.image.attached?).to be_falsey
          end
        end
      end

      context 'for role admin' do
        before(:all) do
          @put_json.merge!(slug: 'hallo-huhuh-3')
          @token = @setup[:admin_token]
        end
        include_examples "update", "forbidden"
        it_behaves_like 'update'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "update", "forbidden"
        it_behaves_like 'update'
      end

      context 'for role facility_owner' do
        describe 'different care facility' do
          before(:all) do
            @put_json.merge!(slug: 'hallo-huhuh-4')
            @token = @setup[:facility_owner_token]
          end
          include_examples "update", "not_found"
          it_behaves_like 'update'
        end

        describe 'own care facility' do
          before(:all) do
            @resource = FactoryBot.create(:care_facility, organization: @setup[:organization], user: @setup[:facility_owner])
            @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
            @token = @setup[:facility_owner_token]
          end
          include_examples "update", "allowed"
          it_behaves_like 'update'
        end
      end

      describe 'assign/remove categories' do
        before(:all) do
          @category1 = FactoryBot.create(:category, organization: @setup[:organization])
          @category2 = FactoryBot.create(:category, organization: @setup[:organization])
          @category3 = FactoryBot.create(:category, organization: @setup[:organization])
          @category4 = FactoryBot.create(:category, organization: @setup[:organization])

          @resource = FactoryBot.create(:care_facility, organization: @setup[:organization])
          @resource.categories << @category1
          @resource.categories << @category2
          @resource.categories << @category3
          @resource.categories << @category4

          @put_json = {
            category_ids: [@category2.id, @category3.id]     
          }

          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:care_facility_admin_token]
        end

        include_examples "update", "allowed"
        
        it 'should assign/remove categories' do
          @resource.reload
          expect(@resource.categories).not_to include @category1
          expect(@resource.categories).to include @category2
          expect(@resource.categories).to include @category3
          expect(@resource.categories).not_to include @category4
        end
      end

      describe 'assign/remove sub_categories' do
        before(:all) do
          @category = FactoryBot.create(:category, organization: @setup[:organization])
          @sub_category1 = FactoryBot.create(:sub_category, organization: @setup[:organization], category: @category)
          @sub_category2 = FactoryBot.create(:sub_category, organization: @setup[:organization], category: @category)
          @sub_category3 = FactoryBot.create(:sub_category, organization: @setup[:organization], category: @category)
          @sub_category4 = FactoryBot.create(:sub_category, organization: @setup[:organization], category: @category)

          @resource = FactoryBot.create(:care_facility, organization: @setup[:organization])
          @resource.sub_categories << @sub_category1
          @resource.sub_categories << @sub_category2
          @resource.sub_categories << @sub_category3
          @resource.sub_categories << @sub_category4

          @put_json = {
            sub_category_ids: [@sub_category2.id, @sub_category3.id]     
          }

          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:care_facility_admin_token]
        end

        include_examples "update", "allowed"
        
        it 'should assign/remove sub_categories' do
          @resource.reload
          expect(@resource.sub_categories).not_to include @sub_category1
          expect(@resource.sub_categories).to include @sub_category2
          expect(@resource.sub_categories).to include @sub_category3
          expect(@resource.sub_categories).not_to include @sub_category4
        end
      end

      describe 'assign/remove sub_sub_categories' do
        before(:all) do
          @category = FactoryBot.create(:category, organization: @setup[:organization])
          @sub_category = FactoryBot.create(:sub_category, organization: @setup[:organization], category: @category)
          
          @sub_sub_category1 = FactoryBot.create(:sub_sub_category, organization: @setup[:organization], sub_category: @sub_category)
          @sub_sub_category2 = FactoryBot.create(:sub_sub_category, organization: @setup[:organization], sub_category: @sub_category)
          @sub_sub_category3 = FactoryBot.create(:sub_sub_category, organization: @setup[:organization], sub_category: @sub_category)

          @resource = FactoryBot.create(:care_facility, organization: @setup[:organization])
          @resource.sub_sub_categories << @sub_sub_category1
          @resource.sub_sub_categories << @sub_sub_category2
          @resource.sub_sub_categories << @sub_sub_category3

          @put_json = {
            sub_sub_category_ids: [@sub_sub_category2.id, @sub_sub_category3.id]     
          }

          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:care_facility_admin_token]
        end

        include_examples "update", "allowed"
        
        it 'should assign/remove sub_sub_categories' do
          @resource.reload
          expect(@resource.sub_sub_categories).not_to include @sub_sub_category1
          expect(@resource.sub_sub_categories).to include @sub_sub_category2
          expect(@resource.sub_sub_categories).to include @sub_sub_category3
        end
      end

      describe 'assign/remove tag_categories' do
        before(:all) do
          @tag_category1 = FactoryBot.create(:tag_category, organization: @setup[:organization])
          @tag_category2 = FactoryBot.create(:tag_category, organization: @setup[:organization])
          @tag_category3 = FactoryBot.create(:tag_category, organization: @setup[:organization])
          @tag_category4 = FactoryBot.create(:tag_category, organization: @setup[:organization])

          @resource = FactoryBot.create(:care_facility, organization: @setup[:organization])
          @resource.tag_categories << @tag_category1
          @resource.tag_categories << @tag_category2
          @resource.tag_categories << @tag_category3
          @resource.tag_categories << @tag_category4

          @put_json = {
            tag_category_ids: [@tag_category2.id, @tag_category3.id]     
          }

          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:care_facility_admin_token]
        end

        include_examples "update", "allowed"
        
        it 'should assign/remove tag_categories' do
          @resource.reload
          expect(@resource.tag_categories).not_to include @tag_category1
          expect(@resource.tag_categories).to include @tag_category2
          expect(@resource.tag_categories).to include @tag_category3
          expect(@resource.tag_categories).not_to include @tag_category4
        end
      end
    end

    describe 'assign/remove tags' do
      before(:all) do
        @tag1 = FactoryBot.create(:tag, organization: @setup[:organization])
        @tag2 = FactoryBot.create(:tag, organization: @setup[:organization])
        @tag3 = FactoryBot.create(:tag, organization: @setup[:organization])
        @tag4 = FactoryBot.create(:tag, organization: @setup[:organization])

        @resource = FactoryBot.create(:care_facility, organization: @setup[:organization])
        @resource.tags << @tag1
        @resource.tags << @tag2
        @resource.tags << @tag3
        @resource.tags << @tag4

        @put_json = {
          tag_ids: [@tag2.id, @tag3.id]     
        }

        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
        @token = @setup[:care_facility_admin_token]
      end

      include_examples "update", "allowed"
      
      it 'should assign/remove tags' do
        @resource.reload
        expect(@resource.tags).not_to include @tag1
        expect(@resource.tags).to include @tag2
        expect(@resource.tags).to include @tag3
        expect(@resource.tags).not_to include @tag4
      end
    end

    describe 'change attribues as facility owner' do
      before(:all) do
        @user = @setup[:facility_owner]
        @user.is_active_on_health_scope = true
        @user.save!
        @resource = FactoryBot.create(:care_facility, organization: @setup[:organization], user: @user)
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
        @token = @setup[:facility_owner_token]

        @put_json = {
          zip: "77777"     
        }
      end
      include_examples "update", "allowed"

      it 'should reset the user active on health scope status' do
        @user.reload
        expect(@user.is_active_on_health_scope).to be_falsey
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "not_found"
        it_behaves_like 'update'
      end
    end
  end
end