require "rails_helper"

RSpec.describe "CareFacility Endpoints - v1/care_facilities", type: :request do
  describe '#delete' do
    before(:all) do
      CareFacility.destroy_all
      @setup = setup_specs("v1", "care_facilities")
    end
    context 'when resource is found' do
      context 'for role root' do
        before(:all) do
          @resource = FactoryBot.create(:care_facility, organization: @setup[:organization])
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:root_token]
        end
        include_examples "delete", CareFacility, "allowed"
        it_behaves_like 'delete'
      end

      context 'for role care_facility_admin' do
        before(:all) do
          @resource = FactoryBot.create(:care_facility, organization: @setup[:organization])
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:care_facility_admin_token]
        end
        include_examples "delete", CareFacility, "allowed"
        it_behaves_like 'delete'
      end

      context 'for role admin' do
        before(:all) do
          @resource = FactoryBot.create(:care_facility, organization: @setup[:organization])
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:admin_token]
        end
        include_examples "delete", CareFacility, "forbidden"
        it_behaves_like 'delete'
      end

      context 'for role user' do
        before(:all) do
          @resource = FactoryBot.create(:care_facility, organization: @setup[:organization])
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:user_token]
        end
        include_examples "delete", CareFacility, "forbidden"
        it_behaves_like 'delete'
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "delete", CareFacility, "not_found"
        it_behaves_like 'delete'
      end
    end
  end
end