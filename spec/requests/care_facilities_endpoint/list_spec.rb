require "rails_helper"

RSpec.describe "CareFacility Endpoints - v1/care_facilities", type: :request do
  describe '#list' do
    before(:all) do
      CareFacility.destroy_all
      @setup = setup_specs("v1", "care_facilities")
      @endpoint = @setup[:endpoint]
      @list = FactoryBot.create_list(:care_facility, 10, organization: @setup[:organization])
      @list.last.user = @setup[:facility_owner]
      @list.last.save!
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @setup[:organization].care_facilities
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @setup[:organization].care_facilities
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role facility_owner' do
        before(:all) do
          @token = @setup[:facility_owner_token]
          @model = @setup[:facility_owner].care_facilities
        end
        include_examples "list", "allowed", 1
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @setup[:organization].care_facilities
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end
    end
  end
end