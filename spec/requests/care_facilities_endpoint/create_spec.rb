require "rails_helper"

RSpec.describe "CareFacility Endpoints - v1/care_facilities", type: :request do
  describe '#create' do
    before(:all) do
      CareFacility.destroy_all
      @setup = setup_specs("v1", "care_facilities")
      @endpoint = @setup[:endpoint]
      @resource = FactoryBot.build(:care_facility)

      base64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="
      base64pdf = "data:application/pdf;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII="

      @post_json = {
        name: @resource.name,
        phone: @resource.phone,
        email: @resource.email,
        website: @resource.website,
        town: @resource.town,
        zip: @resource.zip,
        street: @resource.street,
        description: @resource.description,
        excerpt: @resource.excerpt,
        file: base64,
        logo: base64,
        document: base64pdf
      }
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @post_json.merge!(slug: 'hallo-huhuh')
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'

        it 'should attach an image' do
          @new_care_facility = @setup[:organization].care_facilities.find parsed_response['resource']['id']
          expect(@new_care_facility.image_url).not_to eq nil
          expect(@new_care_facility.image.attached?).to be_truthy
        end

        it 'should attach a document' do
          @new_care_facility = @setup[:organization].care_facilities.find parsed_response['resource']['id']
          expect(@new_care_facility.documents.attached?).to be_truthy
        end

        it 'should attach a logo' do
          @new_care_facility = @setup[:organization].care_facilities.find parsed_response['resource']['id']
          expect(@new_care_facility.logo_url).not_to eq nil
          expect(@new_care_facility.logo.attached?).to be_truthy
        end

        it 'should fill the opening hours' do
          @new_care_facility = @setup[:organization].care_facilities.find parsed_response['resource']['id']
          expect(@new_care_facility.opening_hours).not_to eq nil
        end
      end

      context 'for role admin' do
        before(:all) do
          @post_json.merge!(slug: 'hallo-huhuh-aaa')
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "forbidden"
        it_behaves_like 'create'
      end

      describe 'assign categories' do
        before(:all) do
          @category1 = FactoryBot.create(:category, organization: @setup[:organization])
          @category2 = FactoryBot.create(:category, organization: @setup[:organization])
          @category3 = FactoryBot.create(:category, organization: @setup[:organization])
          @category4 = FactoryBot.create(:category, organization: @setup[:organization])

          @post_json = {
            name: @resource.name,
            category_ids: [@category1.id, @category2.id, @category3.id, @category4.id]     
          }
          @token = @setup[:admin_token]
        end

        include_examples "create", "allowed"
        it 'should assign categories' do
          resource = CareFacility.find parsed_response["resource"]["id"]
          
          expect(resource.categories).to include @category1
          expect(resource.categories).to include @category2
          expect(resource.categories).to include @category3
          expect(resource.categories).to include @category4
        end
      end

      describe 'assign tags' do
        before(:all) do
          @tag1 = FactoryBot.create(:tag, organization: @setup[:organization])
          @tag2 = FactoryBot.create(:tag, organization: @setup[:organization])
          @tag3 = FactoryBot.create(:tag, organization: @setup[:organization])
          @tag4 = FactoryBot.create(:tag, organization: @setup[:organization])

          @post_json = {
            name: @resource.name,
            tag_ids: [@tag1.id, @tag2.id, @tag3.id, @tag4.id]     
          }
          @token = @setup[:admin_token]
        end

        include_examples "create", "allowed"
        it 'should assign tags' do
          resource = CareFacility.find parsed_response["resource"]["id"]
          
          expect(resource.tags).to include @tag1
          expect(resource.tags).to include @tag2
          expect(resource.tags).to include @tag3
          expect(resource.tags).to include @tag4
        end
      end

      describe 'assign sub categories' do
        before(:all) do
          @category = FactoryBot.create(:category, organization: @setup[:organization])
          @sub_category1 = FactoryBot.create(:sub_category, organization: @setup[:organization], category: @category)
          @sub_category2 = FactoryBot.create(:sub_category, organization: @setup[:organization], category: @category)
          @sub_category3 = FactoryBot.create(:sub_category, organization: @setup[:organization], category: @category)
          @sub_category4 = FactoryBot.create(:sub_category, organization: @setup[:organization], category: @category)
          @post_json = {
            name: @resource.name,
            sub_category_ids: [@sub_category1.id, @sub_category2.id, @sub_category3.id, @sub_category4.id]     
          }
          @token = @setup[:admin_token]
        end

        include_examples "create", "allowed"
        it 'should assign sub_categories' do
          resource = CareFacility.find parsed_response["resource"]["id"]
          
          expect(resource.sub_categories).to include @sub_category1
          expect(resource.sub_categories).to include @sub_category2
          expect(resource.sub_categories).to include @sub_category3
          expect(resource.sub_categories).to include @sub_category4
        end
      end

      describe 'assign sub sub categories' do
        before(:all) do
          @category = FactoryBot.create(:category, organization: @setup[:organization])
          @sub_category = FactoryBot.create(:sub_category, organization: @setup[:organization], category: @category)

          @sub_sub_category1 = FactoryBot.create(:sub_sub_category, organization: @setup[:organization], sub_category: @sub_category)
          @sub_sub_category2 = FactoryBot.create(:sub_sub_category, organization: @setup[:organization], sub_category: @sub_category)
          @sub_sub_category3 = FactoryBot.create(:sub_sub_category, organization: @setup[:organization], sub_category: @sub_category)
          @post_json = {
            name: @resource.name,
            sub_sub_category_ids: [@sub_sub_category1.id, @sub_sub_category2.id, @sub_sub_category3.id]     
          }
          @token = @setup[:admin_token]
        end

        include_examples "create", "allowed"
        it 'should assign sub_sub_categories' do
          resource = CareFacility.find parsed_response["resource"]["id"]
          
          expect(resource.sub_sub_categories).to include @sub_sub_category1
          expect(resource.sub_sub_categories).to include @sub_sub_category2
          expect(resource.sub_sub_categories).to include @sub_sub_category3
        end
      end

      describe 'assign tag_categories' do
        before(:all) do
          @tag_category1 = FactoryBot.create(:tag_category, organization: @setup[:organization])
          @tag_category2 = FactoryBot.create(:tag_category, organization: @setup[:organization])
          @tag_category3 = FactoryBot.create(:tag_category, organization: @setup[:organization])
          @tag_category4 = FactoryBot.create(:tag_category, organization: @setup[:organization])

          @post_json = {
            name: @resource.name,
            tag_category_ids: [@tag_category1.id, @tag_category2.id, @tag_category3.id, @tag_category4.id]     
          }
          @token = @setup[:admin_token]
        end

        include_examples "create", "allowed"
        it 'should assign tag_categories' do
          resource = CareFacility.find parsed_response["resource"]["id"]
          
          expect(resource.tag_categories).to include @tag_category1
          expect(resource.tag_categories).to include @tag_category2
          expect(resource.tag_categories).to include @tag_category3
          expect(resource.tag_categories).to include @tag_category4
        end
      end
    end

    context 'when data is not valid' do
      describe 'name missing' do
        before(:all) do
          @post_json = {
            phone: @resource.phone,
            email: @resource.email
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'carefacility.name.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end

      describe 'wrong kind' do
        before(:all) do
          @post_json = {
            phone: @resource.phone,
            email: @resource.email,
            name: 'huhu',
            kind: 'lol'
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'carefacility.kind.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
      describe 'slug invalid' do
        before(:all) do
          @post_json = {
            phone: @resource.phone,
            email: @resource.email,
            name: 'huhu hhahah',
            slug: 'lalal huhu'
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'carefacility.slug.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
    end
  end
end