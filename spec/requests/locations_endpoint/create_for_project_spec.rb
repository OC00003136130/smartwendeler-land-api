require "rails_helper"

RSpec.describe "Location Endpoints - v1/locations", type: :request do
  describe '#create_for_project' do
    before(:all) do
      Organization.destroy_all
      @setup = setup_specs("v1", "locations")
      @resource = FactoryBot.build(:location)

      @project = FactoryBot.create(:project, organization: @setup[:organization])

      @endpoint = "#{@setup[:endpoint]}/project/#{@project.id}"

      @post_json = {
        longitude: @resource.longitude,
        latitude: @resource.latitude,
        project_id: @project.id
      }
    end

    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "forbidden"
        it_behaves_like 'create'
      end
    end

    context 'when data is not valid' do
      context 'project not found' do
        before(:all) do
          @post_json = {
            longitude: @resource.longitude,
            latitude: @resource.latitude
          }
          @token = @setup[:admin_token]
          @endpoint = "#{@setup[:endpoint]}/project/394792387489237498324"
        end
        include_examples "create", "not_found"
        it_behaves_like 'create'
      end

      context 'location missing' do
        before(:all) do
          @post_json = {
            project_id: @project.id
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'location.longitude.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
    end
  end
end