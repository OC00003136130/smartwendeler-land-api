require "rails_helper"

RSpec.describe "Location Endpoints - v1/locations", type: :request do
  describe '#create_for_care_facility' do
    before(:all) do
      Organization.destroy_all
      @setup = setup_specs("v1", "locations")
      @resource = FactoryBot.build(:location)

      @care_facility = FactoryBot.create(:care_facility, organization: @setup[:organization])

      @endpoint = "#{@setup[:endpoint]}/care_facility/#{@care_facility.id}"

      @post_json = {
        longitude: @resource.longitude,
        latitude: @resource.latitude,
        care_facility_id: @care_facility.id
      }
    end

    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "forbidden"
        it_behaves_like 'create'
      end
    end

    context 'when data is not valid' do
      context 'care_facility not found' do
        before(:all) do
          @post_json = {
            longitude: @resource.longitude,
            latitude: @resource.latitude
          }
          @token = @setup[:admin_token]
          @endpoint = "#{@setup[:endpoint]}/care_facility/394792387489237498324"
        end
        include_examples "create", "not_found"
        it_behaves_like 'create'
      end

      context 'location missing' do
        before(:all) do
          @post_json = {
            care_facility_id: @care_facility.id
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'location.longitude.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
    end
  end
end