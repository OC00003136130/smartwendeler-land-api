require "rails_helper"

RSpec.describe "Location Endpoints - v1/locations", type: :request do
  describe '#update' do
    before(:all) do
      Organization.destroy_all
      Location.destroy_all
      @setup = setup_specs("v1", "locations")
      @project = FactoryBot.create(:project, organization: @setup[:organization])
      @resource = FactoryBot.create(:location, project: @project, organization: @setup[:organization])
      @put_json = {
        longitude: 51.0,
        latitude: 10.0
      }
    end

    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "update", "forbidden"
        it_behaves_like 'update'
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "not_found"
        it_behaves_like 'update'
      end
    end
  end
end