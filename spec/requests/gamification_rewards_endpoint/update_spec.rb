require "rails_helper"

RSpec.describe "GamificationReward Endpoints - v1/gamification_rewards", type: :request do
  describe '#update' do
    before(:all) do
      GamificationReward.destroy_all
      @setup = setup_specs("v1", "gamification_rewards")
      @game = FactoryBot.create(:gamification_game, organization: @setup[:organization])
      @resource = FactoryBot.create(:gamification_reward, gamification_game: @game)

      @put_json = {
        name: 'Huhu',
        threshold: rand(1..20)
      }
    end
    context 'when data is valid' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "update", "allowed"
        it_behaves_like 'update'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "update", "forbidden"
        it_behaves_like 'update'
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "update", "not_found"
        it_behaves_like 'update'
      end
    end
  end
end