require "rails_helper"

RSpec.describe "GamificationReward Endpoints - v1/gamification_rewards", type: :request do
  describe '#list' do
    before(:all) do
      GamificationReward.destroy_all
      @setup = setup_specs("v1", "gamification_rewards/gamification_game")
      @game = FactoryBot.create(:gamification_game, organization: @setup[:organization])
      @endpoint = "#{@setup[:endpoint]}/#{@game.id}"
      @list = FactoryBot.create_list(:gamification_reward, 10, gamification_game: @game)
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @game.gamification_rewards
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @game.gamification_rewards
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @game.gamification_rewards
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end
    end
  end
end