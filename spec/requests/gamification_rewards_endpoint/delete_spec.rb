require "rails_helper"

RSpec.describe "GamificationReward Endpoints - v1/gamification_rewards", type: :request do
  describe '#delete' do
    before(:all) do
      GamificationReward.destroy_all
      @setup = setup_specs("v1", "gamification_rewards")
      @game = FactoryBot.create(:gamification_game, organization: @setup[:organization])
    end
    context 'when resource is found' do
      context 'for role root' do
        before(:all) do
          @resource = FactoryBot.create(:gamification_reward, gamification_game: @game)
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:root_token]
        end
        include_examples "delete", GamificationReward, "allowed"
        it_behaves_like 'delete'
      end

      context 'for role admin' do
        before(:all) do
          @resource = FactoryBot.create(:gamification_reward, gamification_game: @game)
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:admin_token]
        end
        include_examples "delete", GamificationReward, "allowed"
        it_behaves_like 'delete'
      end

      context 'for role user' do
        before(:all) do
          @resource = FactoryBot.create(:gamification_reward, gamification_game: @game)
          @endpoint = "#{@setup[:endpoint]}/#{@resource.id}"
          @token = @setup[:user_token]
        end
        include_examples "delete", GamificationReward, "forbidden"
        it_behaves_like 'delete'
      end
    end

    context 'when resource is not found' do
      before(:all) do
        @endpoint = "#{@setup[:endpoint]}/39754493857349875"
      end
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "delete", GamificationReward, "not_found"
        it_behaves_like 'delete'
      end
    end
  end
end