require "rails_helper"

RSpec.describe "GamificationReward Endpoints - v1/gamification_rewards", type: :request do
  describe '#create' do
    before(:all) do
      GamificationReward.destroy_all
      @setup = setup_specs("v1", "gamification_rewards/gamification_game")
      @game = FactoryBot.create(:gamification_game, organization: @setup[:organization])
      
      @endpoint = "#{@setup[:endpoint]}/#{@game.id}"
      @resource = FactoryBot.build(:gamification_reward)
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @post_json = {
            name: @resource.name,
            threshold: rand(1..20)
          }
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role admin' do
        before(:all) do
          @post_json = {
            name: @resource.name,
            threshold: rand(1..20)
          }
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "forbidden"
        it_behaves_like 'create'
      end
    end

    context 'when data is not valid' do
      describe 'name missing' do
        before(:all) do
          @post_json = {
            threshold: rand(1..20)
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'gamificationreward.name.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
      describe 'threshold missing' do
        before(:all) do
          @post_json = {
            name: @resource.name
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'gamificationreward.threshold.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
    end
  end
end