require "rails_helper"

RSpec.describe "Tooltip Endpoints - v1/tooltips", type: :request do
  describe '#list' do
    before(:all) do
      Tooltip.destroy_all
      @setup = setup_specs("v1", "tooltips")
      @list = FactoryBot.create_list(:tooltip, 10, organization: @setup[:organization])

      @endpoint = @setup[:endpoint]
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @setup[:organization].tooltips
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @setup[:organization].tooltips
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @setup[:organization].tooltips
        end
        include_examples "list", "forbidden"
        it_behaves_like 'list'
      end
    end
  end
end