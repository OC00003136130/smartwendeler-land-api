require "rails_helper"

RSpec.describe "Tooltip Endpoints - v1/tooltips", type: :request do
  describe '#create' do
    before(:all) do
      Tooltip.destroy_all
      @setup = setup_specs("v1", "tooltips")
      @resource = FactoryBot.build(:tooltip)

      @endpoint = @setup[:endpoint]

      @post_json = {
        name: @resource.name,
        content: @resource.content
      }
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "forbidden"
        it_behaves_like 'create'
      end
    end

    context 'when data is not valid' do
      context 'name missing' do
        before(:all) do
          @post_json = {
            content: @resource.content
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'tooltip.name.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end
    end
  end
end