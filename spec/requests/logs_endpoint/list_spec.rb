require "rails_helper"

RSpec.describe "Log Endpoints - v1/logs", type: :request do
  describe '#list' do
    before(:all) do
      Log.destroy_all
      @setup = setup_specs("v1", "logs")
      @endpoint = @setup[:endpoint]

      @user1 = FactoryBot.create(:user, organization: @setup[:organization])
      @user2 = FactoryBot.create(:user, organization: @setup[:organization])
      @user3 = FactoryBot.create(:user, organization: @setup[:organization])

      @list = []
      @list << FactoryBot.create(:log, organization: @setup[:organization], user: nil)
      @list << FactoryBot.create(:log, organization: @setup[:organization], user: nil)
      @list << FactoryBot.create(:log, organization: @setup[:organization], user: @setup[:user])
      @list << FactoryBot.create(:log, organization: @setup[:organization], user: @user1)
      @list << FactoryBot.create(:log, organization: @setup[:organization], user: nil)
      @list << FactoryBot.create(:log, organization: @setup[:organization], user: @user1)
      @list << FactoryBot.create(:log, organization: @setup[:organization], user: @setup[:user])
      @list << FactoryBot.create(:log, organization: @setup[:organization], user: @user2)
      @list << FactoryBot.create(:log, organization: @setup[:organization], user: @user3)
      @list << FactoryBot.create(:log, organization: @setup[:organization], user: @setup[:user])
      @list << FactoryBot.create(:log, organization: @setup[:organization], user: @setup[:admin])
      @list << FactoryBot.create(:log, organization: @setup[:organization], user: @setup[:root])
      @list << FactoryBot.create(:log, organization: @setup[:organization], user: nil)
      @list << FactoryBot.create(:log, organization: @setup[:organization], user: nil)
      @list << FactoryBot.create(:log, organization: @setup[:organization], user: nil)
      @list << FactoryBot.create(:log, organization: @setup[:organization], user: @setup[:admin])
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @setup[:organization].logs
        end
        include_examples "list", "allowed", 7
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @setup[:organization].logs
        end
        include_examples "list", "allowed", 8
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @setup[:organization].logs
        end
        include_examples "list", "allowed", 9
        it_behaves_like 'list'
      end
    end
  end
end