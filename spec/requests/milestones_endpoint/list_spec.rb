require "rails_helper"

RSpec.describe "Milestone Endpoints - v1/milestones", type: :request do
  describe '#list' do
    before(:all) do
      Milestone.destroy_all
      @setup = setup_specs("v1", "milestones")
      @project = FactoryBot.create(:project, organization: @setup[:organization])
      @list = FactoryBot.create_list(:milestone, 10, organization: @setup[:organization], project: @project)

      @endpoint = "#{@setup[:endpoint]}/project/#{@project.id}"
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @project.milestones
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @project.milestones
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @project.milestones
        end
        include_examples "list", "forbidden"
        it_behaves_like 'list'
      end
    end

    context 'when ressources are not found' do
      context 'project not found' do
        before(:all) do
          @token = @setup[:admin_token]
          @endpoint = "#{@setup[:endpoint]}/project/394792387489237498324"
        end
        include_examples "list", "not_found"
        it_behaves_like 'list'
      end
    end
  end
end