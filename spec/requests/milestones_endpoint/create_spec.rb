require "rails_helper"

RSpec.describe "Milestone Endpoints - v1/milestones", type: :request do
  describe '#create' do
    before(:all) do
      Milestone.destroy_all
      @setup = setup_specs("v1", "milestones")
      @resource = FactoryBot.build(:milestone)

      @project = FactoryBot.create(:project, organization: @setup[:organization])
      @project_in_different_organization = FactoryBot.create(:project, organization: @setup[:another_organization])

      @endpoint = "#{@setup[:endpoint]}/project/#{@project.id}"

      @post_json = {
        name: @resource.name,
        timestamp: @resource.timestamp
      }
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'

        it 'should have put the menu_order after the next entry' do
          expect(@project.milestones.order(:menu_order).last.menu_order).to eq 1
        end
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "forbidden"
        it_behaves_like 'create'
      end
    end

    context 'when data is not valid' do
      context 'project not found' do
        before(:all) do
          @post_json = {
            name: @resource.name,
            timestamp: @resource.timestamp
          }
          @token = @setup[:admin_token]
          @endpoint = "#{@setup[:endpoint]}/project/394792387489237498324"
        end
        include_examples "create", "not_found"
        it_behaves_like 'create'
      end

      context 'name missing' do
        before(:all) do
          @post_json = {
            timestamp: @resource.timestamp
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'milestone.name.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end

      context 'project in different organization' do
        before(:all) do
          @post_json = {
            name: @resource.name,
            timestamp: @resource.timestamp
          }
          @token = @setup[:admin_token]
          @endpoint = "#{@setup[:endpoint]}/project/#{@project_in_different_organization.id}"
        end
        include_examples "create", "not_found"
        it_behaves_like 'create'
      end
    end
  end
end