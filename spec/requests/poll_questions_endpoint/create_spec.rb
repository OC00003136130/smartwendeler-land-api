require "rails_helper"

RSpec.describe "PollQuestion Endpoints - v1/poll_questions", type: :request do
  describe '#create' do
    before(:all) do
      PollQuestion.destroy_all
      @setup = setup_specs("v1", "poll_questions")
      @resource = FactoryBot.build(:poll_question)

      @poll = FactoryBot.create(:poll, organization: @setup[:organization])
      @poll_in_different_organization = FactoryBot.create(:poll, organization: @setup[:another_organization])

      @endpoint = "#{@setup[:endpoint]}/poll/#{@poll.id}"

      @post_json = {
        name: @resource.name,
        max_score_count: @resource.max_score_count,
        kind: @resource.kind
      }
    end
    context 'when data is valid' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
        end
        include_examples "create", "allowed"
        it_behaves_like 'create'

        it 'should have put the menu_order after the next entry' do
          expect(@poll.poll_questions.order(:menu_order).last.menu_order).to eq 1
        end
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
        end
        include_examples "create", "forbidden"
        it_behaves_like 'create'
      end
    end

    context 'when data is not valid' do
      context 'poll not found' do
        before(:all) do
          @post_json = {
            name: @resource.name,
            max_score_count: @resource.max_score_count,
            kind: @resource.kind
          }
          @token = @setup[:admin_token]
          @endpoint = "#{@setup[:endpoint]}/poll/394792387489237498324"
        end
        include_examples "create", "not_found"
        it_behaves_like 'create'
      end

      context 'name missing' do
        before(:all) do
          @post_json = {
            max_score_count: @resource.max_score_count,
            kind: @resource.kind
          }
          @token = @setup[:admin_token]
          @expected_error_code = 'pollquestion.name.invalid'
        end
        include_examples "create", "not_valid"
        it_behaves_like 'create'
      end

      context 'poll in different organization' do
        before(:all) do
          @post_json = {
            name: @resource.name,
            max_score_count: @resource.max_score_count,
            kind: @resource.kind
          }
          @token = @setup[:admin_token]
          @endpoint = "#{@setup[:endpoint]}/poll/#{@poll_in_different_organization.id}"
        end
        include_examples "create", "not_found"
        it_behaves_like 'create'
      end
    end
  end
end