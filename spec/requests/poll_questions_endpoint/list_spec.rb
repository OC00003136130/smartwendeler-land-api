require "rails_helper"

RSpec.describe "PollQuestion Endpoints - v1/poll_questions", type: :request do
  describe '#list' do
    before(:all) do
      PollQuestion.destroy_all
      @setup = setup_specs("v1", "poll_questions")
      @poll = FactoryBot.create(:poll, organization: @setup[:organization])
      @list = FactoryBot.create_list(:poll_question, 10, organization: @setup[:organization], poll: @poll)

      @endpoint = "#{@setup[:endpoint]}/poll/#{@poll.id}"
    end
    context 'when ressources are found' do
      context 'for role root' do
        before(:all) do
          @token = @setup[:root_token]
          @model = @poll.poll_questions
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role admin' do
        before(:all) do
          @token = @setup[:admin_token]
          @model = @poll.poll_questions
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end

      context 'for role user' do
        before(:all) do
          @token = @setup[:user_token]
          @model = @poll.poll_questions
        end
        include_examples "list", "allowed", 10
        it_behaves_like 'list'
      end
    end

    context 'when ressources are not found' do
      context 'poll not found' do
        before(:all) do
          @token = @setup[:admin_token]
          @endpoint = "#{@setup[:endpoint]}/poll/394792387489237498324"
        end
        include_examples "list", "not_found"
        it_behaves_like 'list'
      end
    end
  end
end