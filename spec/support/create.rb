RSpec.shared_examples "create" do |scope, is_public|
  before(:all) do
    if is_public
      post_json_public @endpoint, @post_json, @setup[:organization]
    else
      post_json @endpoint, @post_json, @token
    end
  end

  if scope == 'allowed'
    it 'should return status 201' do
      expect(response.status).to eq 201
    end

    it 'should list the parsed body as hash' do
      expect(parsed_response.class).to eq Hash
    end

    it 'should output the correct data' do
      resource = parsed_response["resource"]

      resource.keys.each do |key|
        if @post_json[key.to_sym]
          fieldType = @post_json[key.to_sym].class

          if fieldType == BigDecimal || fieldType == Float
            expect(resource[key].to_f).to eq @post_json[key.to_sym]
          elsif fieldType == ActiveSupport::TimeWithZone
            expect(resource[key].to_datetime.to_i).to eq @post_json[key.to_sym].to_datetime.to_i
          elsif fieldType == Array
            expect(resource[key]).to include(*@post_json[key.to_sym])
          else
            expect(resource[key]).to eq @post_json[key.to_sym]
          end
        end
      end
    end

    it 'should include meta data in the parsed response' do
      expect(parsed_response["resource_url"]).not_to eq nil
      expect(parsed_response["retrieved_at"]).not_to eq nil
    end
  end

  if scope == 'forbidden'
    it 'should return status 403' do
      expect(response.status).to eq 403
    end
  end

  if scope == 'not_valid'
    it 'should return status 422' do
      expect(response.status).to eq 422
    end

    it 'should throw an error' do
      if @expected_error_code
        expect(parsed_response['errors'][0]['code']).to eq @expected_error_code
      end
    end
  end

  if scope == 'not_found'
    it 'should return status 404' do
      expect(response.status).to eq 404
    end
  end
end