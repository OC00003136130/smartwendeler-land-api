require 'factory_bot'

FactoryBot.define do
  factory :poll_answer do
    organization
    poll_question
    user
    rating_value { rand(1..10) }
    text_value { Faker::Lorem.paragraph }
  end
end
