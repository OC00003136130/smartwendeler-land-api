require 'factory_bot'

FactoryBot.define do
  factory :care_facility_category do
    care_facility
    category
  end
end
