require 'factory_bot'

FactoryBot.define do
  factory :comment do
    organization
    user
    project
    comment { Faker::Lorem.paragraph }
  end
end
