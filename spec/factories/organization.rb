require 'factory_bot'

FactoryBot.define do
  factory :organization do
    name { Faker::Company.name }
    status { :active }
    public_username { "user_#{Random.rand(1000000000).to_s}" }
    public_password { Faker::Internet.password }
  end
end
