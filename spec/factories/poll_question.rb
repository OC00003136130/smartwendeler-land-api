require 'factory_bot'

FactoryBot.define do
  factory :poll_question do
    organization
    poll
    menu_order { rand(1..10) }
    name { Faker::Lorem.paragraph }
    max_score_count { 5 }
    kind { PollQuestion.kinds.keys.sample }
  end
end
