require 'factory_bot'

FactoryBot.define do
  factory :care_facility do
    organization
    user
    community
    
    name { Faker::Company.name }
    phone { Faker::PhoneNumber.cell_phone_in_e164 }
    email { Faker::Internet.email }
    website { Faker::Internet.url }
    town { Faker::Address.city }
    zip { Faker::Address.zip }
    street { Faker::Address.street_address }
    description { Faker::Lorem.paragraph(sentence_count: 10) }
    excerpt { Faker::Lorem.paragraph(sentence_count: 2) }
    is_active { true }
  end
end
