require 'factory_bot'

FactoryBot.define do
  factory :comment_rating do
    comment
    user
    score { [-1, 1].sample }
  end
end
