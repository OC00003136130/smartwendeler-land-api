require 'factory_bot'

FactoryBot.define do
  factory :project do
    organization
    name { Faker::Company.name }
    start_time { Time.now }
    end_time { Time.now + 3.weeks }
    description { Faker::Lorem.paragraph }
    town { Faker::Address.city }
    zip { Faker::Address.zip }
    leader { Faker::Name.name }
    internal_identifier { Faker::IDNumber.valid }
    is_active { true }
  end
end
