require 'factory_bot'

FactoryBot.define do
  factory :poll do
    organization
    name { Faker::Lorem.paragraph }
    ends_at { Time.now + 3.weeks }
    is_active { [true, false].sample }
  end
end
