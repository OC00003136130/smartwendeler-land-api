require 'factory_bot'

FactoryBot.define do
  factory :gamification_reward do
    gamification_game
    name { Faker::Company.name }
    threshold { rand(1..20) }
  end
end
