require 'factory_bot'

FactoryBot.define do
  factory :sub_sub_category do
    organization
    sub_category
    name { Faker::Company.name }
    scope { 'project' }
  end
end
