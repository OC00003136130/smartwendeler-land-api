require 'factory_bot'

FactoryBot.define do
  factory :pinboard do
    organization
    user
    headline { Faker::Company.name }
    content { Faker::Lorem.paragraph }
  end
end
