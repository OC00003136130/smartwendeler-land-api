require 'factory_bot'

FactoryBot.define do
  factory :project_category do
    project
    category
  end
end
