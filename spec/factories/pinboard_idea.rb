require 'factory_bot'

FactoryBot.define do
  factory :pinboard_idea do
    pinboard
    user
    headline { Faker::Company.name }
    content { Faker::Lorem.paragraph }
    status { PinboardIdea.statuses.keys.sample }
  end
end
