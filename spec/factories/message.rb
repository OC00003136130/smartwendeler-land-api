require 'factory_bot'

FactoryBot.define do
  factory :message do
    project
    organization
    user
    firstname { Faker::Name.first_name }
    lastname { Faker::Name.last_name }
    email { Faker::Internet.email }
    subject { Faker::Company.name }
    message { Faker::Lorem.paragraph }
  end
end
