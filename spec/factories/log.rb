require 'factory_bot'

FactoryBot.define do
  factory :log do
    organization
    user
    change { { test: 'abc' } }
  end
end
