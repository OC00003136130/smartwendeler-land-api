require 'factory_bot'

FactoryBot.define do
  factory :milestone do
    project
    organization
    name { Faker::Lorem.paragraph }
    timestamp { Time.now + rand(100..365).days }
  end
end
