require 'factory_bot'

FactoryBot.define do
  factory :category do
    organization
    name { Faker::Company.name }
    scope { 'project' }
  end
end
