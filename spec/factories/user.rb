require 'factory_bot'

FactoryBot.define do
  factory :user, class: User do
    organization
    firstname { Faker::Name.first_name }
    lastname { Faker::Name.last_name }
    email { Faker::Internet.email }
    role { :user }
    status { :confirmed }
    password { Faker::Internet.password }
    never_expire { false }

    factory :user_admin do
      role { :admin }
    end

    factory :user_care_facility_admin do
      role { :care_facility_admin }
    end

    factory :user_root do
      role { :root }
    end

    factory :user_facility_owner do
      role { :facility_owner }
    end

    factory :user_unconfirmed do
      status { :unconfirmed }
    end

    factory :user_admin_unconfirmed do
      role { :admin }
      status { :unconfirmed }
    end
  end
end
