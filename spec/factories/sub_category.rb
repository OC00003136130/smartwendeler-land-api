require 'factory_bot'

FactoryBot.define do
  factory :sub_category do
    organization
    category
    name { Faker::Company.name }
    scope { 'project' }
  end
end
