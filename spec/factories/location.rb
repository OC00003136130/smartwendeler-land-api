require 'factory_bot'

FactoryBot.define do
  factory :location do
    project
    longitude { Faker::Number.positive(from: 7.053, to: 7.239) }
    latitude { Faker::Number.positive(from: 49.451, to: 49.539) }
  end
end
