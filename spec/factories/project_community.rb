require 'factory_bot'

FactoryBot.define do
  factory :project_community do
    project
    community
  end
end
