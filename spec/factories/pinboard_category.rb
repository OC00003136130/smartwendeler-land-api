require 'factory_bot'

FactoryBot.define do
  factory :pinboard_category do
    category
    pinboard
  end
end
