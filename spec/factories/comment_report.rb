require 'factory_bot'

FactoryBot.define do
  factory :comment_report do
    organization
    user
    comment
  end
end
