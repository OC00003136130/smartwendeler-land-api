require 'factory_bot'

FactoryBot.define do
  factory :tooltip do
    organization
    name { Faker::Lorem.paragraph }
    content { Faker::Lorem.paragraph }
  end
end
