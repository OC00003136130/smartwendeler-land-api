require 'factory_bot'

FactoryBot.define do
  factory :tag do
    organization
    name { Faker::Lorem.paragraph }
    scope { 'care_facility' }
  end
end
