require 'factory_bot'

FactoryBot.define do
  factory :project_rating do
    project
    user
    score { [-1, 1].sample }
  end
end
