require 'factory_bot'

FactoryBot.define do
  factory :community do
    organization
    name { Faker::Company.name }
  end
end
