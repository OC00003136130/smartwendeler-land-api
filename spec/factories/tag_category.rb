require 'factory_bot'

FactoryBot.define do
  factory :tag_category do
    organization
    name { Faker::Lorem.paragraph }
  end
end
