require 'factory_bot'

FactoryBot.define do
  factory :gamification_trophy do
    gamification_reward
    user
  end
end
