require 'factory_bot'

FactoryBot.define do
  factory :gamification_game do
    organization
    name { Faker::Company.name }
    kind { GamificationGame.kinds.keys.sample }
  end
end
