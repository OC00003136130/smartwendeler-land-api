require 'factory_bot'

FactoryBot.define do
  factory :pinboard_idea_rating do
    pinboard_idea
    user
    score { [1, 2, 3, 4].sample }
  end
end
