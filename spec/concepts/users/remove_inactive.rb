require 'rails_helper'

describe 'Users' do
  before(:all) do
    User.destroy_all
    @organization = FactoryBot.create(:organization)
    @user1 = FactoryBot.create(:user, organization: @organization, last_seen: nil, created_at: 23.hours.ago)
    @user2 = FactoryBot.create(:user, organization: @organization, last_seen: 2.days.ago)
    @user3 = FactoryBot.create(:user, organization: @organization, last_seen: nil, created_at: 12.days.ago)
    @user4 = FactoryBot.create(:user, organization: @organization, last_seen: nil, created_at: 49.hours.ago)
    @user5 = FactoryBot.create(:user, organization: @organization, last_seen: nil, created_at: 7.hours.ago)
  end

  context 'remove inactive users' do
    before(:all) do
      @result = User::Operations::RemoveInactive.()
    end

    it 'should remove never seen users created longer than 48 hours ago' do
      @organization.users.reload
      user_ids = @organization.users.pluck(:id)
      expect(user_ids).to include(@user1.id)
      expect(user_ids).to include(@user2.id)
      expect(user_ids).to_not include(@user3.id)
      expect(user_ids).to_not include(@user4.id)
      expect(user_ids).to include(@user5.id)
    end
  end
end