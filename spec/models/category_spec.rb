require 'rails_helper'

describe 'Category Model' do
  before(:all) do
    @organization = FactoryBot.create(:organization)
    @category = FactoryBot.create(:category, organization: @organization, scope: 'pinboard')
    @pinboard = FactoryBot.create(:pinboard, organization: @organization)
    @pinboard_idea1 = FactoryBot.create(:pinboard_idea, pinboard: @pinboard, status: 'is_checked')
    @pinboard_idea2 = FactoryBot.create(:pinboard_idea, pinboard: @pinboard, status: 'confirmed')
    @pinboard_idea3 = FactoryBot.create(:pinboard_idea, pinboard: @pinboard, status: 'confirmed')

    @pinboard.categories << @category
    @pinboard.save!
  end

  context 'custom methods' do
    it 'should correctly count pinboards_with_confirmed_ideas_count' do
      expect(@category.pinboards_with_confirmed_ideas_count).to eq 2
    end
  end
end

