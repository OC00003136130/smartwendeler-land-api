class SpreadsheetParseService

  attr_reader :doc

  def initialize(filepath, extension = nil)
    if (extension == 'csv')
      # use ; as default
      # TODO: support ,
      @doc = Roo::Spreadsheet.open(filepath, extension: extension, csv_options: { col_sep: ";" })
    elsif (extension)
      @doc = Roo::Spreadsheet.open(filepath, extension: extension)
    else
      @doc = Roo::Spreadsheet.open filepath
    end
  end

  # Returns the number of data entries without the headings
  def get_data_count
    return 0 if last_row_id.nil?
    last_row_id - 1
  end

  def get_headings
    return doc.sheet(0).row(1)
  end

  def get_data
    return [] if get_data_count < 1

    last_row_id = doc.sheet(0).last_row
    result = []
    doc.sheet(0).each_with_index do |row, i|
      stripped_row = row.map do |row_entry|
        row_entry.to_s.strip unless row_entry.nil?
      end
      result << stripped_row if i != 0 && i != last_row_id
    end
    result
  end

  # Returns an array of hashes representing each xls row
  def to_array_of_hashes(assigned_headings = nil)
    return [] if get_data_count < 1

    headings = get_headings

    get_data.map do |d|
      hash_entry = { data: {} }
      d.map.with_index do |value, index|
        # for pre assigned headings, different logic
        if assigned_headings
          if assigned_headings[headings[index]].present?
            hash_entry[assigned_headings[headings[index]]] = value unless headings[index].nil?
          else
            # for anthing not found we use data (meta data)
            hash_entry[:data][headings[index].to_sym] = value unless headings[index].nil?
          end
        else
          hash_entry[headings[index].to_sym] = value unless headings[index].nil?
        end
      end
      hash_entry
    end
  end

  private

  def last_row_id
    doc.sheet(0).last_row
  end

end
