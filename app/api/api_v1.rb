class ApiV1 < Grape::API
  before do
    header "Access-Control-Allow-Credentials", true
    header "Access-Control-Allow-Origin", "*"
    header "Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    header "Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE, OPTIONS"
  end

  format :json

  mount Authentication::Api
  mount Organizations::Api
  mount Users::Api
  mount Projects::Api
  mount Comments::Api
  mount Categories::Api
  mount Locations::Api
  mount Milestones::Api
  mount Messages::Api
  mount CommentReports::Api
  mount Tooltips::Api
  mount Polls::Api
  mount PollQuestions::Api
  mount PollAnswers::Api
  mount Communities::Api
  mount Notifications::Api
  mount Logs::Api
  mount GamificationGames::Api
  mount GamificationRewards::Api
  mount GamificationTrophies::Api
  mount CareFacilities::Api
  mount Statistics::Api
  mount Pinboards::Api
  mount PinboardIdeas::Api
  mount Tags::Api
  mount TagCategories::Api

  mount PublicRestricted::Api
  mount Public::Api
end
