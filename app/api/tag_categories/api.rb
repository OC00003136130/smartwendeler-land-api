class TagCategories::Api < Api::Base
  include Trailblazer::Endpoint::Grape::Controller

  def self.assign_policy(ctx, policy:, domain_ctx:, **)
    domain_ctx[:policy] = policy
  end

  # You have to define default behavior.
  def self.options_for_block_options(ctx, controller:, **)
    {
      success_block:          success_block = ->(ctx, endpoint_ctx:, **) { controller.status(endpoint_ctx[:status]); ctx['json'] }, # FIXME: {controller} comes from where?
      failure_block:          success_block,
      protocol_failure_block: success_block
    }
  end

  def self.options_for_domain_ctx(ctx, controller:, path:, representer_class:, **)
    {
      params:   controller.params,

      path: path,
      representer_class: representer_class,
    }
  end

  def self.options_for_endpoint(ctx, controller:, operation_class:, **)
    {
      request:         controller.request,
      operation_class: operation_class,
    }
  end

  directive :options_for_block_options, method(:options_for_block_options)
  directive :options_for_domain_ctx, method(:options_for_domain_ctx)
  directive :options_for_endpoint, method(:options_for_endpoint)

  endpoint protocol: App::Endpoint::Protocol, adapter: App::Endpoint::Adapter
  endpoint TagCategory::Operations::Create
  endpoint TagCategory::Operations::Show do {Output(:not_found) => End(:not_found)} end
  endpoint TagCategory::Operations::Update do {Output(:not_found) => End(:not_found)} end
  endpoint TagCategory::Operations::Delete do {Output(:not_found) => End(:not_found)} end
  endpoint TagCategory::Operations::List, adapter: App::Endpoint::Adapter::List do
    step TagCategories::Api.method(:assign_policy), before: :domain_activity
    {}
  end

  helpers do
    def endpoint(name, **action_options)
      ctx = super(name, operation_class: name, **action_options)
      ctx[:json]
    end
  end

  resource :tag_categories do
    desc 'Retrieve a list of tag_categories'
    get do
      endpoint(TagCategory::Operations::List.to_s, path: 'v1/tags', representer_class: TagCategory::Representers::Full)
    end

    desc 'Create a tag_category'
    post do
      endpoint(TagCategory::Operations::Create.to_s, path: 'v1/tags', representer_class: TagCategory::Representers::Full, success_status: 201)
    end

    route_param :tag_category_id do
      desc 'Retrieve a tag_category'
      get do
        endpoint(TagCategory::Operations::Show.to_s, path: 'v1/tags', representer_class: TagCategory::Representers::Full)
      end

      desc 'Update a tag_category'
      put do
        endpoint(TagCategory::Operations::Update.to_s, path: 'v1/tags', representer_class: TagCategory::Representers::Full)
      end

      desc 'Delete a tag_category'
      delete do
        endpoint(TagCategory::Operations::Delete.to_s, path: 'v1/tags', representer_class: TagCategory::Representers::Full, success_status: 204)
      end
    end
  end
end
