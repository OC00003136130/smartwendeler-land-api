class CareFacilities::Api < Api::Base
  include Trailblazer::Endpoint::Grape::Controller

  def self.assign_policy(ctx, policy:, domain_ctx:, **)
    domain_ctx[:policy] = policy
  end

  # You have to define default behavior.
  def self.options_for_block_options(ctx, controller:, **)
    {
      success_block:          success_block = ->(ctx, endpoint_ctx:, **) { controller.status(endpoint_ctx[:status]); ctx['json'] }, # FIXME: {controller} comes from where?
      failure_block:          success_block,
      protocol_failure_block: success_block
    }
  end

  def self.options_for_domain_ctx(ctx, controller:, path:, representer_class:, **)
    {
      params:   controller.params,

      path: path,
      representer_class: representer_class,
    }
  end

  def self.options_for_endpoint(ctx, controller:, operation_class:, **)
    {
      request:         controller.request,
      operation_class: operation_class,
    }
  end

  directive :options_for_block_options, method(:options_for_block_options)
  directive :options_for_domain_ctx, method(:options_for_domain_ctx)
  directive :options_for_endpoint, method(:options_for_endpoint)

  endpoint protocol: App::Endpoint::Protocol, adapter: App::Endpoint::Adapter
  endpoint CareFacility::Operations::Create
  endpoint CareFacility::Operations::AttachImage do {Output(:not_found) => End(:not_found)} end
  endpoint CareFacility::Operations::PurgeImage do {Output(:not_found) => End(:not_found)} end
  endpoint CareFacility::Operations::AttachDocument do {Output(:not_found) => End(:not_found)} end
  endpoint CareFacility::Operations::PurgeDocument do {Output(:not_found) => End(:not_found)} end
  endpoint CareFacility::Operations::Show do {Output(:not_found) => End(:not_found)} end
  endpoint CareFacility::Operations::Update do {Output(:not_found) => End(:not_found)} end
  endpoint CareFacility::Operations::Delete do {Output(:not_found) => End(:not_found)} end
  endpoint CareFacility::Operations::List, adapter: App::Endpoint::Adapter::List do
    step CareFacilities::Api.method(:assign_policy), before: :domain_activity
    {}
  end

  helpers do
    def endpoint(name, **action_options)
      ctx = super(name, operation_class: name, **action_options)
      ctx[:json]
    end
  end

  resource :care_facilities do
    desc 'Retrieve a list of care_facilities'
    get do
      endpoint(CareFacility::Operations::List.to_s, path: 'v1/care_facilities', representer_class: CareFacility::Representers::Full)
    end

    desc 'Create a care_facility'
    post do
      endpoint(CareFacility::Operations::Create.to_s, path: 'v1/care_facilities', representer_class: CareFacility::Representers::Full, success_status: 201)
    end

    route_param :care_facility_id do
      desc 'Retrieve a care_facility'
      get do
        endpoint(CareFacility::Operations::Show.to_s, path: 'v1/care_facilities', representer_class: CareFacility::Representers::Full)
      end

      desc 'Update a care_facility'
      put do
        endpoint(CareFacility::Operations::Update.to_s, path: 'v1/care_facilities', representer_class: CareFacility::Representers::Full)
      end

      desc 'delete a care_facility'
      delete do
        endpoint(CareFacility::Operations::Delete.to_s, path: 'v1/care_facilities', representer_class: CareFacility::Representers::Full, success_status: 204)
      end

      namespace :documents do
        desc 'attach a new document'
        post do
          endpoint(CareFacility::Operations::AttachDocument.to_s, path: 'v1/care_facilities', representer_class: CareFacility::Representers::Full, success_status: 201)
        end

        route_param :signed_id do
          desc 'purge a document'
          delete do
            endpoint(CareFacility::Operations::PurgeDocument.to_s, path: 'v1/care_facilities', representer_class: CareFacility::Representers::Full, success_status: 204)
          end
        end
      end

      namespace :images do
        desc 'attach a new image'
        post do
          endpoint(CareFacility::Operations::AttachImage.to_s, path: 'v1/care_facilities', representer_class: CareFacility::Representers::Full, success_status: 201)
        end

        route_param :signed_id do
          desc 'purge an image'
          delete do
            endpoint(CareFacility::Operations::PurgeImage.to_s, path: 'v1/care_facilities', representer_class: CareFacility::Representers::Full, success_status: 204)
          end
        end
      end
    end
  end
end
