class PinboardIdeas::Api < Api::Base
  include Trailblazer::Endpoint::Grape::Controller

  def self.assign_policy(ctx, policy:, domain_ctx:, **)
    domain_ctx[:policy] = policy
  end

  # You have to define default behavior.
  def self.options_for_block_options(ctx, controller:, **)
    {
      success_block:          success_block = ->(ctx, endpoint_ctx:, **) { controller.status(endpoint_ctx[:status]); ctx['json'] }, # FIXME: {controller} comes from where?
      failure_block:          success_block,
      protocol_failure_block: success_block
    }
  end

  def self.options_for_domain_ctx(ctx, controller:, path:, representer_class:, **)
    {
      params:   controller.params,

      path: path,
      representer_class: representer_class,
    }
  end

  def self.options_for_endpoint(ctx, controller:, operation_class:, **)
    {
      request:         controller.request,
      operation_class: operation_class,
    }
  end

  directive :options_for_block_options, method(:options_for_block_options)
  directive :options_for_domain_ctx, method(:options_for_domain_ctx)
  directive :options_for_endpoint, method(:options_for_endpoint)

  endpoint protocol: App::Endpoint::Protocol, adapter: App::Endpoint::Adapter
  endpoint PinboardIdea::Operations::Upvote do {Output(:not_found) => End(:not_found)} end
  endpoint PinboardIdea::Operations::RemoveVote do {Output(:not_found) => End(:not_found)} end
  endpoint PinboardIdea::Operations::Create do {Output(:not_found) => End(:not_found)} end
  endpoint PinboardIdea::Operations::Show do {Output(:not_found) => End(:not_found)} end
  endpoint PinboardIdea::Operations::Update do {Output(:not_found) => End(:not_found)} end
  endpoint PinboardIdea::Operations::Delete do {Output(:not_found) => End(:not_found)} end
  endpoint PinboardIdea::Operations::List, adapter: App::Endpoint::Adapter::List do
    step PinboardIdeas::Api.method(:assign_policy), before: :domain_activity
    {Output(:not_found) => End(:not_found)}
  end

  helpers do
    def endpoint(name, **action_options)
      ctx = super(name, operation_class: name, **action_options)
      ctx[:json]
    end
  end

  resource :pinboard_ideas do
    route_param 'pinboard' do
      route_param :pinboard_id do
        desc 'Retrieve a list of pinboard_ideas for a pinboard'
        get do
          endpoint(PinboardIdea::Operations::List.to_s, path: 'v1/pinboard_ideas', representer_class: PinboardIdea::Representers::Full)
        end

        desc 'Create a pinboard_idea for a pinboard'
        post do
          endpoint(PinboardIdea::Operations::Create.to_s, path: 'v1/pinboard_ideas', representer_class: PinboardIdea::Representers::Full, success_status: 201)
        end
      end
    end

    route_param :pinboard_idea_id do
      desc 'Retrieve a pinboard_idea'
      get do
        endpoint(PinboardIdea::Operations::Show.to_s, path: 'v1/pinboard_ideas', representer_class: PinboardIdea::Representers::Full)
      end

      desc 'Update a pinboard_idea'
      put do
        endpoint(PinboardIdea::Operations::Update.to_s, path: 'v1/pinboard_ideas', representer_class: PinboardIdea::Representers::Full)
      end

      desc 'Delete a pinboard_idea'
      delete do
        endpoint(PinboardIdea::Operations::Delete.to_s, path: 'v1/pinboard_ideas', representer_class: PinboardIdea::Representers::Full, success_status: 204)
      end

      desc 'upvote a pinboard_idea'
      put 'upvote' do
        endpoint(PinboardIdea::Operations::Upvote.to_s, path: 'v1/projects', representer_class: PinboardIdea::Representers::Full)
      end

      desc 'remove a vote'
      delete 'remove_vote' do
        endpoint(PinboardIdea::Operations::RemoveVote.to_s, path: 'v1/projects', representer_class: PinboardIdea::Representers::Full, success_status: 204)
      end
    end
  end
end
