class Comments::Api < Api::Base
  include Trailblazer::Endpoint::Grape::Controller

  def self.assign_policy(ctx, policy:, domain_ctx:, **)
    domain_ctx[:policy] = policy
  end

  # You have to define default behavior.
  def self.options_for_block_options(ctx, controller:, **)
    {
      success_block:          success_block = ->(ctx, endpoint_ctx:, **) { controller.status(endpoint_ctx[:status]); ctx['json'] }, # FIXME: {controller} comes from where?
      failure_block:          success_block,
      protocol_failure_block: success_block
    }
  end

  def self.options_for_domain_ctx(ctx, controller:, path:, representer_class:, **)
    {
      params:   controller.params,

      path: path,
      representer_class: representer_class,
    }
  end

  def self.options_for_endpoint(ctx, controller:, operation_class:, **)
    {
      request:         controller.request,
      operation_class: operation_class,
    }
  end

  directive :options_for_block_options, method(:options_for_block_options)
  directive :options_for_domain_ctx, method(:options_for_domain_ctx)
  directive :options_for_endpoint, method(:options_for_endpoint)

  endpoint protocol: App::Endpoint::Protocol, adapter: App::Endpoint::Adapter
  endpoint Comment::Operations::Create do {Output(:not_found) => End(:not_found)} end
  endpoint Comment::Operations::Show do {Output(:not_found) => End(:not_found)} end
  endpoint Comment::Operations::Update do {Output(:not_found) => End(:not_found)} end
  endpoint Comment::Operations::Upvote do {Output(:not_found) => End(:not_found)} end
  endpoint Comment::Operations::Downvote do {Output(:not_found) => End(:not_found)} end
  endpoint Comment::Operations::RemoveVote do {Output(:not_found) => End(:not_found)} end
  endpoint Comment::Operations::Delete do {Output(:not_found) => End(:not_found)} end
  endpoint Comment::Operations::ListAll, adapter: App::Endpoint::Adapter::List
  endpoint Comment::Operations::List, adapter: App::Endpoint::Adapter::List do
    step Comments::Api.method(:assign_policy), before: :domain_activity
    {Output(:not_found) => End(:not_found)}
  end
  endpoint Comment::Operations::ListReplies, adapter: App::Endpoint::Adapter::List do
    step Comments::Api.method(:assign_policy), before: :domain_activity
    {Output(:not_found) => End(:not_found)}
  end

  helpers do
    def endpoint(name, **action_options)
      ctx = super(name, operation_class: name, **action_options)
      ctx[:json]
    end
  end

  resource :comments do
    desc 'Retrieve a list of all comments'
    get do
      endpoint(Comment::Operations::ListAll.to_s, path: 'v1/comments', representer_class: Comment::Representers::Full)
    end
    route_param :comment_id do
      desc 'Retrieve a list of comment replies'
      get 'replies' do
        endpoint(Comment::Operations::ListReplies.to_s, path: 'v1/comments', representer_class: Comment::Representers::Full)
      end
    end
    
    route_param 'project' do
      route_param :project_id do
        desc 'Retrieve a list of comments for a project'
        get do
          endpoint(Comment::Operations::List.to_s, path: 'v1/comments', representer_class: Comment::Representers::Full)
        end

        desc 'Create a comment in a project'
        post do
          endpoint(Comment::Operations::Create.to_s, path: 'v1/comments', representer_class: Comment::Representers::Full, success_status: 201)
        end
      end
    end

    route_param :comment_id do
      desc 'Retrieve a comment'
      get do
        endpoint(Comment::Operations::Show.to_s, path: 'v1/comments', representer_class: Comment::Representers::Full)
      end

      desc 'Update a comment'
      put do
        endpoint(Comment::Operations::Update.to_s, path: 'v1/comments', representer_class: Comment::Representers::Full)
      end

      desc 'Delete a comment'
      delete do
        endpoint(Comment::Operations::Delete.to_s, path: 'v1/comments', representer_class: Comment::Representers::Full, success_status: 204)
      end

      desc 'upvote a comment'
      put 'upvote' do
        endpoint(Comment::Operations::Upvote.to_s, path: 'v1/comments', representer_class: Comment::Representers::Full)
      end

      desc 'downvote a comment'
      put 'downvote' do
        endpoint(Comment::Operations::Downvote.to_s, path: 'v1/comments', representer_class: Comment::Representers::Full)
      end

      desc 'remove a vote'
      delete 'remove_vote' do
        endpoint(Comment::Operations::RemoveVote.to_s, path: 'v1/comments', representer_class: Comment::Representers::Full, success_status: 204)
      end
    end
  end
end
