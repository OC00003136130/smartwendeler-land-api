class CommentReports::Api < Api::Base
  include Trailblazer::Endpoint::Grape::Controller

  def self.assign_policy(ctx, policy:, domain_ctx:, **)
    domain_ctx[:policy] = policy
  end

  # You have to define default behavior.
  def self.options_for_block_options(ctx, controller:, **)
    {
      success_block:          success_block = ->(ctx, endpoint_ctx:, **) { controller.status(endpoint_ctx[:status]); ctx['json'] }, # FIXME: {controller} comes from where?
      failure_block:          success_block,
      protocol_failure_block: success_block
    }
  end

  def self.options_for_domain_ctx(ctx, controller:, path:, representer_class:, **)
    {
      params:   controller.params,

      path: path,
      representer_class: representer_class,
    }
  end

  def self.options_for_endpoint(ctx, controller:, operation_class:, **)
    {
      request:         controller.request,
      operation_class: operation_class,
    }
  end

  directive :options_for_block_options, method(:options_for_block_options)
  directive :options_for_domain_ctx, method(:options_for_domain_ctx)
  directive :options_for_endpoint, method(:options_for_endpoint)

  endpoint protocol: App::Endpoint::Protocol, adapter: App::Endpoint::Adapter
  endpoint CommentReport::Operations::Create do {Output(:not_found) => End(:not_found)} end
  endpoint CommentReport::Operations::Show do {Output(:not_found) => End(:not_found)} end
  endpoint CommentReport::Operations::Delete do {Output(:not_found) => End(:not_found)} end
  endpoint CommentReport::Operations::List, adapter: App::Endpoint::Adapter::List do
    step CommentReports::Api.method(:assign_policy), before: :domain_activity
    {}
  end

  helpers do
    def endpoint(name, **action_options)
      ctx = super(name, operation_class: name, **action_options)
      ctx[:json]
    end
  end

  resource :comment_reports do
    desc 'Retrieve a list of comment_reports'
    get do
      endpoint(CommentReport::Operations::List.to_s, path: 'v1/comment_reports', representer_class: CommentReport::Representers::Full)
    end

    route_param 'comments' do
      route_param :comment_id do
        desc 'Create a comment_report for a comment'
        post do
          endpoint(CommentReport::Operations::Create.to_s, path: 'v1/milestones', representer_class: CommentReport::Representers::Full, success_status: 201)
        end
      end
    end

    route_param :comment_report_id do
      desc 'Retrieve a comment_report'
      get do
        endpoint(CommentReport::Operations::Show.to_s, path: 'v1/comment_reports', representer_class: CommentReport::Representers::Full)
      end

      desc 'Delete a comment_report'
      delete do
        endpoint(CommentReport::Operations::Delete.to_s, path: 'v1/comment_reports', representer_class: CommentReport::Representers::Full, success_status: 204)
      end
    end
  end
end
