class Projects::Api < Api::Base
  include Trailblazer::Endpoint::Grape::Controller

  def self.assign_policy(ctx, policy:, domain_ctx:, **)
    domain_ctx[:policy] = policy
  end

  # You have to define default behavior.
  def self.options_for_block_options(ctx, controller:, **)
    {
      success_block:          success_block = ->(ctx, endpoint_ctx:, **) { controller.status(endpoint_ctx[:status]); ctx['json'] }, # FIXME: {controller} comes from where?
      failure_block:          success_block,
      protocol_failure_block: success_block
    }
  end

  def self.options_for_domain_ctx(ctx, controller:, path:, representer_class:, **)
    {
      params:   controller.params,

      path: path,
      representer_class: representer_class,
    }
  end

  def self.options_for_endpoint(ctx, controller:, operation_class:, **)
    {
      request:         controller.request,
      operation_class: operation_class,
    }
  end

  directive :options_for_block_options, method(:options_for_block_options)
  directive :options_for_domain_ctx, method(:options_for_domain_ctx)
  directive :options_for_endpoint, method(:options_for_endpoint)

  endpoint protocol: App::Endpoint::Protocol, adapter: App::Endpoint::Adapter
  endpoint Project::Operations::Create
  endpoint Project::Operations::AttachImage do {Output(:not_found) => End(:not_found)} end
  endpoint Project::Operations::PurgeImage do {Output(:not_found) => End(:not_found)} end
  endpoint Project::Operations::Show do {Output(:not_found) => End(:not_found)} end
  endpoint Project::Operations::Update do {Output(:not_found) => End(:not_found)} end
  endpoint Project::Operations::Upvote do {Output(:not_found) => End(:not_found)} end
  endpoint Project::Operations::Downvote do {Output(:not_found) => End(:not_found)} end
  endpoint Project::Operations::RemoveVote do {Output(:not_found) => End(:not_found)} end
  endpoint Project::Operations::Delete do {Output(:not_found) => End(:not_found)} end
  endpoint Project::Operations::List, adapter: App::Endpoint::Adapter::List do
    step Projects::Api.method(:assign_policy), before: :domain_activity
    {}
  end

  helpers do
    def endpoint(name, **action_options)
      ctx = super(name, operation_class: name, **action_options)
      ctx[:json]
    end
  end

  resource :projects do
    desc 'Retrieve a list of projects'
    get do
      endpoint(Project::Operations::List.to_s, path: 'v1/projects', representer_class: Project::Representers::Full)
    end

    desc 'Create a project'
    post do
      endpoint(Project::Operations::Create.to_s, path: 'v1/projects', representer_class: Project::Representers::Full, success_status: 201)
    end

    route_param :project_id do
      desc 'Retrieve a project'
      get do
        endpoint(Project::Operations::Show.to_s, path: 'v1/projects', representer_class: Project::Representers::Full)
      end

      desc 'Update a project'
      put do
        endpoint(Project::Operations::Update.to_s, path: 'v1/projects', representer_class: Project::Representers::Full)
      end

      desc 'delete a project'
      delete do
        endpoint(Project::Operations::Delete.to_s, path: 'v1/projects', representer_class: Project::Representers::Full, success_status: 204)
      end

      desc 'upvote a project'
      put 'upvote' do
        endpoint(Project::Operations::Upvote.to_s, path: 'v1/projects', representer_class: Project::Representers::Full)
      end

      desc 'downvote a project'
      put 'downvote' do
        endpoint(Project::Operations::Downvote.to_s, path: 'v1/projects', representer_class: Project::Representers::Full)
      end

      desc 'remove a vote'
      delete 'remove_vote' do
        endpoint(Project::Operations::RemoveVote.to_s, path: 'v1/projects', representer_class: Project::Representers::Full, success_status: 204)
      end

      namespace :images do
        desc 'attach a new image'
        post do
          endpoint(Project::Operations::AttachImage.to_s, path: 'v1/projects', representer_class: Project::Representers::Full, success_status: 201)
        end

        route_param :signed_id do
          desc 'purge an image'
          delete do
            endpoint(Project::Operations::PurgeImage.to_s, path: 'v1/projects', representer_class: Project::Representers::Full, success_status: 204)
          end
        end
      end
    end
  end
end
