class Communities::Api < Api::Base
  include Trailblazer::Endpoint::Grape::Controller

  def self.assign_policy(ctx, policy:, domain_ctx:, **)
    domain_ctx[:policy] = policy
  end

  # You have to define default behavior.
  def self.options_for_block_options(ctx, controller:, **)
    {
      success_block:          success_block = ->(ctx, endpoint_ctx:, **) { controller.status(endpoint_ctx[:status]); ctx['json'] }, # FIXME: {controller} comes from where?
      failure_block:          success_block,
      protocol_failure_block: success_block
    }
  end

  def self.options_for_domain_ctx(ctx, controller:, path:, representer_class:, **)
    {
      params:   controller.params,

      path: path,
      representer_class: representer_class,
    }
  end

  def self.options_for_endpoint(ctx, controller:, operation_class:, **)
    {
      request:         controller.request,
      operation_class: operation_class,
    }
  end

  directive :options_for_block_options, method(:options_for_block_options)
  directive :options_for_domain_ctx, method(:options_for_domain_ctx)
  directive :options_for_endpoint, method(:options_for_endpoint)

  endpoint protocol: App::Endpoint::Protocol, adapter: App::Endpoint::Adapter
  endpoint Community::Operations::Create
  endpoint Community::Operations::Show do {Output(:not_found) => End(:not_found)} end
  endpoint Community::Operations::Update do {Output(:not_found) => End(:not_found)} end
  endpoint Community::Operations::Delete do {Output(:not_found) => End(:not_found)} end
  endpoint Community::Operations::List, adapter: App::Endpoint::Adapter::List do
    step Communities::Api.method(:assign_policy), before: :domain_activity
    {}
  end

  helpers do
    def endpoint(name, **action_options)
      ctx = super(name, operation_class: name, **action_options)
      ctx[:json]
    end
  end

  resource :communities do
    desc 'Retrieve a list of communities'
    get do
      endpoint(Community::Operations::List.to_s, path: 'v1/communities', representer_class: Community::Representers::Full)
    end

    desc 'Create a community'
    post do
      endpoint(Community::Operations::Create.to_s, path: 'v1/communities', representer_class: Community::Representers::Full, success_status: 201)
    end

    route_param :community_id do
      desc 'Retrieve a community'
      get do
        endpoint(Community::Operations::Show.to_s, path: 'v1/communities', representer_class: Community::Representers::Full)
      end

      desc 'Update a community'
      put do
        endpoint(Community::Operations::Update.to_s, path: 'v1/communities', representer_class: Community::Representers::Full)
      end

      desc 'delete a community'
      delete do
        endpoint(Community::Operations::Delete.to_s, path: 'v1/communities', representer_class: Community::Representers::Full, success_status: 204)
      end
    end
  end
end
