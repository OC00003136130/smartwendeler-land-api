class Public::Api < Grape::API
  resource :public do
    resource :users do
      desc 'Request a reset password for a user'
      post '/reset-password' do
        result = User::Operations::ResetPassword.(params: params, path: 'v1/users', request: request,
          representer_class: User::Representers::Full)
        status result['http_status']
        result['json']
      end

      desc 'Register a new user'
      post '/register/:register_token' do
        result = User::Operations::Register.(params: params, path: 'v1/users', request: request,
          representer_class: User::Representers::Full)
        status result['http_status']
        result['json']
      end

      desc 'Register a new user who owns a care facility'
      post '/register_with_facility/:register_token' do
        result = User::Operations::RegisterWithFacility.(params: params, path: 'v1/users', request: request,
          representer_class: User::Representers::Full)
        status result['http_status']
        result['json']
      end

      desc 'Find a user by password token'
      get '/find-by-token/:password_token' do
        result = User::Operations::FindByToken.(params: params, path: 'v1/users', request: request,
          representer_class: User::Representers::Full)
        status result['http_status']
        result['json']
      end

      desc 'Updates a password with a password token'
      put '/update-password/:password_token' do
        result = User::Operations::UpdatePasswordByToken.(params: params, path: 'v1/users', request: request,
          representer_class: User::Representers::Full)
        status result['http_status']
        result['json']
      end

      desc 'Find a user by onboarding token'
      get '/find-by-onboarding-token/:onboarding_token' do
        result = User::Operations::FindByOnboardingToken.(params: params, path: 'v1/users', request: request,
          representer_class: User::Representers::Full)
        status result['http_status']
        result['json']
      end

      desc 'Onboarding a user which has been imported on health scope'
      post '/onboarding-health/:onboarding_token' do
        result = User::Operations::OnboardingHealth.(params: params, path: 'v1/users', request: request,
          representer_class: User::Representers::Full)
        status result['http_status']
        result['json']
      end
    end
  end
end
