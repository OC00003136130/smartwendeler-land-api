class PollAnswers::Api < Api::Base
  include Trailblazer::Endpoint::Grape::Controller

  def self.assign_policy(ctx, policy:, domain_ctx:, **)
    domain_ctx[:policy] = policy
  end

  # You have to define default behavior.
  def self.options_for_block_options(ctx, controller:, **)
    {
      success_block:          success_block = ->(ctx, endpoint_ctx:, **) { controller.status(endpoint_ctx[:status]); ctx['json'] }, # FIXME: {controller} comes from where?
      failure_block:          success_block,
      protocol_failure_block: success_block
    }
  end

  def self.options_for_domain_ctx(ctx, controller:, path:, representer_class:, **)
    {
      params:   controller.params,

      path: path,
      representer_class: representer_class,
    }
  end

  def self.options_for_endpoint(ctx, controller:, operation_class:, **)
    {
      request:         controller.request,
      operation_class: operation_class,
    }
  end

  directive :options_for_block_options, method(:options_for_block_options)
  directive :options_for_domain_ctx, method(:options_for_domain_ctx)
  directive :options_for_endpoint, method(:options_for_endpoint)

  endpoint protocol: App::Endpoint::Protocol, adapter: App::Endpoint::Adapter
  endpoint PollAnswer::Operations::CreateUpdate do {Output(:not_found) => End(:not_found)} end
  endpoint PollAnswer::Operations::List, adapter: App::Endpoint::Adapter::List do
    step PollAnswers::Api.method(:assign_policy), before: :domain_activity
    {Output(:not_found) => End(:not_found)}
  end

  helpers do
    def endpoint(name, **action_options)
      ctx = super(name, operation_class: name, **action_options)
      ctx[:json]
    end
  end

  resource :poll_answers do
    route_param 'poll_question' do
      route_param :poll_question_id do
        desc 'Retrieve a list of poll_answers for a poll_question'
        get do
          endpoint(PollAnswer::Operations::List.to_s, path: 'v1/poll_questions', representer_class: PollAnswer::Representers::Full)
        end

        desc 'Creates or updates a poll_answer for a poll_question'
        post do
          endpoint(PollAnswer::Operations::CreateUpdate.to_s, path: 'v1/poll_questions', representer_class: PollAnswer::Representers::Full, success_status: 201)
        end
      end
    end
  end
end
