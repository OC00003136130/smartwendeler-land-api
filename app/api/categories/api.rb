class Categories::Api < Api::Base
  include Trailblazer::Endpoint::Grape::Controller

  def self.assign_policy(ctx, policy:, domain_ctx:, **)
    domain_ctx[:policy] = policy
  end

  # You have to define default behavior.
  def self.options_for_block_options(ctx, controller:, **)
    {
      success_block:          success_block = ->(ctx, endpoint_ctx:, **) { controller.status(endpoint_ctx[:status]); ctx['json'] }, # FIXME: {controller} comes from where?
      failure_block:          success_block,
      protocol_failure_block: success_block
    }
  end

  def self.options_for_domain_ctx(ctx, controller:, path:, representer_class:, **)
    {
      params:   controller.params,

      path: path,
      representer_class: representer_class,
    }
  end

  def self.options_for_endpoint(ctx, controller:, operation_class:, **)
    {
      request:         controller.request,
      operation_class: operation_class,
    }
  end

  directive :options_for_block_options, method(:options_for_block_options)
  directive :options_for_domain_ctx, method(:options_for_domain_ctx)
  directive :options_for_endpoint, method(:options_for_endpoint)

  endpoint protocol: App::Endpoint::Protocol, adapter: App::Endpoint::Adapter
  endpoint Category::Operations::Create
  endpoint Category::Operations::CreateSubCategory do {Output(:not_found) => End(:not_found)} end
  endpoint Category::Operations::CreateSubSubCategory do {Output(:not_found) => End(:not_found)} end
  endpoint Category::Operations::Show do {Output(:not_found) => End(:not_found)} end
  endpoint Category::Operations::Update do {Output(:not_found) => End(:not_found)} end
  endpoint Category::Operations::Delete do {Output(:not_found) => End(:not_found)} end
  endpoint Category::Operations::ListSubCategories, adapter: App::Endpoint::Adapter::List do {Output(:not_found) => End(:not_found)} end
  endpoint Category::Operations::ListSubSubCategories, adapter: App::Endpoint::Adapter::List do {Output(:not_found) => End(:not_found)} end
  endpoint Category::Operations::List, adapter: App::Endpoint::Adapter::List do
    step Categories::Api.method(:assign_policy), before: :domain_activity
    {}
  end

  helpers do
    def endpoint(name, **action_options)
      ctx = super(name, operation_class: name, **action_options)
      ctx[:json]
    end
  end

  resource :categories do
    desc 'Retrieve a list of categories'
    get do
      endpoint(Category::Operations::List.to_s, path: 'v1/categories', representer_class: Category::Representers::Full)
    end

    desc 'Create a category'
    post do
      endpoint(Category::Operations::Create.to_s, path: 'v1/categories', representer_class: Category::Representers::Full, success_status: 201)
    end

    route_param :category_id do
      desc 'Retrieve a category'
      get do
        endpoint(Category::Operations::Show.to_s, path: 'v1/categories', representer_class: Category::Representers::Full)
      end

      desc 'Update a category'
      put do
        endpoint(Category::Operations::Update.to_s, path: 'v1/categories', representer_class: Category::Representers::Full)
      end

      desc 'delete a category'
      delete do
        endpoint(Category::Operations::Delete.to_s, path: 'v1/categories', representer_class: Category::Representers::Full, success_status: 204)
      end

      desc 'Create a sub category for a category'
      post do
        endpoint(Category::Operations::CreateSubCategory.to_s, path: 'v1/categories', representer_class: Category::Representers::Full, success_status: 201)
      end

      resource :sub_categories do
        desc 'Retrieve sub categories for a category'
        get do
          endpoint(Category::Operations::ListSubCategories.to_s, path: 'v1/categories', representer_class: Category::Representers::Full)
        end

        route_param :sub_category_id do
          desc 'Retrieve sub sub categories for a sub category'
          get do
            endpoint(Category::Operations::ListSubSubCategories.to_s, path: 'v1/categories', representer_class: Category::Representers::Full)
          end

          desc 'Create a sub sub category for a sub category'
          post do
            endpoint(Category::Operations::CreateSubSubCategory.to_s, path: 'v1/categories', representer_class: Category::Representers::Full, success_status: 201)
          end
        end
      end      
    end
  end
end
