class Locations::Api < Api::Base
  include Trailblazer::Endpoint::Grape::Controller

  def self.assign_policy(ctx, policy:, domain_ctx:, **)
    domain_ctx[:policy] = policy
  end

  # You have to define default behavior.
  def self.options_for_block_options(ctx, controller:, **)
    {
      success_block:          success_block = ->(ctx, endpoint_ctx:, **) { controller.status(endpoint_ctx[:status]); ctx['json'] }, # FIXME: {controller} comes from where?
      failure_block:          success_block,
      protocol_failure_block: success_block
    }
  end

  def self.options_for_domain_ctx(ctx, controller:, path:, representer_class:, **)
    {
      params:   controller.params,

      path: path,
      representer_class: representer_class,
    }
  end

  def self.options_for_endpoint(ctx, controller:, operation_class:, **)
    {
      request:         controller.request,
      operation_class: operation_class,
    }
  end

  directive :options_for_block_options, method(:options_for_block_options)
  directive :options_for_domain_ctx, method(:options_for_domain_ctx)
  directive :options_for_endpoint, method(:options_for_endpoint)

  endpoint protocol: App::Endpoint::Protocol, adapter: App::Endpoint::Adapter
  endpoint Location::Operations::CreateForProject do {Output(:not_found) => End(:not_found)} end
  endpoint Location::Operations::CreateForCareFacility do {Output(:not_found) => End(:not_found)} end
  endpoint Location::Operations::Update do {Output(:not_found) => End(:not_found)} end
  endpoint Location::Operations::Delete do {Output(:not_found) => End(:not_found)} end  

  helpers do
    def endpoint(name, **action_options)
      ctx = super(name, operation_class: name, **action_options)
      ctx[:json]
    end
  end

  resource :locations do
    resource 'care_facility' do
      route_param :care_facility_id do
        desc 'Create a location in a care_facility'
        post do
          endpoint(Location::Operations::CreateForCareFacility.to_s, path: 'v1/locations', representer_class: Location::Representers::Full, success_status: 201)
        end
      end
    end
    resource 'project' do
      route_param :project_id do
        desc 'Create a location in a project'
        post do
          endpoint(Location::Operations::CreateForProject.to_s, path: 'v1/locations', representer_class: Location::Representers::Full, success_status: 201)
        end
      end
    end

    route_param :location_id do
      desc 'Update a location'
      put do
        endpoint(Location::Operations::Update.to_s, path: 'v1/locations', representer_class: Location::Representers::Full)
      end

      desc 'Delete a location'
      delete do
        endpoint(Location::Operations::Delete.to_s, path: 'v1/locations', representer_class: Location::Representers::Full, success_status: 204)
      end
    end
  end
end
