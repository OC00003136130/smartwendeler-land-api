class Messages::Api < Api::Base
  include Trailblazer::Endpoint::Grape::Controller

  def self.assign_policy(ctx, policy:, domain_ctx:, **)
    domain_ctx[:policy] = policy
  end

  # You have to define default behavior.
  def self.options_for_block_options(ctx, controller:, **)
    {
      success_block:          success_block = ->(ctx, endpoint_ctx:, **) { controller.status(endpoint_ctx[:status]); ctx['json'] }, # FIXME: {controller} comes from where?
      failure_block:          success_block,
      protocol_failure_block: success_block
    }
  end

  def self.options_for_domain_ctx(ctx, controller:, path:, representer_class:, **)
    {
      params:   controller.params,

      path: path,
      representer_class: representer_class,
    }
  end

  def self.options_for_endpoint(ctx, controller:, operation_class:, **)
    {
      request:         controller.request,
      operation_class: operation_class,
    }
  end

  directive :options_for_block_options, method(:options_for_block_options)
  directive :options_for_domain_ctx, method(:options_for_domain_ctx)
  directive :options_for_endpoint, method(:options_for_endpoint)

  endpoint protocol: App::Endpoint::Protocol, adapter: App::Endpoint::Adapter
  endpoint Message::Operations::Show do {Output(:not_found) => End(:not_found)} end
  endpoint Message::Operations::Update do {Output(:not_found) => End(:not_found)} end
  endpoint Message::Operations::Delete do {Output(:not_found) => End(:not_found)} end
  endpoint Message::Operations::List, adapter: App::Endpoint::Adapter::List do
    step Messages::Api.method(:assign_policy), before: :domain_activity
    {}
  end

  helpers do
    def endpoint(name, **action_options)
      ctx = super(name, operation_class: name, **action_options)
      ctx[:json]
    end
  end

  resource :messages do
    desc 'Retrieve a list of messages'
    get do
      endpoint(Message::Operations::List.to_s, path: 'v1/messages', representer_class: Message::Representers::Full)
    end

    route_param :message_id do
      desc 'Retrieve a message'
      get do
        endpoint(Message::Operations::Show.to_s, path: 'v1/messages', representer_class: Message::Representers::Full)
      end

      desc 'Update a message'
      put do
        endpoint(Message::Operations::Update.to_s, path: 'v1/messages', representer_class: Message::Representers::Full)
      end

      desc 'Delete a message'
      delete do
        endpoint(Message::Operations::Delete.to_s, path: 'v1/messages', representer_class: Message::Representers::Full, success_status: 204)
      end
    end
  end
end
