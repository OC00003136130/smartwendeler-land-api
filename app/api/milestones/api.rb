class Milestones::Api < Api::Base
  include Trailblazer::Endpoint::Grape::Controller

  def self.assign_policy(ctx, policy:, domain_ctx:, **)
    domain_ctx[:policy] = policy
  end

  # You have to define default behavior.
  def self.options_for_block_options(ctx, controller:, **)
    {
      success_block:          success_block = ->(ctx, endpoint_ctx:, **) { controller.status(endpoint_ctx[:status]); ctx['json'] }, # FIXME: {controller} comes from where?
      failure_block:          success_block,
      protocol_failure_block: success_block
    }
  end

  def self.options_for_domain_ctx(ctx, controller:, path:, representer_class:, **)
    {
      params:   controller.params,

      path: path,
      representer_class: representer_class,
    }
  end

  def self.options_for_endpoint(ctx, controller:, operation_class:, **)
    {
      request:         controller.request,
      operation_class: operation_class,
    }
  end

  directive :options_for_block_options, method(:options_for_block_options)
  directive :options_for_domain_ctx, method(:options_for_domain_ctx)
  directive :options_for_endpoint, method(:options_for_endpoint)

  endpoint protocol: App::Endpoint::Protocol, adapter: App::Endpoint::Adapter
  endpoint Milestone::Operations::Create do {Output(:not_found) => End(:not_found)} end
  endpoint Milestone::Operations::Show do {Output(:not_found) => End(:not_found)} end
  endpoint Milestone::Operations::Update do {Output(:not_found) => End(:not_found)} end
  endpoint Milestone::Operations::Delete do {Output(:not_found) => End(:not_found)} end
  endpoint Milestone::Operations::List, adapter: App::Endpoint::Adapter::List do
    step Milestones::Api.method(:assign_policy), before: :domain_activity
    {Output(:not_found) => End(:not_found)}
  end

  helpers do
    def endpoint(name, **action_options)
      ctx = super(name, operation_class: name, **action_options)
      ctx[:json]
    end
  end

  resource :milestones do
    route_param 'project' do
      route_param :project_id do
        desc 'Retrieve a list of milestones for a project'
        get do
          endpoint(Milestone::Operations::List.to_s, path: 'v1/milestones', representer_class: Milestone::Representers::Full)
        end

        desc 'Create a milestone in a project'
        post do
          endpoint(Milestone::Operations::Create.to_s, path: 'v1/milestones', representer_class: Milestone::Representers::Full, success_status: 201)
        end
      end
    end

    route_param :milestone_id do
      desc 'Retrieve a milestone'
      get do
        endpoint(Milestone::Operations::Show.to_s, path: 'v1/milestones', representer_class: Milestone::Representers::Full)
      end

      desc 'Update a milestone'
      put do
        endpoint(Milestone::Operations::Update.to_s, path: 'v1/milestones', representer_class: Milestone::Representers::Full)
      end

      desc 'Delete a milestone'
      delete do
        endpoint(Milestone::Operations::Delete.to_s, path: 'v1/milestones', representer_class: Milestone::Representers::Full, success_status: 204)
      end
    end
  end
end
