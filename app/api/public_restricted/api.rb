class PublicRestricted::Api < Grape::API
  include Trailblazer::Endpoint::Grape::Controller

  def self.assign_policy(ctx, policy:, domain_ctx:, **) # FIXME: use input?
    domain_ctx[:policy] = policy
  end

  # You have to define default behavior.
  def self.options_for_block_options(ctx, controller:, **)
    {
      success_block:          success_block = ->(ctx, endpoint_ctx:, **) { controller.status(endpoint_ctx[:status]); ctx['json'] }, # FIXME: {controller} comes from where?
      failure_block:          success_block,
      protocol_failure_block: success_block
    }
  end

  def self.options_for_domain_ctx(ctx, controller:, path:, representer_class:, **)
    {
      params: controller.params,

      path: path,
      representer_class: representer_class,
    }
  end

  def self.options_for_endpoint(ctx, controller:, operation_class:, **)
    {
      request:         controller.request,
      operation_class: operation_class,
    }
  end

  directive :options_for_block_options, method(:options_for_block_options)
  directive :options_for_domain_ctx, method(:options_for_domain_ctx)
  directive :options_for_endpoint, method(:options_for_endpoint)

  endpoint protocol: App::EndpointPublicRestricted::Protocol, adapter: App::EndpointPublicRestricted::Adapter
  endpoint Project::Operations::Public::List, adapter: App::EndpointPublicRestricted::Adapter::List
  endpoint Project::Operations::Public::Show do {Output(:not_found) => End(:not_found)} end
  endpoint CareFacility::Operations::Public::List, adapter: App::EndpointPublicRestricted::Adapter::List
  endpoint CareFacility::Operations::Public::Show do {Output(:not_found) => End(:not_found)} end
  endpoint Comment::Operations::Public::List, adapter: App::EndpointPublicRestricted::Adapter::List do {Output(:not_found) => End(:not_found)} end
  endpoint Category::Operations::Public::Show do {Output(:not_found) => End(:not_found)} end
  endpoint Category::Operations::Public::List, adapter: App::EndpointPublicRestricted::Adapter::List
  endpoint Category::Operations::Public::ListSubCategories, adapter: App::EndpointPublicRestricted::Adapter::List do {Output(:not_found) => End(:not_found)} end
  endpoint Category::Operations::Public::ListSubSubCategories, adapter: App::EndpointPublicRestricted::Adapter::List do {Output(:not_found) => End(:not_found)} end 
  endpoint Community::Operations::Public::List, adapter: App::EndpointPublicRestricted::Adapter::List
  endpoint Milestone::Operations::Public::List, adapter: App::EndpointPublicRestricted::Adapter::List do {Output(:not_found) => End(:not_found)} end
  endpoint Message::Operations::Public::Create
  endpoint Tooltip::Operations::Public::List, adapter: App::EndpointPublicRestricted::Adapter::List
  endpoint Tag::Operations::Public::List, adapter: App::EndpointPublicRestricted::Adapter::List
  endpoint TagCategory::Operations::Public::List, adapter: App::EndpointPublicRestricted::Adapter::List
  endpoint Pinboard::Operations::Public::List, adapter: App::EndpointPublicRestricted::Adapter::List
  endpoint Pinboard::Operations::Public::Show do {Output(:not_found) => End(:not_found)} end
  endpoint PinboardIdea::Operations::Public::List, adapter: App::EndpointPublicRestricted::Adapter::List do {Output(:not_found) => End(:not_found)} end
  endpoint PinboardIdea::Operations::Public::Show do {Output(:not_found) => End(:not_found)} end

  helpers do
    def endpoint(name, **action_options)
      ctx = super(name, operation_class: name, **action_options)
      ctx[:json]
    end
  end

  resource :public do
    namespace :projects do
      desc 'Lists projects without certain restrictions'
      get do
        endpoint(Project::Operations::Public::List.to_s, path: 'v1/public/projects', representer_class: Project::Representers::Full)
      end

      route_param :project_id do
        desc 'Retrieves a project without certain restrictions'
        get do
          endpoint(Project::Operations::Public::Show.to_s, path: 'v1/public/projects', representer_class: Project::Representers::Full)
        end

        namespace :comments do
          desc 'Lists comments for a project'
          get do
            endpoint(Comment::Operations::Public::List.to_s, path: 'v1/public/comments', representer_class: Comment::Representers::Full)
          end
        end

        namespace :milestones do
          desc 'Lists milestones for a project'
          get do
            endpoint(Milestone::Operations::Public::List.to_s, path: 'v1/public/milestones', representer_class: Milestone::Representers::Full)
          end
        end
      end
    end

    namespace :care_facilities do
      desc 'Lists care_facilities without certain restrictions'
      get do
        endpoint(CareFacility::Operations::Public::List.to_s, path: 'v1/public/care_facilities', representer_class: CareFacility::Representers::Full)
      end

      route_param :care_facility_id do
        desc 'Retrieves a care_facility without certain restrictions'
        get do
          endpoint(CareFacility::Operations::Public::Show.to_s, path: 'v1/public/care_facilities', representer_class: CareFacility::Representers::Full)
        end
      end
    end

    namespace :categories do
      desc 'Lists categories without certain restrictions'
      get do
        endpoint(Category::Operations::Public::List.to_s, path: 'v1/public/categories', representer_class: Category::Representers::Full)
      end

      route_param :category_id do
        desc 'Shows a specific category without certain restrictions'
        get do
          endpoint(Category::Operations::Public::Show.to_s, path: 'v1/public/categories', representer_class: Category::Representers::Full)
        end
        namespace 'sub_categories' do
          desc 'Lists sub categories without certain restrictions'
          get do
            endpoint(Category::Operations::Public::ListSubCategories.to_s, path: 'v1/public/categories', representer_class: Category::Representers::Full)
          end

          route_param :sub_category_id do
            namespace 'sub_sub_categories' do
              desc 'Lists sub sub categories without certain restrictions'
              get do
                endpoint(Category::Operations::Public::ListSubSubCategories.to_s, path: 'v1/public/categories', representer_class: Category::Representers::Full)
              end
            end
          end
        end
      end
    end

    namespace :tags do
      desc 'Lists tags without certain restrictions'
      get do
        endpoint(Tag::Operations::Public::List.to_s, path: 'v1/public/tags', representer_class: Tag::Representers::Full)
      end
    end

    namespace :tag_categories do
      desc 'Lists tag categories without certain restrictions'
      get do
        endpoint(TagCategory::Operations::Public::List.to_s, path: 'v1/public/tag_categories', representer_class: TagCategory::Representers::Full)
      end
    end

    namespace :pinboards do
      desc 'Lists pinboards without certain restrictions'
      get do
        endpoint(Pinboard::Operations::Public::List.to_s, path: 'v1/public/pinboards', representer_class: Pinboard::Representers::Full)
      end

      route_param :pinboard_id do
        desc 'Retrieves a specific pinboard without certain restrictions'
        get do
          endpoint(Pinboard::Operations::Public::Show.to_s, path: 'v1/public/pinboards', representer_class: Pinboard::Representers::Full)
        end
      end
    end

    namespace :pinboard_ideas do
      namespace 'pinboard' do
        route_param :pinboard_id do
          desc 'Lists pinboard ideas for a pinboard without certain restrictions'
          get do
            endpoint(PinboardIdea::Operations::Public::List.to_s, path: 'v1/public/pinboard_ideas', representer_class: PinboardIdea::Representers::Full)
          end
        end
      end

      route_param :pinboard_idea_id do
        desc 'Retrieve a pinboard_idea without certain restrictions'
        get do
          endpoint(PinboardIdea::Operations::Public::Show.to_s, path: 'v1/pinboard_ideas', representer_class: PinboardIdea::Representers::Full)
        end
      end
    end

    namespace :communities do
      desc 'Lists communities without certain restrictions'
      get do
        endpoint(Community::Operations::Public::List.to_s, path: 'v1/public/communities', representer_class: Community::Representers::Full)
      end
    end

    namespace :messages do
      desc 'Creates a new message to the system'
      post do
        endpoint(Message::Operations::Public::Create.to_s, path: 'v1/public/messages', representer_class: Message::Representers::Full, success_status: 201)
      end
    end

    namespace :tooltips do
      desc 'Lists tooltips'
      get do
        endpoint(Tooltip::Operations::Public::List.to_s, path: 'v1/public/tooltips', representer_class: Tooltip::Representers::Full)
      end
    end
  end
end
