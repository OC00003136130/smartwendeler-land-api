class UsersMailer < ApplicationMailer

  def send_invitation(user)
    @user = user
    @organization = user.organization
    @base_url = base_url(@organization)

    subject = "Deine Einladung zur Smart Wendeler Land Projektplattform"
    mail(to: @user.email, subject: subject)
  end

  def send_register(user)
    @user = user
    @organization = user.organization
    @base_url = base_url(@organization)

    subject = "Du hast dich erfolgreich zur Smart Wendeler Land Projektplattform registriert"
    mail(to: @user.email, subject: subject)
  end

  def send_register_with_facility(user)
    @user = user
    @organization = user.organization
    @base_url = base_url(@organization)

    subject = "Du hast dich erfolgreich zur Smart Wendeler Land Gesundheitsplattform registriert"
    mail(to: @user.email, subject: subject)
  end

  def send_confirm_facility_owner_for_health_scope(user)
    @user = user
    @organization = user.organization
    @base_url = base_url(@organization)

    subject = "Deine Einrichtung wurde freigeschaltet"
    mail(to: @user.email, subject: subject)
  end

  def send_password_reset(user)
    @user = user
    @organization = user.organization
    @base_url = base_url(@organization)
    @organization = user.organization

    subject = "Passwort zurücksetzen für die Smart Wendeler Land Plattform"
    mail(to: @user.email, subject: subject)
  end

  def send_notification(user, headline, text, cta_link)
    @user = user
    @headline = headline
    @text = text
    @cta_link = cta_link
    @organization = user.organization
    subject = @headline
    @base_url = base_url(@organization)

    mail(to: @user.email, subject: subject)
  end
end
