class NotificationsMailer < ApplicationMailer

  if Rails.env.production?
    EMAIL_TO = ENV['NOTIFICATION_EMAILS'].split(',')
  else
    EMAIL_TO = ['spam@pocket-rocket.io']
  end

  def send_register_notification(user)
    @user = user
    @organization = user.organization
    @base_url = base_url(@organization)
    subject = "Ein neuer Benutzer hat sich registriert"
    mail(to: EMAIL_TO, subject: subject)
  end

  def send_onboarding_notification(user)
    @user = user
    @organization = user.organization
    @base_url = base_url(@organization)
    subject = "Ein importierter Benutzer hat seine Registrierung bestätigt"
    mail(to: EMAIL_TO, subject: subject)
  end

  def send_update_user_notification(user)
    @user = user
    @organization = user.organization
    @base_url = base_url(@organization)
    subject = "Ein Benutzer hat seine Daten aktualisiert"
    mail(to: EMAIL_TO, subject: subject)
  end
end
