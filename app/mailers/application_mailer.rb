class ApplicationMailer < ActionMailer::Base
  default from: email_address_with_name('smartcity@lkwnd.de', 'Smart Wendeler Land')
  # default from: email_address_with_name('info@pocket-rocket.io', 'Smart Wendeler Land')
  layout 'mailer'

  def base_url(organization)
    if ENV && ENV["FRONTEND_URL_#{organization.scope.upcase}"]
      return ENV["FRONTEND_URL_#{organization.scope.upcase}"]
    else
      return 'http://localhost:8080'
    end
  end
end
