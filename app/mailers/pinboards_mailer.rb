class PinboardsMailer < ApplicationMailer
  def send_is_checked(user, cta_link)
    @user = user
    @organization = user.organization
    @base_url = base_url(@organization)
    @cta_link = cta_link
    subject = "Deine Idee wird überprüft"
    mail(to: @user.email, subject: subject)
  end

  def send_confirmed(user, cta_link)
    @user = user
    @organization = user.organization
    @base_url = base_url(@organization)
    @cta_link = cta_link
    subject = "Deine Idee wurde freigegeben"
    mail(to: @user.email, subject: subject)
  end

  def send_rejected(user, cta_link)
    @user = user
    @organization = user.organization
    @base_url = base_url(@organization)
    @cta_link = cta_link
    subject = "Deine Idee wurde abgelehnt"
    mail(to: @user.email, subject: subject)
  end
end
