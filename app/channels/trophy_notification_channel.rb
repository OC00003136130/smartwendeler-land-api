class TrophyNotificationChannel < ApplicationCable::Channel

  # TODO consider some token here instead of the plain id
  def subscribed
    stream_from "trophy_notification_channel_#{params[:userId]}"
    puts "Streaming from trophy_notification_channel_#{params[:userId]}"
  end
end
