class Tag::Contracts::Create < Tag::Contracts::Validate
  property :name
  property :scope
  property :menu_order
end
