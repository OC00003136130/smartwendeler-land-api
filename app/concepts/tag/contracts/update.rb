class Tag::Contracts::Update < Tag::Contracts::Validate
  property :name
  property :scope
  property :menu_order
end
