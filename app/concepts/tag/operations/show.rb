class Tag::Operations::Show < Trailblazer::Operation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:tag_id]) }, Output(:failure) => End(:not_found)
end
