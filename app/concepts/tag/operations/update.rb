class Tag::Operations::Update < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:tag_id]) }, Output(:failure) => End(:not_found)
  step Contract::Build( constant: Tag::Contracts::Update )
  
  step Contract::Validate()
  step Contract::Persist()
  
  fail :process_errors
end
