class Tag::Operations::Create < BaseOperation
  step Model( Tag, :new )
  step :scope_model_to_organization
  step :set_default_order
  step Contract::Build( constant: Tag::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()
  
  fail :process_errors

  def set_default_order(ctx, current_organization:, params:, **)
    last_tag = current_organization.tags.order(:menu_order).last if current_organization.tags.present?
    if last_tag
      params[:menu_order] = last_tag.menu_order + 1
    else
      params[:menu_order] = 0
    end
    true
  end
end
