class Tag::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :tags, :list
  end

  def show?
    can_access? :tags, :show
  end

  def create?
    can_access? :tags, :create
  end

  def update?
    can_access? :tags, :update
  end

  def delete?
    can_access? :tags, :delete
  end

  def resolve
    scope = nil
    tag_category_id = nil

    if params && params[:scope].present?
      scope = params[:scope]
    end

    if params && params[:tag_category_id].present?
      tag_category_id = params[:tag_category_id]
    end

    args = {
      scope: scope,
      tag_category_id: tag_category_id
    }.compact
    
    organization.tags.where(args)
  end

  def find(id)
    organization.tags.find id
    rescue ActiveRecord::RecordNotFound => err
  end
end
