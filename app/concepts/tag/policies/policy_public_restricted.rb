class Tag::Policies::PolicyPublicRestricted < App::Policies::PolicyPublicRestricted
  def list?
    true
  end

  def resolve
    scope = nil
    tag_category_id = nil

    if params && params[:scope].present?
      scope = params[:scope]
    end

    if params && params[:tag_category_id].present?
      tag_category_id = params[:tag_category_id]
    end

    args = {
      scope: scope,
      tag_category_id: tag_category_id
    }.compact
    
    organization.tags.where(args)
  end
end
