class Tag::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :name
  property :scope
  property :menu_order

  property :created_at
  property :updated_at
end
