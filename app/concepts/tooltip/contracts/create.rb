class Tooltip::Contracts::Create < Tooltip::Contracts::Validate
  property :name
  property :content
  property :url
end
