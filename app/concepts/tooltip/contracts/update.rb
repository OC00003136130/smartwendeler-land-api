class Tooltip::Contracts::Update < Tooltip::Contracts::Validate
  property :name
  property :content
  property :url
end
