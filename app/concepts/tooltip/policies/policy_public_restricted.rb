class Tooltip::Policies::PolicyPublicRestricted < App::Policies::PolicyPublicRestricted
  def list?
    true
  end

  def resolve
    organization.tooltips
  end
end
