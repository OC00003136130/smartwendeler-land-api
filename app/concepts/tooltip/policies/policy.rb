class Tooltip::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :tooltips, :list
  end

  def show?
    can_access? :tooltips, :show
  end

  def create?
    can_access? :tooltips, :create
  end

  def update?
    can_access? :tooltips, :update
  end

  def delete?
    can_access? :tooltips, :delete
  end

  def resolve
    organization.tooltips
  end

  def find(id)
    organization.tooltips.find id
    rescue ActiveRecord::RecordNotFound => err
  end
end
