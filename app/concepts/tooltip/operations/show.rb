class Tooltip::Operations::Show < Trailblazer::Operation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:tooltip_id]) }, Output(:failure) => End(:not_found)
end
