class Tooltip::Operations::Create < BaseOperation
  step Model( Tooltip, :new )
  step :scope_model_to_organization
  step Contract::Build( constant: Tooltip::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()
  
  fail :process_errors
end
