class Tooltip::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :name
  property :content
  property :url

  property :created_at
  property :updated_at
end
