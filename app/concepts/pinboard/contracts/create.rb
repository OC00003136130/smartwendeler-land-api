class Pinboard::Contracts::Create < Pinboard::Contracts::Validate
  property :headline
  property :content
  property :menu_order
  property :is_active
  property :start_time
  property :end_time
end
