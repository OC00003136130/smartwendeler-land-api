class Pinboard::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :pinboards, :list
  end

  def show?
    can_access? :pinboards, :show
  end

  def create?
    can_access? :pinboards, :create
  end

  def update?
    can_access? :pinboards, :update
  end

  def delete?
    can_access? :pinboards, :delete
  end

  def resolve
    organization.pinboards
  end

  def find(id)
    organization.pinboards.find id
    rescue ActiveRecord::RecordNotFound => err
  end
end
