class Pinboard::Policies::PolicyPublicRestricted < App::Policies::PolicyPublicRestricted
  def list?
    true
  end

  def show?
    true
  end

  def find(id)
    organization.pinboards.where(is_active: true).find id
    rescue ActiveRecord::RecordNotFound => err
  end

  def resolve
    organization.pinboards.where(is_active: true).where("start_time <= ?", Time.now)
  end
end
