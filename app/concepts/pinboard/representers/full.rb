class Pinboard::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :headline
  property :content
  property :menu_order
  property :is_active
  property :category_ids
  property :start_time
  property :end_time

  property :pinboard_ideas_count
  property :pinboard_confirmed_ideas_count

  collection :categories do
    property :id
    property :name
  end

  property :created_at
  property :updated_at
end
