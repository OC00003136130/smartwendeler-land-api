class Pinboard::Operations::Update < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:pinboard_id]) }, Output(:failure) => End(:not_found)

  step Contract::Build( constant: Pinboard::Contracts::Update )
  step Contract::Validate()
  step Contract::Persist()
  step Pinboard::Steps::AssignCategories
  
  fail :process_errors
end
