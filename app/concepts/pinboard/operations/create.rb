class Pinboard::Operations::Create < BaseOperation
  step Model( Pinboard, :new )
  step :scope_model_to_organization
  step :scope_model_to_user
  step :set_default_order
  step Contract::Build( constant: Pinboard::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()
  step Pinboard::Steps::AssignCategories

  def set_default_order(ctx, current_organization:, params:, **)
    last_pinboard = current_organization.pinboards.order(:menu_order).last if current_organization.pinboards.present?
    if last_pinboard
      params[:menu_order] = last_pinboard.menu_order + 1
    else
      params[:menu_order] = 0
    end
    true
  end
  
  fail :process_errors
end
