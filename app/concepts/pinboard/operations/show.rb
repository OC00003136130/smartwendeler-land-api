class Pinboard::Operations::Show < Trailblazer::Operation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:pinboard_id]) }, Output(:failure) => End(:not_found)
end
