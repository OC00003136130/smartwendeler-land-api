# Classs for all pundit style policies
class App::Policies::Policy < App::Policies::Base
  attr_reader :user, :organization, :model, :organization_id, :params

  def initialize(user, model, params, organization = nil)
    @params = params
    @user = user

    # sometimes the user is not available when creating a policy
    if user
      @organization = user.organization
    else
      @organization = organization
    end

    raise "The given organization is nil!" if @organization.nil?
    @model = model
    raise "The given model class is nil!" if @model.nil?
    @organization_id = @organization.id
  end

  protected
    # helper for calling UserPrivileges object with the current user in sub-classes
    def can_access?(endpoint, action)
      App::AccessPrivileges.can_access?(user.role, endpoint, action)
    end

    # helper to check if user is allowed to perform updates on certain attributes
    def attributes_allowed?(endpoint)
      App::AccessPrivileges.attributes_allowed?(user.role, endpoint, @params)
    end
end
