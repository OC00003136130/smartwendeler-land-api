# shared between private and public policy classes
class App::Policies::Base
  def resolve
    raise NotImplementedError.new 'Not Implemented! Please implement me in the according sub-class!'
  end

  # all sub-implementations should raise an ActiveRecord::NotFound Exception when no object could be found
  def find(id)
    raise NotImplementedError.new 'Not Implemented! Please implement me in the according sub-class!'
  end
  
  def add_search_query(query, searchstring, model, fields, exact_match)
    if !searchstring.nil?
      searchstring.downcase.strip!
      matcher = exact_match ? "#{searchstring}%" : "%#{searchstring}%"
      integer_matcher = searchstring.to_i

      sub_query = ""
      fields.each do |field|
        sub_query += " OR " if sub_query.length > 0
        if field == :id
          sub_query = "#{sub_query}\"#{model.name.underscore.pluralize}\".\"#{field.to_s}\" = '#{searchstring}'"
        else
          sub_query = "#{sub_query}\"#{model.name.underscore.pluralize}\".\"#{field.to_s}\" ILIKE :matcher"
        end
      end

      query.where(sub_query, matcher: matcher, integer_matcher: integer_matcher)

    end
  end
end