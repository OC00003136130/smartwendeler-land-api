class App::Policies::PolicyPublicRestricted < App::Policies::Base
  attr_reader :organization, :params, :model

  def initialize(organization, params, model)
    @params = params
    @organization = organization
    @model = model
  end
end
