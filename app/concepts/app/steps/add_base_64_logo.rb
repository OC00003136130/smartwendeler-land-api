# adds an image on base 64 basis

# @params['file'] = base64 file or 'purge' if image should get removed
# @params['tag'] = optional if image should get tagged
class App::Steps::AddBase64Logo < App::Steps::Base
  extend Uber::Callable

  require "mini_magick"

  def self.call(options, params:, model:, **)
    return true if !params['logo'].present?

    if params['logo'] == 'purge'
      model.logo.purge
    else
      f = get_image_from_base64_string(params[:logo])

      model.logo.attach(io: f, filename: SecureRandom.hex)
    end
    true
  end
end
