# adds multiple base64 documents
class App::Steps::AddBase64Document < App::Steps::Base
  extend Uber::Callable

  def self.call(options, params:, model:, **)

    return true if params['mode'] == 'blob'

    if params['document']
      str = params['document'].split(",")[1]
      to_decode = nil

      if str
        to_decode = str
      else
        to_decode = params['document']
      end

      documentname = params['documentname']

      if !documentname
        documentname = SecureRandom.hex
      end

      if params[:tag]
        documentname = "#{documentname}-#{params[:tag]}"
      end

      blob = Base64.decode64(to_decode)
      mimetype = (params['document'].match /^data:(?<mimetype>[a-z\-]+\/[a-z\.\+\-]+);base64/)[:mimetype]

      File.open(documentname, 'w+b') do |document|
        document.write blob
        document.rewind
        model.documents.attach(
          io: document,
          filename: documentname,
          content_type: mimetype
        )
      end

      File.delete(documentname)
    end

    true
  end
end
