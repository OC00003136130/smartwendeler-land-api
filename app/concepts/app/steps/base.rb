class App::Steps::Base
  def self.logger
    Rails.logger
  end
  
  require "mini_magick"

  # some convenient and reuseable methods
  def self.get_image_from_base64_string(base64)
    str = base64.split(",")[1]
    blob = Base64.decode64(str)
    image = MiniMagick::Image.read(blob)
    image.quality(90)
    image.resize('2000x')
    return File.open(image.path)
  end
end
