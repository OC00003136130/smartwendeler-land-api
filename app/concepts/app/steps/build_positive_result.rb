# Sets the json to the according entity output using the representer_class given to the operation
class App::Steps::BuildPositiveResult < App::Steps::Base
  def self.call(options, request:, path:, representer_class:, **)
    logger.debug "Executing #{self}"
    current_user_id = nil
    if options['current_user']
      current_user_id = options['current_user'].id
      current_user_role = options['current_user'].role
    end
    
    options['json'] = App::Representers::Entity.new(result: options, request: request,
      path: path, representer_class: representer_class, current_user_id: current_user_id, current_user_role: current_user_role).as_json
  end
end
