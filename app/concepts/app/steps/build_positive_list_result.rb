# Sets the json to the according list output
class App::Steps::BuildPositiveListResult < App::Steps::Base
  def self.call(options, domain_ctx:, request:, path:, representer_class:, **)

    current_user_id = nil
    current_user_role = nil
    if options['current_user']
      current_user_id = options['current_user'].id
      current_user_role = options['current_user'].role
    end

    options['json'] = App::Representers::List.new(
      result: options,
      request: request,
      path: path,
      representer_class: representer_class,
      params: domain_ctx[:params],
      countless: options['countless'] || 0,
      current_user_id: current_user_id,
      current_user_role: current_user_role
    ).as_json
    
    return true
  end
end
