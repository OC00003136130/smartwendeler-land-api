# adds an image on base 64 basis

# @params['file'] = base64 file or 'purge' if image should get removed
# @params['tag'] = optional if image should get tagged
class App::Steps::AddBase64Image < App::Steps::Base
  extend Uber::Callable

  require "mini_magick"

  def self.call(options, params:, model:, **)
    return true if !params['file'].present?

    if params['file'] == 'purge'
      model.image.purge
    else
      f = get_image_from_base64_string(params[:file])

      if params[:multiple]
        model.images.attach(io: f, filename: "#{SecureRandom.hex}-#{params['tag']}")
      else
        model.image.attach(io: f, filename: SecureRandom.hex)
      end
    end
    true
  end
end
