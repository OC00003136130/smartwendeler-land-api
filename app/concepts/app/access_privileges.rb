# Container for general access rules for endpoints
#
# - Special conditions are handled in the according policies -
# e.g. a user with role 'user' wants to update a lead
# which is not in any of their organization
class App::AccessPrivileges

  def self.can_access?(role, endpoint, action)
    case role
    when 'root'
      return true
    when 'admin', 'user', 'facility_owner', 'care_facility_admin'
      check_role_privileges? role, endpoint, action
    else
      raise "Unknown user role: #{role} !"
    end
  end

  # parameters which the user is allowed to access
  def self.attributes_allowed?(role, endpoint, params)
    case role
    when 'root'
      check_attributes_allowed? role, endpoint, params
    when 'admin', 'user', 'facility_owner', 'care_facility_admin'
      check_attributes_allowed? role, endpoint, params
    else
      raise "Unknown user role: #{role} !"
    end
  end

  def self.root
    {
      role: 'root'
    }
  end

  def self.facility_owner
    {
      role: 'facility_owner',
      endpoints: {
        users: {
          me: true,
          logout: true,
          list: false,
          show: true,
          invite: false,
          update: true,
          update_password: true,
          delete: false,
          delete_me: true
        },
        care_facilities: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
          attach_image: true,
          purge_image: true,
          attach_document: true,
          purge_document: true
        },
        categories: {
          list: true,
          show: true,
          create: false,
          delete: false,
          update: false,
          create_sub_category: false,
          list_sub_categories: true,
          create_sub_sub_category: false,
          list_sub_sub_categories: true
        },
        communities: {
          list: true,
          show: true,
          create: false,
          delete: false,
          update: false,
        },
        tags: {
          list: true,
          show: true,
          create: true,
          delete: false,
          update: false,
        },
        tag_categories: {
          list: true,
          show: true,
          create: true,
          delete: false,
          update: false,
        },
        locations: {
          create: true,
          delete: true,
          update: true,
        },
      },
      locked_attributes: {
        care_facilities: ['status'],
        users: ['is_active_on_health_scope']
      }
    }
  end

  def self.user
    {
      role: 'user',
      endpoints: {
        users: {
          me: true,
          logout: true,
          list: false,
          show: true,
          invite: false,
          update: true,
          update_password: true,
          delete: false,
          delete_me: true
        },
        projects: {
          list: true,
          show: true,
          create: false,
          delete: false,
          update: false,
          attach_image: false,
          purge_image: false,
          upvote: true,
          downvote: true,
          remove_vote: true
        },
        comments: {
          list: true,
          list_all: false,
          list_replies: true,
          show: true,
          create: true,
          delete: true,
          update: true,
          upvote: true,
          downvote: true,
          remove_vote: true
        },
        categories: {
          list: true,
          show: true,
          create: false,
          delete: false,
          update: false,
          create_sub_category: false,
          list_sub_categories: true,
          create_sub_sub_category: false,
          list_sub_sub_categories: true
        },
        communities: {
          list: true,
          show: true,
          create: false,
          delete: false,
          update: false,
        },
        locations: {
          create: false,
          delete: false,
          update: false,
        },
        milestones: {
          list: false,
          show: false,
          create: false,
          delete: false,
          update: false,
        },
        messages: {
          list: false,
          show: false,
          delete: false,
          update: false,
        },
        comment_reports: {
          list: false,
          show: false,
          create: true,
          delete: false
        },
        tooltips: {
          list: false,
          show: false,
          create: false,
          delete: false,
          update: false,
        },
        polls: {
          list: true,
          show: false,
          create: false,
          delete: false,
          update: false,
          answers: false
        },
        poll_questions: {
          list: true,
          show: false,
          create: false,
          delete: false,
          update: false,
        },
        poll_answers: {
          list: false,
          create_update: true
        },
        notifications: {
          send: false
        },
        logs: {
          list: true
        },
        gamification_games: {
          list: true,
          show: true,
          create: false,
          delete: false,
          update: false
        },
        gamification_rewards: {
          list: true,
          show: false,
          create: false,
          delete: false,
          update: false
        },
        gamification_trophies: {
          list: true
        },
        care_facilities: {
          list: true,
          show: true,
          create: false,
          delete: false,
          update: false,
          attach_image: false,
          purge_image: false,
          attach_document: false,
          purge_document: false
        },
        statistics: {
          list: false
        },
        pinboards: {
          list: true,
          show: true,
          create: false,
          delete: false,
          update: false,
        },
        pinboard_ideas: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
          upvote: true,
          remove_vote: true
        },
        tags: {
          list: true,
          show: true,
          create: false,
          delete: false,
          update: false,
        },
        tag_categories: {
          list: true,
          show: true,
          create: false,
          delete: false,
          update: false,
        },
      },
      locked_attributes: {
        users: ['role', 'password', 'is_active_on_health_scope']
      }
    }
  end

  def self.admin
    {
      role: 'admin',
      endpoints: {
        users: {
          me: true,
          logout: true,
          list: true,
          show: true,
          invite: true,
          update: true,
          update_password: true,
          delete: true,
          delete_me: true
        },
        projects: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
          attach_image: true,
          purge_image: true,
          upvote: true,
          downvote: true,
          remove_vote: true
        },
        comments: {
          list: true,
          list_all: true,
          list_replies: true,
          show: true,
          create: true,
          delete: true,
          update: true,
          upvote: true,
          downvote: true,
          remove_vote: true
        },
        categories: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
          create_sub_category: true,
          list_sub_categories: true,
          create_sub_sub_category: true,
          list_sub_sub_categories: true
        },
        communities: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
        },
        locations: {
          create: true,
          delete: true,
          update: true,
        },
        milestones: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
        },
        messages: {
          list: true,
          show: true,
          delete: true,
          update: true,
        },
        comment_reports: {
          list: true,
          show: true,
          create: true,
          delete: true
        },
        tooltips: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
        },
        polls: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
          answers: true
        },
        poll_questions: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
        },
        poll_answers: {
          list: true,
          create_update: true
        },
        notifications: {
          send: true
        },
        logs: {
          list: true
        },
        gamification_games: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true
        },
        gamification_rewards: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true
        },
        gamification_trophies: {
          list: true
        },
        news_articles: {
          list: true
        },
        care_facilities: {
          list: true,
          show: true,
          create: true,
          delete: false,
          update: false,
          attach_image: true,
          purge_image: true,
          attach_document: true,
          purge_document: true
        },
        statistics: {
          list: true
        },
        pinboards: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
        },
        pinboard_ideas: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
          upvote: true,
          remove_vote: true
        },
        tags: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
        },
        tag_categories: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
        }
      }
    }
  end

  def self.care_facility_admin
    {
      role: 'care_facility_admin',
      endpoints: {
        users: {
          me: true,
          logout: true,
          list: true,
          show: true,
          invite: true,
          update: true,
          update_password: true,
          delete: true,
          delete_me: true
        },
        projects: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
          attach_image: true,
          purge_image: true,
          upvote: true,
          downvote: true,
          remove_vote: true
        },
        comments: {
          list: true,
          list_all: true,
          list_replies: true,
          show: true,
          create: true,
          delete: true,
          update: true,
          upvote: true,
          downvote: true,
          remove_vote: true
        },
        categories: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
          create_sub_category: true,
          list_sub_categories: true,
          create_sub_sub_category: true,
          list_sub_sub_categories: true
        },
        communities: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
        },
        locations: {
          create: true,
          delete: true,
          update: true,
        },
        messages: {
          list: true,
          show: true,
          delete: true,
          update: true,
        },
        comment_reports: {
          list: true,
          show: true,
          create: true,
          delete: true
        },
        tooltips: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
        },
        notifications: {
          send: true
        },
        logs: {
          list: true
        },
        news_articles: {
          list: true
        },
        care_facilities: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
          attach_image: true,
          purge_image: true,
          attach_document: true,
          purge_document: true
        },
        statistics: {
          list: true
        },
        tags: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
        },
        tag_categories: {
          list: true,
          show: true,
          create: true,
          delete: true,
          update: true,
        }
      }
    }
  end

private
  def self.check_role_privileges?(role, endpoint, action)
    role = role.to_sym
    endpoint = endpoint.to_sym
    action = action.to_sym
    privileges = self.send(role.to_sym)
    is_allowed = privileges.dig(:endpoints, endpoint, action)
    # puts "Role: #{role} | endpoint: #{endpoint} | action: #{action} \nis_allowed: #{is_allowed}"
    is_allowed == true
  end

  def self.check_attributes_allowed?(role, endpoint, params)
    role = role.to_sym

    endpoint = endpoint.to_sym
    privileges = self.send(role.to_sym)

    locked_attributes = privileges.dig(:locked_attributes, endpoint)

    # check if params are included in blocked keys
    is_allowed = []
    if locked_attributes
      is_allowed = params.keys & locked_attributes
    end

    is_allowed == []
  end
end
