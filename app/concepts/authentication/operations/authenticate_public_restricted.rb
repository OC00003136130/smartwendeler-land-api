class Authentication::Operations::AuthenticatePublicRestricted < BaseOperation
  step :verify_token
  step :set_current_organization

  def verify_token(options, request:, **)
    logger.debug "Executing Base Authentification check"
    auth_header = request.headers['Authorization']

    basic_auth_encoded_token = auth_header.split(' ').last
    decoded_auth_token = Base64.decode64(basic_auth_encoded_token)

    options['decoded_username'] = decoded_auth_token.split(':').first
    options['decoded_password'] = decoded_auth_token.split(':').last

    return true
  rescue StandardError => err
    logger.info "verify_token failed!"
    add_error options, message: 'Could not decode the given credentials! Place your token in the Authentication header for http requests!', exception: nil, code: 'authentication.unauthorized'
    options['json'] = {}
    options['http_status'] = 401
    false
  end

  def set_current_organization(options, **)
    logger.debug "Executing set_current_organization"
    organization = Organization.find_by(
        public_username: options['decoded_username'])&.authenticate_public_password(options['decoded_password']
      )
    if organization
      options['current_organization'] = organization
    else
      logger.info "set_current_organization failed!"
      add_error options, message: 'The given token could not authenticate the organization!', code: 'authentication.unauthorized'
      options['http_status'] = 401
      return false
    end
    true
  end
end
