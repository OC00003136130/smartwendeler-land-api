require 'date'

class Authentication::Operations::GenerateToken < BaseOperation
  step :check_credentials
  step :check_token_expired
  step :generate_token
  step :persist_user
  step ->(ctx, current_user:, **) {
    GamificationTrophy::Operations::Create.(current_user: current_user, kind: 'login')
  }

  fail :process_errors

  def check_credentials(options, params:, **)
    logger.info "Executing check_credentials ..."
    email = params[:email]
    password = params[:password]

    if email.nil?
      add_error options,
        message: "No email provided! Could not authenticate user!",
        exception: nil,
        code: 'authentication.unauthorized',
        field_name: :email
      return false
    end

    # only downcase emails are allowed for users, always check with downcased email
    # We also downcase the search query here
    # consider token for multiple user usage because a user's email can be present in multiple organizations

    users = User.where("lower(email) = ?", email.downcase)
    if users.count > 1
      if !params[:register_token].present?
        add_error options,
          message: "A register_token parameter is required to find the correct user as we found multiple in the database",
          exception: nil,
          code: 'authentication.unauthorized'
        return false
      end
      org_for_register_token = Organization.find_by(register_token: params[:register_token])
      user = users.find_by(organization_id: org_for_register_token.id)
    elsif users.count == 1
      user = users.first
    else
      user = nil
    end

    if is_user_invalid_for_login?(user, password)
      logger.info "Failed login attempt for email #{email} at #{Time.now}. Incorrect user/password combination ..."
      add_error options,
        message: "Incorrect user/password combination! Could not authenticate user!",
        exception: nil,
        code: 'authentication.unauthorized'
      return false
    end

    if is_user_in_incorrect_scope?(user, params[:scope])
      logger.info "Failed login attempt for email #{email} at #{Time.now}. Wrong scope provided ..."
      add_error options,
        message: "Incorrect scope for user! Could not authenticate!",
        exception: nil,
        code: 'authentication.wrong_scope'
      return false
    end

    options['current_user'] = user
    if !options['current_user'].never_expire
      options['current_user'].never_expire = params[:never_expire]
    end
    options['current_user'].last_seen = Time.now
    options['current_user'].login_count += 1
    options['current_organization_id'] = user.organization_id
  end

  def check_token_expired(options, **)
    logger.info "Executing check_token_expired for user with id: #{options['current_user'].id} ..."
    token = options['current_user'].jwt_token
    # exit early if no token is present for the user
    if token.nil?
      logger.info "No jwt_token found for user! Returning early!"
      return true
    end

    if !JwtService.expired?(token)
      logger.info "jwt_token for user is still valid"
      options['jwt_token'] = token
    else
      logger.info "jwt_token expired for user with id: #{options['current_user'].id} !"
    end
    return true
  end

  # example - https://www.pluralsight.com/guides/ruby-ruby-on-rails/token-based-authentication-with-ruby-on-rails-5-api
  def generate_token(options, **)
    logger.info "Executing generate_token ..."
    # return if token does not need to be recreated
    return true unless options['jwt_token'].nil?
    logger.info "Generating new token for user with id: #{options['current_user'].id} ..."

    jwt = JwtService.generate('user_id', options['current_user'].id, {
      'email': options['current_user'].email,
      'user_id': options['current_user'].id,
      'organization_id': options['current_organization_id'],
      'role': options['current_user'].role
    }, options['current_user'].never_expire)
    options['jwt_token'] = jwt
    options['current_user'].jwt_token = jwt
  end

  def persist_user(options, **)
    options['current_user'].save
  end

private

  def is_user_invalid_for_login?(user, password)
    return true if user.nil?
    return true if user.status != 'confirmed'
    return !user.authenticate(password)
  end

  def is_user_in_incorrect_scope?(user, scope)
    # fallback if no scope is presented
    if !scope.present?
      scope = 'project'
    end

    return true if user.organization.scope != scope
    return false
  end
end
