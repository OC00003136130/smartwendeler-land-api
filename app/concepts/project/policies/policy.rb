class Project::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :projects, :list
  end

  def show?
    can_access? :projects, :show
  end

  def create?
    can_access? :projects, :create
  end

  def update?
    can_access? :projects, :update
  end

  def delete?
    can_access? :projects, :delete
  end

  def upvote?
    can_access? :projects, :upvote
  end

  def downvote?
    can_access? :projects, :downvote
  end

  def remove_vote?
    can_access? :projects, :remove_vote
  end

  def attach_image?
    can_access? :projects, :attach_image
  end

  def purge_image?
    can_access? :projects, :purge_image
  end

  def resolve
    if user.role == 'admin' || user.role == 'root'
      organization.projects
    else
      organization.projects.where(is_active: true)
    end
  end

  def resolve_with_search(fields=[:name, :zip, :town], exact_match=false)
    return add_search_query(self.resolve, params[:search], model, fields, exact_match)
  end

  def find(id)
    if user.role == 'admin' || user.role == 'root'
      organization.projects.find id
    else
      organization.projects.find_by(id: id, is_active: true)
    end
    
    rescue ActiveRecord::RecordNotFound => err
  end

  def find_unscoped(id)
    organization.projects.find id
  end
end
