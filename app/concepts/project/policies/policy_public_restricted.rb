class Project::Policies::PolicyPublicRestricted < App::Policies::PolicyPublicRestricted
  def list?
    true
  end

  def show?
    true
  end

  def find(id)
    project = organization.projects.find_by(slug: id, is_active: true)
    project = organization.projects.find_by!(id: id, is_active: true) if project.nil?
    return project
    rescue ActiveRecord::RecordNotFound => err
  end

  def resolve
    organization.projects.where(is_active: true)
  end

  def resolve_with_search(fields=[:name, :zip, :town], exact_match=false)
    return add_search_query(self.resolve, params[:search], model, fields, exact_match)
  end
end
