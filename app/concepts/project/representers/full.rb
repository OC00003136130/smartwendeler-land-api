class Project::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :name
  property :start_time
  property :end_time
  property :description
  property :comment_count
  property :costs
  property :internal_identifier
  property :town
  property :zip
  property :leader
  property :excerpt
  property :is_active
  property :category_ids
  property :community_ids
  property :rating_kind
  property :rating_results_public
  property :rating_start
  property :rating_end
  property :menu_order
  property :slug

  property :upvote_count, getter: -> (represented:, **options) do
    if represented.rating_results_public || represented.current_user_role == 'admin' || represented.current_user_role == 'root'
      return represented.upvote_count
    end
  end

  property :downvote_count, getter: -> (represented:, **options) do
    if represented.rating_results_public || represented.current_user_role == 'admin' || represented.current_user_role == 'root'
      return represented.downvote_count
    end
  end

  property :total_vote_count, getter: -> (represented:, **options) do
    if represented.rating_results_public || represented.current_user_role == 'admin' || represented.current_user_role == 'root'
      return represented.total_vote_count
    end
  end

  property :has_upvoted_project, getter: -> (represented:, **options) do
    return represented.project_ratings.where(score: 1, user_id: represented.current_user_id).present?
  end

  property :has_downvoted_project, getter: -> (represented:, **options) do
    return represented.project_ratings.where(score: -1, user_id: represented.current_user_id).present?
  end

  property :image_url
  property :sanitized_images

  collection :categories do
    property :id
    property :name
  end

  collection :communities do
    property :id
    property :name
  end

  collection :locations

  property :created_at
  property :updated_at
end
