class Project::Operations::RemoveVote < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find_unscoped(params[:project_id]) }, Output(:failure) => End(:not_found)
  step :find_and_remove_project_rating
  fail :process_errors

  def find_and_remove_project_rating(ctx, model:, current_user:, **)
    project_rating = ProjectRating.find_by!(
      project_id: model.id,
      user_id: current_user.id
    )

    if project_rating
      project_rating.delete
      model.score = model.project_ratings.sum(:score)
      model.save!
      return true
    else
      raise ActiveRecord::RecordNotFound
    end
  end
end
