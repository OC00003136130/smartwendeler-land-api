class Project::Operations::Show < Trailblazer::Operation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:project_id]) }, Output(:failure) => End(:not_found)
end
