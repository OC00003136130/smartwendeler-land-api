class Project::Operations::Update < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:project_id]) }, Output(:failure) => End(:not_found)

  step Contract::Build( constant: Project::Contracts::Update )
  step Contract::Validate()
  step Contract::Persist()
  step :log_important_updates
  step App::Steps::AddBase64Image
  step Project::Steps::AssignCategories
  step Project::Steps::AssignCommunities

  def log_important_updates(ctx, model:, current_organization:, current_user:, **)
    
    log_code = 'project.is_active' if model.saved_changes["is_active"] && model.saved_changes["is_active"][1]
    log_code = 'project.rating_results_public' if model.saved_changes["rating_results_public"] && model.saved_changes["rating_results_public"][1]

    if log_code
      Log.create!(
        organization: current_organization,
        change: {},
        model_class_name: model.class.name,
        model_id: model.id,
        model_record_name: model.name,
        log_code: log_code
      )
    end
    true
  end
  
  fail :process_errors
end
