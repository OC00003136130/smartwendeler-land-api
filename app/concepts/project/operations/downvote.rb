class Project::Operations::Downvote < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find_unscoped(params[:project_id]) }, Output(:failure) => End(:not_found)
  step :check_if_downvote_possible
  step :find_or_create_project_rating
  step ->(ctx, current_user:, **) {
    GamificationTrophy::Operations::Create.(current_user: current_user, kind: 'rating')
  }
  fail :process_errors

  def check_if_downvote_possible(ctx, model:, **)
    if model.rating_kind == 'upvote'
      add_error ctx, message: 'Downvote not possible!', exception: nil, code: 'project.downvote.blocked', field_name: 'downvote'
      return false
    end
    true
  end

  def find_or_create_project_rating(ctx, model:, current_user:, **)
    project_rating = ProjectRating.find_or_create_by!(
      project_id: model.id,
      user_id: current_user.id
    )

    project_rating.score = -1
    project_rating.save!

    model.score = model.project_ratings.sum(:score)
    model.save!

    true
  end
end
