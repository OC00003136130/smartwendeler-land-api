class Project::Operations::Create < BaseOperation
  step Model( Project, :new )
  step :scope_model_to_organization
  step :set_default_order
  step Contract::Build( constant: Project::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()
  step App::Steps::AddBase64Image
  step Project::Steps::AssignCategories
  step Project::Steps::AssignCommunities

  def set_default_order(ctx, current_organization:, params:, **)
    last_project = current_organization.projects.order(:menu_order).last if current_organization.projects.present?
    if last_project
      params[:menu_order] = last_project.menu_order + 1
    else
      params[:menu_order] = 0
    end
    true
  end
  
  fail :process_errors
end
