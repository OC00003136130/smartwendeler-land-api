class Project::Operations::Public::Show < PublicRestrictedEndpointOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:project_id]) }, Output(:failure) => End(:not_found)
end
