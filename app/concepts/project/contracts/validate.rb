class Project::Contracts::Validate < Reform::Form
  validation do
    params do
      required(:name).filled(:string)
      optional(:start_time)
      optional(:end_time)
      optional(:rating_start)
      optional(:rating_end)
    end

    rule(:start_time, :end_time) do
      key.failure(text: 'end and start date have to be provided', code: 'project.start_and_end_time_must_be_provided') if (values[:end_time] && !values[:start_time]) || (values[:start_time] && !values[:end_time])
      key.failure(text: 'must be in correct order', code: 'project.start_and_end_time_must_be_in_correct_order') if (values[:end_time] && values[:start_time]) && (values[:end_time] < values[:start_time])
    end

    rule(:rating_start, :rating_end) do
      key.failure(text: 'end and start of a rating period have to be provided', code: 'project.rating_start_and_end_must_be_provided') if (values[:rating_end] && !values[:rating_start]) || (values[:rating_start] && !values[:rating_end])
      key.failure(text: 'must be in correct order', code: 'project.rating_start_and_end_must_be_in_correct_order') if (values[:rating_end] && values[:rating_start]) && (values[:rating_end] < values[:rating_start])
    end
  end
end
