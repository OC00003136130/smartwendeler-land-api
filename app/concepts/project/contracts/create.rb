class Project::Contracts::Create < Project::Contracts::Validate
  property :name
  property :start_time
  property :end_time
  property :description
  property :costs
  property :internal_identifier
  property :town
  property :zip
  property :leader
  property :excerpt
  property :rating_kind
  property :rating_results_public
  property :rating_start
  property :rating_end
  property :menu_order
  property :slug
end
