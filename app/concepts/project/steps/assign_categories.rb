class Project::Steps::AssignCategories < App::Steps::Base
  extend Uber::Callable

  def self.call(options, model:, params:, current_organization:, **)
    if params[:category_ids].present?
      categories = current_organization.categories.where(id: params[:category_ids])

      # check which old categories have to be deleted
      project_category_ids = model.project_categories.pluck(:category_id)
      to_be_deleted_ids = project_category_ids.difference(params[:category_ids])
      to_be_deleted_categories = model.categories.where(id: to_be_deleted_ids)

      if to_be_deleted_categories.present?
        to_be_deleted_categories.each do |del_fac|
          model.categories.delete(del_fac)
        end
      end

      categories.each do |fac|
        if (!model.categories.where(id: fac.id).present?)
          model.categories << fac
        end
      end
    end

    true
  end
end
