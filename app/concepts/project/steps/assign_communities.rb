class Project::Steps::AssignCommunities < App::Steps::Base
  extend Uber::Callable

  def self.call(options, model:, params:, current_organization:, **)
    if params[:community_ids].present?
      communities = current_organization.communities.where(id: params[:community_ids])

      # check which old communities have to be deleted
      project_community_ids = model.project_communities.pluck(:community_id)
      to_be_deleted_ids = project_community_ids.difference(params[:community_ids])
      to_be_deleted_communities = model.communities.where(id: to_be_deleted_ids)

      if to_be_deleted_communities.present?
        to_be_deleted_communities.each do |del_fac|
          model.communities.delete(del_fac)
        end
      end

      communities.each do |fac|
        if (!model.communities.where(id: fac.id).present?)
          model.communities << fac
        end
      end
    end

    true
  end
end
