class Organization::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :name
  property :public_username
  property :status
  property :image_url

  property :created_at
  property :updated_at
end
