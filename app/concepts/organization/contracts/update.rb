class Organization::Contracts::Update < Reform::Form
  property :name
  property :status
  property :email
  property :public_username

  validation do
    params do
      required(:name).filled(:string)
    end
  end
end
