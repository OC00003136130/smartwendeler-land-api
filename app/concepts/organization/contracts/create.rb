class Organization::Contracts::Create < Reform::Form
  property :name
  property :status
  property :email
  property :public_username

  validation do
    params do
      # TODO: make public_username unique
      required(:name).filled(:string)
    end
  end
end
