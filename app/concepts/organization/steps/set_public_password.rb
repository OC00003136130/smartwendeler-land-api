class Organization::Steps::SetPublicPassword < App::Steps::Base
  def self.call(options, params:, model:, **)
    if params[:public_password].present?
      model.public_password = params[:public_password]
      model.save!
    end
    true
  end
end
