class Organization::Operations::Show < Trailblazer::Operation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:organization_id]) }, Output(:failure) => End(:not_found)
end
