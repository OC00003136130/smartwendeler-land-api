class Organization::Operations::Update < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:organization_id]) }, Output(:failure) => End(:not_found)

  step Contract::Build( constant: Organization::Contracts::Update )
  step Contract::Validate()
  step Contract::Persist()
  step Organization::Steps::SetPublicPassword
  step App::Steps::AddBase64Image

  fail :process_errors
end
