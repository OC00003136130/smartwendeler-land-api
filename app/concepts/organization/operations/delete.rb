class Organization::Operations::Delete < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:organization_id]) }, Output(:failure) => End(:not_found) # FIXME

  step App::Steps::DestroyModel
  fail :process_errors
end
