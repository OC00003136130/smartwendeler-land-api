class Statistic::Operations::List < BaseOperation
  step :fetch_statistics
  fail :process_errors

  def fetch_statistics(ctx, current_organization:, **)

    ctx['result'] = {
      users_count: current_organization.users.count,
      active_care_facilities_count: current_organization.care_facilities.where(is_active: true).count,
      active_projects_count: current_organization.projects.where(is_active: true).count,
      comments_count: current_organization.comments.count,
      messages_count: current_organization.messages.count,
      poll_answers_count: current_organization.poll_answers.count,
      comment_reports_count: current_organization.comment_reports.count,
      pinboard_ideas_is_checked_status_count: current_organization.pinboard_ideas.where(status: 'is_checked').count,
      pinboard_ideas_confirmed_status_count: current_organization.pinboard_ideas.where(status: 'confirmed').count
    }
    true
  end
end
