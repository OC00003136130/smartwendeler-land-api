class Statistic::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :statistics, :list
  end
end
