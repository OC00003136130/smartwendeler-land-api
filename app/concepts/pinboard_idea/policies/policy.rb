class PinboardIdea::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :pinboard_ideas, :list
  end

  def show?
    can_access? :pinboard_ideas, :show
  end

  def create?
    can_access? :pinboard_ideas, :create
  end

  def update?
    can_access? :pinboard_ideas, :update
  end

  def delete?
    can_access? :pinboard_ideas, :delete
  end

  def upvote?
    can_access? :pinboard_ideas, :upvote
  end

  def remove_vote?
    can_access? :pinboard_ideas, :remove_vote
  end

  def resolve(pinboard)
    case user.role
    when 'root', 'admin'
      pinboard.pinboard_ideas
    when 'user'
      pinboard.pinboard_ideas.where(status: 'confirmed')
    end
  end

  def find_pinboard(id)
    organization.pinboards.find id
    rescue ActiveRecord::RecordNotFound => err
  end

  def find_unscoped(id)
    case user.role
    when 'root', 'admin'
      organization.pinboard_ideas.find id
    when 'user'
      user.pinboard_ideas.find id
    end
    rescue ActiveRecord::RecordNotFound => err
  end

  def find(id)
    organization.pinboard_ideas.find id
    rescue ActiveRecord::RecordNotFound => err
  end
end
