class PinboardIdea::Policies::PolicyPublicRestricted < App::Policies::PolicyPublicRestricted
  def list?
    true
  end

  def show?
    true
  end

  def find_pinboard(id)
    organization.pinboards.where(is_active: true).find id
    rescue ActiveRecord::RecordNotFound => err
  end

  def find(id)
    organization.pinboards.find id
    rescue ActiveRecord::RecordNotFound => err
  end

  def resolve(pinboard)
    pinboard.pinboard_ideas.where(status: 'confirmed')
  end

  def find_unscoped(id)
    organization.pinboard_ideas.where(status: 'confirmed').find id
    rescue ActiveRecord::RecordNotFound => err
  end
end
