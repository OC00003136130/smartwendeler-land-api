class PinboardIdea::Contracts::Validate < Reform::Form
  validation do
    params do
      required(:headline).filled(:string)
      required(:content).filled(:string)
    end
  end
end
