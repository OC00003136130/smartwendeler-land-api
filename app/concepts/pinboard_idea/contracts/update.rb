class PinboardIdea::Contracts::Update <  PinboardIdea::Contracts::Validate
  property :headline
  property :content
  property :menu_order
  property :status
  property :pinboard_id
end
