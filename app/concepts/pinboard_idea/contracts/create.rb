class PinboardIdea::Contracts::Create < PinboardIdea::Contracts::Validate
  property :headline
  property :content
  property :menu_order
  property :status
  property :pinboard_id
end
