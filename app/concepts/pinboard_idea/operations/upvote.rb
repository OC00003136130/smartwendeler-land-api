class PinboardIdea::Operations::Upvote < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:pinboard_idea_id]) }, Output(:failure) => End(:not_found)
  step :find_or_create_pinboard_idea_rating
  step ->(ctx, current_user:, **) {
    GamificationTrophy::Operations::Create.(current_user: current_user, kind: 'pinboard_idea')
  }
  fail :process_errors

  def find_or_create_pinboard_idea_rating(ctx, model:, current_user:, **)

    pinboard_idea_rating = PinboardIdeaRating.find_or_create_by!(
      pinboard_idea_id: model.id,
      user_id: current_user.id
    )

    pinboard_idea_rating.score = 1
    pinboard_idea_rating.save!

    model.score = model.pinboard_idea_ratings.sum(:score)
    model.save!

    true
  end
end
