class PinboardIdea::Operations::Create < BaseOperation
  step ->(ctx, params:, policy:, **) {
    ctx[:pinboard] = policy.find_pinboard(params[:pinboard_id])
  }, Output(:failure) => End(:not_found)

  step Model( PinboardIdea, :new )
  step :scope_model_to_user
  step :set_default_order
  step Contract::Build( constant: PinboardIdea::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()
  step :send_notification

  def set_default_order(ctx, current_organization:, params:, **)
    last_pinboard = current_organization.pinboards.order(:menu_order).last if current_organization.pinboards.present?
    if last_pinboard
      params[:menu_order] = last_pinboard.menu_order + 1
    else
      params[:menu_order] = 0
    end
    true
  end

  def send_notification(ctx, model:, pinboard:,  **)
    if model.status = 'is_checked'
      # TODO check for correct app url
      cta_link = "public/pinboards/#{pinboard.id}"
      PinboardsMailer.send_is_checked(model.user, cta_link).deliver_now
    end
    true
  end
  
  fail :process_errors
end
