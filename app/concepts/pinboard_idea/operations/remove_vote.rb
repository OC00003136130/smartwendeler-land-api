class PinboardIdea::Operations::RemoveVote < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:pinboard_idea_id]) }, Output(:failure) => End(:not_found)
  step :find_and_remove_pinboard_idea_rating
  fail :process_errors

  def find_and_remove_pinboard_idea_rating(ctx, model:, current_user:, **)
    pinboard_idea_rating = PinboardIdeaRating.find_by!(
      pinboard_idea_id: model.id,
      user_id: current_user.id
    )

    if pinboard_idea_rating
      pinboard_idea_rating.delete
      model.score = model.pinboard_idea_ratings.sum(:score)
      model.save!
      return true
    else
      raise ActiveRecord::RecordNotFound
    end
  end
end
