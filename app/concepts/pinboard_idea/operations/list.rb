class PinboardIdea::Operations::List < BaseOperation
  step ->(ctx, params:, policy:, **) {
    ctx[:pinboard] = policy.find_pinboard(params[:pinboard_id])
  }, Output(:failure) => End(:not_found)

  step :set_model_to_scoped_query_filtered
  step :retrieve_list
  fail :process_errors

  def set_model_to_scoped_query_filtered(ctx, policy:, pinboard:, **)
    ctx[:model] = policy.resolve(pinboard)
    true
  end
end
