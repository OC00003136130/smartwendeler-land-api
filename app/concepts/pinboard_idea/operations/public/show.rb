class PinboardIdea::Operations::Public::Show < Trailblazer::Operation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find_unscoped(params[:pinboard_idea_id]) }, Output(:failure) => End(:not_found)
end
