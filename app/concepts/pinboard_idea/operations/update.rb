class PinboardIdea::Operations::Update < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find_unscoped(params[:pinboard_idea_id]) }, Output(:failure) => End(:not_found)

  step Contract::Build( constant: PinboardIdea::Contracts::Update )
  step Contract::Validate()
  step Contract::Persist()
  step :handle_confirmed_idea
  step :handle_rejected_idea
  step :log_changes_on_status_change
  
  fail :process_errors

  def handle_confirmed_idea(ctx, model:, **)
    ctx['has_confirmed_status_change'] = model.saved_changes['status'] == ["is_checked", "confirmed"] || model.saved_changes['status'] == ["rejected", "confirmed"]
    if ctx['has_confirmed_status_change']
      model.approved_at = Time.now
      model.save!

      cta_link = "public/pinboards/#{model.pinboard.id}"
      PinboardsMailer.send_confirmed(model.user, cta_link).deliver_now
    end

    true
  end

  def handle_rejected_idea(ctx, model:,  **)
    ctx['has_rejected_status_change'] = model.saved_changes['status'] == ["is_checked", "rejected"] || model.saved_changes['status'] == ["confirmed", "rejected"]
    if ctx['has_rejected_status_change']
      # TODO check for correct app url
      cta_link = "public/pinboards/#{model.pinboard.id}"
      PinboardsMailer.send_rejected(model.user, cta_link).deliver_now
    end
    true
  end

  def log_changes_on_status_change(ctx, model:, current_organization:, params:, **)
    return true if !ctx['has_confirmed_status_change'] && !ctx['has_rejected_status_change']

    status_change = 'confirmed' if ctx['has_confirmed_status_change']
    status_change = 'rejected' if ctx['has_rejected_status_change']

    Log.create!(
      organization: current_organization,
      change: { status_change: status_change },
      model_class_name: model.class.name,
      model_id: model.id,
      model_record_name: model.headline,
      user_id: model.user.id,
      log_code: "pinboard_idea.#{status_change}"
    )
    true
  end
end
