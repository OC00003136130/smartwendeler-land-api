class PinboardIdea::Operations::Delete < BaseOperation
step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find_unscoped(params[:pinboard_idea_id]) }, Output(:failure) => End(:not_found)

step App::Steps::DestroyModel
fail :process_errors
end
