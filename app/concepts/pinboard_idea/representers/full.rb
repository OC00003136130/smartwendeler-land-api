class PinboardIdea::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :headline
  property :content
  property :menu_order
  property :status
  property :score
  property :approved_at

  property :user do
    property :id
    property :email
    property :firstname
    property :lastname
    property :name
    property :image_url
  end

  property :has_upvoted_pinboard_idea, getter: -> (represented:, **options) do
    return represented.pinboard_idea_ratings.where(score: 1, user_id: represented.current_user_id).present?
  end

  property :created_at
  property :updated_at
end
