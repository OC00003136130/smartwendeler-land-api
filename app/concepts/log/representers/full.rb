class Log::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :change
  property :user_id
  property :model_class_name
  property :model_id
  property :log_code
  property :model_record_name
  
  property :user do
    property :id
    property :firstname
    property :lastname
    property :name
  end

  property :created_at
  property :updated_at
end
