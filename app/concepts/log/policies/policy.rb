class Log::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :logs, :list
  end

  def resolve
    organization.logs.where(user_id: [nil, user.id])
  end
end
