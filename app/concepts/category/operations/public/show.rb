class Category::Operations::Public::Show < PublicRestrictedEndpointOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:category_id]) }, Output(:failure) => End(:not_found)
end
