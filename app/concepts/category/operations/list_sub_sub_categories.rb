class Category::Operations::ListSubSubCategories < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:parent_category] = policy.find_sub_category(params[:sub_category_id]) }, Output(:failure) => End(:not_found)

  step :set_model_to_scoped_query_filtered
  step :retrieve_list
  fail :process_errors

  def set_model_to_scoped_query_filtered(options, policy:, parent_category:, **)
    options['model'] = policy.resolve_sub_sub_categories(parent_category)
    true
  end
end
