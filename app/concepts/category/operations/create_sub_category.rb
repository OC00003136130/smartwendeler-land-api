class Category::Operations::CreateSubCategory < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:parent_category] = policy.find(params[:category_id]) }, Output(:failure) => End(:not_found)

  step Model( SubCategory, :new )
  step :scope_model_to_organization
  step :scope_model_to_parent_category
  step :set_default_order
  step Contract::Build( constant: Category::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()
  step App::Steps::AddBase64Image

  def set_default_order(ctx, parent_category:, params:, **)
    last_category = parent_category.sub_categories.order(:menu_order).last if parent_category.sub_categories.present?
    if last_category
      params[:menu_order] = last_category.menu_order + 1
    else
      params[:menu_order] = 0
    end
    true
  end

  def scope_model_to_parent_category(options, parent_category:, params:, **)
    params[:parent_id] = parent_category.id
    true
  end

  fail :process_errors
end
