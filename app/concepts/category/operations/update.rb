class Category::Operations::Update < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:category_id]) }, Output(:failure) => End(:not_found)

  step Contract::Build( constant: Category::Contracts::Update )
  step Contract::Validate()
  step Contract::Persist()
  step App::Steps::AddBase64Image
  
  fail :process_errors
end
