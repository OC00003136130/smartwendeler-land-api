class Category::Operations::Create < BaseOperation
  step Model( Category, :new )
  step :scope_model_to_organization
  step :set_default_order
  step Contract::Build( constant: Category::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()
  step App::Steps::AddBase64Image

  def set_default_order(ctx, current_organization:, params:, **)
    last_category = current_organization.categories.order(:menu_order).last if current_organization.categories.present?
    if last_category
      params[:menu_order] = last_category.menu_order + 1
    else
      params[:menu_order] = 0
    end
    true
  end
  
  fail :process_errors
end
