class Category::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :categories, :list
  end

  def list_sub_categories?
    can_access? :categories, :list_sub_categories
  end

  def list_sub_sub_categories?
    can_access? :categories, :list_sub_sub_categories
  end

  def show?
    can_access? :categories, :show
  end

  def create?
    can_access? :categories, :create
  end

  def create_sub_category?
    can_access? :categories, :create_sub_category
  end

  def create_sub_sub_category?
    can_access? :categories, :create_sub_sub_category
  end

  def update?
    can_access? :categories, :update
  end

  def delete?
    can_access? :categories, :delete
  end

  def resolve
    scope = nil
    if params && params[:scope].present?
      scope = params[:scope]
    end
    args = {
      scope: scope
    }.compact

    organization.categories.where(parent_id: nil).where(args)
  end

  def resolve_sub_categories(parent_category)
    scope = nil
    if params && params[:scope].present?
      scope = params[:scope]
    end
    args = {
      scope: scope
    }.compact

    parent_category.sub_categories.where(args)
  end

  def resolve_sub_sub_categories(parent_category)
    scope = nil
    if params && params[:scope].present?
      scope = params[:scope]
    end
    args = {
      scope: scope
    }.compact

    parent_category.sub_sub_categories.where(args)
  end

  def find(id)
    organization.categories.find id
    rescue ActiveRecord::RecordNotFound => err
  end

  def find_sub_category(id)
    organization.sub_categories.find id
    rescue ActiveRecord::RecordNotFound => err
  end
end
