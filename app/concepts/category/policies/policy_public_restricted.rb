class Category::Policies::PolicyPublicRestricted < App::Policies::PolicyPublicRestricted
  def list?
    true
  end

  def show?
    true
  end

  def list_sub_categories?
    true
  end

  def list_sub_sub_categories?
    true
  end

  def find(id)
    organization.categories.find id
    rescue ActiveRecord::RecordNotFound => err
  end

  def find_sub_category(id)
    organization.sub_categories.find id
    rescue ActiveRecord::RecordNotFound => err
  end

  def resolve
    scope = nil
    if params && params[:scope].present?
      scope = params[:scope]
    end
    args = {
      scope: scope
    }.compact

    organization.categories.where(parent_id: nil).where(args)
  end

  def resolve_sub_categories(parent_category)
    parent_category.sub_categories
  end

  def resolve_sub_sub_categories(parent_category)
    parent_category.sub_sub_categories
  end
end
