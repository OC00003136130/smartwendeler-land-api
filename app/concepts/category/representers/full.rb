class Category::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :name
  property :menu_order
  property :tags
  property :scope
  property :url
  property :url_kind
  property :description
  property :button_text

  property :name_with_projects_count
  property :projects_count
  property :name_with_pinboards_count
  property :pinboards_count
  property :pinboards_with_confirmed_ideas_count
  property :active_and_started_pinboards_count
  property :care_facilities_count
  property :care_facilities_active_count
  property :name_with_care_facilities_count
  property :image_url

  collection :sub_categories do
    property :id
    property :name
    property :description
    property :tags
    property :image_url
    property :button_text
    property :url
    property :url_kind

    collection :sub_sub_categories do
      property :id
      property :name
      property :description
      property :tags
      property :image_url
      property :button_text
      property :url
      property :url_kind
    end
  end

  collection :sub_sub_categories do
    property :id
    property :name
    property :tags
    property :description
    property :image_url
    property :button_text
    property :url
    property :url_kind
  end

  property :created_at
  property :updated_at
end
