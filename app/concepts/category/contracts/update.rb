class Category::Contracts::Update <  Category::Contracts::Validate
  property :name
  property :menu_order
  property :tags
  property :scope
  property :url
  property :url_kind
  property :description
  property :button_text
end
