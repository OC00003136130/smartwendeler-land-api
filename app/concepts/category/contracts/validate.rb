class Category::Contracts::Validate < Reform::Form
  validation do
    params do
      required(:name).filled(:string)
      required(:scope).filled(:string)
    end
  end
end
