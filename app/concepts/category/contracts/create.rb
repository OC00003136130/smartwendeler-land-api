class Category::Contracts::Create < Category::Contracts::Validate
  property :name
  property :menu_order
  property :tags
  property :parent_id
  property :scope
  property :url
  property :url_kind
  property :description
  property :button_text
end
