class GamificationReward::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :gamification_rewards, :list
  end

  def show?
    can_access? :gamification_rewards, :show
  end

  def create?
    can_access? :gamification_rewards, :create
  end

  def update?
    can_access? :gamification_rewards, :update
  end

  def delete?
    can_access? :gamification_rewards, :delete
  end

  def attach_image?
    can_access? :gamification_rewards, :attach_image
  end

  def purge_image?
    can_access? :gamification_rewards, :purge_image
  end

  def resolve(gamification_game)
    gamification_game.gamification_rewards
  end

  def find_game(id)
    organization.gamification_games.find id
    rescue ActiveRecord::RecordNotFound => err
  end

  def find_unscoped(id)
    organization.gamification_rewards.find id
    rescue ActiveRecord::RecordNotFound => err
  end

  def find(game, id)
    game.gamification_rewards.find id
    rescue ActiveRecord::RecordNotFound => err
  end
end
