class GamificationReward::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :threshold
  property :name

  property :created_at
  property :updated_at
end
