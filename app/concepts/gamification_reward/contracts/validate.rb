class GamificationReward::Contracts::Validate < Reform::Form
  validation do
    params do
      required(:name).filled(:string)
      required(:threshold).filled(:integer)
    end
  end
end
