class GamificationReward::Contracts::Create < GamificationReward::Contracts::Validate
  property :name
  property :threshold
end
