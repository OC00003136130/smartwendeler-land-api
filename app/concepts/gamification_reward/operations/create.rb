class GamificationReward::Operations::Create < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:game] = policy.find_game(params[:gamification_game_id]) }, Output(:failure) => End(:not_found)

  step Model( GamificationReward, :new )
  step ->(ctx, model:, game:, **) {  model.gamification_game_id = game.id }
  step Contract::Build( constant: GamificationReward::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()
  
  fail :process_errors
end
