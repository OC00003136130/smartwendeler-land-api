class GamificationReward::Operations::List < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:game] = policy.find_game(params[:gamification_game_id]) }, Output(:failure) => End(:not_found)

  step :set_model_to_scoped_query_filtered
  step :retrieve_list
  fail :process_errors

  def set_model_to_scoped_query_filtered(ctx, game:, policy:, **)
    ctx['model'] = policy.resolve(game)
  end
end
