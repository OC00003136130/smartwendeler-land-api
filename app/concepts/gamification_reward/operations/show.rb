class GamificationReward::Operations::Show < Trailblazer::Operation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find_unscoped(params[:gamification_reward_id]) }, Output(:failure) => End(:not_found)
end
