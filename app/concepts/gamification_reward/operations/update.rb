class GamificationReward::Operations::Update < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find_unscoped(params[:gamification_reward_id]) }, Output(:failure) => End(:not_found)

  step Contract::Build( constant: GamificationReward::Contracts::Update )
  step Contract::Validate()
  step Contract::Persist()
  
  fail :process_errors
end
