class Comment::Contracts::Create < Comment::Contracts::Validate
  property :comment
  property :project_id
  property :parent_id
end
