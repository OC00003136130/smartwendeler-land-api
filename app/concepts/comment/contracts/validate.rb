class Comment::Contracts::Validate < Reform::Form
  validation do
    params do
      required(:comment).filled(:string)
    end
  end
end
