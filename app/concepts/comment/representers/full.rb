class Comment::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :comment
  property :score
  property :upvote_count
  property :downvote_count
  property :replies_count
  property :project_id
  property :reported_history

  property :project do
    property :id
    property :name
  end

  property :user do
    property :id
    property :firstname
    property :lastname
    property :name
    property :image_url
  end

  property :has_already_reported_comment, getter: -> (represented:, **options) do
    return represented.comment_reports.where(user_id: represented.current_user_id).present?
  end

  property :has_upvoted_comment, getter: -> (represented:, **options) do
    return represented.comment_ratings.where(score: 1, user_id: represented.current_user_id).present?
  end

  property :has_downvoted_comment, getter: -> (represented:, **options) do
    return represented.comment_ratings.where(score: -1, user_id: represented.current_user_id).present?
  end

  property :created_at
  property :updated_at
end
