class Comment::Operations::Upvote < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find_unscoped(params[:comment_id]) }, Output(:failure) => End(:not_found)
  step :find_or_create_comment_rating
  step :log_changes
  fail :process_errors

  def find_or_create_comment_rating(ctx, model:, current_user:, **)

    comment_rating = CommentRating.find_or_create_by!(
      comment_id: model.id,
      user_id: current_user.id
    )

    comment_rating.score = 1
    comment_rating.save!

    model.score = model.comment_ratings.sum(:score)
    model.save!

    true
  end

  def log_changes(ctx, model:, current_organization:, current_user:, **)
    return true if current_user.id == model.user.id
    
    Log.create!(
      organization: current_organization,
      change: {},
      model_class_name: model.class.name,
      model_id: model.project.id,
      model_record_name: model.name,
      user_id: model.user.id,
      log_code: 'comment.liked'
    )
    true
  end
end
