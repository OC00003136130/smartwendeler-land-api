class Comment::Operations::Downvote < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find_unscoped(params[:comment_id]) }, Output(:failure) => End(:not_found)
  step :find_or_create_comment_rating
  fail :process_errors

  def find_or_create_comment_rating(ctx, model:, current_user:, **)
    comment_rating = CommentRating.find_or_create_by!(
      comment_id: model.id,
      user_id: current_user.id
    )

    comment_rating.score = -1
    comment_rating.save!

    model.score = model.comment_ratings.sum(:score)
    model.save!

    true
  end
end
