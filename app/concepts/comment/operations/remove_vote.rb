class Comment::Operations::RemoveVote < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find_unscoped(params[:comment_id]) }, Output(:failure) => End(:not_found)
  step :find_and_remove_comment_rating
  fail :process_errors

  def find_and_remove_comment_rating(ctx, model:, current_user:, **)
    comment_rating = CommentRating.find_by!(
      comment_id: model.id,
      user_id: current_user.id
    )

    if comment_rating
      comment_rating.delete
      model.score = model.comment_ratings.sum(:score)
      model.save!
      return true
    else
      raise ActiveRecord::RecordNotFound
    end
  end
end
