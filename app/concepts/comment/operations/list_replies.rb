class Comment::Operations::ListReplies < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:comment] = policy.find_unscoped(params[:comment_id]) }, Output(:failure) => End(:not_found)
  step :set_model_to_scoped_query_filtered
  step :retrieve_list
  fail :process_errors

  def set_model_to_scoped_query_filtered(ctx, policy:, comment:, **)
    ctx[:model] = policy.resolve_replies(comment)
    true
  end
end
