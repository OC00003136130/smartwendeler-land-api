class Comment::Operations::Create < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:project] = policy.find_project(params[:project_id]) }, Output(:failure) => End(:not_found)

  step :block_if_cannot_comment
  step Model( Comment, :new )
  step :scope_model_to_organization
  step :scope_model_to_user
  step Contract::Build( constant: Comment::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()
  step :send_email_notification_to_parent
  step :log_changes_on_replies
  step ->(ctx, current_user:, **) {
    GamificationTrophy::Operations::Create.(current_user: current_user, kind: 'comment')
  }

  def block_if_cannot_comment(ctx, current_user:, **)
    if !current_user.can_comment
      add_error ctx, message: 'Comment feature blocked!', exception: nil, code: 'comment.blocked', field_name: 'comment'
      return false
    end
    true
  end

  def send_email_notification_to_parent(ctx, model:, params:, current_user:, **)
    return true if !params[:parent_id] || current_user.id == model.parent.user.id

    if model.parent.user.notification_preferences.include? 'comment_reply'
      UsersMailer.send_notification(model.parent.user, "Neue Antwort auf Dein Kommentar", "Jemand hat auf Deinen Kommentar im Projekt #{model.project.name} geantwortet. Inhalt des Kommentars: \"#{model.comment}\"", "public/projects/#{model.project.id}").deliver_now
    end
    true
  end

  def log_changes_on_replies(ctx, model:, current_organization:, current_user:, params:, **)
    return true if !params[:parent_id] || current_user.id == model.parent.user.id
    Log.create!(
      organization: current_organization,
      change: { replied_by: current_user.name },
      model_class_name: model.class.name,
      model_id: model.project.id,
      model_record_name: model.name,
      user_id: model.user.id,
      log_code: 'comment.replied'
    )
    true
  end
  
  fail :process_errors
end
