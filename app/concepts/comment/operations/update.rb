class Comment::Operations::Update < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:comment_id]) }, Output(:failure) => End(:not_found)
  step Contract::Build( constant: Comment::Contracts::Update )
  
  step Contract::Validate()
  step Contract::Persist()
  
  fail :process_errors
end
