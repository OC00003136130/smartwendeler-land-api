class Comment::Operations::Show < Trailblazer::Operation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:comment_id]) }, Output(:failure) => End(:not_found)
end
