class Comment::Operations::Public::List < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:project] = policy.find_project(params[:project_id]) }, Output(:failure) => End(:not_found)

  step :set_model_to_scoped_query_filtered
  step :retrieve_list
  fail :process_errors

  def set_model_to_scoped_query_filtered(ctx, policy:, project:, **)
    ctx[:model] = policy.resolve(project)
    true
  end
end
