class Comment::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :comments, :list
  end

  def list_all?
    can_access? :comments, :list_all
  end

  def list_replies?
    can_access? :comments, :list_replies
  end

  def show?
    can_access? :comments, :show
  end

  def create?
    return (attributes_allowed?(:comments) && can_access?(:comments, :create))
  end

  def update?
    can_access? :comments, :update
  end

  def delete?
    can_access? :comments, :delete
  end

  def upvote?
    can_access? :comments, :upvote
  end

  def downvote?
    can_access? :comments, :downvote
  end

  def remove_vote?
    can_access? :comments, :remove_vote
  end

  def resolve(project)
    project.comments.where(parent_id: nil)
  end

  def resolve_all
    organization.comments
  end

  def resolve_replies(comment)
    comment.replies
  end

  def find_project(project_id)
    organization.projects.find project_id
    rescue ActiveRecord::RecordNotFound => err
  end

  def find(id)
    case user.role
    when 'root', 'admin'
      organization.comments.find id
    when 'user'
      user.comments.find id
    end

    rescue ActiveRecord::RecordNotFound => err
  end

  def find_unscoped(id)
    organization.comments.find id
    rescue ActiveRecord::RecordNotFound => err
  end
end
