class Location::Contracts::Create < Location::Contracts::Validate
  property :longitude
  property :latitude
  property :project_id
  property :care_facility_id
  property :street
  property :zip
  property :town
end
