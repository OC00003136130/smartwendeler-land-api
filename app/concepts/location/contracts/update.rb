class  Location::Contracts::Update <  Location::Contracts::Validate
  property :longitude
  property :latitude
  property :street
  property :zip
  property :town
end
