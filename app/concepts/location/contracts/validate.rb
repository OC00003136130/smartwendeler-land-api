class Location::Contracts::Validate < Reform::Form
  validation do
    params do
      required(:longitude).filled(:decimal)
      required(:latitude).filled(:decimal)
    end
  end
end
