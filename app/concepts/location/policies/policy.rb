class Location::Policies::Policy < App::Policies::Policy
  def create_for_project?
    can_access? :locations, :create
  end

  def create_for_care_facility?
    can_access? :locations, :create
  end

  def update?
    can_access? :locations, :update
  end

  def delete?
    can_access? :locations, :delete
  end

  def find_project(project_id)
    organization.projects.find project_id
    rescue ActiveRecord::RecordNotFound => err
  end

  def find_care_facility(care_facility_id)
    if user.role == 'facility_owner'
      user.care_facilities.find care_facility_id
    else
      organization.care_facilities.find care_facility_id
    end
    rescue ActiveRecord::RecordNotFound => err
  end

  def find(id)
    case user.role
    when 'root', 'admin'
      organization.locations.find id
    when 'facility_owner'
      Location.joins(care_facility: :user).where(users: { id: user.id }).find id
    end

    rescue ActiveRecord::RecordNotFound => err
  end

  def find_unscoped(id)
    organization.locations.find id
  end
end
