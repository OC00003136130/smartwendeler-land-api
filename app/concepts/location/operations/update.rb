class Location::Operations::Update < BaseOperation

  step ->(ctx, params:, policy:, **) { 
    ctx[:model] = policy.find(params[:location_id])
  }, Output(:failure) => End(:not_found)

  step Contract::Build( constant: Location::Contracts::Update )
  step Contract::Validate()
  step Contract::Persist()

  fail :process_errors
end
