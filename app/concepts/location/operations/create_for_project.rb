class Location::Operations::CreateForProject < BaseOperation
  step ->(ctx, params:, policy:, **) {
    ctx[:project] = policy.find_project(params[:project_id])
  }, Output(:failure) => End(:not_found)

  step Model( Location, :new )
  step :scope_model_to_organization
  step Contract::Build( constant: Location::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()

  fail :process_errors
end
