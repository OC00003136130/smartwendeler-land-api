class Location::Operations::CreateForCareFacility < BaseOperation
  step ->(ctx, params:, policy:, **) {
    ctx[:care_facility] = policy.find_care_facility(params[:care_facility_id])
  }, Output(:failure) => End(:not_found)

  step Model( Location, :new )
  step :scope_model_to_organization
  step Contract::Build( constant: Location::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()

  fail :process_errors
end
