class Location::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :longitude
  property :latitude
  property :street
  property :zip
  property :town

  property :created_at
  property :updated_at
end
