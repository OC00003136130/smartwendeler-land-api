class PollAnswer::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :poll_answers, :list
  end

  def create_update?
    can_access? :poll_answers, :create_update
  end

  def resolve(poll_question)

    user_id = nil

    if params && params[:user_id].present?
      user_id = params[:user_id]
    end

    args = {
      user_id: user_id
    }.compact

    poll_question.poll_answers.where(args)
  end

  def find_poll_question(poll_question_id)
    organization.poll_questions.find poll_question_id
    rescue ActiveRecord::RecordNotFound => err
  end
end
