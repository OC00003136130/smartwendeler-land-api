class PollAnswer::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :rating_value
  property :text_value
  property :choices_answers

  property :created_at
  property :updated_at
end
