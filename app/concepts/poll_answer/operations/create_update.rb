class PollAnswer::Operations::CreateUpdate < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:poll_question] = policy.find_poll_question(params[:poll_question_id]) }, Output(:failure) => End(:not_found)

  step :find_or_create_poll_answer
  step Contract::Build( constant: PollAnswer::Contracts::CreateUpdate )
  step Contract::Validate()
  step Contract::Persist()
  step ->(ctx, current_user:, **) {
    GamificationTrophy::Operations::Create.(current_user: current_user, kind: 'poll')
  }
  
  fail :process_errors

  def find_or_create_poll_answer(ctx, params:, current_user:, current_organization:, poll_question:, **)
    ctx['model'] = PollAnswer.find_or_create_by(
      organization: current_organization,
      user: current_user,
      poll_question_id: poll_question.id
    )
    return true
  end
end
