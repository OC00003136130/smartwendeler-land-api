class PollAnswer::Operations::List < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:poll_question] = policy.find_poll_question(params[:poll_question_id]) }, Output(:failure) => End(:not_found)
  step :set_model_to_scoped_query_filtered
  step :retrieve_list
  fail :process_errors

  def set_model_to_scoped_query_filtered(ctx, policy:, poll_question:, **)
    ctx[:model] = policy.resolve(poll_question)
    true
  end
end
