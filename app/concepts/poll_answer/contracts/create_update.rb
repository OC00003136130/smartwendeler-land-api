class PollAnswer::Contracts::CreateUpdate < PollAnswer::Contracts::Validate
  property :rating_value
  property :text_value
  property :choices_answers
end
