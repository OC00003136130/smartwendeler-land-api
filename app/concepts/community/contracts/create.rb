class Community::Contracts::Create < Community::Contracts::Validate
  property :name
  property :menu_order
  property :towns
  property :zip
end
