class Community::Contracts::Update < Community::Contracts::Validate
  property :name
  property :menu_order
  property :towns
  property :zip
end
