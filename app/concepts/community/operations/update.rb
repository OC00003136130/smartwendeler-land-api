class Community::Operations::Update < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:community_id]) }, Output(:failure) => End(:not_found)

  step Contract::Build( constant: Community::Contracts::Update )
  step Contract::Validate()
  step Contract::Persist()
  
  fail :process_errors
end
