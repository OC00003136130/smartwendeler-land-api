class Community::Operations::Create < BaseOperation
  step Model( Community, :new )
  step :scope_model_to_organization
  step :set_default_order
  step Contract::Build( constant: Community::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()

  def set_default_order(ctx, current_organization:, params:, **)
    last_community = current_organization.communities.order(:menu_order).last if current_organization.communities.present?
    if last_community
      params[:menu_order] = last_community.menu_order + 1
    else
      params[:menu_order] = 0
    end
    true
  end
  
  fail :process_errors
end
