class Community::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :name
  property :menu_order
  property :towns
  property :zip

  property :projects_count
  property :name_with_projects_count
  property :care_facilities_count
  property :care_facilities_active_count

  property :created_at
  property :updated_at
end
