class Community::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :communities, :list
  end

  def show?
    can_access? :communities, :show
  end

  def create?
    can_access? :communities, :create
  end

  def update?
    can_access? :communities, :update
  end

  def delete?
    can_access? :communities, :delete
  end

  def attach_image?
    can_access? :communities, :attach_image
  end

  def purge_image?
    can_access? :communities, :purge_image
  end

  def resolve
    organization.communities
  end

  def find(id)
    organization.communities.find id
    rescue ActiveRecord::RecordNotFound => err
  end
end
