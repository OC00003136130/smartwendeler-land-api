class Milestone::Contracts::Update < Milestone::Contracts::Validate
  property :name
  property :timestamp
  property :menu_order
end
