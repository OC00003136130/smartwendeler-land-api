class Milestone::Contracts::Create < Milestone::Contracts::Validate
  property :name
  property :timestamp
  property :project_id
  property :menu_order
end
