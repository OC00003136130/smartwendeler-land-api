class Milestone::Contracts::Validate < Reform::Form
  validation do
    params do
      required(:name).filled(:string)
    end
  end
end
