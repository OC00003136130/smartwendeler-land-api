class Milestone::Policies::PolicyPublicRestricted < App::Policies::PolicyPublicRestricted
  def list?
    true
  end

  def find_project(project_id)
    organization.projects.find project_id
    rescue ActiveRecord::RecordNotFound => err
  end

  def resolve(project)
    project.milestones
  end
end
