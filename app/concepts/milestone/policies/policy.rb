class Milestone::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :milestones, :list
  end

  def show?
    can_access? :milestones, :show
  end

  def create?
    can_access? :milestones, :create
  end

  def update?
    can_access? :milestones, :update
  end

  def delete?
    can_access? :milestones, :delete
  end

  def resolve(project)
    project.milestones
  end

  def find_project(project_id)
    organization.projects.find project_id
    rescue ActiveRecord::RecordNotFound => err
  end

  def find(id)
    organization.milestones.find id
    rescue ActiveRecord::RecordNotFound => err
  end

  def find_unscoped(id)
    organization.milestones.find id
  end
end
