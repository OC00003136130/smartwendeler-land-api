class Milestone::Operations::Create < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:project] = policy.find_project(params[:project_id]) }, Output(:failure) => End(:not_found)

  step Model( Milestone, :new )
  step :scope_model_to_organization
  step :set_default_order
  step Contract::Build( constant: Milestone::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()

  def set_default_order(ctx, project:, params:, **)
    last_milestone = project.milestones.order(:menu_order).last if project.milestones.present?
    if last_milestone
      params[:menu_order] = last_milestone.menu_order + 1
    else
      params[:menu_order] = 0
    end
    true
  end

  fail :process_errors
end
