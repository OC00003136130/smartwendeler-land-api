class Milestone::Operations::Show < Trailblazer::Operation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:milestone_id]) }, Output(:failure) => End(:not_found)
end
