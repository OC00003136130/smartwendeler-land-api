class Notification::Operations::Send < BaseOperation
  require 'uri'
  require 'net/http'
  require 'openssl'

  step :check_params
  step :send_email_notifications
  step :send_push_notifications

  def check_params(ctx, params:, **)
    if !params[:kind].present? || !params[:text].present? || !params[:headline].present?
      add_error ctx, message: 'Required parameters kind, text and headline not filled!', exception: nil, code: 'notifications.required_params_not_provided', field_name: nil
      return false
    end
    true
  end

  def send_email_notifications(ctx, params:, policy:, **)
    users = policy.resolve_users(params[:kind])

    users.each do |user|
      UsersMailer.send_notification(user, params[:headline], params[:text], params[:cta_link]).deliver_now
    end

    ctx[:result] = { code: 'notifications.successfully_delivered' }
    true
  end

  def send_push_notifications(ctx, params:, **)
    return true if params[:app_notification_enabled] == 'false' || params[:app_notification_enabled] == false

    url = URI("https://onesignal.com/api/v1/notifications")

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true

    request = Net::HTTP::Post.new(url)
    request["accept"] = 'application/json'
    request["Authorization"] = "Basic #{Rails.application.credentials.dig(:onesignal, :api_key)}"
    request["content-type"] = 'application/json'
    request.body = "{\"included_segments\":[\"Subscribed Users\"],\"contents\":{\"en\":\"#{params[:headline]}\",\"de\":\"#{params[:headline]}\"}, \"app_id\":\"#{Rails.application.credentials.dig(:onesignal, :app_id)}\"}"

    response = http.request(request)
    puts response.read_body

    true
  end
  
  fail :process_errors
end
