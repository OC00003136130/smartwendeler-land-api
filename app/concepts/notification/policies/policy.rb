class Notification::Policies::Policy < App::Policies::Policy
  def send?
    can_access? :notifications, :send
  end

  # TODO: refactor
  def resolve_users(notification_kind)
    user_ids = []
    organization.users.each do |user|
      if user.notification_preferences.include? notification_kind
        user_ids << user.id
      end
    end
    organization.users.where(id: user_ids)
  end
end
