class CareFacility::Operations::Create < BaseOperation
  step Model( CareFacility, :new )
  step :scope_model_to_organization
  step :scope_model_to_user
  step :add_example_opening_hours
  step Contract::Build( constant: CareFacility::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()
  step App::Steps::AddBase64Image
  step App::Steps::AddBase64Logo
  step App::Steps::AddBase64Document
  step CareFacility::Steps::AssignCategories
  step CareFacility::Steps::AssignSubCategories
  step CareFacility::Steps::AssignSubSubCategories
  step CareFacility::Steps::AssignTagCategories
  step CareFacility::Steps::AssignTags
  
  fail :process_errors

  def add_example_opening_hours(ctx, params:, **)
    if !params[:opening_hours].present?
      params[:opening_hours] = [
        { day: "Montag", placeholder: "z.B. 08:00 - 12:00 und 13:00 - 17:00", hours: "" },
        { day: "Dienstag", placeholder: "z.B. 08:00 - 12:00 und 13:00 - 17:00", hours: "" },
        { day: "Mittwoch", placeholder: "z.B. 08:00 - 12:00 und 13:00 - 17:00", hours: "" },
        { day: "Donnerstag", placeholder: "z.B. 08:00 - 12:00 und 13:00 - 17:00", hours: "" },
        { day: "Freitag", placeholder: "z.B. 08:00 - 12:00 und 13:00 - 17:00", hours: "" },
        { day: "Samstag", placeholder: "z.B. geschlossen", hours: "" },
        { day: "Sonntag", placeholder: "z.B. geschlossen", hours: ""}
      ]
    end

    true
  end
end
