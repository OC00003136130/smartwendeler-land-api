class CareFacility::Operations::Update < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:care_facility_id]) }, Output(:failure) => End(:not_found)
  step Contract::Build( constant: CareFacility::Contracts::Update )
  step Contract::Validate()
  step Contract::Persist()
  step :reset_health_scope_status_on_attribute_change
  step :insert_geocode_address
  
  step App::Steps::AddBase64Image
  step App::Steps::AddBase64Logo
  step App::Steps::AddBase64Document
  step CareFacility::Steps::AssignCategories
  step CareFacility::Steps::AssignSubCategories
  step CareFacility::Steps::AssignSubSubCategories
  step CareFacility::Steps::AssignTagCategories
  step CareFacility::Steps::AssignTags
  
  
  fail :process_errors

  def reset_health_scope_status_on_attribute_change(ctx, model:, current_user:, **)
    return true if current_user.role != 'facility_owner'
    relevant_info_changed = false
    relevant_info_changed = model.saved_changes.keys.include?('phone') || model.saved_changes.keys.include?('zip') || model.saved_changes.keys.include?('street') || model.saved_changes.keys.include?('community_id') || model.saved_changes.keys.include?('town') || model.saved_changes.keys.include?('email')

    if relevant_info_changed
      current_user.is_active_on_health_scope = false
      current_user.save!
      NotificationsMailer.send_update_user_notification(current_user).deliver_now
    end

    true
  end

  def insert_geocode_address(options, model:, **)
    if !Rails.env.test? && (model.saved_changes.keys.include?('zip') || model.saved_changes.keys.include?('street') || model.saved_changes.keys.include?('town'))
      response = HTTParty.get(
        "https://geocode.maps.co/search?postalcode=#{URI.encode_www_form_component(model.zip)}&street=#{URI.encode_www_form_component(model.street)}&country=DE&city=#{URI.encode_www_form_component(model.town)}"
      )
      result = JSON.parse(response.body)
      model.geocode_address = result
      model.save!
    end
    true
  end
end
