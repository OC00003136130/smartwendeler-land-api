class CareFacility::Operations::AttachDocument < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:care_facility_id]) }, Output(:failure) => End(:not_found)
  step ->(ctx, params:, **) { params[:multiple] = true }
  step App::Steps::AddBase64Document
  
  fail :process_errors
end
