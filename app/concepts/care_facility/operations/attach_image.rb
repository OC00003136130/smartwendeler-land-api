class CareFacility::Operations::AttachImage < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:care_facility_id]) }, Output(:failure) => End(:not_found)
  step ->(ctx, params:, **) { params[:multiple] = true }
  step App::Steps::AddBase64Image
  
  fail :process_errors
end
