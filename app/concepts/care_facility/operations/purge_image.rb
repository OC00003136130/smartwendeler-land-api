class CareFacility::Operations::PurgeImage < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:care_facility_id]) }, Output(:failure) => End(:not_found)
  step ->(ctx, params:, **) { ctx[:image] = ActiveStorage::Blob.find_signed(params[:signed_id]) }, Output(:failure) => End(:not_found)
  step :purge_image

  def purge_image(ctx, image:, **)
    image.attachments.first.purge
    true
  end
  
  fail :process_errors
end
