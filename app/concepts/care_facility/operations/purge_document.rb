class CareFacility::Operations::PurgeDocument < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:care_facility_id]) }, Output(:failure) => End(:not_found)
  step ->(ctx, params:, **) { ctx[:document] = ActiveStorage::Blob.find_signed(params[:signed_id]) }, Output(:failure) => End(:not_found)
  step :purge_document

  def purge_document(ctx, document:, **)
    document.attachments.first.purge
    true
  end
  
  fail :process_errors
end
