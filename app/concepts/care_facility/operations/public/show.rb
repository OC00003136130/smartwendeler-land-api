class CareFacility::Operations::Public::Show < PublicRestrictedEndpointOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:care_facility_id]) }, Output(:failure) => End(:not_found)
end
