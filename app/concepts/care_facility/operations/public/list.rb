class CareFacility::Operations::Public::List < PublicRestrictedEndpointOperation
  step :set_model_to_scoped_query
  step :retrieve_list
  fail :process_errors
end
