class CareFacility::Operations::Show < Trailblazer::Operation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:care_facility_id]) }, Output(:failure) => End(:not_found)
end
