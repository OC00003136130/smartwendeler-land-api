class CareFacility::Steps::AssignTags < App::Steps::Base
  extend Uber::Callable

  def self.call(options, model:, params:, current_organization:, **)
    if params[:tag_ids].present?
      tags = current_organization.tags.where(id: params[:tag_ids])

      # check which old tags have to be deleted
      care_facility_tag_ids = model.care_facility_tags.pluck(:tag_id)
      to_be_deleted_ids = care_facility_tag_ids.difference(params[:tag_ids])
      to_be_deleted_tags = model.tags.where(id: to_be_deleted_ids)

      if to_be_deleted_tags.present?
        to_be_deleted_tags.each do |del_fac|
          model.tags.delete(del_fac)
        end
      end

      tags.each do |fac|
        if (!model.tags.where(id: fac.id).present?)
          model.tags << fac
        end
      end
    end

    true
  end
end
