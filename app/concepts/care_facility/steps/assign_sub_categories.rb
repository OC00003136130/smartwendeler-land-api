class CareFacility::Steps::AssignSubCategories < App::Steps::Base
  extend Uber::Callable

  def self.call(options, model:, params:, current_organization:, **)
    if params[:sub_category_ids].present?
      sub_categories = current_organization.sub_categories.where(id: params[:sub_category_ids])

      # check which old sub_categories have to be deleted
      care_facility_sub_category_ids = model.care_facility_sub_categories.pluck(:sub_category_id)
      to_be_deleted_ids = care_facility_sub_category_ids.difference(params[:sub_category_ids])
      to_be_deleted_sub_categories = model.sub_categories.where(id: to_be_deleted_ids)

      if to_be_deleted_sub_categories.present?
        to_be_deleted_sub_categories.each do |del_fac|
          model.sub_categories.delete(del_fac)
        end
      end

      sub_categories.each do |fac|
        if (!model.sub_categories.where(id: fac.id).present?)
          model.sub_categories << fac
        end
      end
    end

    true
  end
end
