class CareFacility::Steps::AssignTagCategories < App::Steps::Base
  extend Uber::Callable

  def self.call(options, model:, params:, current_organization:, **)
    if params[:tag_category_ids].present?
      tag_categories = current_organization.tag_categories.where(id: params[:tag_category_ids])

      # check which old tag_categories have to be deleted
      care_facility_tag_category_ids = model.care_facility_tag_categories.pluck(:tag_category_id)
      to_be_deleted_ids = care_facility_tag_category_ids.difference(params[:tag_category_ids])
      to_be_deleted_tag_categories = model.tag_categories.where(id: to_be_deleted_ids)

      if to_be_deleted_tag_categories.present?
        to_be_deleted_tag_categories.each do |del_fac|
          model.tag_categories.delete(del_fac)
        end
      end

      tag_categories.each do |fac|
        if (!model.tag_categories.where(id: fac.id).present?)
          model.tag_categories << fac
        end
      end
    end

    true
  end
end
