class CareFacility::Policies::PolicyPublicRestricted < App::Policies::PolicyPublicRestricted
  def list?
    true
  end

  def show?
    true
  end

  def find(id)
    cf = organization.care_facilities.joins(:user).where(user: { is_active_on_health_scope: true }).find_by(slug: id, is_active: true)
    cf = organization.care_facilities.joins(:user).where(user: { is_active_on_health_scope: true }).find_by!(id: id, is_active: true) if cf.nil?
    return cf
    rescue ActiveRecord::RecordNotFound => err
  end

  def resolve
    
    # tags = null
    # if params[:filter_tags] && params[:filter_tags] != 'null'
    #   tags = params[:filter_tags]
    # end

    kinds = params[:kind].split(',') if params[:kind]

    args = {
      kind: kinds
    }.compact

    organization.care_facilities.where(is_active: true).where(args).joins(:user).where(user: { is_active_on_health_scope: true })
  end

  def resolve_with_search(fields=[:name, :zip, :town], exact_match=false)
    return add_search_query(self.resolve, params[:search], model, fields, exact_match)
  end
end
