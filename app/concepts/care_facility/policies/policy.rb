class CareFacility::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :care_facilities, :list
  end

  def show?
    can_access? :care_facilities, :show
  end

  def create?
    can_access? :care_facilities, :create
  end

  def update?
    can_access? :care_facilities, :update
  end

  def delete?
    can_access? :care_facilities, :delete
  end

  def attach_image?
    can_access? :care_facilities, :attach_image
  end

  def purge_image?
    can_access? :care_facilities, :purge_image
  end

  def attach_document?
    can_access? :care_facilities, :attach_document
  end

  def purge_document?
    can_access? :care_facilities, :purge_document
  end

  def resolve

    kinds = params[:kind].split(',') if params[:kind]

    args = {
      kind: kinds
    }.compact

    if user.role == 'facility_owner'
      user.care_facilities.where(args)
    else
      organization.care_facilities.where(args)
    end
  end

  def find(id)
    if user.role == 'facility_owner'
      user.care_facilities.find id
    else
      organization.care_facilities.find id
    end
    
    rescue ActiveRecord::RecordNotFound => err
  end

  def resolve_with_search(fields=[:name], exact_match=false)
    return add_search_query(self.resolve, params[:search], model, fields, exact_match)
  end
end
