class CareFacility::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :name
  property :phone
  property :email
  property :website
  property :town
  property :zip
  property :street
  property :description
  property :excerpt
  property :is_active
  property :category_ids
  property :sub_category_ids
  property :sub_sub_category_ids
  property :tag_category_ids
  property :logo_url
  property :image_url
  property :kind
  property :email
  property :sanitized_images
  property :sanitized_documents
  property :opening_hours
  property :course_start
  property :course_end
  property :community_id
  property :additional_address_info
  property :event_dates
  property :billable_through_health_insurance
  property :health_insurance_name
  property :billable_through_health_insurance_approved
  property :tag_ids
  property :user_care_facility
  property :name_instructor
  property :course_outside_facility
  property :name_responsible_person
  property :geocode_address
  property :event_duration
  property :slug

  collection :categories do
    property :id
    property :name
  end

  collection :sub_categories do
    property :id
    property :name
  end

  collection :sub_sub_categories do
    property :id
    property :name
  end

  collection :tag_categories do
    property :id
    property :name
  end

  collection :tags do
    property :id
    property :name
  end

  property :community do
    property :id
    property :name
    property :zip
  end

  property :user do
    property :id
    property :name
    property :firstname
    property :lastname
    property :image_url
    property :is_active_on_health_scope
    property :role
    property :onboarding_token
    property :imported
    property :last_seen
    collection :care_facilities do
      property :id
      property :name
      property :slug
      property :kind
      property :description
      property :logo_url
      property :image_url
      property :event_dates
    end
  end

  collection :locations

  property :created_at
  property :updated_at
end
