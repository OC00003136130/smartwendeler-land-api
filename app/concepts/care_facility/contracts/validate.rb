class CareFacility::Contracts::Validate < Reform::Form
  validation do
    params do
      required(:name).filled(:string)
      required(:kind).filled(:string)
    end

    rule(:kind) do
      key.failure("must be either #{CareFacility.kinds.keys}") if !CareFacility.kinds.keys.include? values[:kind]
    end
  end
end
