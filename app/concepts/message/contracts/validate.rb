class Message::Contracts::Validate < Reform::Form
  validation do
    params do
      required(:subject).filled(:string)
      required(:message).filled(:string)
      required(:email).filled(:string)
    end
  end
end
