class Message::Contracts::Create < Message::Contracts::Validate
  property :user_id
  property :subject
  property :message
  property :email
  property :firstname
  property :lastname
  property :project_id
  property :replied
end
