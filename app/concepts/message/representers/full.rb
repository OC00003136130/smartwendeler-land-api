class Message::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :user_id
  property :subject
  property :message
  property :email
  property :firstname
  property :lastname
  property :project_id
  property :replied

  property :user do
    property :id
    property :name
    property :firstname
    property :lastname
    property :email
  end

  property :project do
    property :id
    property :name
  end

  property :created_at
  property :updated_at
end
