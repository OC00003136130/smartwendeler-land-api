class Message::Operations::Show < Trailblazer::Operation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:message_id]) }, Output(:failure) => End(:not_found)
end
