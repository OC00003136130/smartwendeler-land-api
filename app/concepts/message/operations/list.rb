class Message::Operations::List < BaseOperation
  step :set_model_to_scoped_query_filtered
  step :retrieve_list
  fail :process_errors

  def set_model_to_scoped_query_filtered(ctx, policy:, **)
    ctx[:model] = policy.resolve
    true
  end
end
