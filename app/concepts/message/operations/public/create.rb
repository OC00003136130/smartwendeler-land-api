class Message::Operations::Public::Create < PublicRestrictedEndpointOperation
  step Model( Message, :new )
  step :scope_model_to_organization
  step Contract::Build( constant: Message::Contracts::Create )
  step Contract::Validate()
  
  step Contract::Persist()
  
  fail :process_errors
end
