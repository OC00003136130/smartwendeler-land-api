class Message::Policies::PolicyPublicRestricted < App::Policies::PolicyPublicRestricted
  def create?
    true
  end
end
