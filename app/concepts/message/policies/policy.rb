class Message::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :messages, :list
  end

  def show?
    can_access? :messages, :show
  end

  def update?
    can_access? :messages, :update
  end

  def delete?
    can_access? :messages, :delete
  end

  def resolve
    organization.messages
  end

  def find(id)
    organization.messages.find id
    rescue ActiveRecord::RecordNotFound => err
  end
end
