class PollQuestion::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :poll_id
  property :menu_order
  property :name
  property :max_score_count
  property :kind
  property :choices

  property :created_at
  property :updated_at
end
