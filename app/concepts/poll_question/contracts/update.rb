class PollQuestion::Contracts::Update < PollQuestion::Contracts::Validate
  property :poll_id
  property :menu_order
  property :name
  property :max_score_count
  property :kind
  property :choices
end
