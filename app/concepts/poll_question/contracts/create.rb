class PollQuestion::Contracts::Create < PollQuestion::Contracts::Validate
  property :poll_id
  property :name
  property :menu_order
  property :max_score_count
  property :kind
  property :choices
end
