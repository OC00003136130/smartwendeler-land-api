class PollQuestion::Operations::Create < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:poll] = policy.find_poll(params[:poll_id]) }, Output(:failure) => End(:not_found)

  step Model( PollQuestion, :new )
  step :scope_model_to_organization
  step Contract::Build( constant: PollQuestion::Contracts::Create )
  step :set_default_order
  step Contract::Validate()
  step Contract::Persist()
  
  fail :process_errors

  def set_default_order(ctx, poll:, params:, **)
    last_poll_question = poll.poll_questions.order(:menu_order).last if poll.poll_questions.present?
    if last_poll_question && last_poll_question.menu_order
      params[:menu_order] = last_poll_question.menu_order + 1
    else
      params[:menu_order] = 0
    end
    true
  end
end
