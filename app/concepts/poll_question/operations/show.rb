class PollQuestion::Operations::Show < Trailblazer::Operation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:poll_question_id]) }, Output(:failure) => End(:not_found)
end
