class PollQuestion::Operations::List < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:poll] = policy.find_poll(params[:poll_id]) }, Output(:failure) => End(:not_found)
  step :set_model_to_scoped_query_filtered
  step :retrieve_list
  fail :process_errors

  def set_model_to_scoped_query_filtered(ctx, policy:, poll:, **)
    ctx[:model] = policy.resolve(poll)
    true
  end
end
