class PollQuestion::Operations::Update < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:poll_question_id]) }, Output(:failure) => End(:not_found)
  step Contract::Build( constant: PollQuestion::Contracts::Update )
  
  step Contract::Validate()
  step Contract::Persist()
  
  fail :process_errors
end
