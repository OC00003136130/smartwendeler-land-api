class PollQuestion::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :poll_questions, :list
  end

  def show?
    can_access? :poll_questions, :show
  end

  def create?
    can_access? :poll_questions, :create
  end

  def update?
    can_access? :poll_questions, :update
  end

  def delete?
    can_access? :poll_questions, :delete
  end

  def resolve(poll)
    poll.poll_questions
  end

  def find_poll(poll_id)
    organization.polls.find poll_id
    rescue ActiveRecord::RecordNotFound => err
  end

  def find(id)
    organization.poll_questions.find id
    rescue ActiveRecord::RecordNotFound => err
  end
end
