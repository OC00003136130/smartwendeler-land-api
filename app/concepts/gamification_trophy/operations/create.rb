class GamificationTrophy::Operations::Create < BaseOperation
  step :init_policy
  step :find_game
  step :resolve_rewards
  step :assign_trophy_if_not_exist

  def init_policy(ctx, current_user:, **)
    ctx[:policy] = GamificationTrophy::Policies::Policy.new(current_user, GamificationTrophy.new, nil) if !ctx[:policy]
    true
  end

  def find_game(ctx, kind:, policy:, **)
    ctx[:gamification_game] = policy.find_game(kind)
    true
  end

  def resolve_rewards(ctx, gamification_game:, policy:, **)
    # TODO: leave here:
    ctx[:gamification_rewards] = nil
    return true if !gamification_game.present?
    ctx[:gamification_rewards] = policy.resolve_rewards(gamification_game)
    true
  end

  def assign_trophy_if_not_exist(ctx, current_user:, gamification_game:, gamification_rewards:, kind:, **)
    # TODO: leave here:
    return true if !gamification_game.present?
    return true if !gamification_rewards.present?

    user_threshold = current_user.login_count if kind == 'login'
    user_threshold = current_user.project_ratings_count if kind == 'rating'
    user_threshold = current_user.poll_answers_count if kind == 'poll'
    user_threshold = current_user.comments_count if kind == 'comment'

    if user_threshold
      rewards = gamification_rewards.where("threshold <= ?", user_threshold)

      rewards.each do |reward|
        trophy = GamificationTrophy.find_or_create_by(user: current_user, gamification_reward: reward) do |trophy|
          if trophy.new_record?
            Log.create!(
              organization: current_user.organization,
              change: {},
              model_class_name: reward.class.name,
              model_id: reward.id,
              model_record_name: reward.name,
              user_id: current_user.id,
              log_code: "trophy.rewarded"
            )

            ActionCable.server.broadcast("trophy_notification_channel_#{current_user.id}", {
              status: 'trophy_rewarded',
              reward_name: reward.name
            })
          end
        end
      end
    end

    true
  end

  fail :process_errors
end