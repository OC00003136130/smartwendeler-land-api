class GamificationTrophy::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :gamification_trophies, :list
  end

  def resolve_rewards(game)
    game.gamification_rewards
  end

  def find_game(kind)
    organization.gamification_games.find_by(kind: kind)
  end

  def resolve
    user.gamification_trophies
  end
end
