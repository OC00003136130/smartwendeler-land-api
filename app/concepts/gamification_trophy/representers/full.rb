class GamificationTrophy::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  
  property :gamification_reward do
    property :id
    property :threshold
    property :name
    property :gamification_game do
      property :id
      property :name
    end
  end

  property :created_at
  property :updated_at
end
