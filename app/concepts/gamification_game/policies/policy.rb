class GamificationGame::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :gamification_games, :list
  end

  def show?
    can_access? :gamification_games, :show
  end

  def create?
    can_access? :gamification_games, :create
  end

  def update?
    can_access? :gamification_games, :update
  end

  def delete?
    can_access? :gamification_games, :delete
  end

  def attach_image?
    can_access? :gamification_games, :attach_image
  end

  def purge_image?
    can_access? :gamification_games, :purge_image
  end

  def resolve
    organization.gamification_games
  end

  def find(id)
    organization.gamification_games.find id
    rescue ActiveRecord::RecordNotFound => err
  end
end
