class GamificationGame::Operations::Show < Trailblazer::Operation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:gamification_game_id]) }, Output(:failure) => End(:not_found)
end
