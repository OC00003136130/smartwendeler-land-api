class GamificationGame::Operations::Create < BaseOperation
  step Model( GamificationGame, :new )
  step :scope_model_to_organization
  step Contract::Build( constant: GamificationGame::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()
  
  fail :process_errors
end
