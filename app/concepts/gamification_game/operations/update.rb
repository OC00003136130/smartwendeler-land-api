class GamificationGame::Operations::Update < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:gamification_game_id]) }, Output(:failure) => End(:not_found)

  step Contract::Build( constant: GamificationGame::Contracts::Update )
  step Contract::Validate()
  step Contract::Persist()
  
  fail :process_errors
end
