class GamificationGame::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :name
  property :kind

  property :gamification_rewards_count

  collection :gamification_rewards do
    property :id
    property :name
    property :threshold
  end

  property :created_at
  property :updated_at
end
