class User::Operations::RegisterWithFacility < PublicEndpointOperation
  step Model( User, :new )
  step :set_organization_by_register_token
  step :set_registration_source
  step :set_active_on_health_scope
  step Contract::Build( constant: User::Contracts::RegisterWithFacility )
  step Contract::Validate()
  step Contract::Persist()

  step :create_care_facility

  step User::Steps::SetPassword
  step User::Steps::SendRegisterWithFacilityEmail

  step :set_generic_result
  step :send_email_notification
  step App::Steps::BuildPositiveGenericResult

  fail :process_errors
  fail :set_fail_result

  def create_care_facility(ctx, model:, params:, **)
    care_facility_params = {}
    care_facility_params['name'] = params[:care_facility_name]
    care_facility_params['zip'] = params[:care_facility_zip]
    care_facility_params['town'] = params[:care_facility_town]
    care_facility_params['community_id'] = params[:care_facility_community_id]
    care_facility = CareFacility.new
    policy = CareFacility::Policies::Policy.new(model, care_facility, care_facility_params)
    res = CareFacility::Operations::Create.(params: care_facility_params, current_user: model, current_organization: model.organization, policy: policy)
    
    if !res.success?
      model.destroy
    end

    return res.success?
  end

  def set_registration_source(ctx, model:, params:, request:, **)
    registration_source = request.headers["Request-Source"]
    if request.headers["Request-Platform"]
      registration_platform = request.headers["Request-Platform"]
    else
      registration_platform = 'default'
    end
    model.registration_platform = registration_platform
    model.registration_source = registration_source
    true
  end

  def set_active_on_health_scope(ctx, params:, **)
    params[:is_active_on_health_scope] = false
    true
  end

  def set_organization_by_register_token(ctx, params:, model:, **)
    if !params[:register_token].present?
      add_error ctx, message: 'A register token has to be provided in order to assign the user to the correct platform scope.',
                exception: nil, code: 'user.register_with_facility.register_token_missing'
      return false
    end

    model.organization = Organization.find_by(register_token: params[:register_token])

    if model.organization.scope != 'health'
      ctx[:errors] << { code: 'organization_not_in_health_scope' }
      return false
    end

    true
  end

  def send_email_notification(ctx, model:, **)
    NotificationsMailer.send_register_notification(model).deliver_now
  end

  def set_generic_result(ctx, model:, **)
    ctx['http_status'] = 201
    ctx['result'] = { id: model.id, code: 'successfully.registered', message: 'Registration successful' }
    true
  end

  def set_fail_result(ctx, model:, **)
    ctx['http_status'] = 422
    ctx['json'] = { code: 'registration.failed', message: 'Registration failed', errors: ctx[:errors] }
    true
  end
end
