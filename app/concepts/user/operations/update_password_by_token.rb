class User::Operations::UpdatePasswordByToken < PublicEndpointOperation
  step ->(ctx, params:, **) {
    ctx['model'] = User.find_by(password_token: params[:password_token])
    if ctx['model']
      true
    else
      ctx['http_status'] = 404
      false
    end
  }
  step :set_password
  step App::Steps::BuildPositiveGenericResult

  def set_password(ctx, model:, params:, **)
    valid_password = true

    if params[:password].blank?
      ctx['http_status'] = 422
      return false
    end

    if params[:password] != params[:password_confirmation]
      ctx['http_status'] = 422
      valid_password = false
    end

    if params[:password].length < 5
      ctx['http_status'] = 422
      valid_password = false
    end

    return false if !valid_password

    ctx['model'].password_changed_at = Time.now
    ctx['model'].password = params[:password]
    ctx['model'].password_token = nil
    ctx['model'].save!
    ctx['result'] = { code: 'user.password_updated', message: 'Password successfully updated' }
    true
  end

  fail :process_errors
  fail :set_fail_result

  def set_fail_result(ctx, **)
    ctx['json'] = { code: 'user.password_not_updated', message: 'The password could not get updated' }
    true
  end
end
