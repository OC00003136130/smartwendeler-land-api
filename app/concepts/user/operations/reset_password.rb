class User::Operations::ResetPassword < PublicEndpointOperation
  step :check_user
  step User::Steps::SetPasswordToken
  step :send_password_reset_mail
  step :set_generic_result
  step App::Steps::BuildPositiveGenericResult

  fail :process_errors
  fail :set_fail_result

  def check_user(options, params:, **)
    return false if !params[:email]

    user = User.where("lower(email) = ?", params[:email].downcase).first

    if user
      options['model'] = user
      return true
    else
      return false
    end
  end

  def set_generic_result(options, model:, **)
    options['result'] = { id: model.id, code: 'successfully.confirmed', message: 'New password was sent' }
    true
  end

  def send_password_reset_mail(options, model:, **)
    UsersMailer.send_password_reset(model).deliver_now
    true
  end

  def set_fail_result(ctx, **)
    ctx['http_status'] = 422
    ctx['json'] = { code: 'reset_password.failed', message: 'Password reset failed', errors: ctx[:errors] }
    true
  end
end
