class User::Operations::DeleteMe < BaseOperation
  step ->(ctx, current_user:, **) {
    ctx[:model] = current_user
    true
  }

  step App::Steps::DestroyModel
  fail :process_errors
end
