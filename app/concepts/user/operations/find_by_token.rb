class User::Operations::FindByToken < PublicEndpointOperation
  step ->(ctx, params:, **) {
    user = User.find_by(password_token: params[:password_token])
    if user
      ctx['result'] = { code: 'user.found', message: 'User found', user_id: user.id }
      true
    else
      false
    end
  }
  step App::Steps::BuildPositiveGenericResult

  fail :process_errors
  fail :set_fail_result

  def set_fail_result(ctx, **)
    ctx['http_status'] = 404
    ctx['json'] = { code: 'user.not_found', message: 'User could not be found' }
    true
  end
end
