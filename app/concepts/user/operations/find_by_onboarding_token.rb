class User::Operations::FindByOnboardingToken < PublicEndpointOperation
  step ->(ctx, params:, **) {
    user = User.find_by(onboarding_token: params[:onboarding_token])
    if user
      care_facility = user.care_facilities.first
      ctx['result'] = { code: 'user.found', message: 'User found', user_id: user.id, user: {
        firstname: user.firstname,
        lastname: user.lastname,
        email: user.email
        },
        care_facility: {
          name: care_facility&.name,
          street: care_facility&.street,
          zip: care_facility&.zip,
          town: care_facility&.town,
          phone: care_facility&.phone,
          email: care_facility&.email,
          website: care_facility&.website,
          community_id: care_facility&.community_id
        }
      }
      true
    else
      false
    end
  }
  step App::Steps::BuildPositiveGenericResult

  fail :process_errors
  fail :set_fail_result

  def set_fail_result(ctx, **)
    ctx['http_status'] = 404
    ctx['json'] = { code: 'user.not_found', message: 'User could not be found' }
    true
  end
end
