class User::Operations::Update < BaseOperation
  step ->(ctx, params:, policy:, **) {
    ctx[:model] = policy.find(params[:user_id])
  }, Output(:failure) => End(:not_found)
  step Contract::Build( constant: User::Contracts::Update )
  step Contract::Validate()
  step Contract::Persist()
  step :send_notification_on_health_scope
  step App::Steps::AddBase64Image
  step :update_password_if_requested
  
  fail :process_errors

  # todo: remove this if it works via reform
  def update_password_if_requested(options, model:, current_user:, params:, **)
    return true if !params[:password].present?
    if (current_user.role == 'admin' || current_user.role == 'root')
      model.password = params[:password]
    end
    model.save!
  end

  def send_notification_on_health_scope(options, model:, params:, **)
    if model.saved_changes && model.saved_changes["is_active_on_health_scope"] == [false, true]
      UsersMailer.send_confirm_facility_owner_for_health_scope(model).deliver_now
    end
    true
  end
end
