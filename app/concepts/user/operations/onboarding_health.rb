# onboarding of a user which is on health scope and has been pre-added before

class User::Operations::OnboardingHealth < PublicEndpointOperation
  step ->(ctx, params:, **) {
    if params[:onboarding_token].blank?
      return false
    end

    ctx[:model] = User.find_by(onboarding_token: params[:onboarding_token])
    if !ctx[:model].present?
      return false
    else
      return true
    end
  }

  step :validate_email
  # step :set_active_on_health_scope
  step :reset_onboarding_token
  
  step User::Steps::SetPassword
  #step User::Steps::SendRegisterWithFacilityEmail

  step :send_email_notification
  step :set_generic_result
  step App::Steps::BuildPositiveGenericResult

  fail :process_errors
  fail :set_fail_result

  def validate_email(ctx, model:, params:, **)
    if params[:email].blank? || !EmailValidator.regexp.match(params[:email]) || params[:firstname].blank? || params[:lastname].blank?
      return false
    end

    existing_onboarded_user = model.organization.users.where(email: params[:email], onboarding_token: nil).first

    if existing_onboarded_user
      return false
    end

    model.firstname = params[:firstname]
    model.lastname = params[:lastname]
    model.email = params[:email]
    true
  end

  # def set_active_on_health_scope(ctx, model:, **)
  #   model.is_active_on_health_scope = true
  #   true
  # end

  def reset_onboarding_token(ctx, model:, **)
    model.onboarding_token = nil
    model.save!
    true
  end

  def send_email_notification(ctx, model:, **)
    NotificationsMailer.send_onboarding_notification(model).deliver_now
    true
  end

  def set_fail_result(ctx, **)
    ctx['http_status'] = 422
    ctx['json'] = { code: 'onboarding.failed', message: 'Onboarding failed', errors: ctx[:errors] }
    true
  end

  def set_generic_result(ctx, model:, **)
    ctx['http_status'] = 200
    ctx['result'] = { id: model.id, code: 'successfully.onboarded', message: 'Onboarding successful' }
    true
  end
end