class User::Operations::Invite < BaseOperation
  step Model( User, :new )
  step :set_organization_by_register_token
  step :scope_model_to_organization
  step Contract::Build( constant: User::Contracts::Invite )
  step Contract::Validate()
  step Contract::Persist()

  step User::Steps::SetPassword
  step User::Steps::SendInvitationEmail

  fail :process_errors

  def set_organization_by_register_token(ctx, params:, model:, **)
    if !params[:register_token].present?
      add_error ctx, message: 'A register token has to be provided in order to assign the user to the correct platform scope.',
                exception: nil, code: 'user.invite.register_token_missing'
      return false
    end

    model.organization = Organization.find_by(register_token: params[:register_token])
    true
  end
end
