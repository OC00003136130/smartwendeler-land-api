class User::Operations::Register < PublicEndpointOperation
  step Model( User, :new )
  step :set_organization_by_register_token
  step :set_registration_source
  step Contract::Build( constant: User::Contracts::Register )
  step Contract::Validate()
  step Contract::Persist()

  step User::Steps::SetPassword
  step User::Steps::SendRegisterEmail

  step :set_generic_result
  step App::Steps::BuildPositiveGenericResult

  fail :process_errors
  fail :set_fail_result

  def set_registration_source(ctx, model:, params:, request:, **)
    registration_source = request.headers["Request-Source"]
    if request.headers["Request-Platform"]
      registration_platform = request.headers["Request-Platform"]
    else
      registration_platform = 'default'
    end
    model.registration_platform = registration_platform
    model.registration_source = registration_source
    true
  end

  def set_organization_by_register_token(ctx, params:, model:, **)
    if !params[:register_token].present?
      add_error ctx, message: 'A register token has to be provided in order to assign the user to the correct platform scope.',
                exception: nil, code: 'user.register.register_token_missing'
      return false
    end

    model.organization = Organization.find_by(register_token: params[:register_token])
    true
  end

  def set_generic_result(ctx, model:, **)
    ctx['http_status'] = 201
    ctx['result'] = { id: model.id, code: 'successfully.registered', message: 'Registration successful' }
    true
  end

  def set_fail_result(ctx, **)
    ctx['http_status'] = 422
    ctx['json'] = { code: 'registration.failed', message: 'Registration failed', errors: ctx[:errors] }
    true
  end
end
