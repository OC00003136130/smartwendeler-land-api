class User::Operations::Logout < Trailblazer::Operation
  step :logout

  def logout(options, current_user:, **)
    current_user.jwt_token = nil
    current_user.save!

    options['result'] = { code: 'user.successfully_logged_out', message: 'The user\'s token has been invalidated. Please authorize again.' }
    true
  end
end
