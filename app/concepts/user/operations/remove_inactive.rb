class User::Operations::RemoveInactive < Trailblazer::Operation

  THRESHOLD = 48.hours.ago

  step :get_users
  step :remove_users

  def get_users(ctx, **)
    ctx['model'] = User.where(last_seen: nil).where('created_at < ?', THRESHOLD)
    true
  end

  def remove_users(ctx, model:, **)
    model.destroy_all
    true
  end
end
