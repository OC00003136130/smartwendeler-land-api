class User::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :email
  property :name
  property :firstname
  property :lastname
  property :created_at
  property :updated_at
  property :organization_id
  property :status
  property :role
  property :last_seen
  property :has_reported_count
  property :was_reported_count
  property :can_comment
  property :notification_preferences
  property :registration_source
  property :registration_platform
  property :never_expire
  property :commercial_register_number
  property :is_active_on_health_scope
  property :phone
  property :care_facility_name
  property :password_changed_at
  property :onboarding_token
  property :imported

  property :comments_count
  property :login_count
  property :project_ratings_count
  property :poll_answers_count
  property :pinboard_idea_ratings_count
  property :trophies_count

  property :image_url

  property :organization do
    property :id
    property :name
    property :image_url
    property :created_at
  end

  collection :care_facilities do
    property :id
    property :slug
    property :name
    property :kind
    property :is_active
  end

  property :permissions
end