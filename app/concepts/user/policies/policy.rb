class User::Policies::Policy < App::Policies::Policy
  def me?
    can_access? :users, :me
  end

  def logout?
    can_access? :users, :logout
  end

  def list?
    can_access? :users, :list
  end

  def show?
    can_access? :users, :show
  end

  def invite?
    can_access? :users, :invite
  end

  def update?
    return (attributes_allowed?(:users) && can_access?(:users, :update))
  end

  def update_password?
    can_access? :users, :update_password
  end

  def delete?
    can_access? :users, :delete
  end

  def delete_me?
    can_access? :users, :delete_me
  end

  def resolve
    organization.users
  end
  
  def find(id)
    case user.role
    when 'root'
      model.find id
    when 'admin', 'care_facility_admin'
      organization.users.find id
    when 'user', 'facility_owner'
      user if user.id == id
    end
    rescue ActiveRecord::RecordNotFound => err
  end

  # searches model which has been processed before
  def resolve_with_search(fields=[:firstname, :lastname, :email], exact_match=false)

    if params[:search].present?
      return add_search_query(self.resolve, params[:search], model, fields, exact_match)
    end

    return self.resolve
  end
end
