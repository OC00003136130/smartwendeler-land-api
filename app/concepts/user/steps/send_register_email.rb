class User::Steps::SendRegisterEmail < App::Steps::Base
  extend Uber::Callable

  def self.call(options, model:, **)
    UsersMailer.send_register(model).deliver_now
    true
  end
end
