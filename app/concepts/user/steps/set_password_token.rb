class User::Steps::SetPasswordToken < App::Steps::Base
  extend Uber::Callable

  def self.call(options, model:, params:, **)

    model.password_token = SecureRandom.base64(80).tr('+/=', '0aZ')
    model.save!

    true
  end
end
