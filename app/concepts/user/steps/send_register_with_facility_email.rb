class User::Steps::SendRegisterWithFacilityEmail < App::Steps::Base
  extend Uber::Callable

  def self.call(options, model:, **)
    UsersMailer.send_register_with_facility(model).deliver_now
    true
  end
end
