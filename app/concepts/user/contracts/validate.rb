class User::Contracts::Validate < Reform::Form
  validation do
    params do
      required(:email).filled(:string)
      required(:role).filled(:string)
    end

    rule(:email) do
      key.failure('must_be_valid') if !EmailValidator.regexp.match(value)
    end

    rule(:role) do
      key.failure('not_allowed') if value == 'root'
    end
  end
end
