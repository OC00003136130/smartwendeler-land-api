class User::Contracts::Register < Reform::Form
  property :firstname
  property :lastname
  property :email
  property :status, default: 'confirmed'
  property :role, default: 'user'
  property :notification_preferences
  property :registration_source
  property :registration_platform
  property :never_expire
  property :phone

  validation do
    params do
      required(:email).filled(:string)
      required(:role).filled(:string)
    end

    rule(:email) do
      key.failure('must_be_valid') if !EmailValidator.regexp.match(value)
    end

    rule(:role) do
      key.failure('not_allowed') if value != 'user'
    end
  end
end
