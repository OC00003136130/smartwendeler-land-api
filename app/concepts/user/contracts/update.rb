class User::Contracts::Update < Reform::Form
  property :firstname
  property :lastname
  property :status
  property :role
  property :organization_id
  property :can_comment
  property :notification_preferences
  property :never_expire
  property :is_active_on_health_scope
  property :phone

  validation do
    params do
      optional(:role).filled(:string)
      optional(:email)
    end

    rule(:email) do
      key.failure('must_be_valid') if value && !EmailValidator.regexp.match(value)
    end

    # rule(:role) do
    #   key.failure('not_allowed') if value == 'root'
    # end
  end
end
