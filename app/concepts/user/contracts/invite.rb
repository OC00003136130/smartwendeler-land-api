class User::Contracts::Invite < User::Contracts::Validate
  property :firstname
  property :lastname
  property :email
  property :status, default: 'confirmed'
  property :role, default: 'user'
  property :notification_preferences
  property :never_expire
  property :is_active_on_health_scope
  property :phone
end
