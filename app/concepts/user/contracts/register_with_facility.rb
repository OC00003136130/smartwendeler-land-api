class User::Contracts::RegisterWithFacility < Reform::Form
  property :firstname
  property :lastname
  property :email
  property :status, default: 'confirmed'
  property :role, default: 'facility_owner'
  property :notification_preferences
  property :registration_source
  property :registration_platform
  property :never_expire
  property :commercial_register_number
  property :is_active_on_health_scope
  property :care_facility_name, virtual: true
  property :care_facility_zip, virtual: true
  property :care_facility_town, virtual: true
  property :care_facility_community_id, virtual: true
  property :phone

  validation do
    params do
      required(:firstname).filled(:string)
      required(:lastname).filled(:string)
      required(:email).filled(:string)
      required(:status).filled(:string)
      required(:role).filled(:string)
      required(:commercial_register_number).filled(:string)
      required(:care_facility_name).filled(:string)
      required(:care_facility_zip).filled(:string)
      required(:care_facility_town).filled(:string)
      required(:care_facility_community_id).filled
    end

    rule(:email) do
      key.failure('must_be_valid') if !EmailValidator.regexp.match(value)
    end

    rule(:role) do
      key.failure('not_allowed') if value != 'facility_owner'
    end

    rule(:status) do
      key.failure('not_allowed') if value != 'confirmed'
    end
  end
end
