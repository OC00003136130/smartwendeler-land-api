class Poll::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :name
  property :ends_at
  property :is_active
  property :poll_questions_count
  property :participations_count
  property :project_id

  collection :poll_questions do
    property :menu_order
    property :name
    property :max_score_count
    property :kind
  end

  property :project do
    property :id
    property :name
  end

  property :created_at
  property :updated_at
end
