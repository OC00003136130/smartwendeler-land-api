class Poll::Operations::Answers < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:poll_id]) }, Output(:failure) => End(:not_found)
  step :sanitize_poll_answers

  fail :process_errors

  def sanitize_poll_answers(ctx, model:, **)

    ctx['result'] = {}
     
    model.poll_questions.each do |question|
      if question.kind == 'rating'
        ctx['result'][question.id] = { question: question.name, kind: question.kind, average_rating: question.poll_answers.average(:rating_value).to_f }
      elsif question.kind == 'text'
        ctx['result'][question.id] = { question: question.name, kind: question.kind, values: question.poll_answers.pluck(:text_value) }
      elsif question.kind == 'single_choice' || question.kind == 'multiple_choice'
        ctx['result'][question.id] = { question: question.name, kind: question.kind, values: question.poll_answers.pluck(:choices_answers) }
      end
    end

    true
  end
end
