class Poll::Operations::Create < BaseOperation
  step Model( Poll, :new )
  step :scope_model_to_organization
  step Contract::Build( constant: Poll::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()
  
  fail :process_errors
end
