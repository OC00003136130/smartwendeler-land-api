class Poll::Operations::Update < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:poll_id]) }, Output(:failure) => End(:not_found)
  step Contract::Build( constant: Poll::Contracts::Update )
  
  step Contract::Validate()
  step :check_if_public_poll_is_active_already
  step Contract::Persist()

  def check_if_public_poll_is_active_already(ctx, params:, model:, current_organization:,  **)
    return true if model.project
    return true if params[:is_active] != 'true' && params[:is_active] != true

    existing_public = current_organization.polls.where.not(id: model.id).where(is_active: true, project_id: nil).first
    if existing_public
      add_error ctx, message: 'There is already an active public poll!', exception: nil, code: 'poll.public.existing', field_name: 'poll'
      return false
    end

    true
  end
  
  fail :process_errors
end
