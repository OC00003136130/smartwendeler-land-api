class Poll::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :polls, :list
  end

  def show?
    can_access? :polls, :show
  end

  def create?
    can_access? :polls, :create
  end

  def update?
    can_access? :polls, :update
  end

  def delete?
    can_access? :polls, :delete
  end

  def answers?
    can_access? :polls, :answers
  end

  def resolve
    is_active = nil
    project_id = nil

    if params && params[:is_active].present?
      is_active = params[:is_active] == 'true'
    end

    if params && params[:project_id].present?
      project_id = params[:project_id]
    end

    args = {
      is_active: is_active,
      project_id: project_id
    }.compact

    if params && params[:ignore_projects].present?
      organization.polls.where(args).where(project_id: nil)
    else
      organization.polls.where(args)
    end
  end

  def find(id)
    organization.polls.find id
    rescue ActiveRecord::RecordNotFound => err
  end
end
