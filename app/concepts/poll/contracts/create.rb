class Poll::Contracts::Create < Poll::Contracts::Validate
  property :name
  property :ends_at
  property :is_active
  property :project_id
end
