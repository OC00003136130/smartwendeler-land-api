class CommentReport::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :user_id
  property :comment_id

  # user who reported the comment
  property :user do
    property :id
    property :name
    property :firstname
    property :lastname
  end

  # user who owns the comment that got reported
  property :comment_user do
    property :id
    property :name
    property :firstname
    property :lastname
    property :image_url
  end

  property :comment do
    property :id
    property :comment
    property :name
  end

  property :project do
    property :id
    property :name
  end

  property :created_at
  property :updated_at
end
