class CommentReport::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :comment_reports, :list
  end

  def show?
    can_access? :comment_reports, :show
  end

  def create?
    can_access? :comment_reports, :create
  end

  def delete?
    can_access? :comment_reports, :delete
  end

  def resolve
    organization.comment_reports
  end

  def find_comment(comment_id)
    organization.comments.find comment_id
    rescue ActiveRecord::RecordNotFound => err
  end

  def find(id)
    organization.comment_reports.find id
    rescue ActiveRecord::RecordNotFound => err
  end

  # TODO: refactor
  def resolve_users_for_notitification
    user_ids = []
    organization.users.where(role: ['root', 'admin']).each do |user|
      if user.notification_preferences.include? 'comment_report'
        user_ids << user.id
      end
    end
    organization.users.where(id: user_ids)
  end
end
