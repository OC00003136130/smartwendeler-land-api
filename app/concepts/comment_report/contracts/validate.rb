class CommentReport::Contracts::Validate < Reform::Form
  validation do
    params do
      required(:user_id).filled(:string)
      required(:comment_id).filled(:string)
    end
  end
end
