class CommentReport::Operations::Create < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:comment] = policy.find_comment(params[:comment_id]) }, Output(:failure) => End(:not_found)

  step Model( CommentReport, :new )
  step :scope_model_to_organization
  step :scope_model_to_user
  step :check_if_own_comment
  step Contract::Build( constant: CommentReport::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()
  step :send_email_notifications

  def check_if_own_comment(ctx, comment:, current_user:, **)
    if comment.user_id == current_user.id
      add_error ctx, message: 'Reporting own comments not possible!', exception: nil, code: 'commentreport.own_comment.not_possible', field_name: nil
      return false
    end
    true
  end

  def send_email_notifications(ctx, comment:, current_user:, policy:, **)
    users = policy.resolve_users_for_notitification

    users.each do |user|
      UsersMailer.send_notification(user, 'Neue Kommentarmeldung', "Es wurde gerade ein Kommentar im Projekt #{comment.project.name} gemeldet. Inhalt des Kommentars: \"#{comment.comment}\"", "public/projects/#{comment.project.id}").deliver_now
    end
    true
  end
  
  fail :process_errors
end
