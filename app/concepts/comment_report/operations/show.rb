class CommentReport::Operations::Show < Trailblazer::Operation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:comment_report_id]) }, Output(:failure) => End(:not_found)
end
