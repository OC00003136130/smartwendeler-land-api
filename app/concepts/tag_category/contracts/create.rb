class TagCategory::Contracts::Create < TagCategory::Contracts::Validate
  property :name
  property :identifier
  property :parent_id
  property :menu_order
  property :kind
  property :filter_type
end