class TagCategory::Policies::PolicyPublicRestricted < App::Policies::PolicyPublicRestricted
  def list?
    true
  end

  def resolve
    parent_id = params[:parent_id]
    if params[:show_all].present?
      args = {
        parent_id: parent_id
      }.compact
    else
      args = {
        parent_id: parent_id
      } # not compacted!
    end

    organization.tag_categories.where(args)
  end
end
