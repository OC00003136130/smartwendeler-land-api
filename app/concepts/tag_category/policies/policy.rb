class TagCategory::Policies::Policy < App::Policies::Policy
  def list?
    can_access? :tag_categories, :list
  end

  def show?
    can_access? :tag_categories, :show
  end

  def create?
    can_access? :tag_categories, :create
  end

  def update?
    can_access? :tag_categories, :update
  end

  def delete?
    can_access? :tag_categories, :delete
  end

  def resolve
    parent_id = params[:parent_id]
    if params[:show_all].present?
      args = {
        parent_id: parent_id
      }.compact
    else
      args = {
        parent_id: parent_id
      } # not compacted!
    end

    organization.tag_categories.where(args)
  end

  def find(id)
    organization.tag_categories.find id
    rescue ActiveRecord::RecordNotFound => err
  end
end
