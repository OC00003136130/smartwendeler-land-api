class TagCategory::Operations::Show < Trailblazer::Operation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:tag_category_id]) }, Output(:failure) => End(:not_found)
end
