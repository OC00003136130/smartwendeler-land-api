class TagCategory::Operations::Update < BaseOperation
  step ->(ctx, params:, policy:, **) { ctx[:model] = policy.find(params[:tag_category_id]) }, Output(:failure) => End(:not_found)
  step Contract::Build( constant: TagCategory::Contracts::Update )
  
  step Contract::Validate()
  step Contract::Persist()
  
  fail :process_errors
end
