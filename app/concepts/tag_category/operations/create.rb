class TagCategory::Operations::Create < BaseOperation
  step Model( TagCategory, :new )
  step :scope_model_to_organization
  step :set_default_order
  step Contract::Build( constant: TagCategory::Contracts::Create )
  step Contract::Validate()
  step Contract::Persist()
  
  fail :process_errors

  def set_default_order(ctx, current_organization:, params:, **)
    last_tag_category = current_organization.tag_categories.order(:menu_order).last if current_organization.tag_categories.present?
    if last_tag_category
      params[:menu_order] = last_tag_category.menu_order + 1
    else
      params[:menu_order] = 0
    end
    true
  end
end
