class TagCategory::Representers::Full < Representable::Decorator
  include Representable::JSON

  property :id
  property :name
  property :identifier
  property :parent_id
  property :menu_order
  property :kind
  property :filter_type
  property :care_facilities_count
  property :care_facilities_active_count

  property :created_at
  property :updated_at
end
