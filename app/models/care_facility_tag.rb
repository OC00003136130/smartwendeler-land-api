class CareFacilityTag < ApplicationRecord
  belongs_to :tag
  belongs_to :care_facility
end
