class Poll < ApplicationRecord
  belongs_to :organization
  belongs_to :project, required: false

  has_many :poll_questions, dependent: :destroy
  has_many :poll_answers, through: :poll_questions

  def poll_questions_count
    poll_questions.count
  end

  def participations_count
    poll_answers.uniq(&:user_id).count
  end
end
