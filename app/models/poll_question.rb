class PollQuestion < ApplicationRecord
  belongs_to :organization
  belongs_to :poll

  has_many :poll_answers

  enum kind: {
    rating: 0,
    text: 1,
    single_choice: 2,
    multiple_choice: 3
  }
end
