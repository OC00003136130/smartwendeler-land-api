class Comment < ApplicationRecord
  attr_accessor :current_user_id

  belongs_to :organization
  belongs_to :user
  belongs_to :project
  belongs_to :parent, class_name: 'Comment', required: false
  has_many :replies, class_name: 'Comment', foreign_key: :parent_id, dependent: :destroy

  has_many :comment_ratings, dependent: :destroy
  has_many :comment_reports, dependent: :destroy

  def name
    comment
  end

  def upvote_count
    comment_ratings.where(score: 1).count
  end

  def downvote_count
    comment_ratings.where(score: -1).count
  end

  def replies_count
    replies.count
  end

  def reported_history
    hist = []
    comment_reports.each do |cr|
      hist << {
        reported_by: cr.user_id,
        reported_at: cr.created_at
      }
    end

    return hist
  end
end
