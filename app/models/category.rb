class Category < ApplicationRecord
  include ActiveStorageSupport::SupportForBase64
  has_one_base64_attached :image

  belongs_to :organization
  has_many :project_categories, dependent: :destroy
  has_many :projects, through: :project_categories

  has_many :pinboard_categories, dependent: :destroy
  has_many :pinboards, through: :pinboard_categories

  has_many :pinboard_ideas, through: :pinboards

  has_many :care_facility_categories, dependent: :destroy
  has_many :care_facilities, through: :care_facility_categories

  has_many :sub_categories, foreign_key: :parent_id, class_name: "SubCategory", dependent: :destroy
  has_many :sub_sub_categories, through: :sub_categories, class_name: "SubSubCategory", dependent: :destroy

  # can get scoped to projects, care_facilities or pinboards
  enum scope: {
    project: 0,
    pinboard: 1,
    care_facility: 2
  }

  enum url_kind: {
    external: 0,
    internal: 1
  }

  def name_with_projects_count
    "#{name} (#{projects.where(is_active: true).count})"
  end

  def projects_count
    projects.count
  end

  def name_with_pinboards_count
    "#{name} (#{pinboards.where(is_active: true).count})"
  end

  def pinboards_with_confirmed_ideas_count
    pinboard_ideas.where(status: 'confirmed').count
  end

  def pinboards_count
    pinboards.count
  end

  def care_facilities_count
    care_facilities.count
  end

  def care_facilities_active_count
    care_facilities.where(is_active: true).count
  end

  def name_with_care_facilities_count
    "#{name} (#{care_facilities.where(is_active: true).count})"
  end

  def active_and_started_pinboards_count
    pinboards.where(is_active: true).where("start_time <= ?", Time.now).count
  end

  def image_url
    if image.attached?
      Rails.application.routes.url_helpers.url_for(image)
    end
  end
end
