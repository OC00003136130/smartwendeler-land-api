class CareFacilitySubSubCategory < ApplicationRecord
  belongs_to :sub_sub_category
  belongs_to :care_facility
end
