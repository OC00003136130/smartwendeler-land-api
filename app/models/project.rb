class Project < ApplicationRecord
  attr_accessor :current_user_id
  attr_accessor :current_user_role
  
  include ActiveStorageSupport::SupportForBase64
  has_one_base64_attached :image
  
  has_many_attached :images

  belongs_to :organization
  
  has_many :project_categories
  has_many :categories, through: :project_categories
  has_many :project_communities
  has_many :communities, through: :project_communities
  has_many :comments, dependent: :destroy
  has_many :locations, dependent: :destroy
  has_many :project_ratings, dependent: :destroy
  has_many :milestones, dependent: :destroy
  has_many :messages, dependent: :nullify
  has_many :polls, dependent: :nullify

  validates :slug, length: { maximum: 300 }, allow_nil: true, uniqueness: { scope: :organization_id }, format: { with: /\A[a-zA-Z0-9_-]*\z/, message: "only allows letters, numbers, hyphens, and underscores" }

  enum rating_kind: {
    upvote: 0,
    upvote_downvote: 1
  }

  def comment_count
    comments.count
  end

  def image_url
    if image.attached?
      Rails.application.routes.url_helpers.url_for(image)
    end
  end

  # used to provide a proper JSON with only necessary information of the images
  def sanitized_images
    imgs = []

    images.each do |img|
      imgs << {
        url: Rails.application.routes.url_helpers.url_for(img),
        signed_id: img.signed_id,
        tag: img.filename.to_s.split('-')[1],
        delete_modal_open: false
      }
    end

    return imgs
  end

  def total_vote_count
    project_ratings.count
  end

  def upvote_count
    project_ratings.where(score: 1).count
  end

  def downvote_count
    project_ratings.where(score: -1).count
  end

  def category_ids
    categories.pluck(:id)
  end

  def community_ids
    communities.pluck(:id)
  end
end
