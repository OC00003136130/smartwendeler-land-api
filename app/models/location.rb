class Location < ApplicationRecord
  belongs_to :organization
  belongs_to :project, required: false
  belongs_to :care_facility, required: false
end