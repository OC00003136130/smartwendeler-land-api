class Log < ApplicationRecord
  belongs_to :organization
  belongs_to :user, required: false
end
