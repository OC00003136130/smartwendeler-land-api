class Organization < ApplicationRecord
  has_secure_token :register_token, length: 36
  has_secure_password :public_password, validations: false

  include ActiveStorageSupport::SupportForBase64
  has_one_base64_attached :image

  has_many :users, dependent: :destroy
  has_many :projects, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :milestones, dependent: :destroy
  has_many :categories, dependent: :destroy
  has_many :sub_categories, dependent: :destroy
  has_many :sub_sub_categories, dependent: :destroy
  has_many :locations, dependent: :destroy
  has_many :messages, dependent: :destroy
  has_many :comment_reports, dependent: :destroy
  has_many :tooltips, dependent: :destroy
  has_many :polls, dependent: :destroy
  has_many :poll_questions, dependent: :destroy
  has_many :poll_answers, dependent: :destroy
  has_many :communities, dependent: :destroy
  has_many :logs, dependent: :destroy
  has_many :gamification_games, dependent: :destroy
  has_many :gamification_rewards, through: :gamification_games, dependent: :destroy
  has_many :care_facilities, dependent: :destroy
  has_many :pinboards, dependent: :destroy
  has_many :pinboard_ideas, through: :pinboards, dependent: :destroy
  has_many :tags, dependent: :destroy
  has_many :tag_categories, dependent: :destroy
  # validation on public_username is needed
  validates :public_username, uniqueness: true, allow_nil: true

  enum status: {
    inactive: 0,
    active: 1,
    initialization: 2
  }

  enum scope: {
    project: 0,
    health: 1
  }

  def image_url
    if image.attached?
      Rails.application.routes.url_helpers.url_for(image)
    end
  end
end
