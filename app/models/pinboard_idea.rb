class PinboardIdea < ApplicationRecord
  attr_accessor :current_user_id
  
  belongs_to :user
  belongs_to :pinboard

  has_many :pinboard_idea_ratings

  enum status: {
    is_checked: 0,
    confirmed: 1,
    rejected: 2
  }
end
