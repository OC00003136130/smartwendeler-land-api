class CareFacilitySubCategory < ApplicationRecord
  belongs_to :care_facility
  belongs_to :sub_category
end
