class SubCategory < Category
  belongs_to :category, foreign_key: :parent_id, class_name: 'Category'

  has_many :care_facility_sub_categories, dependent: :destroy
  has_many :care_facilities, through: :care_facility_sub_categories

  has_many :sub_sub_categories, foreign_key: :parent_id, class_name: 'SubSubCategory'
end
