class SubSubCategory < SubCategory
  belongs_to :sub_category, foreign_key: :parent_id, class_name: 'SubCategory'

  has_many :care_facility_sub_sub_categories, dependent: :destroy
  has_many :care_facilities, through: :care_facility_sub_sub_categories
end
