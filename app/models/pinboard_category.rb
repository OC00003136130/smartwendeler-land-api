class PinboardCategory < ApplicationRecord
  belongs_to :category
  belongs_to :pinboard
end
