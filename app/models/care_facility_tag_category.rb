class CareFacilityTagCategory < ApplicationRecord
  belongs_to :care_facility
  belongs_to :tag_category
end
