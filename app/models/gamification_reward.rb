class GamificationReward < ApplicationRecord
  belongs_to :gamification_game
  has_many :gamification_trophies
end
