# TODO: Note that CareFacility should get renamed one day as it can also feature events and news. So the name is not appropriate anymore
# Better name: 'CareItem'

class CareFacility < ApplicationRecord
  include ActiveStorageSupport::SupportForBase64
  has_one_base64_attached :image
  has_one_base64_attached :logo
  
  has_many_attached :images
  has_many_attached :documents
  has_many :care_facility_categories, dependent: :destroy
  has_many :categories, through: :care_facility_categories
  has_many :care_facility_sub_categories, dependent: :destroy
  has_many :sub_categories, through: :care_facility_sub_categories
  has_many :care_facility_sub_sub_categories, dependent: :destroy
  has_many :sub_sub_categories, through: :care_facility_sub_sub_categories
  has_many :care_facility_tag_categories, dependent: :destroy
  has_many :tag_categories, through: :care_facility_tag_categories
  has_many :locations, dependent: :destroy
  has_many :care_facility_tags, dependent: :destroy
  has_many :tags, through: :care_facility_tags

  belongs_to :user, required: false
  belongs_to :organization
  belongs_to :community, required: false

  validates :slug, length: { maximum: 300 }, allow_nil: true, uniqueness: { scope: :organization_id }, format: { with: /\A[a-zA-Z0-9_-]*\z/, message: "only allows letters, numbers, hyphens, and underscores" }

  # TODO this has to be true but specs are failing if it is
  belongs_to :user, required: false

  enum kind: {
    facility: 0,
    # Kurse & Events
    event: 1,
    news: 2,
    course: 3
  }

  def logo_url
    if logo.attached?
      Rails.application.routes.url_helpers.url_for(logo)
    end
  end

  def image_url
    if image.attached?
      Rails.application.routes.url_helpers.url_for(image)
    end
  end

  # used to provide a proper JSON with only necessary information of the images
  def sanitized_images
    imgs = []

    images.each do |img|
      imgs << {
        url: Rails.application.routes.url_helpers.url_for(img),
        signed_id: img.signed_id,
        tag: img.filename.to_s.split('-')[1],
        delete_modal_open: false
      }
    end

    return imgs
  end

  def sanitized_documents
    result = []

    documents.each do |document|
      tag = nil
      tag = document.filename.to_s.split('-')[1] if document.filename.to_s.split('-')
      result << {
        url: Rails.application.routes.url_helpers.url_for(document),
        signed_id: document.signed_id,
        name: document.filename.to_s,
        tag: tag,
        delete_modal_open: false,
        mime_type: document.content_type
      }
    end

    return result
  end

  def tag_category_ids
    tag_categories.pluck(:id)
  end

  def category_ids
    categories.pluck(:id)
  end

  def sub_category_ids
    sub_categories.pluck(:id)
  end

  def sub_sub_category_ids
    sub_sub_categories.pluck(:id)
  end

  def tag_ids
    tags.pluck(:id)
  end

  def user_care_facility
    if user && user.care_facilities && user.care_facilities.count > 0
      cfs = user.care_facilities.where(kind: 'facility')
      if cfs.count > 0
        return {
          id: cfs.first.id,
          name: cfs.first.name
        }
      end
    end
    return {}
  end
end
