class User < ApplicationRecord
  has_secure_password validations: false
  include ActiveStorageSupport::SupportForBase64
  has_one_base64_attached :image

  belongs_to :organization

  has_many :comments, dependent: :destroy
  has_many :comment_ratings, dependent: :destroy
  has_many :project_ratings, dependent: :destroy
  has_many :messages, dependent: :destroy
  has_many :comment_reports, dependent: :destroy
  has_many :reported_comments, through: :comments, source: :comment_reports
  has_many :poll_answers, dependent: :destroy
  has_many :logs, dependent: :destroy
  has_many :gamification_trophies, dependent: :destroy
  has_many :pinboard_ideas, dependent: :destroy
  has_many :pinboard_idea_ratings, dependent: :destroy
  has_many :care_facilities, dependent: :nullify

  # uniqueness rule here
  validates :email, uniqueness: { scope: :organization_id }
  
  enum role: {
    root: 0,
    user: 1,
    admin: 2,
    platform_admin: 3,
    facility_owner: 4,
    care_facility_admin: 5
  }, _suffix: true

  enum status: {
    unconfirmed: 0,
    confirmed: 1,
    disabled: 2
  }

  def name
    "#{firstname} #{lastname}"
  end

  def comments_count
    comments.count
  end

  def project_ratings_count
    project_ratings.count
  end

  def poll_answers_count
    poll_answers.count
  end

  def pinboard_idea_ratings_count
    pinboard_idea_ratings.count
  end

  # how many comments of this user were reported
  def was_reported_count
    reported_comments.count
  end

  def has_reported_count
    comment_reports.count
  end

  def trophies_count
    gamification_trophies.count
  end

  def image_url
    if image.attached?
      Rails.application.routes.url_helpers.url_for(image)
    end
  end

  def care_facility_name
    if care_facilities.count > 0
      cfs = care_facilities.where(kind: 'facility')
      if cfs.count > 0
        return cfs.first.name
      end
    end
    return nil
  end

  def permissions
    App::AccessPrivileges.send(role.to_sym)
  end
end
