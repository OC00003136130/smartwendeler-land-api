class CommentReport < ApplicationRecord
  belongs_to :comment
  belongs_to :user
  belongs_to :organization

  has_one :project, through: :comment
  has_one :comment_user, through: :comment, source: :user

  # one user can only report the same comment one time
  validates :user, uniqueness: { scope: :comment }
end
