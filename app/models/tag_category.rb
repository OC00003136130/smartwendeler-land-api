class TagCategory < ApplicationRecord
  belongs_to :organization
  belongs_to :parent_tag_category, class_name: 'TagCategory', required: false
  has_many :tag_categories, class_name: 'TagCategory', foreign_key: 'parent_id'

  has_many :care_facility_tag_categories, dependent: :destroy
  has_many :care_facilities, through: :care_facility_tag_categories

  has_many :tags

  enum kind: {
    facility: 0,
    # Kurse & Events
    event: 1,
    news: 2,
    course: 3,
    project: 4
  }

  enum filter_type: {
    filter_facility: 0,
    filter_service: 1
  }

  def care_facilities_count
    care_facilities.count
  end

  def care_facilities_active_count
    care_facilities.where(is_active: true).count
  end
end
