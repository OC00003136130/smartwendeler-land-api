class Tag < ApplicationRecord
  belongs_to :organization
  belongs_to :tag_category, required: false

  has_many :care_facility_tags, dependent: :destroy
  has_many :care_facilities, through: :care_facility_tags

  # can get scoped to care_facilities
  enum scope: {
    care_facility: 0,
    event: 1,
    news: 2,
    course: 3
  }
end
