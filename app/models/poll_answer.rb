class PollAnswer < ApplicationRecord
  belongs_to :organization
  belongs_to :poll_question
  belongs_to :user
end
