class GamificationGame < ApplicationRecord
  belongs_to :organization
  has_many :gamification_rewards

  enum kind: {
    login: 0,
    rating: 1,
    poll: 2,
    comment: 3,
    pinboard_idea: 4
  }

  validates :kind, uniqueness: { scope: :organization_id }

  def gamification_rewards_count
    gamification_rewards.count
  end
end
