class Pinboard < ApplicationRecord
  belongs_to :organization
  belongs_to :user

  has_many :pinboard_categories
  has_many :categories, through: :pinboard_categories
  has_many :pinboard_ideas

  def pinboard_ideas_count
    pinboard_ideas.count
  end

  def pinboard_confirmed_ideas_count
    pinboard_ideas.where(status: 'confirmed').count
  end
end
