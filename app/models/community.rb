class Community < ApplicationRecord
  belongs_to :organization
  has_many :project_communities
  has_many :projects, through: :project_communities

  has_many :care_facilities

  validates :name, uniqueness: { scope: :organization_id }

  def name_with_projects_count
    "#{name} (#{projects.where(is_active: true).count})"
  end

  def projects_count
    projects.count
  end

  def care_facilities_count
    care_facilities.count
  end

  def care_facilities_active_count
    care_facilities.where(is_active: true).count
  end
end
