class PinboardIdeaRating < ApplicationRecord
  belongs_to :user
  belongs_to :pinboard_idea
end
