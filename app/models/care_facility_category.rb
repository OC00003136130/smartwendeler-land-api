class CareFacilityCategory < ApplicationRecord
  belongs_to :care_facility
  belongs_to :category
end
