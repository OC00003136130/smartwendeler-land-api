class Message < ApplicationRecord
  belongs_to :organization
  belongs_to :project, required: false
  belongs_to :user, required: false
end
