class GamificationTrophy < ApplicationRecord
  belongs_to :gamification_reward
  belongs_to :user

  # only one trophy per reward for the user
  validates :gamification_reward, uniqueness: { scope: :user }
end
