class IntroduceApprovedAtForIdeas < ActiveRecord::Migration[7.0]
  def change
    add_column :pinboard_ideas, :approved_at, :datetime
  end
end
