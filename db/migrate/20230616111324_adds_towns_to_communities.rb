class AddsTownsToCommunities < ActiveRecord::Migration[7.0]
  def change
    add_column :communities, :towns, :jsonb, array: true, default: []
    add_column :communities, :zip, :string
    add_reference :care_facilities, :community
    remove_column :care_facilities, :community
  end
end
