class AdaptForFilterLogic < ActiveRecord::Migration[7.0]
  def change
    enable_extension "pgcrypto"
    enable_extension "plpgsql"
    enable_extension "uuid-ossp"

    create_table :care_facility_tag_categories, id: :uuid do |t|
      t.belongs_to :tag_category, null: false, foreign_key: true, type: :uuid
      t.belongs_to :care_facility, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end

    remove_column :care_facilities, :tags
    drop_table :care_facility_tags
  end
end
