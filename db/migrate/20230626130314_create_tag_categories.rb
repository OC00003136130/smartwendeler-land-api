class CreateTagCategories < ActiveRecord::Migration[7.0]
  def change
    create_table :tag_categories, id: :uuid do |t|
      t.string :name
      t.string :identifier
      t.uuid :parent_id
      t.integer :menu_order, default: 0

      t.timestamps
    end

    add_reference :tag_categories, :organization, type: :uuid
    add_reference :tags, :tag_category, type: :uuid
  end
end
