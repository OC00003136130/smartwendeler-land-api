class AddsStartAndEndTimeToPinboards < ActiveRecord::Migration[7.0]
  def change
    add_column :pinboards, :start_time, :datetime
    add_column :pinboards, :end_time, :datetime
  end
end
