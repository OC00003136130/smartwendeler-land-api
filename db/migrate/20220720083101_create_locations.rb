class CreateLocations < ActiveRecord::Migration[7.0]
  def change
    create_table :locations, id: :uuid do |t|
      t.decimal :longitude
      t.decimal :latitude
      t.references :organization, type: :uuid
      t.references :project, type: :uuid

      t.timestamps
    end
  end
end
