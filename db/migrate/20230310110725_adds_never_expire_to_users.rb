class AddsNeverExpireToUsers < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :never_expire, :boolean, default: false
  end
end
