class CreateProjectRatings < ActiveRecord::Migration[7.0]
  def change
    create_table :project_ratings do |t|
      t.integer :score
      t.uuid :user_id
      t.uuid :project_id
      t.timestamps
    end

    add_column :projects, :score, :integer, default: 0, null: false
    add_index :project_ratings, :user_id
    add_index :project_ratings, :project_id
  end
end
