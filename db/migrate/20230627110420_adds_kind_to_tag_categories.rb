class AddsKindToTagCategories < ActiveRecord::Migration[7.0]
  def change
    add_column :tag_categories, :kind, :integer, default: 0
  end
end
