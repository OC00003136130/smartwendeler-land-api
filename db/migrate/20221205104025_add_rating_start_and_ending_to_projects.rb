class AddRatingStartAndEndingToProjects < ActiveRecord::Migration[7.0]
  def change
    add_column :projects, :rating_start, :datetime
    add_column :projects, :rating_end, :datetime
  end
end
