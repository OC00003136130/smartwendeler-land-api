class CreateTags < ActiveRecord::Migration[7.0]
  def change
    create_table :tags, id: :uuid do |t|
      t.belongs_to :organization, null: false, foreign_key: true, type: :uuid
      t.string :name
      t.integer :scope

      t.timestamps
    end

    create_table :care_facility_tags, id: :uuid do |t|
      t.belongs_to :tag, null: false, foreign_key: true, type: :uuid
      t.belongs_to :care_facility, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
