class CreatePinboardIdeas < ActiveRecord::Migration[7.0]
  def change
    create_table :pinboard_ideas, id: :uuid do |t|
      t.string :headline
      t.text :content
      t.integer :status

      t.timestamps
    end

    add_reference :pinboard_ideas, :user, type: :uuid
  end
end
