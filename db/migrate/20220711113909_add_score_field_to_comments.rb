class AddScoreFieldToComments < ActiveRecord::Migration[7.0]
  def change
    add_column :comments, :score, :integer, :null => false, :default => 0
    add_index :comments, :score
  end
end
