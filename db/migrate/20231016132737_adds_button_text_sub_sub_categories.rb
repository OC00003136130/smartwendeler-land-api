class AddsButtonTextSubSubCategories < ActiveRecord::Migration[7.0]
  def change
    enable_extension "pgcrypto"
    add_column :categories, :button_text, :string
  end
end
