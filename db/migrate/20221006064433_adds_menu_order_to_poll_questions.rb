class AddsMenuOrderToPollQuestions < ActiveRecord::Migration[7.0]
  def change
    remove_column :poll_questions, :order
    add_column :poll_questions, :menu_order, :integer, default: 0
  end
end
