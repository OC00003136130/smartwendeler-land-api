class ChangeCostsFieldToBigintForProjects < ActiveRecord::Migration[7.0]
  def change
    change_column :projects, :costs, :bigint
  end
end
