class CreateTooltips < ActiveRecord::Migration[7.0]
  def change
    create_table :tooltips, id: :uuid do |t|
      t.references :organization, type: :uuid
      t.string :name
      t.text :content

      t.timestamps
    end
  end
end
