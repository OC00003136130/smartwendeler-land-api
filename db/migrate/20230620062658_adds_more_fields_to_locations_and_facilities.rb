class AddsMoreFieldsToLocationsAndFacilities < ActiveRecord::Migration[7.0]
  def change
    enable_extension 'uuid-ossp'
    enable_extension 'pgcrypto'
    
    add_column :care_facilities, :additional_address_info, :string
    add_column :locations, :street, :string
    add_column :locations, :zip, :string
    add_column :locations, :town, :string
  end
end
