class CreateCommunities < ActiveRecord::Migration[7.0]
  def change
    create_table :communities, id: :uuid do |t|
      t.references :organization, type: :uuid
      t.string :name
      t.timestamps
    end

    add_reference :projects, :community, type: :uuid
    remove_column :projects, :community
  end
end
