class AddSlugsToProjectsAndCareFacilities < ActiveRecord::Migration[7.0]
  def change
    add_column :care_facilities, :slug, :string
    add_column :projects, :slug, :string
  end
end
