class AddsMenuOrderToCategories < ActiveRecord::Migration[7.0]
  def change
    add_column :categories, :menu_order, :integer, default: 0
  end
end
