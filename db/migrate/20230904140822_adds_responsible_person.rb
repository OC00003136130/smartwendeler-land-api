class AddsResponsiblePerson < ActiveRecord::Migration[7.0]
  def change
    enable_extension "pgcrypto"
    add_column :care_facilities, :name_responsible_person, :string
  end
end
