class ChangeOpeningHours < ActiveRecord::Migration[7.0]
  def change
    enable_extension "pgcrypto"
    
    remove_column :care_facilities, :opening_hours
    add_column :care_facilities, :opening_hours, :jsonb, default: []
  end
end
