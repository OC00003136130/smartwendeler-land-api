class AddsUserToCareFacility < ActiveRecord::Migration[7.0]
  def change
    enable_extension 'uuid-ossp'
    enable_extension 'pgcrypto'
    
    remove_column :care_facilities, :user_id
    add_reference :care_facilities, :user, type: :uuid
    add_column :users, :commercial_register_number, :string
  end
end
