class CreateCareFacilitySubSubCategories < ActiveRecord::Migration[7.0]
  def change
    create_table :care_facility_sub_sub_categories, id: :uuid do |t|
      t.belongs_to :care_facility, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end

    add_reference :care_facility_sub_sub_categories, :sub_sub_category, null: false, type: :uuid

    add_column :categories, :url, :string
  end
end
