class CreatePinboardCategories < ActiveRecord::Migration[7.0]
  def change
    create_table :pinboard_categories, id: :uuid do |t|

      t.timestamps
    end

    add_reference :pinboard_categories, :category, type: :uuid
    add_reference :pinboard_categories, :pinboard, type: :uuid
  end
end
