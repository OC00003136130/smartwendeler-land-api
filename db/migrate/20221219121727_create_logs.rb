class CreateLogs < ActiveRecord::Migration[7.0]
  def change
    create_table :logs, id: :uuid do |t|
      t.jsonb :change
      t.references :organization, type: :uuid
      t.references :user, foreign_key: 'relevant_for_user_id', type: :uuid
      t.string :model_class_name
      t.string :model_id
      t.string :log_code
      t.string :model_record_name
      t.timestamps
    end
  end
end
