class CreatePinboards < ActiveRecord::Migration[7.0]
  def change
    create_table :pinboards, id: :uuid do |t|
      t.string :headline
      t.text :content

      t.timestamps
    end
    add_reference :pinboards, :user, type: :uuid
    add_reference :pinboards, :organization, type: :uuid
  end
end
