class CreatePinboardIdeaRatings < ActiveRecord::Migration[7.0]
  def change
    create_table :pinboard_idea_ratings, id: :uuid do |t|
      t.integer :score
      t.timestamps
    end

    add_reference :pinboard_idea_ratings, :user, type: :uuid
    add_reference :pinboard_idea_ratings, :pinboard_idea, type: :uuid
    add_reference :pinboard_ideas, :pinboard, type: :uuid

    add_column :pinboards, :menu_order, :integer, default: 0
    add_column :pinboard_ideas, :menu_order, :integer, default: 0
    add_column :pinboards, :is_active, :boolean, default: false
    add_column :pinboard_ideas, :score, :integer, default: 0
  end
end
