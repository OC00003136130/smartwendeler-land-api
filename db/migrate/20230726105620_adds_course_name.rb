class AddsCourseName < ActiveRecord::Migration[7.0]
  def change
    enable_extension "pgcrypto"
    enable_extension "plpgsql"
    enable_extension "uuid-ossp"
    add_column :care_facilities, :name_instructor, :string
  end
end
