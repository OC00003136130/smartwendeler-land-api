class AddsFilterTypeToTagCategories < ActiveRecord::Migration[7.0]
  def change
    add_column :tag_categories, :filter_type, :integer, default: 0
  end
end
