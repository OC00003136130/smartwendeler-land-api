class AddsMoreFieldsToUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :phone, :string, if_not_exists: true
    add_column :care_facilities, :billable_through_health_insurance, :boolean, default: false, if_not_exists: true
    add_column :care_facilities, :health_insurance_name, :string, if_not_exists: true
  end
end
