class CreateCommentReports < ActiveRecord::Migration[7.0]
  def change
    create_table :comment_reports, id: :uuid do |t|
      t.references :comment, type: :uuid
      t.references :user, type: :uuid
      t.references :organization, type: :uuid

      t.timestamps
    end
  end
end
