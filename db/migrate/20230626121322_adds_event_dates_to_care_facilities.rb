class AddsEventDatesToCareFacilities < ActiveRecord::Migration[7.0]
  def change
    add_column :care_facilities, :event_dates, :jsonb, array: :true, default: []
  end
end
