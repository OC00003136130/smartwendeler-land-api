class CreateProjectCategories < ActiveRecord::Migration[7.0]
  def change
    create_table :project_categories, id: :uuid do |t|
      t.references :project, type: :uuid
      t.references :category, type: :uuid

      t.timestamps
    end

    remove_column :projects, :category_id
  end
end
