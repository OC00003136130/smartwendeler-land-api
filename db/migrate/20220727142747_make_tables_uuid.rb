class MakeTablesUuid < ActiveRecord::Migration[7.0]
  def up
    add_column :project_ratings, :uuid, :uuid, default: "gen_random_uuid()", null: false
    rename_column :project_ratings, :id, :integer_id
    rename_column :project_ratings, :uuid, :id
    execute "ALTER TABLE project_ratings drop constraint project_ratings_pkey;"
    execute "ALTER TABLE project_ratings ADD PRIMARY KEY (id);"

    # Optionally you remove auto-incremented
    # default value for integer_id column
    execute "ALTER TABLE ONLY project_ratings ALTER COLUMN integer_id DROP DEFAULT;"
    change_column_null :project_ratings, :integer_id, true
    execute "DROP SEQUENCE IF EXISTS project_ratings_id_seq"
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
