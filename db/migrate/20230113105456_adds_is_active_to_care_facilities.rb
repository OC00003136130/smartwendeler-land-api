class AddsIsActiveToCareFacilities < ActiveRecord::Migration[7.0]
  def change
    add_column :care_facilities, :is_active, :boolean, default: false
    add_column :categories, :tags, :text, array: true, default: []
  end
end
