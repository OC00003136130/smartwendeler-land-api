class AddsActiveFlagToProjects < ActiveRecord::Migration[7.0]
  def change
    add_column :projects, :is_active, :boolean, default: false

    # make all existing projects active
    Project.update_all(is_active: true)
  end
end
