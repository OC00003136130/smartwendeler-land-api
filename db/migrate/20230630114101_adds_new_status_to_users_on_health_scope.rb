class AddsNewStatusToUsersOnHealthScope < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :is_active_on_health_scope, :boolean, default: true, if_not_exists: true

    remove_column :care_facilities, :status
  end
end
