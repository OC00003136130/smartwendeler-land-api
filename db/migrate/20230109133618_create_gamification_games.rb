class CreateGamificationGames < ActiveRecord::Migration[7.0]
  def change
    create_table :gamification_games, id: :uuid do |t|
      t.references :organization, type: :uuid
      t.integer :kind
      t.string :name

      t.timestamps
    end

    create_table :gamification_rewards, id: :uuid do |t|
      t.references :gamification_game, type: :uuid
      t.integer :threshold
      t.string :name

      t.timestamps
    end
    create_table :gamification_trophies, id: :uuid do |t|
      t.references :gamification_reward, type: :uuid
      t.references :user, type: :uuid

      t.timestamps
    end
  end
end
