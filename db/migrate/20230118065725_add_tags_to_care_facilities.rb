class AddTagsToCareFacilities < ActiveRecord::Migration[7.0]
  def change
    create_table :care_facility_sub_categories, id: :uuid do |t|
      t.references :care_facility, null: false, foreign_key: true, type: :uuid
      t.uuid :sub_category_id, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
    add_index :care_facility_sub_categories, :sub_category_id
    add_column :care_facilities, :tags, :text, default: [], array: true
  end
end
