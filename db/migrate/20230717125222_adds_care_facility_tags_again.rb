class AddsCareFacilityTagsAgain < ActiveRecord::Migration[7.0]
  def change
    create_table :care_facility_tags, id: :uuid, if_not_exists: :true do |t|
      t.belongs_to :tag, null: false, foreign_key: true, type: :uuid
      t.belongs_to :care_facility, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
