class CreateMessages < ActiveRecord::Migration[7.0]
  def change
    create_table :messages, id: :uuid do |t|
      t.references :organization, type: :uuid
      t.references :user, type: :uuid
      t.string :subject
      t.text :message
      t.string :email
      t.string :firstname
      t.string :lastname
      t.references :project, type: :uuid

      t.timestamps
    end
  end
end
