class AddsAddressValidation < ActiveRecord::Migration[7.0]
  def change
    enable_extension "pgcrypto"
    add_column :care_facilities, :geocode_address, :jsonb, array: true, default: []

    CareFacility.all.each do |cf|
      sleep 1.1
      response = HTTParty.get(
        "https://geocode.maps.co/search?postalcode=#{URI.encode_www_form_component(cf.zip)}&street=#{URI.encode_www_form_component(cf.street)}&country=DE&city=#{URI.encode_www_form_component(cf.town)}"
      )
      result = JSON.parse(response.body)

      if result.present?
        cf.geocode_address = result
        cf.save!
      end
    end
  end
end
