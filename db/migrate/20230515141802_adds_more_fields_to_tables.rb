class AddsMoreFieldsToTables < ActiveRecord::Migration[7.0]
  def change
    add_column :care_facilities, :status, :integer, default: 0
    add_column :care_facilities, :opening_hours, :text
    add_column :care_facilities, :course_start, :datetime
    add_column :care_facilities, :course_end, :datetime
    add_reference :care_facilities, :user
  end
end
