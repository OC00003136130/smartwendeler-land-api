class AddsMenuOrderToMilestones < ActiveRecord::Migration[7.0]
  def change
    enable_extension 'pgcrypto'
    add_column :milestones, :menu_order, :integer, default: 0

    Project.all.each do |project|
      project.milestones.order(timestamp: :asc).each_with_index do |milestone, index|
        milestone.menu_order = index
        milestone.save!
      end
    end
  end
end
