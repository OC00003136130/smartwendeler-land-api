class AddsRegistrationSourceToUsers < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :registration_source, :string
    add_column :users, :registration_platform, :string
  end
end
