class AddMoreFieldsToCareFacilities < ActiveRecord::Migration[7.0]
  def change
    add_column :care_facilities, :kind, :integer, default: 0
  end
end
