class CreateCareFacilityCategories < ActiveRecord::Migration[7.0]
  def change
    create_table :care_facility_categories, id: :uuid do |t|
      t.references :care_facility, null: false, foreign_key: true, type: :uuid
      t.references :category, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
