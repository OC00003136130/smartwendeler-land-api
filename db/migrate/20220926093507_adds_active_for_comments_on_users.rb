class AddsActiveForCommentsOnUsers < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :can_comment, :boolean, default: true
  end
end
