class AddsMenuOrderToTags < ActiveRecord::Migration[7.0]
  def change
    enable_extension 'pgcrypto'
    add_column :tags, :menu_order, :integer, default: 0
    add_column :categories, :url_kind, :integer, default: 0

    Tag.all.each_with_index do |tag, index|
      tag.menu_order = index
      tag.save!
    end
  end
end
