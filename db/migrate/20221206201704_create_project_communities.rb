class CreateProjectCommunities < ActiveRecord::Migration[7.0]
  def change
    create_table :project_communities, id: :uuid do |t|
      t.references :project, type: :uuid
      t.references :community, type: :uuid

      t.timestamps
    end

    remove_column :projects, :community_id
  end
end
