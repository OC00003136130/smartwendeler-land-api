class AddsScoreToPinboardIdeaIfNotExists < ActiveRecord::Migration[7.0]
  def change
    add_column :pinboard_ideas, :score, :integer, default: 0, if_not_exists: true
  end
end
