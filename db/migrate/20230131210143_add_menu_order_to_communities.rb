class AddMenuOrderToCommunities < ActiveRecord::Migration[7.0]
  def change
    add_column :communities, :menu_order, :integer, default: 0
  end
end
