class AddsWeblinkToTooltips < ActiveRecord::Migration[7.0]
  def change
    add_column :tooltips, :url, :string
  end
end
