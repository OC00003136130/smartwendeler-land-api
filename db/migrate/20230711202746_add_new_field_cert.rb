class AddNewFieldCert < ActiveRecord::Migration[7.0]
  def change
    add_column :care_facilities, :billable_through_health_insurance_approved, :boolean, default: false
    add_column :users, :is_active_on_health_scope, :boolean, default: true, if_not_exists: true
    add_column :users, :phone, :string, if_not_exists: true
    add_column :care_facilities, :billable_through_health_insurance, :boolean, default: false, if_not_exists: true
    add_column :care_facilities, :health_insurance_name, :string, if_not_exists: true

    add_column :communities, :towns, :jsonb, array: true, default: [], if_not_exists: true
    add_column :communities, :zip, :string, if_not_exists: true

    add_column :care_facilities, :additional_address_info, :string, if_not_exists: true
    add_column :locations, :street, :string, if_not_exists: true
    add_column :locations, :zip, :string, if_not_exists: true
    add_column :locations, :town, :string, if_not_exists: true

    #add_reference :tags, :tag_category
  end
end
