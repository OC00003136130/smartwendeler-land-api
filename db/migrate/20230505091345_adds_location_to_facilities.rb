class AddsLocationToFacilities < ActiveRecord::Migration[7.0]
  def change
    add_reference :locations, :care_facility, type: :uuid
  end
end
