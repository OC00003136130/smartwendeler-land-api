class AddsMenuOrderToProjects < ActiveRecord::Migration[7.0]
  def change
    add_column :projects, :menu_order, :integer, default: 0
  end
end
