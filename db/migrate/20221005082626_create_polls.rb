class CreatePolls < ActiveRecord::Migration[7.0]
  def change
    create_table :polls, id: :uuid do |t|
      t.references :organization, type: :uuid
      t.string :name
      t.datetime :ends_at
      t.boolean :is_active, default: false

      t.timestamps
    end
  end
end
