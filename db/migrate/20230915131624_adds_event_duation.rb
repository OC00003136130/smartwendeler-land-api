class AddsEventDuation < ActiveRecord::Migration[7.0]
  def change
    enable_extension "pgcrypto"
    add_column :care_facilities, :event_duration, :string
  end
end
