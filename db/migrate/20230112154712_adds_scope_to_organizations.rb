class AddsScopeToOrganizations < ActiveRecord::Migration[7.0]
  def change
    add_column :organizations, :scope, :integer, default: 0
  end
end
