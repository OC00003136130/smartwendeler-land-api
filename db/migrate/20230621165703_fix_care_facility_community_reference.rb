class FixCareFacilityCommunityReference < ActiveRecord::Migration[7.0]
  def change
    remove_column :care_facilities, :community_id
    add_reference :care_facilities, :community, type: :uuid
  end
end
