class CreatePollAnswers < ActiveRecord::Migration[7.0]
  def change
    create_table :poll_answers, id: :uuid do |t|
      t.references :organization, type: :uuid
      t.references :poll_question, type: :uuid
      t.references :user, type: :uuid
      t.integer :rating_value
      t.text :text_value

      t.timestamps
    end
  end
end
