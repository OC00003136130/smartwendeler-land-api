class MakeSLugLonger < ActiveRecord::Migration[7.0]
  def change
    change_column :care_facilities, :slug, :text
    change_column :projects, :slug, :text
  end
end
