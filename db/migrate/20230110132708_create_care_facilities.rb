class CreateCareFacilities < ActiveRecord::Migration[7.0]
  def change
    create_table :care_facilities, id: :uuid do |t|
      t.references :organization, type: :uuid
      t.string :name
      t.string :phone
      t.string :email
      t.string :website
      t.string :town
      t.string :community
      t.string :zip
      t.string :street
      t.text :description
      t.text :excerpt

      t.timestamps
    end
  end
end
