class CreatePollQuestions < ActiveRecord::Migration[7.0]
  def change
    create_table :poll_questions, id: :uuid do |t|
      t.references :poll, type: :uuid
      t.references :organization, type: :uuid
      t.integer :order
      t.string :name
      t.integer :max_score_count
      t.integer :kind, default: 0

      t.timestamps
    end
  end
end
