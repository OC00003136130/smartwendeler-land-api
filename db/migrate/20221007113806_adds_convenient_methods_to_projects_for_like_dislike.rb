class AddsConvenientMethodsToProjectsForLikeDislike < ActiveRecord::Migration[7.0]
  def change
    add_column :projects, :rating_kind, :integer, default: 0
    add_column :projects, :rating_results_public, :boolean, default: false
  end
end
