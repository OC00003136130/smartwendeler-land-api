class AddsNewAttributesForOnboarding < ActiveRecord::Migration[7.0]
  def change
    enable_extension "pgcrypto"
    add_column :users, :imported, :boolean, default: false
    add_column :users, :onboarding_token, :string
  end
end
