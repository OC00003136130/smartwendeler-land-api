class AddsProjectToPolls < ActiveRecord::Migration[7.0]
  def change
    add_reference :polls, :project, type: :uuid
  end
end
