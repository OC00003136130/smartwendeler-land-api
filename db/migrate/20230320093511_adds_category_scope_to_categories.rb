class AddsCategoryScopeToCategories < ActiveRecord::Migration[7.0]
  def change
    add_column :categories, :scope, :integer
    Category.all.each do |cat|
      cat.scope = 'project'
      cat.save!
    end
    change_column :categories, :scope, :integer, null: false
  end
end
