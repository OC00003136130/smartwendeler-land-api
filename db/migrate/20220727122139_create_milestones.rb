class CreateMilestones < ActiveRecord::Migration[7.0]
  def change
    create_table :milestones, id: :uuid do |t|
      t.references :project, type: :uuid
      t.references :organization, type: :uuid
      t.string :name
      t.datetime :timestamp
      t.timestamps
    end
  end
end
