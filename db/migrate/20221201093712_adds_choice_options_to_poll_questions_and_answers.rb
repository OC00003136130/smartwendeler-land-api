class AddsChoiceOptionsToPollQuestionsAndAnswers < ActiveRecord::Migration[7.0]
  def change
    add_column :poll_questions, :choices, :jsonb, default: []
    add_column :poll_answers, :choices_answers, :jsonb, default: []
  end
end
