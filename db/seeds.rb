# project scope
org1 = Organization.create!(
  name: 'Smartwendelerland Projektplattform',
  status: 'active',
  public_username: 'pocketrocket_project',
  public_password: '3536282',
  scope: 'project'
)

user_project = User.create!(
  firstname: 'Admin',
  lastname: 'Project',
  email: 'project@pocket-rocket.io',
  password: 'PassWord9876',
  organization: org1,
  role: :admin,
  status: 'confirmed'
)

# health scope
org2 = Organization.create!(
  name: 'Smartwendelerland Gesundheitsplattform',
  status: 'active',
  public_username: 'pocketrocket_health',
  public_password: '7891011',
  scope: 'health'
)

user_health = User.create!(
  firstname: 'Admin',
  lastname: 'Health',
  email: 'health@pocket-rocket.io',
  password: 'PassWord9876',
  organization: org2,
  role: :admin,
  status: 'confirmed'
)

# adapt with your community names
communitiy_names = [
  'Freisen', 'Marpingen', 'Namborn', 'Nohfelden', 'Nonnweiler', 'Oberthal', 'Tholey', 'St. Wendel'
]

communitiy_names.each do |name|
  if (name == 'Freisen')
    comm = Community.create(name: name, organization: org)
    comm.towns = [
      "Asweiler",
      "Eitzweiler",
      "Freisen",
      "Grügelborn",
      "Haupersweiler",
      "Oberkirchen",
      "Reitscheid",
      "Schwarzerden",
      "Seitzweiler"
    ]
    comm.zip = "66629"
    comm.save!
  end
if (name == 'Marpingen')
    comm = Community.create(name: name, organization: org)
    comm.towns = [
    "Alsweiler",
    "Berschweiler",
    "Marpingen",
    "Urexweiler"
    ]
    comm.zip = "66646"
    comm.save!
  end
if (name == 'Namborn')
    comm = Community.create(name: name, organization: org)
    comm.towns = [
    "Baltersweiler",
    "Eisweiler",
    "Furschweiler",
    "Gehweiler",
    "Heisterberg",
    "Hirstein",
    "Hofeld-Mauschbach",
    "Namborn",
    "Pinsweiler",
    "Roschberg"
    ]
    comm.zip = "66640"
    comm.save!
  end
if (name == 'Nohfelden')
    comm = Community.create(name: name, organization: org)
    comm.towns = [
    "Bosen",
    "Eckelhausen",
    "Eisen",
    "Eiweiler",
    "Gonnesweiler",
    "Mosberg-Richweiler",
    "Neunkirchen-Nahe",
    "Nohfelden",
    "Selbach",
    "Sötern",
    "Türkismühle",
    "Wolfersweiler"
    ]
    comm.zip = "66625"
    comm.save!
  end
if (name == 'Nonnweiler')
    comm = Community.create(name: name, organization: org)
    comm.towns = [
    "Bierfeld",
    "Braunshausen",
    "Kastel",
    "Mariahütte",
    "Mettnich",
    "Mühlfeld",
    "Nonnweiler",
    "Otzenhausen",
    "Primstal",
    "Richweiler",
    "Schwarzenbach",
    "Sitzerath"
    ]
    comm.zip = "66620"
    comm.save!
  end
if (name == 'Oberthal')
    comm = Community.create(name: name, organization: org)
    comm.towns = [
    "Gronig",
    "Güdesweiler",
    "Oberthal",
    "Steinberg-Deckenhardt"
    ]
    comm.zip = "66649"
    comm.save!
  end
if (name == 'Tholey')
    comm = Community.create(name: name, organization: org)
    comm.towns = [
    "Bergweiler",
    "Hasborn-Dautweiler",
    "Leitzweiler",
    "Neipel",
    "Scheuern",
    "Sotzweiler",
    "Theley",
    "Tholey"
    ]
    comm.zip = "66636"
    comm.save!
  end
if (name == 'St. Wendel')
    comm = Community.create(name: name, organization: org)
    comm.towns = [
    "Bliesen",
    "Bubach",
    "Dörrenbach",
    "Hoof",
    "Leitersweiler",
    "Marth",
    "Niederkirchen",
    "Niederlinxweiler",
    "Oberlinxweiler",
    "Osterbrücken",
    "Remmesweiler",
    "Saal",
    "St. Wendel",
    "Urweiler",
    "Werschweiler",
    "Winterbach"
    ]
    comm.zip = "66606"
    comm.save!
  end
end
