# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2024_02_01_142223) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.uuid "record_id", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", precision: nil, null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "care_facilities", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "organization_id"
    t.string "name"
    t.string "phone"
    t.string "email"
    t.string "website"
    t.string "town"
    t.string "zip"
    t.string "street"
    t.text "description"
    t.text "excerpt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_active", default: false
    t.integer "kind", default: 0
    t.datetime "course_start"
    t.datetime "course_end"
    t.uuid "user_id"
    t.string "additional_address_info"
    t.uuid "community_id"
    t.jsonb "event_dates", default: [], array: true
    t.boolean "billable_through_health_insurance", default: false
    t.string "health_insurance_name"
    t.boolean "billable_through_health_insurance_approved", default: false
    t.string "name_instructor"
    t.boolean "course_outside_facility", default: false
    t.jsonb "opening_hours", default: []
    t.string "name_responsible_person"
    t.jsonb "geocode_address", default: [], array: true
    t.string "event_duration"
    t.text "slug"
    t.index ["community_id"], name: "index_care_facilities_on_community_id"
    t.index ["organization_id"], name: "index_care_facilities_on_organization_id"
    t.index ["user_id"], name: "index_care_facilities_on_user_id"
  end

  create_table "care_facility_categories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "care_facility_id", null: false
    t.uuid "category_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["care_facility_id"], name: "index_care_facility_categories_on_care_facility_id"
    t.index ["category_id"], name: "index_care_facility_categories_on_category_id"
  end

  create_table "care_facility_sub_categories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "care_facility_id", null: false
    t.uuid "sub_category_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["care_facility_id"], name: "index_care_facility_sub_categories_on_care_facility_id"
    t.index ["sub_category_id"], name: "index_care_facility_sub_categories_on_sub_category_id"
  end

  create_table "care_facility_sub_sub_categories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "care_facility_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "sub_sub_category_id", null: false
    t.index ["care_facility_id"], name: "index_care_facility_sub_sub_categories_on_care_facility_id"
    t.index ["sub_sub_category_id"], name: "index_care_facility_sub_sub_categories_on_sub_sub_category_id"
  end

  create_table "care_facility_tag_categories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "tag_category_id", null: false
    t.uuid "care_facility_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["care_facility_id"], name: "index_care_facility_tag_categories_on_care_facility_id"
    t.index ["tag_category_id"], name: "index_care_facility_tag_categories_on_tag_category_id"
  end

  create_table "care_facility_tags", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "tag_id", null: false
    t.uuid "care_facility_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["care_facility_id"], name: "index_care_facility_tags_on_care_facility_id"
    t.index ["tag_id"], name: "index_care_facility_tags_on_tag_id"
  end

  create_table "categories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.uuid "organization_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "menu_order", default: 0
    t.uuid "parent_id"
    t.text "tags", default: [], array: true
    t.integer "scope", null: false
    t.string "url"
    t.text "description"
    t.integer "url_kind", default: 0
    t.string "button_text"
    t.index ["organization_id"], name: "index_categories_on_organization_id"
  end

  create_table "comment_ratings", force: :cascade do |t|
    t.integer "score", default: 1, null: false
    t.uuid "comment_id"
    t.uuid "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["comment_id"], name: "index_comment_ratings_on_comment_id"
    t.index ["user_id"], name: "index_comment_ratings_on_user_id"
  end

  create_table "comment_reports", force: :cascade do |t|
    t.uuid "comment_id"
    t.uuid "user_id"
    t.uuid "organization_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["comment_id"], name: "index_comment_reports_on_comment_id"
    t.index ["organization_id"], name: "index_comment_reports_on_organization_id"
    t.index ["user_id"], name: "index_comment_reports_on_user_id"
  end

  create_table "comments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "comment"
    t.uuid "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "user_id", null: false
    t.uuid "organization_id", null: false
    t.integer "score", default: 0, null: false
    t.uuid "parent_id"
    t.index ["organization_id"], name: "index_comments_on_organization_id"
    t.index ["project_id"], name: "index_comments_on_project_id"
    t.index ["score"], name: "index_comments_on_score"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "communities", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "organization_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "menu_order", default: 0
    t.jsonb "towns", default: [], array: true
    t.string "zip"
    t.index ["organization_id"], name: "index_communities_on_organization_id"
  end

  create_table "gamification_games", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "organization_id"
    t.integer "kind"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["organization_id"], name: "index_gamification_games_on_organization_id"
  end

  create_table "gamification_rewards", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "gamification_game_id"
    t.integer "threshold"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gamification_game_id"], name: "index_gamification_rewards_on_gamification_game_id"
  end

  create_table "gamification_trophies", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "gamification_reward_id"
    t.uuid "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gamification_reward_id"], name: "index_gamification_trophies_on_gamification_reward_id"
    t.index ["user_id"], name: "index_gamification_trophies_on_user_id"
  end

  create_table "locations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.decimal "longitude"
    t.decimal "latitude"
    t.uuid "organization_id"
    t.uuid "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "care_facility_id"
    t.string "street"
    t.string "zip"
    t.string "town"
    t.index ["care_facility_id"], name: "index_locations_on_care_facility_id"
    t.index ["organization_id"], name: "index_locations_on_organization_id"
    t.index ["project_id"], name: "index_locations_on_project_id"
  end

  create_table "logs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.jsonb "change"
    t.uuid "organization_id"
    t.uuid "user_id"
    t.string "model_class_name"
    t.string "model_id"
    t.string "log_code"
    t.string "model_record_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["organization_id"], name: "index_logs_on_organization_id"
    t.index ["user_id"], name: "index_logs_on_user_id"
  end

  create_table "messages", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "organization_id"
    t.uuid "user_id"
    t.string "subject"
    t.text "message"
    t.string "email"
    t.string "firstname"
    t.string "lastname"
    t.uuid "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "replied", default: false
    t.index ["organization_id"], name: "index_messages_on_organization_id"
    t.index ["project_id"], name: "index_messages_on_project_id"
    t.index ["user_id"], name: "index_messages_on_user_id"
  end

  create_table "milestones", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "project_id"
    t.uuid "organization_id"
    t.string "name"
    t.datetime "timestamp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "menu_order", default: 0
    t.index ["organization_id"], name: "index_milestones_on_organization_id"
    t.index ["project_id"], name: "index_milestones_on_project_id"
  end

  create_table "organizations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.integer "status"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "public_username"
    t.string "public_password_digest"
    t.string "register_token"
    t.integer "scope", default: 0
  end

  create_table "pinboard_categories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "category_id"
    t.uuid "pinboard_id"
    t.index ["category_id"], name: "index_pinboard_categories_on_category_id"
    t.index ["pinboard_id"], name: "index_pinboard_categories_on_pinboard_id"
  end

  create_table "pinboard_idea_ratings", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "user_id"
    t.uuid "pinboard_idea_id"
    t.index ["pinboard_idea_id"], name: "index_pinboard_idea_ratings_on_pinboard_idea_id"
    t.index ["user_id"], name: "index_pinboard_idea_ratings_on_user_id"
  end

  create_table "pinboard_ideas", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "headline"
    t.text "content"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "user_id"
    t.uuid "pinboard_id"
    t.integer "menu_order", default: 0
    t.integer "score", default: 0
    t.datetime "approved_at"
    t.index ["pinboard_id"], name: "index_pinboard_ideas_on_pinboard_id"
    t.index ["user_id"], name: "index_pinboard_ideas_on_user_id"
  end

  create_table "pinboards", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "headline"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "user_id"
    t.uuid "organization_id"
    t.integer "menu_order", default: 0
    t.boolean "is_active", default: false
    t.datetime "start_time"
    t.datetime "end_time"
    t.index ["organization_id"], name: "index_pinboards_on_organization_id"
    t.index ["user_id"], name: "index_pinboards_on_user_id"
  end

  create_table "poll_answers", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "organization_id"
    t.uuid "poll_question_id"
    t.uuid "user_id"
    t.integer "rating_value"
    t.text "text_value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "choices_answers", default: []
    t.index ["organization_id"], name: "index_poll_answers_on_organization_id"
    t.index ["poll_question_id"], name: "index_poll_answers_on_poll_question_id"
    t.index ["user_id"], name: "index_poll_answers_on_user_id"
  end

  create_table "poll_questions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "poll_id"
    t.uuid "organization_id"
    t.string "name"
    t.integer "max_score_count"
    t.integer "kind", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "menu_order"
    t.jsonb "choices", default: []
    t.index ["organization_id"], name: "index_poll_questions_on_organization_id"
    t.index ["poll_id"], name: "index_poll_questions_on_poll_id"
  end

  create_table "polls", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "organization_id"
    t.string "name"
    t.datetime "ends_at"
    t.boolean "is_active", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "project_id"
    t.index ["organization_id"], name: "index_polls_on_organization_id"
    t.index ["project_id"], name: "index_polls_on_project_id"
  end

  create_table "project_categories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "project_id"
    t.uuid "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_project_categories_on_category_id"
    t.index ["project_id"], name: "index_project_categories_on_project_id"
  end

  create_table "project_communities", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "project_id"
    t.uuid "community_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["community_id"], name: "index_project_communities_on_community_id"
    t.index ["project_id"], name: "index_project_communities_on_project_id"
  end

  create_table "project_ratings", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.bigint "integer_id"
    t.integer "score"
    t.uuid "user_id"
    t.uuid "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_project_ratings_on_project_id"
    t.index ["user_id"], name: "index_project_ratings_on_user_id"
  end

  create_table "projects", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.datetime "start_time"
    t.datetime "end_time"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "costs"
    t.string "internal_identifier"
    t.uuid "organization_id", null: false
    t.string "leader"
    t.string "town"
    t.string "zip"
    t.text "excerpt"
    t.integer "score", default: 0, null: false
    t.boolean "is_active", default: false
    t.integer "rating_kind", default: 0
    t.boolean "rating_results_public", default: false
    t.datetime "rating_start"
    t.datetime "rating_end"
    t.integer "menu_order", default: 0
    t.text "slug"
    t.index ["organization_id"], name: "index_projects_on_organization_id"
  end

  create_table "tag_categories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name"
    t.string "identifier"
    t.uuid "parent_id"
    t.integer "menu_order", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "organization_id"
    t.integer "kind", default: 0
    t.integer "filter_type", default: 0
    t.index ["organization_id"], name: "index_tag_categories_on_organization_id"
  end

  create_table "tags", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "organization_id", null: false
    t.string "name"
    t.integer "scope"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "menu_order", default: 0
    t.uuid "tag_category_id"
    t.index ["organization_id"], name: "index_tags_on_organization_id"
    t.index ["tag_category_id"], name: "index_tags_on_tag_category_id"
  end

  create_table "tooltips", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "organization_id"
    t.string "name"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "url"
    t.index ["organization_id"], name: "index_tooltips_on_organization_id"
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "firstname"
    t.string "lastname"
    t.string "email"
    t.integer "role"
    t.integer "status"
    t.text "jwt_token"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "last_seen", precision: nil
    t.uuid "organization_id", null: false
    t.boolean "can_comment", default: true
    t.string "notification_preferences", default: [], array: true
    t.integer "login_count", default: 0
    t.string "registration_source"
    t.string "registration_platform"
    t.boolean "never_expire", default: false
    t.text "password_token"
    t.string "commercial_register_number"
    t.boolean "is_active_on_health_scope", default: true
    t.string "phone"
    t.datetime "password_changed_at"
    t.boolean "imported", default: false
    t.string "onboarding_token"
    t.index ["organization_id"], name: "index_users_on_organization_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "care_facility_categories", "care_facilities"
  add_foreign_key "care_facility_categories", "categories"
  add_foreign_key "care_facility_sub_categories", "care_facilities"
  add_foreign_key "care_facility_sub_sub_categories", "care_facilities"
  add_foreign_key "care_facility_tag_categories", "care_facilities"
  add_foreign_key "care_facility_tag_categories", "tag_categories"
  add_foreign_key "care_facility_tags", "care_facilities"
  add_foreign_key "care_facility_tags", "tags"
  add_foreign_key "comments", "organizations"
  add_foreign_key "comments", "users"
  add_foreign_key "logs", "users"
  add_foreign_key "projects", "organizations"
  add_foreign_key "tags", "organizations"
  add_foreign_key "users", "organizations"
end
