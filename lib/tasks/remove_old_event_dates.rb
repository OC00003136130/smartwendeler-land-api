namespace :care_facilities do
  desc 'Remove event dates from the past'
  task :remove_old_event_dates => :environment do
    care_facilities = CareFacility.where.not(event_dates: [])

    desired_time_zone = 'CEST'
    current_time = DateTime.now.new_offset(desired_time_zone)

    # event_dates = ["19.10.2023 23:35", "27.10.2023 11:15", "31.10.2023 22:22"]
    # iterate over all care facilities and remove event dates which are in the past. considering event dates are created in Berlin timezone
    care_facilities.each do |care_facility|
      event_dates = care_facility.event_dates
      filtered_dates = event_dates.select do |event_date|
        date = DateTime.parse(event_date).new_offset(desired_time_zone)
        date > current_time
      end
      care_facility.update(event_dates: filtered_dates)
    end
  end
end
