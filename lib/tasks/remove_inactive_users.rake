namespace :users do
  desc 'Triggers operation to remove inactive users'
  task :remove_inactive => :environment do
    User::Operations::RemoveInactive.()
  end
end
