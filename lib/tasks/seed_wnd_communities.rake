namespace :db do
  desc 'Seeds communities for the WND organization'
  task :seed_wnd_communities => :environment do
    organization = Organization.first

    communitiy_names = [
      'Freisen', 'Marpingen', 'Namborn', 'Nohfelden', 'Nonnweiler', 'Oberthal', 'Tholey', 'St. Wendel'
    ]

    communitiy_names.each do |name|
      Community.create(name: name, organization: organization)
    end
  end
end