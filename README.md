# README

## Introduction to Smartwendeler Land API Setup

Welcome to the Smartwendeler Land API, a robust and dynamic Ruby on Rails application designed to cater to two primary scopes: `health` and `project`. The `health` scope encompasses all functionalities related to the "Gesundheitsplattform", while the `project` scope is tailored for the "Projektplattform". This document guides you through the essential steps to set up and configure the application effectively in an open-source environment.

## Prerequisites

Before proceeding with the setup, ensure you have the following prerequisites:

- Ruby (Version `3.2.2`)
- PostgreSQL
- A text editor (e.g., Vim, Nano, or Visual Studio Code)

## Setup Instructions

### Step 1: Install Ruby

- Install Ruby using RVM (Ruby Version Manager). Follow the instructions at [RVM Installation Guide](https://rvm.io/). Ensure you're using Ruby version `3.2.2`, as it's the currently supported version for this API.

### Step 2: Install PostgreSQL

- Install PostgreSQL, a powerful open-source object-relational database system. For macOS users, this can be done using Homebrew. Detailed instructions are available on the [PostgreSQL Homebrew Wiki](https://wiki.postgresql.org/wiki/Homebrew).

### Step 3: Generate Credentials

- Rails credentials are vital for managing sensitive information securely. Begin by generating a new credentials file and a `master.key` file with the following command:
`EDITOR=vim rails credentials:edit`

More about Rails credentials can be found in the [Rails Security Guide](https://edgeguides.rubyonrails.org/security.html).

Your new credentials file should include the following structure:

```
aws:
   access_key_id: <YOUR_ACCESS_KEY_ID>
   secret_access_key: <YOUR_SECRET_ACCESS_KEY>
   ses_username: <YOUR_SES_USERNAME>
   ses_password: <YOUR_SES_PASSWORD>
onesignal:
   app_id: <YOUR_APP_ID>
   api_key: <YOUR_API_KEY>
secret_key_base: <YOUR_SECRET_KEY_BASE>
jwt_expiration_time: 24
```

**AWS Configuration:**
Enter your AWS S3 access key and secret for image storage and upload.
Include SES username and password for email functionality.
Ensure S3 has full read/write access, and SES has sending rights.

**OneSignal Configuration:**
Necessary for iOS and Android push notifications. Include your App ID and API Key.

**Secret Key Base:**
Generate a new secret key with rails secret and insert it into the credentials file.

### Step 4: Install Dependencies
Execute bundle install to install the required Ruby gems, including Ruby on Rails.

### Step 5: Database Setup
Initialize your database with `rails db:setup`. This command also executes the seed file located in db/seeds.rb. You might want to customize the seed file to align with your application's requirements.

### Step 6: Start the Rails Server
Launch the Rails server using `rails server`. By default, the server runs at `http://localhost:3000` and is now ready to accept requests.

## Additional Information
### Viewing Endpoint Routes
`bundle exec rake grape:routes`

### Support
Pocket Rocket GmbH
admin@pocket-rocket.io