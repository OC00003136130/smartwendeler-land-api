# locate a csv file in the main app directory and upload it to the database

require 'csv'
require 'open-uri'

o = Organization.find "8e9e109a-ec4c-41ea-9b38-372892061f64"

# Replace 'path/to/file.csv' with the actual path to your CSV file
path = 'https://pocket-rocket-customers.s3.eu-central-1.amazonaws.com/gesundheitsanbieter_final.csv'
service = SpreadsheetParseService.new(path, 'csv')

data = service.to_array_of_hashes

data.each do |row|
  user = User.new
  user.organization = o
  user.email = row[:Email]
  user.firstname = row[:Vorname]
  user.lastname = row[:Nachname]
  user.onboarding_token = SecureRandom.uuid
  user.is_active_on_health_scope = false
  user.imported = true
  user.role = 'facility_owner'
  user.status = :confirmed
  user.last_seen = Time.now
  user.save!(validate: false)

  c = CareFacility.new(
    name: row[:Name],
    street: "#{row[:Straße]} #{row[:Hn]}#{row[:"Hn-Zusatz"]}",
    zip: row[:PLZ],
    town: row[:Ort],
    user: user,
    organization: o,
    phone: row[:Telefon],
    email: row[:Email],
    website: row[:url],
    community_id: row[:Wirkungsgemeinde],
    slug: row[:Name].parameterize
  )
  c.save!(validate: false)
  c.tag_categories << TagCategory.find(row[:Typ]) if row[:Typ]
  c.tag_categories << TagCategory.find(row[:Spezialisierung]) if row[:Spezialisierung]
end